; 脚本用 Inno Setup 脚本向导生成。
; 查阅文档获取创建 INNO SETUP 脚本文件详细资料!

[Setup]
AppName=MyDweep
AppVerName=MyDweep 1.20.0083
AppPublisher=Acme
DefaultDirName={pf}\MyDweep
DefaultGroupName=My Dweep
AllowNoIcons=yes
LicenseFile=F:\pjz\000\Dweep\MyDweep\eula.txt
InfoBeforeFile=F:\pjz\000\Dweep\MyDweep\readme.txt
OutputBaseFilename=MyDweepSetup
SetupIconFile=F:\d\iconFolderXP\无标题 (23).ico
Compression=lzma
SolidCompression=yes
BackSolid=no
BackColor=$FBE3D1
BackColor2=$E3AB84
WizardImageFile=F:\pjz\000\Dweep\MyDweep\orange.bmp
WizardSmallImageFile=F:\pjz\000\Dweep\MyDweep\small.bmp

[Languages]
Name: "chi"; MessagesFile: "compiler:Default.isl"

[Components]
Name: "main"; Description: "主程序"; Flags: fixed;types:full compact custom
Name: "edit"; description: "关卡包编辑器";types:full compact
name:"rec";description:"部分关卡录像和自定义关卡文件";types:full
name:"orig";description:"Dweep Gold! 白金珍藏版（原版）"
;name:"flash";description:"支持文档";types:full
name:"dat";description:"(调试！)";flags:fixed

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
source:"F:\pjz\VS6\VB98\vbA\VB6_Alpha_DIB_Section_Demonstration\my\AlphaDIB.dll";destdir:"{sys}";flags:regserver uninsneveruninstall
source:"F:\pjz\VS6\VB98\vbA\VB6_CommandBar_Full_Source\Release\vbalCmdBar6.ocx";destdir:"{sys}";flags:regserver uninsneveruninstall
source:"F:\pjz\VS6\VB98\vbA\VB6_SSubTmr_Source_Code\Release\SSubTmr6.dll";destdir:"{sys}";flags:regserver uninsneveruninstall
source:"F:\pjz\VS6\VB98\vbA\vbalIml6.ocx";destdir:"{sys}";flags:regserver uninsneveruninstall
source:"d:\windows\system32\zlib.dll";destdir:"{app}";flags:ignoreversion

Source:"F:\pjz\000\Dweep\MyDweep\Dweep.exe";DestDir:"{app}"; Flags:ignoreversion
Source:"F:\pjz\000\Dweep\MyDweep\Solution.dat";DestDir:"{app}"; Flags:ignoreversion;components:dat
Source:"F:\pjz\000\Dweep\MyDweep\Solution_CHS.dat";DestDir:"{app}"; Flags:ignoreversion;components:dat
Source:"d:\windows\system32\fmod.dll";DestDir:"{app}"; Flags:ignoreversion
Source:"F:\pjz\000\Dweep\MyDweep\Dweep.exe.manifest";DestDir:"{app}"; Flags:ignoreversion
Source:"F:\pjz\000\Dweep\MyDweep\Dweep.mpq";DestDir:"{app}"; Flags:ignoreversion
source:"F:\pjz\000\Dweep\dwpedit\Dwpedit.exe.manifest";destdir:"{app}"; Flags:ignoreversion;Components:edit
source:"F:\pjz\000\Dweep\dwpedit\Dwpedit.exe";destdir:"{app}";Flags:ignoreversion; Components: edit
;source:"F:\pjz\000\Dweep\dwpedit\Dwpedit.dll";destdir:"{app}";Flags:ignoreversion; Components: dat

;Source:"F:\pjz\000\Dweep\MyDweep\sound\*";DestDir:"{app}\Sound"; Flags:ignoreversion
Source:"F:\pjz\000\Dweep\MyDweep\Record\*";DestDir:"{app}\Record"; Flags:ignoreversion;components:rec
Source:"F:\pjz\000\Dweep\MyDweep\Levels\*";DestDir:"{app}\Levels"; Flags:ignoreversion;components:rec
Source:"F:\pjz\000\Dweep\Dweep.dwp";DestDir:"{app}"; Flags:ignoreversion
Source:"F:\pjz\000\Dweep\Bonus.dwp";DestDir:"{app}"; Flags:ignoreversion
Source:"F:\pjz\000\Dweep\My.dwp";DestDir:"{app}"; Flags:ignoreversion;components:rec
;Source:"F:\pjz\000\Dweep\MyDweep\Menu.mid";DestDir:"{app}"; Flags:ignoreversion
;Source:"F:\pjz\000\Dweep\MyDweep\Victory.mid";DestDir:"{app}"; Flags:ignoreversion
;Source:"F:\pjz\000\Dweep\MyDweep\Defeat.mid";DestDir:"{app}"; Flags:ignoreversion
;Source:"F:\pjz\000\Dweep\MyDweep\Stage1.mid";DestDir:"{app}"; Flags:ignoreversion
;Source:"F:\pjz\000\Dweep\MyDweep\Stage2.mid";DestDir:"{app}"; Flags:ignoreversion
;Source:"F:\pjz\000\Dweep\MyDweep\Stage3.mid";DestDir:"{app}"; Flags:ignoreversion
;Source:"F:\pjz\000\Dweep\MyDweep\Stage4.mid";DestDir:"{app}"; Flags:ignoreversion
;Source:"F:\pjz\000\Dweep\MyDweep\Stage5.mid";DestDir:"{app}"; Flags:ignoreversion

; 注意: 不要在任何共享系统文件中使用“Flags: ignoreversion”

source:"F:\pjz\000\Dweep\dwpedit\dwphelp.chm";destdir:"{app}"; Flags:ignoreversion
;source:"F:\pjz\000\Dweep\dweep-cw.html";destdir:"{app}"; Flags:ignoreversion;components:sol
;source:"F:\pjz\000\Dweep\bonus.htm";destdir:"{app}"; Flags:ignoreversion;components:sol

source:"F:\pjz\000\Dweep\Dweep.exe";destdir:"{app}\Dweep Gold"; destname:"Dweep.exe"; Flags:ignoreversion;components:orig
source:"F:\pjz\000\Dweep\Dweep.dwp";destdir:"{app}\Dweep Gold"; Flags:ignoreversion;components:orig
source:"F:\pjz\000\Dweep\Bonus.dwp";destdir:"{app}\Dweep Gold"; Flags:ignoreversion;components:orig
source:"F:\pjz\000\Dweep\Dweep.dat";destdir:"{app}\Dweep Gold"; Flags:ignoreversion;components:orig
source:"F:\pjz\000\Dweep\Defeat.dwm";destdir:"{app}\Dweep Gold"; Flags:ignoreversion;components:orig
source:"F:\pjz\000\Dweep\Menu.dwm";destdir:"{app}\Dweep Gold"; Flags:ignoreversion;components:orig
source:"F:\pjz\000\Dweep\Stage1.dwm";destdir:"{app}\Dweep Gold"; Flags:ignoreversion;components:orig
source:"F:\pjz\000\Dweep\Stage2.dwm";destdir:"{app}\Dweep Gold"; Flags:ignoreversion;components:orig
source:"F:\pjz\000\Dweep\Stage3.dwm";destdir:"{app}\Dweep Gold"; Flags:ignoreversion;components:orig
source:"F:\pjz\000\Dweep\Stage4.dwm";destdir:"{app}\Dweep Gold"; Flags:ignoreversion;components:orig
source:"F:\pjz\000\Dweep\Stage5.dwm";destdir:"{app}\Dweep Gold"; Flags:ignoreversion;components:orig
source:"F:\pjz\000\Dweep\Victory.dwm";destdir:"{app}\Dweep Gold"; Flags:ignoreversion;components:orig


[Icons]
Name: "{group}\MyDweep"; Filename: "{app}\Dweep.exe"
Name: "{group}\MyDweep 调试模式"; Filename: "{app}\Dweep.exe debug"; components:dat
Name: "{group}\{cm:UninstallProgram,MyDweep}"; Filename: "{uninstallexe}"
Name: "{userdesktop}\MyDweep"; Filename: "{app}\Dweep.exe"; Tasks: desktopicon
Name: "{group}\Dweep关卡包编辑器"; Filename: "{app}\Dwpedit.exe" ;components: edit
Name: "{group}\MyDweep帮助和关卡解答"; Filename: "{app}\dwphelp.chm"
;Name: "{group}\部分基本关卡解法"; Filename: "{app}\dweep-cw.html" ;components: sol
;Name: "{group}\部分奖励关卡解法(英文)"; Filename: "{app}\bonus.htm" ;components: sol
Name: "{group}\Dweep白金珍藏版"; Filename: "{app}\Dweep Gold\Dweep.exe" ;components: orig
Name: "{userdesktop}\Dweep Gold"; Filename: "{app}\Dweep Gold\Dweep.exe";components: orig; Tasks: desktopicon

[Registry]
Root:HKLM;subkey:"SOFTWARE\Dexterity Software";flags:uninsdeletekeyifempty
Root:HKLM;subkey:"SOFTWARE\Dexterity Software\Dweep";flags:uninsdeletekeyifempty
Root:HKLM;subkey:"SOFTWARE\Dexterity Software\Dweep";valuename:"Hints";valuetype:dword;valuedata:1;components:orig
Root:HKLM;subkey:"SOFTWARE\Dexterity Software\Dweep";valuename:"Sound";valuetype:dword;valuedata:1;components:orig
Root:HKLM;subkey:"SOFTWARE\Dexterity Software\Dweep";valuename:"音乐";valuetype:dword;valuedata:1;components:orig
Root:HKLM;subkey:"SOFTWARE\Dexterity Software\Dweep";valuename:"Music";valuetype:dword;valuedata:1;components:orig
Root:HKLM;subkey:"SOFTWARE\Dexterity Software\Dweep";valuename:"Path";valuetype:string;valuedata:"{app}\Dweep Gold";components:orig
Root:HKLM;subkey:"SOFTWARE\Dexterity Software\Dweep";valuename:"name";valuetype:string;valuedata:"Muzi";components:orig
Root:HKLM;subkey:"SOFTWARE\Dexterity Software\Dweep";valuename:"code";valuetype:string;valuedata:"IYRQYSAEXNE";components:orig

[Run]
Filename: "{app}\Dweep.exe"; Description: "{cm:LaunchProgram,MyDweep}"; Flags: nowait postinstall skipifsilent

