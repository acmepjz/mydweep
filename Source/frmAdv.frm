VERSION 5.00
Begin VB.Form frmAdv 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Options"
   ClientHeight    =   4185
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3810
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   279
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   254
   StartUpPosition =   2  '屏幕中心
   Begin VB.PictureBox p1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   375
      Index           =   0
      Left            =   120
      ScaleHeight     =   25
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   65
      TabIndex        =   2
      Top             =   480
      Width           =   975
      Begin VB.HScrollBar hs6 
         Height          =   255
         LargeChange     =   32
         Left            =   1080
         Max             =   255
         TabIndex        =   49
         TabStop         =   0   'False
         Top             =   1590
         Width           =   2415
      End
      Begin VB.HScrollBar hs5 
         Height          =   255
         LargeChange     =   32
         Left            =   1080
         Max             =   255
         TabIndex        =   48
         TabStop         =   0   'False
         Top             =   630
         Width           =   2415
      End
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "x"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   5
         Top             =   1320
         Width           =   3000
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "sound"
         Height          =   855
         Index           =   5
         Left            =   120
         TabIndex        =   46
         Top             =   1080
         Width           =   3495
         Begin VB.Label Label1 
            BackColor       =   &H00FFFFFF&
            Caption         =   "x"
            Height          =   255
            Index           =   10
            Left            =   120
            TabIndex        =   47
            Top             =   540
            Width           =   3000
         End
      End
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "x"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   3000
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Music"
         Height          =   855
         Index           =   4
         Left            =   120
         TabIndex        =   44
         Top             =   120
         Width           =   3495
         Begin VB.Label Label1 
            BackColor       =   &H00FFFFFF&
            Caption         =   "x"
            Height          =   255
            Index           =   9
            Left            =   120
            TabIndex        =   45
            Top             =   540
            Width           =   3000
         End
      End
   End
   Begin VB.PictureBox p1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   495
      Index           =   4
      Left            =   120
      ScaleHeight     =   33
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   65
      TabIndex        =   4
      Top             =   1680
      Width           =   975
      Begin VB.CommandButton Command4 
         Caption         =   "start updater"
         Height          =   375
         Left            =   840
         TabIndex        =   54
         Top             =   1440
         Visible         =   0   'False
         Width           =   2055
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "update"
         Height          =   735
         Index           =   6
         Left            =   120
         TabIndex        =   53
         Top             =   1200
         Visible         =   0   'False
         Width           =   3375
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "xx"
         Height          =   615
         Index           =   3
         Left            =   120
         TabIndex        =   38
         Top             =   480
         Width           =   3375
         Begin VB.HScrollBar hs4 
            Height          =   255
            LargeChange     =   25
            Left            =   960
            Max             =   100
            SmallChange     =   5
            TabIndex        =   40
            Top             =   240
            Width           =   1935
         End
         Begin VB.Label Label2 
            BackColor       =   &H00FFFFFF&
            Caption         =   "xxx"
            Height          =   255
            Index           =   3
            Left            =   3000
            TabIndex        =   41
            Top             =   240
            Width           =   270
         End
         Begin VB.Label Label1 
            BackColor       =   &H00FFFFFF&
            Caption         =   "quality"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   39
            Top             =   240
            Width           =   3000
         End
      End
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "obsolete"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   23
         Top             =   120
         Visible         =   0   'False
         Width           =   2415
      End
   End
   Begin VB.PictureBox p1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   735
      Index           =   2
      Left            =   2520
      ScaleHeight     =   49
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   89
      TabIndex        =   3
      Top             =   1080
      Width           =   1335
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "auto optimize"
         Height          =   255
         Index           =   12
         Left            =   120
         TabIndex        =   50
         Top             =   2640
         Width           =   3360
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "pathfinding"
         Height          =   1095
         Index           =   2
         Left            =   120
         TabIndex        =   13
         Top             =   1200
         Width           =   3495
         Begin VB.HScrollBar hs 
            Height          =   225
            Index           =   0
            LargeChange     =   4
            Left            =   960
            Max             =   8
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   240
            Width           =   2175
         End
         Begin VB.HScrollBar hs 
            Height          =   225
            Index           =   1
            LargeChange     =   4
            Left            =   960
            Max             =   8
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   480
            Width           =   2175
         End
         Begin VB.HScrollBar hs 
            Height          =   225
            Index           =   2
            LargeChange     =   4
            Left            =   960
            Max             =   8
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   720
            Width           =   2175
         End
         Begin VB.Label Label1 
            BackColor       =   &H00FFFFFF&
            Caption         =   "xx"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   22
            Top             =   240
            Width           =   3000
         End
         Begin VB.Label Label2 
            BackColor       =   &H00FFFFFF&
            Caption         =   "0"
            Height          =   255
            Index           =   0
            Left            =   3240
            TabIndex        =   21
            Top             =   240
            Width           =   135
         End
         Begin VB.Label Label1 
            BackColor       =   &H00FFFFFF&
            Caption         =   "yy"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   20
            Top             =   480
            Width           =   3000
         End
         Begin VB.Label Label2 
            BackColor       =   &H00FFFFFF&
            Caption         =   "0"
            Height          =   255
            Index           =   1
            Left            =   3240
            TabIndex        =   19
            Top             =   480
            Width           =   135
         End
         Begin VB.Label Label1 
            BackColor       =   &H00FFFFFF&
            Caption         =   "zz"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   18
            Top             =   720
            Width           =   3000
         End
         Begin VB.Label Label2 
            BackColor       =   &H00FFFFFF&
            Caption         =   "0"
            Height          =   255
            Index           =   2
            Left            =   3240
            TabIndex        =   17
            Top             =   720
            Width           =   135
         End
      End
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "auto save"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   11
         Top             =   2400
         Width           =   3360
      End
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "hints"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   12
         Top             =   840
         Width           =   3360
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "game speed"
         Height          =   615
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   120
         Visible         =   0   'False
         Width           =   3495
         Begin VB.HScrollBar hs2 
            Height          =   225
            LargeChange     =   5
            Left            =   360
            Max             =   20
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   240
            Visible         =   0   'False
            Width           =   2655
         End
         Begin VB.Label Label1 
            BackColor       =   &H00FFFFFF&
            Caption         =   "fast"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   10
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Caption         =   "slow"
            Height          =   255
            Index           =   5
            Left            =   2040
            TabIndex        =   9
            Top             =   240
            Width           =   1305
         End
      End
   End
   Begin VB.PictureBox p1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Index           =   3
      Left            =   1800
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   65
      TabIndex        =   34
      Top             =   3480
      Width           =   975
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "show tooltip"
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   37
         Top             =   600
         Width           =   3360
      End
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "show shortcut key"
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   35
         Top             =   360
         Width           =   3360
      End
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "auto use item"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   36
         Top             =   120
         Width           =   3360
      End
   End
   Begin VB.PictureBox p1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   495
      Index           =   5
      Left            =   120
      ScaleHeight     =   33
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   65
      TabIndex        =   42
      Top             =   1080
      Width           =   975
      Begin VB.CommandButton Command3 
         Caption         =   "show solution"
         Height          =   375
         Left            =   120
         TabIndex        =   52
         Top             =   720
         Width           =   3360
      End
      Begin VB.CheckBox c2 
         BackColor       =   &H00FFFFFF&
         Caption         =   "no redraw"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   51
         Top             =   360
         Width           =   3360
      End
      Begin VB.CheckBox c2 
         BackColor       =   &H00FFFFFF&
         Caption         =   "level record debug"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   43
         Top             =   120
         Width           =   3360
      End
   End
   Begin VB.PictureBox p1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   495
      Index           =   1
      Left            =   1200
      ScaleHeight     =   33
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   81
      TabIndex        =   24
      Top             =   1080
      Width           =   1215
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "3xspeed"
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   33
         Top             =   840
         Width           =   3375
      End
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "no wrong pixel"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   32
         Top             =   600
         Width           =   3375
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "shadow transparency"
         Height          =   615
         Index           =   1
         Left            =   120
         TabIndex        =   28
         Top             =   1560
         Width           =   3495
         Begin VB.HScrollBar hs3 
            Height          =   255
            LargeChange     =   32
            Left            =   600
            Max             =   255
            TabIndex        =   29
            TabStop         =   0   'False
            Top             =   240
            Width           =   2055
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Caption         =   "opaque"
            Height          =   255
            Index           =   8
            Left            =   2040
            TabIndex        =   31
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label Label1 
            BackColor       =   &H00FFFFFF&
            Caption         =   "trans"
            Height          =   255
            Index           =   7
            Left            =   120
            TabIndex        =   30
            Top             =   240
            Width           =   1200
         End
      End
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "fade in and fade out"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   26
         Top             =   360
         Width           =   3375
      End
      Begin VB.CheckBox c1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "fullscreen"
         Enabled         =   0   'False
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   25
         Top             =   120
         Visible         =   0   'False
         Width           =   3375
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "restart required"
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   27
         Top             =   1320
         Width           =   3135
      End
   End
   Begin MyDweep.TabHeader th 
      Align           =   1  'Align Top
      Height          =   375
      Left            =   0
      Negotiate       =   -1  'True
      Top             =   0
      Width           =   3810
      _ExtentX        =   6720
      _ExtentY        =   661
      PanelWidth      =   56
      HighlightBold   =   -1  'True
      FontSize        =   8.25
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   2520
      TabIndex        =   1
      Top             =   480
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   1320
      TabIndex        =   0
      Top             =   480
      Width           =   1095
   End
   Begin VB.Line Line1 
      BorderColor     =   &H009C9B91&
      X1              =   88
      X2              =   240
      Y1              =   64
      Y2              =   64
   End
End
Attribute VB_Name = "frmAdv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Declare Function GetCurrentProcessId Lib "kernel32" () As Long
Private Const SYNCHRONIZE = &H100000
Private Const PROCESS_QUERY_INFORMATION = 1024
Private Const STANDARD_RIGHTS_REQUIRED = &HF0000

Private Sub c2_Click(index As Integer)
Dim b As Boolean
b = c2(index).Value = 1
Select Case index
Case 0
 DebugReg = b
Case 1
 dbgNoRedraw = b
End Select
End Sub

Private Sub Command1_Click()
Dim i As Long
sMusic = c1(0).Value = 1
sSound = c1(1).Value = 1
sHint = c1(2).Value = 1
sPath = c1(3).Value = 1
sAuto = c1(4).Value = 1
frmOpt = c1(5).Value = 1
sFull = c1(6).Value = 1
sFade = c1(7).Value = 1
sWrong = c1(8).Value = 1
sFan = c1(9).Value = 1
sNum = c1(10).Value = 1
sTool = c1(11).Value = 1
sOpt = c1(12).Value = 1
For i = 0 To 2
 Weight(i) = hs(i).Value
Next i
sSpeed = hs2.Value
sShadow = hs3.Value
ssQ = hs4.Value
sMusic2 = hs5.Value
sSound2 = hs6.Value
SaveOption
Unload Me
End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Command3_Click()
Dim s As String
s = CStr(App.Path) + "\Solution.dat"
If Dir(s) = "" Then
 MsgBox objText.GetText("File not found."), vbCritical, objText.GetText("Error") '"找不到文件。", "错误"
Else
 sol.LoadFile s
 s = CStr(App.Path) + "\Solution_CHS.dat"
 If Dir(s) = "" Then
  MsgBox objText.GetText("File not found."), vbCritical, objText.GetText("Error")
 Else
  sol.LoadFile2 s
 End If
End If
End Sub

Private Sub Command4_Click()
'On Error GoTo a
'Dim pid As Long, h As Long
'pid = Shell(CStr(App.Path) + "\dwpupdate.exe " + CStr(GetCurrentProcessId), vbNormalFocus)
'If pid <> 0 Then
' h = OpenProcess(SYNCHRONIZE Or PROCESS_QUERY_INFORMATION, 0, pid)
' If h <> 0 Then
'  Me.Enabled = False
'  Do While WaitForSingleObject(h, 100)
'   DoEvents
'  Loop
'  CloseHandle h
' End If
'End If
'a:
'Me.Enabled = True
End Sub

Private Sub Form_Load()
Dim i As Long
'///
Me.Caption = objText.GetText("Options") '选项
Command1.Caption = objText.GetText("&OK")
Command2.Caption = objText.GetText("&Cancel")
Frame1(4).Caption = objText.GetText("Music")
c1(0).Caption = objText.GetText("&Music")
Label1(9).Caption = objText.GetText("Volume") '音量
Frame1(5).Caption = objText.GetText("Sound Effects")
c1(1).Caption = objText.GetText("&Sound Effects")
Label1(10).Caption = objText.GetText("Volume")
c1(2).Caption = objText.GetText("Show hin&ts") '显示提示(&T)
Frame1(2).Caption = objText.GetText("Pathfinding") '寻路算法
Label1(1).Caption = objText.GetText("Freeze plate weight") '冷砖权重
Label1(2).Caption = objText.GetText("Heat plate weight") '热砖权重
Label1(3).Caption = objText.GetText("Laser weight") '激光权重
c1(3).Caption = objText.GetText("Save &record automatically") '自动保存游戏录像(&R)
c1(12).Caption = objText.GetText("&Optimize record automatically") '自动优化游戏录像(&O)
Frame1(3).Caption = objText.GetText("Screenshots") '屏幕截图
Label1(0).Caption = objText.GetText("Image quality") '图像品质
c1(4).Caption = objText.GetText("Use items &automatically") '自动使用道具(&A)
c1(10).Caption = objText.GetText("&Show shortcut key in inventory") '在道具栏显示快捷键(&S)
c1(11).Caption = objText.GetText("Show item &tooltips") '显示道具提示(&T)
c1(6).Caption = objText.GetText("&Fullscreen") '全屏显示(&F)
c1(7).Caption = objText.GetText("Use fa&de in and fade out") '使用淡入淡出效果(&D)
c1(9).Caption = objText.GetText("Fan rotating a&t 3x speed") '3倍风扇旋转速度(&T)
Label1(6).Caption = objText.GetText("Following options require restart the game") '下列选项需要重新启动游戏才能生效
Frame1(1).Caption = objText.GetText("Shadow transparency") '阴影透明度
Label1(7).Caption = objText.GetText("Transparent") '透明
Label1(8).Caption = objText.GetText("Opaque") '不透明
c2(0).Caption = objText.GetText("Show level record debug info") '显示录像除错信息
c2(1).Caption = objText.GetText("Disable redraw") '禁止重绘
Command3.Caption = objText.GetText("Show level solution") '显示关卡解答
Frame1(0).Caption = objText.GetText("Game Speed") '游戏速度
Label1(4).Caption = objText.GetText("Fast")
Label1(5).Caption = objText.GetText("Slow")
'ad-hoc!!!
c1(8).Caption = objText.GetText("Don't draw &wrong pixels") '绘图时不绘制错误的像素(&W)
'///
Command4.Enabled = False
c1(0).Value = IIf(sMusic, 1, 0)
c1(1).Value = IIf(sSound, 1, 0)
c1(2).Value = IIf(sHint, 1, 0)
c1(3).Value = IIf(sPath, 1, 0)
c1(4).Value = IIf(sAuto, 1, 0)
c1(5).Value = 1
c1(5).Enabled = False
c1(6).Value = IIf(sFull, 1, 0)
c1(7).Value = IIf(sFade, 1, 0)
c1(8).Value = IIf(sWrong, 1, 0)
c1(9).Value = IIf(sFan, 1, 0)
c1(10).Value = IIf(sNum, 1, 0)
c1(11).Value = IIf(sTool, 1, 0)
c1(12).Value = IIf(sOpt, 1, 0)
For i = 0 To 2
 hs(i).Value = Weight(i)
Next i
hs2.Value = sSpeed
hs3.Value = sShadow
hs4.Value = ssQ
hs5.Value = sMusic2
hs6.Value = sSound2
'///////////
th.Add objText.GetText("Sound") '"声音"
th.Add objText.GetText("Video") '"显示"
th.Add objText.GetText("Game") '"游戏"
th.Add objText.GetText("Tools") '"工具"
th.Add objText.GetText("Misc") '"杂项"
If isDebug Then
 th.Add objText.GetText("Debug") '"除错"
 c2(0).Value = IIf(DebugReg, 1, 0)
 c2(1).Value = IIf(dbgNoRedraw, 1, 0)
End If
th.SelectedPanel = 1
th_PanelClick 1
Form_Resize
End Sub

Private Sub Form_Resize()
Dim i As Long
For i = 0 To p1.UBound
 p1(i).Move 0, 25, Me.ScaleWidth, Me.ScaleHeight - 65
Next i
With Line1
 .X1 = 0
 .y1 = Me.ScaleHeight - 40
 .x2 = Me.ScaleWidth
 .y2 = .y1
End With
Command1.Move Me.ScaleWidth - 160, Me.ScaleHeight - 32, 72, 24
Command2.Move Me.ScaleWidth - 80, Me.ScaleHeight - 32, 72, 24
End Sub

Private Sub hs_Change(index As Integer)
Label2(index).Caption = CStr(hs(index).Value)
End Sub

Private Sub hs_scroll(index As Integer)
Label2(index).Caption = CStr(hs(index).Value)
End Sub

Private Sub hs4_Change()
Label2(3).Caption = CStr(hs4.Value)
End Sub

Private Sub hs4_Scroll()
hs4_Change
End Sub

Private Sub th_PanelClick(ByVal index As Long)
Dim i As Long
For i = 0 To p1.UBound
 p1(i).Visible = i + 1 = index
Next i
End Sub
