VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsUndoNode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private d As typeGameData
Public NextNode As clsUndoNode

Private Sub Class_Initialize()
Debug.Print "Create " + CStr(Time)
End Sub

Private Sub Class_Terminate()
Debug.Print "Terminate " + CStr(Time)
End Sub

Friend Property Get dat() As typeGameData
dat = d
End Property

Friend Property Let dat(xx As typeGameData)
d = xx
End Property

