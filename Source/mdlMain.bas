Attribute VB_Name = "mdlMain"
Option Explicit

Public Declare Function GetAsyncKeyState Lib "user32" (ByVal vKey As Long) As Integer
Public Declare Function EnableWindow Lib "user32" (ByVal hWnd As Long, ByVal fEnable As Long) As Long
'Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Public Declare Function PostMessage Lib "user32" Alias "PostMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

Private Declare Function GetProp Lib "user32" Alias "GetPropA" (ByVal hWnd As Long, ByVal lpString As String) As Long
Private Declare Function SetProp Lib "user32" Alias "SetPropA" (ByVal hWnd As Long, ByVal lpString As String, ByVal hData As Long) As Long
Private Declare Function RemoveProp Lib "user32" Alias "RemovePropA" (ByVal hWnd As Long, ByVal lpString As String) As Long
Private Declare Function BringWindowToTop Lib "user32" (ByVal hWnd As Long) As Long
Private Declare Function FlashWindow Lib "user32" (ByVal hWnd As Long, ByVal bInvert As Long) As Long
Private Declare Sub InitCommonControls Lib "comctl32" ()

Public bmpBmp As New cAlphaDibSection
Public bmpBack As New cAlphaDibSection
Public bmpCache As New cAlphaDibSection
Public bmpText As New cAlphaDibSection
Public Workhdc As Long
Public lvPath As String
Public lvFile As String
Public lvNow As String
Public lvCount As Integer

Public bmpBackBmp As New cAlphaDibSection
Public bmpButton1 As New cAlphaDibSection
Public bmpButton2 As New cAlphaDibSection
Public bmpButton3 As New cAlphaDibSection
Public bmpDBmp As New cAlphaDibSection
Public bmpLvBk As New cAlphaDibSection

Public Type typeBitmapOffsetItem
 Top As Long
 Width As Long
 Height As Long
 CenterX As Integer
 CenterY As Integer
End Type

Public Type typeBitmapOffset
 count As Integer
 OffsetData(1 To 128) As typeBitmapOffsetItem
End Type

Public bmpWall As New cAlphaDibSection
Public bmpItem As New cAlphaDibSection
Public bmpDweep As New cAlphaDibSection
Public offDweep As typeBitmapOffset
Public bmpIceDweep As New cAlphaDibSection
Public offIceDweep As typeBitmapOffset
Public bmpExit As New cAlphaDibSection
Public offExit As typeBitmapOffset
Public bmpLaser As New cAlphaDibSection
Public offLaser As typeBitmapOffset
Public bmpMirror As New cAlphaDibSection
Public offMirror As typeBitmapOffset
Public bmpMirror2 As New cAlphaDibSection
Public bmpFan As New cAlphaDibSection
Public offFan As typeBitmapOffset
Public bmpMouse2 As New cAlphaDibSection
Public offMouse As typeBitmapOffset
Public bmpFire As New cAlphaDibSection
Public bmpIce As New cAlphaDibSection
Public bmpFocus As New cAlphaDibSection
Public offFocus As typeBitmapOffset
Public bmpExp As New cAlphaDibSection
Public offExp As typeBitmapOffset
Public bmpExp2 As New cAlphaDibSection
Public offExp2 As typeBitmapOffset
Public bmpBomb As New cAlphaDibSection
Public offBomb As typeBitmapOffset

Public bmpLaser2 As New cAlphaDibSection
Public bmpBorder As New cAlphaDibSection

'Public mSound(1 To 19) As New Class1
'Public mMusic(1 To 9) As New Class1
Public hSound(1 To 20) As Long, hMoi(1 To 20) As Long
Public hMusic(1 To 8) As Long
Public mNo As Integer

Public MouseX As Integer
Public MouseY As Integer

Public dItemT As Integer
Public pressT As Boolean, pressR As Boolean
Public pressD As Boolean
Public sItemT As Integer
Public RPress As Boolean

Public m_objFPS As New clsTiming

Public NowScene As Integer

Public lpName As String, lvPass As String

'Public func As New clsFunctions
Public List1 As New clsList

Public ld As New clsLoader
Public cd As New cCommonDialog
Public fileLv As New clsFileList
Public fileRec As New clsFileList
Public fileCLv As New clsFileList
Public tb As New clsTable
Public tb2 As New clsTable
Public tut As New clsTutorials
Public crc As New cCRC32
Public sol As New clsSolution

Public WHTTExx2005 As String
Public sdgdsgs As String, YouArePig As String
Public IsT As Boolean
Public IsHOU123 As Boolean
'the monkey tells us continue or exit
Public IsTest As Boolean, TestWnd As Long, TestFileName As String
Public IsCancel As Boolean

Public Debug1 As Boolean, DebugReg As Boolean
Public isDebug As Boolean, dbgNoRedraw As Boolean

Public ssTime As Long

Public objText As New clsGNUGetText

Public Sub ShowCursor(ByVal n As Long)
''
End Sub

Public Function fDebug() As Boolean
isDebug = True
End Function

Public Sub Main()
Dim i As Long, s As String
'/////debug
Debug.Assert Not fDebug
'/////new!! fmod
FSOUND_Init 44100, 32, 0
'/////end
m_objFPS.MinPeriod = 1000 / 30
'/////
s = Command
If s <> "" Then
 If s = "World of Warcraft" Then
  Debug1 = True
 ElseIf s = "Debug Mode" Then
  isDebug = True
 ElseIf Left(s, 4) = "test" Then
  IsTest = True
  TestWnd = Val(Mid(s, 6))
  'TestFileName = Mid(s, 7 + Len(CStr(TestWnd)))
  EnableWindow TestWnd, 0
  Sleep 100
 End If
End If
'MsgBox App.Path
Loading
LoadOption
InitCommonControls
Randomize Timer
bmpBack.Create 640, 480
bmpText.Create 256, 32
If sFull And False Then 'TODO:Fullscreen
Else
 frmMain.Show
 Workhdc = frmMain.hDC
End If
DoEvents
lvPath = reg2_GetSettings("App.Path", CStr(App.Path))
LoadBitmapLoop
ApplyOption
ShowMenu True
ld.CloseFile
If sFade And Not IsTest Then
 For i = 5 To 255 Step 50
  BitBlt bmpCache.hDC, 0, 0, 640, 480, 0, 0, 0, BLACKNESS
  bmpBack.AlphaPaintPicture bmpCache.hDC, , , , , , , i
  fPaint bmpCache.hDC
  DoEvents
  m_objFPS.WaitForNextFrame
 Next i
End If
'///////
fileLv.Calc lvPath, "*.dwp"
fileRec.Calc CStr(App.Path) + "\record", "*.dwr"
fileCLv.Calc CStr(App.Path) + "\levels", "*.dwl"
'///////
If IsTest Then
 TestMap
Else
 MenuLoop
End If
End Sub

Public Sub ShowMenu(Optional ByVal n As Boolean)
Dim i As Integer
RedrawBack
For i = 1 To 6
 If i = dItemT Then
  If pressT Then
   BitBlt bmpBack.hDC, 200, i * 50 + 50, 240, 33, bmpButton1.hDC, 0, 33, vbSrcCopy
  Else
   BitBlt bmpBack.hDC, 200, i * 50 + 50, 240, 33, bmpButton1.hDC, 0, 0, vbSrcCopy
  End If
  DrawTextB bmpBack.hDC, TheMid(1, 0, i), frmMain.Font, 200, i * 50 + 50, 240, 33, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
 Else
  BitBlt bmpBack.hDC, 200, i * 50 + 50, 240, 33, bmpButton1.hDC, 0, 0, vbSrcCopy
  DrawTextB bmpBack.hDC, TheMid(1, 0, i), frmMain.Font, 200, i * 50 + 50, 240, 33, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
 End If
Next i
RedrawMouse
If Not n Then fPaint bmpBack.hDC
'nowFPS = nowFPS + 1
End Sub

Public Sub fPaint(ByVal hDC As Long)
'//////////////////draw text
If ssTime > 0 Then
 bmpText.AlphaPaintPicture hDC, 0, 0, , , , , ssTime * 5, True
 ssTime = ssTime - 1
End If
'//////////////////
If Not dbgNoRedraw Then
 BitBlt Workhdc, 0, 0, 640, 480, hDC, 0, 0, vbSrcCopy
End If
End Sub

Public Sub MenuLoop()
Do
 sItemT = 0
 NowScene = 0
 Do
  ShowMenu
  Play2
  DoEvents
  m_objFPS.WaitForNextFrame
  If GetAsyncKeyState(vbKeyQ) = &H8000 Then
   sItemT = 6
  ElseIf GetAsyncKeyState(vbKeyReturn) = &H8000 Or _
  GetAsyncKeyState(vbKeyG) = &H8000 Then
   sItemT = 1
  ElseIf GetAsyncKeyState(vbKeyE) = &H8000 Then
   sItemT = 4
  ElseIf GetAsyncKeyState(vbKeyR) = &H8000 Then
   sItemT = 3
  ElseIf GetAsyncKeyState(vbKeyO) = &H8000 Then
   sItemT = 2
  ElseIf GetAsyncKeyState(vbKeyH) = &H8000 Or _
  GetAsyncKeyState(vbKeyF1) = &H8000 Then
   sItemT = 5
  End If
 Loop Until sItemT > 0
 Select Case sItemT
 Case 1
  MenuLvPackLoop
 Case 2
  ShowOptionsLoop
 Case 3
  MenuRecLoop
 Case 4
  EditLoop
 Case 6
  Unload frmMain
 Case Else
  HelpLoop
 End Select
Loop
End Sub

Public Sub MenuLvPackLoop()
On Error GoTo a
Dim i As Integer, vars As String
ReStart:
sItemT = 0
NowScene = 1
'ShowCursor 1
'////////////////////////////////////////Load Levels
ReLoadLv:
If fileLv.count = 0 Then
 If MsgBox(objText.GetText("Can't find level files! Do you want to find manually?"), vbCritical + vbYesNo) = vbYes Then '"没有关卡文件！是否重新设置？"
  vars = ShowOpen(LoadResString(132), lvPath)
  For i = Len(vars) To 1 Step -1
   If Mid(vars, i, 1) = "\" Then Exit For
  Next i
  lvPath = Left(vars, i - 1)
  fileLv.Path = lvPath
  GoTo ReLoadLv
 End If
Else
 'frmMain.lstLvPack.Clear
 '////////////////////////class
 With List1
  Set .Font = New StdFont
  With .Font
   .Name = "Tahoma"
   .Size = 12
  End With
  .ForeColor = vbWhite
  .ItemHeight = 24
  .Create bmpBack.hDC, 100, 100, 440, 250
 End With
 For i = 1 To fileLv.count
  vars = fileLv.FileNameWithPath(i)
  Open vars For Binary As #1
  If Len(vars) > 32 Then vars = "..." + Right(vars, 32)
  vars = vars + " - " + GetString(1, 7, 40)
  List1.Add , vars, , vbYellow
  Close
 Next i
End If
List1.Add , objText.GetText("Custom Levels..."), , vbGreen '"自定义关卡..."
'////////////////////////////////////////End
'frmMain.lstLvPack.Visible = True
Do
 RedrawBack
 For i = 1 To 2
  If i = dItemT Then
   If pressT Then
    BitBlt bmpBack.hDC, i * 300 - 250, 400, 240, 33, bmpButton1.hDC, 0, 33, vbSrcCopy
   Else
    BitBlt bmpBack.hDC, i * 300 - 250, 400, 240, 33, bmpButton1.hDC, 0, 0, vbSrcCopy
   End If
   DrawTextB bmpBack.hDC, TheMid(2, 0, i + 3), frmMain.Font, i * 300 - 250, 400, 240, 33, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
  Else
   BitBlt bmpBack.hDC, i * 300 - 250, 400, 240, 33, bmpButton1.hDC, 0, 0, vbSrcCopy
   DrawTextB bmpBack.hDC, TheMid(2, 0, i + 3), frmMain.Font, i * 300 - 250, 400, 240, 33, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
  End If
 Next i
 DrawTextB bmpBack.hDC, objText.GetText("Choose a Level Pack"), frmMain.Label1.Font, 244, 20, 152, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True '"选择一个关卡包"
 List1.Redraw
 RedrawMouse
 fPaint bmpBack.hDC
 'nowFPS = nowFPS + 1
 'frmMain.lstLvPack.Refresh
 '//////////////////////////////////////////////////End of drawing
 Play2
 DoEvents
 m_objFPS.WaitForNextFrame
 If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
  sItemT = 1
 ElseIf GetAsyncKeyState(vbKeyReturn) = &H8000 Then
  sItemT = 2
 End If
Loop Until sItemT > 0
'frmMain.lstLvPack.Visible = False
'ShowCursor 0
List1.Destroy
Select Case sItemT
Case 2
 If List1.ListIndex = -1 Then
  MsgBox objText.GetText("Please choose a level pack!"), vbCritical '"请选择一个关卡包！"
 ElseIf List1.ListIndex = List1.ListCount - 1 Then
  sItemT = 0
  MenuCLvLoop
 Else
  LevelPackFile = fileLv.FileName(List1.ListIndex + 1)
  lvFile = fileLv.FileNameWithPath(List1.ListIndex + 1)
  Open lvFile For Binary As #1
  LevelPackName = GetString(1, 7, 40)
  Close
  MenuLvLoop
 End If
 GoTo ReStart
End Select
a:
Close
End Sub

Private Sub MenuLvLoop()
'On Error GoTo a
Dim i As Integer, vars As String, varS2 As String
Dim varName As String
Dim strPath As String
Dim lp As Long, var1 As Byte, var2 As Long
NowScene = 2
ReStart:
'ShowCursor 1
sItemT = 0
'////////////////////////////////////////Load Levels
'frmMain.lstLevel.ListItems.Clear
With List1
 Set .Font = frmMain.Font
 .ForeColor = vbWhite
 .ItemHeight = 16
 .SetImageList frmMain.Image1(1).Picture, 24, 24
 .Create bmpBack.hDC, 100, 100, 440, 250
End With
Open lvFile For Binary As #1
Get #1, 47, var1
varName = GetString(1, 7, 40)
lvCount = var1
strPath = "Software\Dexterity Software\Dweep"
If GetKeyValue(strPath, varName, varS2) = False Then
 'MsgBox "打开注册表失败。", vbCritical
 'CreateNewKey var2, strPath
End If
If Len(varS2) < lvCount Then varS2 = varS2 + String(lvCount - Len(varS2), "0")
lpName = varName
lvPass = varS2
For i = 1 To lvCount
 vars = GetString(1, 51& + 488& * (i - 1), 40)
 vars = Replace(objText.GetText("Level %d"), "%d", CStr(i)) + vbTab + vbTab + vars '第 %d 关
 If Mid(varS2, i, 1) = "1" Then
  List1.Add , vars, 2, vbYellow
 Else
  List1.Add , vars, 1, &HC0C0&
 End If
Next i
Close
'////////////////////////////////////////End
'frmMain.lstLevel.Visible = True
Do
 RedrawBack
 For i = 1 To 3
  If i = dItemT And pressT Then
   BitBlt bmpBack.hDC, i * 190 - 130, 400, 140, 25, bmpButton2.hDC, 0, 25, vbSrcCopy
  Else
   BitBlt bmpBack.hDC, i * 190 - 130, 400, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
  End If
  If dItemT = i Then
   DrawTextB bmpBack.hDC, TheMid(4, 11, i), frmMain.Font, i * 190 - 130, 400, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
  Else
   DrawTextB bmpBack.hDC, TheMid(4, 11, i), frmMain.Font, i * 190 - 130, 400, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
  End If
 Next i
 DrawTextB bmpBack.hDC, objText.GetText("Please Choose a Level"), frmMain.Label1.Font, 244, 20, 152, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True '"请选择一个关卡"
 List1.Redraw
 RedrawMouse
 fPaint bmpBack.hDC
 'nowFPS = nowFPS + 1
 '//////////////////////////////////////////////////End of drawing
 Play2
 DoEvents
 m_objFPS.WaitForNextFrame
 If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
  sItemT = 1
 ElseIf GetAsyncKeyState(vbKeyReturn) = &H8000 Then
  sItemT = 3
 ElseIf GetAsyncKeyState(vbKeyDelete) = &H8000 Then
  sItemT = 2
 End If
Loop Until sItemT > 0
'frmMain.lstLevel.Visible = False
'ShowCursor 0
List1.Destroy
Select Case sItemT
Case 2
 'TODO:
 sItemT = 0
 'bug??? always show dialog
 If MsgBox(objText.GetText("Are you sure?"), vbQuestion + vbYesNo) = vbYes Then '"你确定吗？"
  varS2 = String(lvCount, "0")
  If SetKeyValue(strPath, varName, varS2) = False Then
   'MsgBox "打开注册表失败。", vbCritical
  End If
 End If
 GoTo ReStart
Case 1
Case 3
 GameLoop
End Select
a:
Close
End Sub

Public Sub RedrawBack(Optional ByVal hDC As Long)
Dim i As Integer, j As Integer
If hDC = 0 Then hDC = bmpBack.hDC
For i = 0 To 7
 For j = 0 To 6
  BitBlt hDC, i * 80, j * 73, 80, 73, bmpBackBmp.hDC, 0, 0, vbSrcCopy
 Next j
Next i
DrawTextB hDC, WHTTExx2005, frmMain.Font, 0, 452, 320, 28, DT_LEFT, vbYellow, , True
DrawTextB hDC, sdgdsgs, frmMain.Font, 320, 452, 320, 80, DT_RIGHT, vbYellow, , True
End Sub

Public Sub RedrawMouse()
bmpMouse2.AlphaPaintPicture bmpBack.hDC, MouseX, MouseY, 39, 39, , , 255, True
End Sub

Public Sub CloseAllMusic()
'Dim i As Long
'For i = 1 To 9
' If mMusic(i).status = "PLAYING" Then mMusic(i).PauseMusic
'Next i
FMUSIC_StopAllSongs
End Sub

Public Sub CloseAllChannels()
FSOUND_StopSound FSOUND_ALL
End Sub

Public Sub Play(ByVal i As Long)
mNo = i
CloseAllMusic
If sMusic Then
 'mMusic(i).playFrom = 0
 'mMusic(i).PlayMusic
 FMUSIC_PlaySong hMusic(i)
End If
End Sub

Public Sub Play2()
'Dim s As String
'If … And sMusic Then
' … = False
' 's = mMusic(mNo).status
' 'Debug.Print [Nothing]
' 'If s = "STOPPED" Or s = "PAUSED" Then
' If FMUSIC_IsPlaying(hMusic(mNo)) = 0 Then
'  'CloseAllMusic
'  'DoEvents
'  'mMusic(mNo).playFrom = 0
'  'mMusic(mNo).PlayMusic
'  FMUSIC_PlaySong hMusic(mNo)
' End If
'End If
'///////////////???
'If IsTest Then SendMessage TestWnd, &H2005&, 3, ByVal 0
End Sub

Public Sub sPlay(ByVal i As Long, Optional ByVal n As Boolean)
Dim j As Long
If sSound Then
 If n Then
  If hMoi(i) <> 0 Then
   If FSOUND_IsPlaying(hMoi(i)) = 0 Then
    FSOUND_StopSound hMoi(i)
    hMoi(i) = 0
   End If
  End If
  If hMoi(i) = 0 Then
   hMoi(i) = FSOUND_PlaySound(-1, hSound(i))
  End If
 Else
  j = FSOUND_PlaySound(-1, hSound(i))
  If j = 0 Then
   j = FSOUND_GetError
   MsgBox "FMOD Sound System error:" + vbCrLf + vbCrLf + FSOUND_GetErrorString(j), vbCritical, "Error"
  End If
 End If
End If
End Sub

Private Sub Loading()
On Error Resume Next
Dim m As Long, i As Long
Dim d() As Byte
Dim cp As New cCompress
'////////////loading
Open CStr(App.Path) + "\Dweep.mpq" For Binary As #1
Open CStr(App.Path) + "\Dweep.tmp" For Binary As #2
Get #1, 4, i
m = LOF(1) - 7
ReDim d(m - 1)
Get #1, 8, d
cp.DecompressData d, i
Put #2, 1, d
Close
ld.LoadFile CStr(App.Path) + "\Dweep.tmp"
'/////////////xxxxx
WHTTExx2005 = "My Dweep     ver " + CStr(App.Major) + "." + CStr(App.Minor) + "." + Format(App.Revision, "0000")
sdgdsgs = objText.GetText("Free software, licensed under GNU GPLv3+") + vbCrLf + App.LegalCopyright '"自由软件, 以 GNU GPLv3+ 授权"
End Sub

Public Function ShowOpen(ByVal sFilter As String, ByVal InitDir As String, Optional ByVal FileName As String) As String
If cd.VBGetOpenFileName(FileName, , , , , True, sFilter, , InitDir, , , frmMain.hWnd) = True Then
 ShowOpen = FileName
Else
 Err.Raise 32755 '&HDEADBEEF
End If
End Function

Public Function ShowSave(ByVal sFilter As String, ByVal InitDir As String, Optional ByVal FileName As String) As String
Dim s As String
s = Mid(sFilter, InStr(1, sFilter, "|") + 1)
If cd.VBGetSaveFileName(FileName, , , sFilter, , InitDir, , s, frmMain.hWnd) = True Then
 ShowSave = FileName
Else
 Err.Raise 32755 '&HDEADBEEF
End If
End Function

Public Sub LoadTimeDate()
Dim s As String, d() As Byte
Dim kongking As String
ld.LoadToArray d
s = CStr(App.Path) + "\temp3.tmp"
Open s For Binary As #1
Put #1, 1, d
Close 1
Open s For Input As #1
Line Input #1, kongking
Line Input #1, YouArePig
Close 1
Kill s
WHTTExx2005 = WHTTExx2005 + vbCrLf + "Last Compile:" + kongking
'//////////Help.txt
ld.LoadToArray d 'do nothing
'Open s For Binary As #1
'Put #1, 1, d
'Close 1
'tb.LoadFile s, True
'Kill s
tb.TableType = 1
'//////////Tutorial.txt
ld.LoadToArray d
Open s For Binary As #1
Put #1, 1, d
Close 1
tb2.LoadFile s
Kill s
'//////////
tut.LoadTutorial tb2
End Sub

Public Function TheMid(ByVal index As Long, ByVal nReserved As Long, ByVal n As Long) As String
Select Case index * &H100& + n

Case &H101: TheMid = objText.GetText("&Game") '游戏(&G)
Case &H102: TheMid = objText.GetText("&Options") '选项(&O)
Case &H103: TheMid = objText.GetText("&Records") '录像(&R)
Case &H104: TheMid = objText.GetText("&Editor") '编辑(&E)
Case &H105: TheMid = objText.GetText("&Help") '帮助(&H)
Case &H106: TheMid = objText.GetText("&Quit") '退出(&Q)

Case &H201: TheMid = objText.GetText("&Restart Level") '重玩(&R)
Case &H202: TheMid = objText.GetText("&Quit")
Case &H203: TheMid = objText.GetText("&Options")
Case &H204: TheMid = objText.GetText("Cancel") + " (Esc)" '取消
Case &H205: TheMid = objText.GetText("OK") + " (Enter)" '确定

Case &H301: TheMid = objText.GetText("&Open") '打开(&O)
Case &H302: TheMid = objText.GetText("&Save") '保存(&S)
Case &H303: TheMid = objText.GetText("Inven&tory") '道具箱(&T)
Case &H304: TheMid = objText.GetText("&Quit")

Case &H401: TheMid = objText.GetText("Cancel") + " (Esc)"
Case &H402: TheMid = objText.GetText("Clear Progress") + " (Del)" '清除过关纪录
Case &H403: TheMid = objText.GetText("OK") + " (Enter)"

Case &H501: TheMid = objText.GetText("Quit") + " (Esc)" '退出
Case &H502: TheMid = objText.GetText("&Restart Level") '重玩(&R)
Case &H503: TheMid = objText.GetText("&Save Record") '保存录像(&S)
Case &H504: TheMid = objText.GetText("Continue") + " (Enter)" '继续

Case &H601: TheMid = objText.GetText("Quit") + " (Esc)"
Case &H602: TheMid = objText.GetText("&Replay Level Record") '再看一次(&R)

Case &H701: TheMid = objText.GetText("Quit") + " (Esc)"
Case &H702: TheMid = objText.GetText("&Tutorial") '教程(&T)
Case &H703: TheMid = "<<"
Case &H704: TheMid = ">>"

End Select
End Function

Public Function HelpLoop()
Dim hlp As New clsHelp
Dim i As Long, n As Long, s As String
n = 1
NowScene = 2005
hlp.PredrawTo bmpCache.hDC, 1
Do
 '/////
 bmpCache.PaintPicture bmpBack.hDC
 For i = 1 To 4
  BitBlt bmpBack.hDC, 80 + (i - 1) * 125, 430, 105, 19, bmpButton3.hDC, 0, IIf(dItemT = i And pressT, 19, 0), vbSrcCopy
  DrawTextB bmpBack.hDC, TheMid(7, 7, i), frmMain.Font, 80 + (i - 1) * 125, 430, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = i, vbYellow, vbWhite), , True
 Next i
 DrawTextB bmpBack.hDC, objText.GetText("Help of the Game Dweep"), frmMain.Label1.Font, 0, 25, 640, 25, DT_CENTER, vbYellow, , True '"Dweep游戏帮助"
 DrawTextB bmpBack.hDC, CStr(n) + "/" + CStr(hlp.count), frmMain.Font, 330, 430, 230, 20, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
 hlp.DrawTo bmpBack.hDC, n
 RedrawMouse
 fPaint bmpBack.hDC
 '/////
 DoEvents
 m_objFPS.WaitForNextFrame
 'nowFPS = nowFPS + 1
 Play2
 If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
  sItemT = 1
 ElseIf GetAsyncKeyState(vbKeyT) = &H8000 Then
  sItemT = 2
 ElseIf GetAsyncKeyState(vbKeyLeft) = &H8000 Then
  sItemT = 3
 ElseIf GetAsyncKeyState(vbKeyRight) = &H8000 Then
  sItemT = 4
 End If
 Select Case sItemT
 Case 1
  Exit Do
 Case 2 'tutorial
  sItemT = 0
  BitBlt bmpCache.hDC, 0, 0, 640, 480, bmpBack.hDC, 0, 0, vbSrcCopy
  With List1
   Set .Font = frmMain.Font
   .ForeColor = vbBlue
   .ItemHeight = 16
   .Create bmpBack.hDC, 120, 160, 400, 150
   For i = 1 To tb2.count
    s = tb2.StringTable(i)
    .Add Left(s, 3), CStr(i) + ". " + Mid(s, 5), , vbBlack
   Next i
  End With
  NowScene = 3333
  Do
   BitBlt bmpBack.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, vbSrcCopy
   DrawDialogBox bmpBack.hDC, 100, 120, 440, 240
   DrawTextB bmpBack.hDC, objText.GetText("Tutorial"), frmMain.Label1.Font, 290, 135, 64, 24, DT_CENTER, vbBlue, , True '"教程"
   For i = 1 To 3 Step 2
    If dItemT = i And pressT Then
     BitBlt bmpBack.hDC, 60 + i * 110, 320, 105, 19, bmpButton3.hDC, 0, 19, vbSrcCopy
    Else
     BitBlt bmpBack.hDC, 60 + i * 110, 320, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
    End If
   Next i
   List1.Redraw
   DrawTextB bmpBack.hDC, objText.GetText("Cancel") + " (Esc)", frmMain.Font, 170, 320, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = 1, vbYellow, vbWhite), , True
   DrawTextB bmpBack.hDC, objText.GetText("OK") + " (Enter)", frmMain.Font, 390, 320, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = 3, vbYellow, vbWhite), , True
   RedrawMouse
   fPaint bmpBack.hDC
   '///
   DoEvents
   m_objFPS.WaitForNextFrame
   'nowFPS = nowFPS + 1
   Play2
   '///
   If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
    sItemT = 1
   ElseIf GetAsyncKeyState(vbKeyReturn) = &H8000 Then
    sItemT = 3
   End If
   If IsHOU123 Then sItemT = 3
   Select Case sItemT
   Case 1
    Exit Do
   Case 3
    If List1.ListIndex = -1 Then
     MsgBox objText.GetText("Please select a level!") '"请选择一个关卡"
    Else
     IsT = True
     List1.Enable = False
     Set tLev = tut.TheData(List1.ListIndex + 1)
     tLev.StartGame
     GameLoop
     sItemT = 0
     IsT = False
     NowScene = 3333
    End If
    If IsHOU123 And List1.ListIndex + 1 < List1.ListCount Then
     List1.ListIndex = List1.ListIndex + 1
    Else
     sItemT = 0
     List1.Enable = True
     IsHOU123 = False
     Play 1
    End If
   End Select
  Loop
  sItemT = 0
  NowScene = 2005
  hlp.PredrawTo bmpCache.hDC, n
 Case 3 'left
  If n > 1 Then n = n - 1
  hlp.PredrawTo bmpCache.hDC, n
  sItemT = 0
 Case 4 'right
  If n < hlp.count Then n = n + 1
  hlp.PredrawTo bmpCache.hDC, n
  sItemT = 0
 End Select
Loop
'tLev.Clear
Set tLev = Nothing
End Function

Public Function ConvertString(ByVal s As String) As String
s = Replace(s, vbCrLf, vbLf)
s = Replace(s, "\n", vbLf)
s = Replace(s, "\t", vbTab)
s = Replace(s, "\\", "\")
s = Replace(s, vbLf, vbCrLf)
ConvertString = s
End Function

Private Sub SetStringToProp(ByVal hWnd As Long, ByVal PropName As String, ByVal s As String)
Dim i As Long, m As Long
m = Len(s)
SetProp hWnd, PropName + "_Count", m
For i = 1 To m
 SetProp hWnd, PropName + "__" + CStr(i), Asc(Mid(s, i, 1))
Next i
End Sub

Private Function GetStringFromProp(ByVal hWnd As Long, ByVal PropName As String) As String
Dim s As String
Dim i As Long, m As Long
m = GetPropAndDeleteIt(hWnd, PropName + "_Count")
For i = 1 To m
 s = s + Chr(GetPropAndDeleteIt(hWnd, PropName + "__" + CStr(i)))
Next i
GetStringFromProp = s
End Function

Private Function GetPropAndDeleteIt(ByVal hWnd As Long, ByVal PropName As String) As Long
GetPropAndDeleteIt = GetProp(hWnd, PropName)
RemoveProp hWnd, PropName
End Function

Public Sub TestMap()
frmMain.Show
BringWindowToTop frmMain.hWnd
FlashWindow frmMain.hWnd, 1
'/////////////////////////LoadCustomLevel TestFileName, LData
Dim i As Long, j As Long, k As Long, n As Long
Dim b(3) As Byte
With LData
 .LevelNo = GetPropAndDeleteIt(TestWnd, "__lvNo")
 .LevelName = GetStringFromProp(TestWnd, "__lvName")
 .LevelHint = GetStringFromProp(TestWnd, "__lvHint")
 .DweepStartX = 0
 .DweepStartY = 0
 For j = 1 To 10
  For i = 1 To 16 Step 4 'read data
   n = GetPropAndDeleteIt(TestWnd, "__lvDat__" + CStr(i) + "_" + CStr(j))
   CopyMemory b(0), n, 4&
   For k = 0 To 3
    .LevelData(i + k, j) = b(k)
   Next k
  Next i
  For i = 1 To 16 'dweep start?
   If .LevelData(i, j) = 5 Then
    .DweepStartX = i
    .DweepStartY = j
    .LevelData(i, j) = 0
   End If
  Next i
 Next j
 n = GetPropAndDeleteIt(TestWnd, "__lvTC")
 .ItemCount = n
 For i = 1 To n
  .ItemStart(i) = GetPropAndDeleteIt(TestWnd, "__lvT__" + CStr(i))
 Next i
End With
'//////////////////////////
GameLoop
End Sub

Public Sub SnapShot()
Dim bm As New cDIBSection, jpg As New clsJpeg
Dim s As String, s1 As String
bm.Create 640, 480
TestDir CStr(App.Path) + "\ScreenShots\"
s1 = "Dweep" + Format(Date, "yyyymmdd") + CStr(CLng(Timer * 100)) + ".jpg"
s = CStr(App.Path) + "\ScreenShots\" + s1
bmpBack.PaintPicture bm.hDC
jpg.SaveJPG bm, s, ssQ
'MsgBox "已截图"
bmpText.Cls
DrawTextB bmpText.hDC, Replace(objText.GetText("Screenshot saved to %s"), "%s", s1), frmMain.Font, 0, 0, 256, 32, , vbYellow, , True
ssTime = 51
End Sub

Private Sub TestDir(ByVal s As String)
On Error Resume Next
MkDir s
End Sub

