VERSION 5.00
Object = "{DE8CE233-DD83-481D-844C-C07B96589D3A}#1.1#0"; "VBALSG~1.OCX"
Object = "{396F7AC0-A0DD-11D3-93EC-00C0DFE7442A}#1.0#0"; "vbalIml6.ocx"
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Look at your files!"
   ClientHeight    =   4620
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7065
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   308
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   471
   StartUpPosition =   3  'Windows Default
   Begin vbalIml6.vbalImageList iml 
      Left            =   3960
      Top             =   120
      _ExtentX        =   953
      _ExtentY        =   953
      IconSizeX       =   9
      IconSizeY       =   9
      ColourDepth     =   24
      Size            =   840
      Images          =   "Form -2.frx":0000
      Version         =   131072
      KeyCount        =   2
      Keys            =   "�"
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Split Pack File"
      Height          =   255
      Left            =   5640
      TabIndex        =   11
      Top             =   3720
      Value           =   1  'Checked
      Width           =   1335
   End
   Begin VB.ComboBox Text1 
      Height          =   315
      Left            =   4200
      TabIndex        =   10
      Text            =   "*.*"
      Top             =   3720
      Width           =   975
   End
   Begin VB.FileListBox f1 
      Height          =   285
      Left            =   5760
      TabIndex        =   9
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.TextBox txtEd 
      Height          =   285
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Add Folder"
      Height          =   255
      Left            =   2520
      TabIndex        =   6
      Top             =   3720
      Width           =   975
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Delete File"
      Height          =   255
      Left            =   1440
      TabIndex        =   5
      Top             =   4080
      Width           =   975
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Add File"
      Height          =   255
      Left            =   1440
      TabIndex        =   4
      Top             =   3720
      Width           =   975
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Delete Pack"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   4080
      Width           =   975
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Add Pack"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   3720
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Make File"
      Height          =   255
      Left            =   6120
      TabIndex        =   1
      Top             =   120
      Width           =   855
   End
   Begin vbAcceleratorSGrid6.vbalGrid g1 
      Height          =   3135
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   5530
      GridLines       =   -1  'True
      GridLineMode    =   1
      BackgroundPictureHeight=   0
      BackgroundPictureWidth=   0
      GridLineColor   =   -2147483638
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeaderDragReorderColumns=   0   'False
      Editable        =   -1  'True
      DisableIcons    =   -1  'True
      DrawFocusRectangle=   0   'False
      SelectionAlphaBlend=   -1  'True
      SelectionOutline=   -1  'True
   End
   Begin VB.Label Label1 
      Caption         =   "Filter"
      Height          =   255
      Left            =   3600
      TabIndex        =   7
      Top             =   3720
      Width           =   495
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private shl As New Shell
Private cd As New cCommonDialog
Private mpq As New clsFileEx

Private Function GetFolder() As String
On Error GoTo a
Dim obj As Folder3
Set obj = shl.BrowseForFolder(Me.hWnd, "", 0, "f:\pjz\000\dweep\")
GetFolder = obj.Self.Path
a:
End Function

Private Sub Check1_Click()
mpq.SplitFile = Check1.Value = 1
End Sub

Private Sub Command1_Click()
SaveFile CStr(App.Path) + "\Upload.lst"
MsgBox "OK!"
End Sub

Private Sub Command2_Click()
With g1
 .Rows = .Rows + 1
 .CellText(.Rows, 1) = "Unnamed"
 .CellIcon(.Rows, 1) = 1
 .CellItemData(.Rows, 1) = -1
 .CellText(.Rows, 2) = "File Count:0"
 .CellText(.Rows, 3) = "CRC:?"
 .CellBackColor(.Rows, 1) = vbButtonFace
 .CellBackColor(.Rows, 2) = vbButtonFace
 .CellBackColor(.Rows, 3) = vbButtonFace
End With
End Sub

Private Sub Command3_Click()
Dim i As Long, j As Long, m As Long
With g1
 For i = .SelectedRow To 1 Step -1
  If .CellItemData(i, 1) = -1 Then Exit For
 Next i
 .Redraw = False
 m = .CellItemData(i, 2)
 For j = 1 To m + 1
  .RemoveRow i
 Next j
 .Redraw = True
End With
End Sub

Private Sub Command4_Click()
Dim s As String
If cd.VBGetOpenFileName(s, , , , , True, , , CStr(App.Path), , , Me.hWnd) Then
 AddFile s
End If
End Sub

Private Sub Command5_Click()
Dim i As Long, j As Long
With g1
 If .CellItemData(.SelectedRow, 1) = -1 Then Exit Sub
 For i = .SelectedRow To 1 Step -1
  If .CellItemData(i, 1) = -1 Then Exit For
 Next i
 j = .CellItemData(i, 2)
 .CellItemData(i, 2) = j - 1
 .CellText(i, 2) = "File Count:" + CStr(j - 1)
 .RemoveRow .SelectedRow
End With
End Sub

Private Sub Command6_Click()
Dim s As String
Dim i As Long
s = GetFolder
If s <> "" And Left(s, 1) <> ":" And Left(s, 1) <> "/" And Left(s, 1) <> "\" Then
 If Right(s, 1) <> "\" Then s = s + "\"
 f1.Pattern = Text1.Text
 f1.Path = s
 g1.Redraw = False
 For i = 0 To f1.ListCount - 1
  AddFile s + f1.List(i)
 Next i
 g1.Redraw = True
End If
End Sub

Private Sub Form_Load()
On Error Resume Next
MkDir CStr(App.Path) + "\Upload"
g1.ImageList = iml.hIml
g1.AddColumn , "File Name", , , 64
g1.AddColumn , "Src", , , 256
g1.AddColumn , "Dest", , , 96
Text1.AddItem "*.*"
Text1.AddItem "*.dwp"
Text1.AddItem "*.dwl"
Text1.AddItem "*.dwr"
LoadFile CStr(App.Path) + "\Upload.lst"
End Sub

Private Sub LoadFile(ByVal fn As String)
Dim i As Long, j As Long, k As Long, m As Long
Dim r As Long, lp As Long, s As String
Open fn For Binary As #1
Get #1, 1, m
lp = 5
With g1
 .Redraw = False
 For i = 1 To m
  .AddRow
  r = .Rows
  .CellBackColor(r, 1) = vbButtonFace
  .CellBackColor(r, 2) = vbButtonFace
  .CellBackColor(r, 3) = vbButtonFace
  .CellItemData(r, 1) = -1
  .CellIcon(r, 1) = 1
  lp = lp + LoadString(1, lp, s)
  Get #1, lp, k
  lp = lp + 4
  .CellText(r, 1) = s
  .CellText(r, 2) = "File Count:" + CStr(k)
  .CellItemData(r, 2) = k
  For j = 1 To k
   .AddRow
   lp = lp + LoadString(1, lp, s)
   .CellText(.Rows, 2) = s
   lp = lp + LoadString(1, lp, s)
   .CellText(.Rows, 3) = s
   .CellItemData(.Rows, 2) = -1
   .CellItemData(.Rows, 3) = -1
  Next j
  Get #1, lp, k
  lp = lp + 4
  s = Hex(k)
  s = String(8 - Len(s), "0") + s
  .CellText(r, 3) = "CRC:" + s
 Next i
 .Redraw = True
End With
Close
End Sub

Private Sub AddFile(ByVal fn As String)
Dim i As Long, j As Long
Dim s As String
With g1
 For i = .SelectedRow To 1 Step -1
  If .CellItemData(i, 1) = -1 Then Exit For
 Next i
 '////
 .AddRow .SelectedRow + 1
 j = .SelectedRow + 1
 .CellText(j, 2) = fn
 .CellItemData(j, 2) = -1
 '//
 s = Right(fn, InStr(1, StrReverse(fn), "\") - 1)
 Select Case LCase(Right(fn, 4))
 Case ".dwl"
  s = "Levels\" + s
 Case ".dwr"
  s = "Record\" + s
 End Select
 '//
 .CellText(j, 3) = s
 .CellItemData(j, 3) = -1
 '////
 j = .CellItemData(i, 2)
 .CellItemData(i, 2) = j + 1
 .CellText(i, 2) = "File Count:" + CStr(j + 1)
End With
End Sub

Private Sub SaveFile(ByVal fn As String)
Dim i As Long, j As Long, k As Long, m As Long
Dim lp As Long, lp2 As Long
Dim s As String, s1 As String
s1 = CStr(App.Path) + "\Upload\update.dat"
Open fn For Output As #1
Close
Open s1 For Output As #1
Close
Open fn For Binary As #3
Open s1 For Binary As #4
lp = 5
lp2 = 5
With g1
 i = 1
 Do Until i > .Rows
  'init
  m = .CellItemData(i, 2)
  mpq.Clear
  s = .CellText(i, 1)
  mpq.OutputFile = s
  lp = lp + SaveString(3, lp, s)
  Put #3, lp, m
  lp = lp + 4
  lp2 = lp2 + SaveString(4, lp2, s)
  Put #4, lp2, m
  lp2 = lp2 + 4
  'add files
  For j = i + 1 To i + m
   mpq.Add .CellText(j, 2)
   lp = lp + SaveString(3, lp, .CellText(j, 2))
   lp = lp + SaveString(3, lp, .CellText(j, 3))
   lp2 = lp2 + SaveString(4, lp2, .CellText(j, 3))
   Put #4, lp2, FileLen(.CellText(j, 2))
   lp2 = lp2 + 4
  Next j
  'compress
  mpq.MakeMPQEx
  Put #4, lp2, mpq.InputFileSize
  .CellText(i, 2) = "File Count:" + CStr(m) + ", Packs:" + CStr(mpq.OutputFileCount)
  Put #4, lp2 + 4, mpq.OutputFileCount
  Put #4, lp2 + 8, ValueCompressedSize
  lp2 = lp2 + 12
  'crc
  s = Hex(mpq.CRCResult)
  s = "CRC:" + String(8 - Len(s), "0") + s
  .CellBackColor(i, 3) = IIf(s = .CellText(i, 3), vbButtonFace, &HC0C0FF)
  .CellText(i, 3) = s
  Put #3, lp, mpq.CRCResult
  lp = lp + 4
  Put #4, lp2, mpq.CRCResult
  lp2 = lp2 + 4
  '////
  i = i + m + 1
  k = k + 1
 Loop
End With
Put #3, 1, k
Put #4, 1, k
Close
End Sub

Private Sub g1_CancelEdit()
txtEd.Visible = False
End Sub

Private Sub g1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single, bDoDefault As Boolean)
Dim a As Long, b As Long
Dim i As Long, m As Long
g1.CellFromPoint x \ 15, y \ 15, a, b
If a > 0 And b = 1 Then
 If g1.CellItemData(a, b) = -1 Then
  g1.CellBoundary a, b, 0, i, 0, 0
  If x < 105 And y - i < 180 Then  '??
   g1.Redraw = False
   g1.CellIcon(a, 1) = 1 - g1.CellIcon(a, 1)
   m = g1.CellItemData(a, 2)
   For i = a + 1 To a + m
    g1.RowVisible(i) = g1.CellIcon(a, 1) = 1
   Next i
   g1.Redraw = True
  End If
 End If
End If
End Sub

Private Sub g1_PreCancelEdit(ByVal lRow As Long, ByVal lCol As Long, newValue As Variant, bStayInEditMode As Boolean)
g1.CellText(lRow, lCol) = txtEd.Text
End Sub

Private Sub g1_RequestEdit(ByVal lRow As Long, ByVal lCol As Long, ByVal iKeyAscii As Integer, bCancel As Boolean)
Dim x As Long, y As Long, w As Long, h As Long
If g1.CellItemData(lRow, lCol) = -1 Then
 g1.CellBoundary lRow, lCol, x, y, w, h
 txtEd.Text = g1.CellText(lRow, lCol)
 txtEd.Move g1.Left + x \ 15 + 2, g1.Top + y \ 15 + 3, w \ 15 - 2, h \ 15 - 2
 txtEd.Visible = True
 txtEd.SetFocus
Else
 bCancel = True
End If
End Sub

Private Function SaveString(ByVal FileNo As Long, ByVal lp As Long, ByVal s As String) As Long
Dim i As Long, m As Integer, x As Integer
m = Len(s)
SaveString = m * 2 + 2
Put #FileNo, lp, m
For i = 1 To m
 x = Asc(Mid(s, i, 1))
 m = m Xor x
 Put #FileNo, lp + 2 * i, m
Next i
End Function

Private Function LoadString(ByVal FileNo As Long, ByVal lp As Long, s As String) As Long
Dim i As Long, m As Integer, x As Integer
s = ""
Get #FileNo, lp, m
LoadString = m * 2 + 2
For i = 1 To m
 Get #FileNo, lp + 2 * i, x
 m = m Xor x
 s = s + Chr(m)
 m = x
Next i
End Function

