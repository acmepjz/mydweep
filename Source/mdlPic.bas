Attribute VB_Name = "mdlPic"
Option Explicit

Public Declare Function GetPixel Lib "gdi32" (ByVal hDC As Long, ByVal x As Long, ByVal y As Long) As Long
Public focusX As Integer, focusY As Integer, focusN As Integer
Public transX As Integer, transY As Integer, transD As Integer

Public isPause As Boolean


Private Type SAFEARRAYBOUND
    cElements As Long
    lLbound As Long
End Type
Private Type SAFEARRAY2D
    cDims As Integer
    fFeatures As Integer
    cbElements As Long
    cLocks As Long
    pvData As Long
    Bounds(0 To 1) As SAFEARRAYBOUND
End Type
Private Declare Function VarPtrArray Lib "msvbvm60.dll" Alias "VarPtr" (ptr() As Any) As Long


Public Sub DrawDweep(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal State As Integer, ByVal t As Long)
Dim bmpNo As Integer
If State > 1000 Then
 Select Case State
 Case 1024 To 1036
  bmpNo = State - 933
 Case 2048 To 2058
  bmpNo = State - 1945
 Case 4096 To 4105
  DrawSprite hDC, bmpDweep, offDweep, x, y, 0, 0, 1, False
  DrawSprite hDC, bmpIceDweep, offIceDweep, x, y, 0, 0, State - 4095, True
  Exit Sub
 End Select
 DrawSprite hDC, bmpDweep, offDweep, x, y, 0, 0, bmpNo, False
Else
 Select Case State Mod 8
 Case 0
  Select Case State \ 32
  Case 0
   bmpNo = 16 + t Mod 10
  Case 32
   bmpNo = 6 + t Mod 10
  Case 64
   bmpNo = 26 + t Mod 10
  Case 96
   bmpNo = 36 + t Mod 10
  End Select
 Case 1
  Select Case State \ 32
  Case 0
   bmpNo = 61 + t Mod 10
  Case 32
   bmpNo = 51 + t Mod 10
  Case 64
   bmpNo = 71 + t Mod 10
  Case 96
   bmpNo = 81 + t Mod 10
  End Select
 Case 2
  Select Case State \ 32
  Case 96
   bmpNo = 102
  Case Else
   bmpNo = 101
  End Select
 Case 3
  DrawSprite hDC, bmpDweep, offDweep, x, y, 0, 0, 1, False
  DrawSprite hDC, bmpIceDweep, offIceDweep, x, y, 0, 0, 1, True
  Exit Sub
 End Select
 DrawSprite hDC, bmpDweep, offDweep, x, y, 0, 0, bmpNo, False
End If
End Sub

Private Sub DrawSprite(ByVal hDC As Long, bmpSprite As cAlphaDibSection, offSprite As typeBitmapOffset, ByVal x As Integer, ByVal y As Integer, ByVal xCOff As Integer, ByVal yCOff As Integer, ByVal i As Integer, ByVal isTrans As Boolean)
Dim h As Long
h = offSprite.OffsetData(i).Height
If sWrong Then h = h - 1
If isTrans Then
 bmpSprite.AlphaPaintPicture hDC, x + xCOff - offSprite.OffsetData(i).CenterX, y + yCOff - offSprite.OffsetData(i).CenterY, _
 offSprite.OffsetData(i).Width, h, _
 0, offSprite.OffsetData(i).Top, 128, True
Else
 bmpSprite.AlphaPaintPicture hDC, x + xCOff - offSprite.OffsetData(i).CenterX, y + yCOff - offSprite.OffsetData(i).CenterY, _
 offSprite.OffsetData(i).Width, h, _
 0, offSprite.OffsetData(i).Top, 255, True
End If
End Sub

Public Sub DrawLaser(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal FS As Integer, ByVal State As Integer, ByVal t As Long, Optional ByVal IsItem As Boolean = False)
'FS
'1=up
'2=right
'3=down
'4=left
DrawSprite hDC, bmpLaser, offLaser, x, y, 20, 14, FS * 4 + t Mod 4 - 3, IsItem
'TODO:Sparkle
If State > 0 Then
 DrawSprite hDC, bmpExp2, offExp2, x, y, 20, 14, State Mod 9 + 1, False
End If
End Sub

Public Sub DrawExplode(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal n As Integer)
DrawSprite hDC, bmpExp, offExp, x, y, 20, 20, n, False
End Sub

Public Sub DrawSparkle(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal n As Integer)
'TODO:
End Sub

Public Sub DrawFan(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal FS As Integer, ByVal State As Integer, ByVal t As Long, Optional ByVal IsItem As Boolean = False)
Dim i As Integer
'FS
'1=up
'2=right
'3=down
'4=left
i = t Mod 8
If sFan Then
 i = i * 3
 i = i Mod 8
End If
DrawSprite hDC, bmpFan, offFan, x, y, 20, 10, FS * 8 + i - 7, IsItem
'TODO:Sparkle
If State > 0 Then
 DrawSprite hDC, bmpExp2, offExp2, x, y, 20, 14, State Mod 9 + 1, False
End If
End Sub

Public Sub DrawMirror(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal FS As Integer, ByVal State As Integer, ByVal t As Long, Optional ByVal IsItem As Boolean = False)
Dim i As Integer
'FS
'1=/
'2=\
'State
'1=Normal
'2-9=Broken
'10-?=Broken
i = IIf(State > 9, 9, State)
Select Case FS
Case 1
 DrawSprite hDC, bmpMirror, offMirror, x, y, 20, 5, i, IsItem
Case 2
 DrawSprite hDC, bmpMirror2, offMirror, x, y, 20, 5, i, IsItem
End Select
End Sub

Public Sub DrawBlock(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal lv As Integer)
Dim dX As Integer
dX = (lv - 1) Mod 5
dX = dX * 40
bmpWall.AlphaPaintPicture hDC, x, y - 40, 40, 80, dX, 0, 255, True
End Sub

Public Sub DrawTile(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal id As Integer, ByVal t As Long)
Dim tt As Integer
Select Case id
Case 1
 tt = t Mod 10
 If tt > 5 Then tt = 10 - tt
 BitBlt hDC, x, y, 40, 40, bmpFire.hDC, 0, tt * 40, vbSrcCopy
Case 2
 tt = t Mod 20
 If tt > 10 Then tt = 20 - tt
 BitBlt hDC, x, y, 40, 40, bmpIce.hDC, 0, tt * 40, vbSrcCopy
End Select
bmpBorder.AlphaPaintPicture hDC, x, y, 40, 40, , , , True
End Sub

Public Sub DrawEnd(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal t As Long)
BitBlt hDC, x, y, 40, 40, bmpItem.hDC, 760, 0, vbSrcCopy
bmpBorder.AlphaPaintPicture hDC, x, y, 40, 40, , , , True
DrawSprite hDC, bmpExit, offExit, x + 9, y + 3, 0, 0, 1 + t Mod 12, False
DrawSprite hDC, bmpExit, offExit, x + 32, y + 13, 0, 0, 13 + (t + 4) Mod 12, False
DrawSprite hDC, bmpExit, offExit, x + 18, y + 27, 0, 0, 25 + (t + 8) Mod 12, False
End Sub

Public Sub DrawMouseEx(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal State As Integer, ByVal dis As Boolean)
DrawSprite hDC, bmpMouse2, offMouse, x, y, 0, 0, State + 1, False
If dis Then
 If State = 0 Then
  DrawSprite hDC, bmpMouse2, offMouse, x + 20, y + 20, 0, 0, 8, False
 Else
  DrawSprite hDC, bmpMouse2, offMouse, x, y, 0, 0, 8, False
 End If
 If pressD Then pressD = False: sPlay 14
End If
End Sub

Public Sub DrawCollect(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal id As Integer)
BitBlt hDC, x, y, 40, 40, bmpItem.hDC, (id - 1) * 40, 0, vbSrcCopy
End Sub

Public Sub DrawBomb(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal State As Integer, Optional ByVal IsItem As Boolean = False)
Dim i As Integer
If State = 1 Then
 i = 1
Else
 i = 2 + State Mod 3
End If
DrawSprite hDC, bmpBomb, offBomb, x, y, 20, 16, i, IsItem
End Sub

Public Sub DrawFocus(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal n As Integer)
DrawSprite hDC, bmpFocus, offFocus, x, y, 20, 20, n, True
End Sub

Public Sub DrawBeam(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal id As Integer, ByVal t As Integer)
Dim i As Integer
i = t Mod 4
Select Case id
Case 0
Case 1
 bmpLaser2.AlphaPaintPicture hDC, x + 18, y, 5, 40, i * 5, 0, , True
Case 2
 bmpLaser2.AlphaPaintPicture hDC, x, y + 9, 40, 5, 40, i * 5, , True
Case 3
 bmpLaser2.AlphaPaintPicture hDC, x + 18, y, 5, 40, i * 5, 0, , True
 bmpLaser2.AlphaPaintPicture hDC, x, y + 9, 40, 5, 40, i * 5, , True
Case 4
 bmpLaser2.AlphaPaintPicture hDC, x + 18, y + 10, 5, 30, i * 5, 10, , True
 bmpLaser2.AlphaPaintPicture hDC, x + 20, y + 9, 20, 5, 60, i * 5, , True
Case 5
 bmpLaser2.AlphaPaintPicture hDC, x, y + 9, 9, 5, 40, i * 5, , True
Case 6
 bmpLaser2.AlphaPaintPicture hDC, x + 18, y + 10, 5, 30, i * 5, 10, , True
 bmpLaser2.AlphaPaintPicture hDC, x + 20, y + 9, 20, 5, 60, i * 5, , True
 bmpLaser2.AlphaPaintPicture hDC, x, y + 9, 9, 5, 40, i * 5, , True
Case 7
 bmpLaser2.AlphaPaintPicture hDC, x + 18, y + 10, 5, 30, i * 5, 10, , True
 bmpLaser2.AlphaPaintPicture hDC, x, y + 9, 20, 5, 40, i * 5, , True
Case 8
 bmpLaser2.AlphaPaintPicture hDC, x + 31, y + 9, 9, 5, 71, i * 5, , True
Case 9
 bmpLaser2.AlphaPaintPicture hDC, x + 18, y + 10, 5, 30, i * 5, 10, , True
 bmpLaser2.AlphaPaintPicture hDC, x, y + 9, 20, 5, 40, i * 5, , True
 bmpLaser2.AlphaPaintPicture hDC, x + 31, y + 9, 9, 5, 71, i * 5, , True
End Select
End Sub

Public Sub DrawLevel(ByVal hDC As Long, lvDat As typeGameData)
Dim i As Integer, j As Integer
Dim x As Integer, y As Integer
Dim t As Long, lv As Integer, st As Integer
Dim var1 As Integer
t = lvDat.GameTime
lv = lvDat.LevelNo
'//////////////////////////////////////////////////////////Draw background
BitBlt hDC, 0, 0, 640, 400, bmpLvBk.hDC, 0, 400 * ((lv - 1) Mod 5), vbSrcCopy
BitBlt hDC, 0, 400, 640, 80, bmpLvBk.hDC, 0, 2000, vbSrcCopy
'//////////////////////////////////////////////////////////Draw item
For i = 1 To lvDat.ItemCount
 BitBlt hDC, i * 50 - 39, 424, 36, 37, bmpItem.hDC _
 , ChangeItemData(lvDat.ItemData(i)) * 40 - 38, 2, vbSrcCopy
 If sNum Then
  DrawTextB hDC, CStr(i Mod 10), frmMain.Font, i * 50 - 36, 424, 16, 16, , vbBlack, , True
 End If
Next i
For j = 1 To 10
 For i = 1 To 16
  var1 = lvDat.LevelData(i, j)
  st = lvDat.LevelState(i, j)
  x = (i - 1) * 40
  y = (j - 1) * 40
  '////////////////////////////////////////////////////Draw blocks
  Select Case var1
  Case 1
   DrawBlock hDC, x, y, lv
  Case 2, 3
   DrawTile hDC, x, y, var1 - 1, t
  Case 4
   DrawEnd hDC, x, y, t
  Case 6 To 9
   DrawLaser hDC, x, y, var1 - 5, st, t, False
  Case 10, 11
   DrawMirror hDC, x, y, var1 - 9, st + 1, t, False
  Case 12 To 15
   DrawFan hDC, x, y, var1 - 11, st, t, False
  Case 16
   DrawBomb hDC, x, y, st + 1, False
  Case 17 To 32
   DrawCollect hDC, x, y, var1 - 16
  End Select
  DrawBeam hDC, x, y, lvDat.LevelLaser(i, j), t
  '////////////////////////////////////////////////////////Focus
  If i = focusX And j = focusY And focusN > 0 Then
   DrawFocus hDC, focusX * 40 - 40, focusY * 40 - 40, focusN
   focusN = focusN + 1
   If focusN > 6 Then focusN = 0
  End If
  '///////////////////////////////////////////////////////Transparent thing
  If i = transX And j = transY And transD >= 0 Then
   Select Case transD
   Case 0
    bmpLvBk.AlphaPaintPicture hDC, transX * 40 - 40, transY * 40 - 40, 40, 40, _
    transX * 40 - 40, transY * 40 - 40 + ((lv - 1) Mod 5) * 400, 128
   Case 1
    bmpWall.AlphaPaintPicture hDC, transX * 40 - 40, transY * 40 - 80, 40, 80, ((lv - 1) Mod 5) * 40, 0, 128, True
   Case 2
    bmpFire.AlphaPaintPicture hDC, transX * 40 - 40, transY * 40 - 40, 40, 40, _
    0, (t Mod 6) * 40, 128
   Case 3
    bmpIce.AlphaPaintPicture hDC, transX * 40 - 40, transY * 40 - 40, 40, 40, _
    0, (t Mod 11) * 40, 128
   Case 4
    bmpItem.AlphaPaintPicture hDC, transX * 40 - 40, transY * 40 - 40, 40, 40, 760, 0, 128
   Case 5
    DrawSprite hDC, bmpDweep, offDweep, transX * 40 - 40, transY * 40 - 40, 20, 10, 6 + t Mod 10, True
   Case 6 To 9
    DrawLaser hDC, transX * 40 - 40, transY * 40 - 40, transD - 5, 0, t, True
   Case 10, 11
    DrawMirror hDC, transX * 40 - 40, transY * 40 - 40, transD - 9, 1, t, True
   Case 12 To 15
    DrawFan hDC, transX * 40 - 40, transY * 40 - 40, transD - 11, 0, t, True
   Case 16
    DrawBomb hDC, transX * 40 - 40, transY * 40 - 40, 0, True
   Case 17 To 32
    bmpItem.AlphaPaintPicture hDC, transX * 40 - 40, transY * 40 - 40, 40, 40, _
    (transD - 17) * 40, 0, 128, False
   End Select
  End If
  '/////////////////////////////////////////////////////////Draw Explode
  If lvDat.LevelExplode(i, j) > 0 Then
   DrawExplode hDC, x, y, lvDat.LevelExplode(i, j)
  End If
 Next i
 '/////////////////////////////////////////////////////////Draw Dweep
 If lvDat.DweepY \ 10 = j - 1 Then
  DrawDweep hDC, lvDat.DweepX * 4, lvDat.DweepY * 4 - 10, lvDat.DweepState, t
 End If
Next j
'/////////////////////////////////////////////Draw Button
If IsRecord Then
 If dItemT = -1 Then
  If pressT Then
   BitBlt bmpBack.hDC, 520, 420, 105, 19, bmpButton3.hDC, 0, 19, vbSrcCopy
  Else
   BitBlt bmpBack.hDC, 520, 420, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
  End If
 Else
  BitBlt bmpBack.hDC, 520, 420, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
 End If
 DrawTextB bmpBack.hDC, IIf(isPause, objText.GetText("&Continue"), objText.GetText("&Pause")), frmMain.Font, 520, 420, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = -1, vbYellow, vbWhite), , True '"����(&C)", "��ͣ(&P)"
ElseIf IsEdit And Not IsCLv Then
 If dItemT = -1 Then
  If pressT Then
   BitBlt bmpBack.hDC, 520, 420, 105, 19, bmpButton3.hDC, 0, 19, vbSrcCopy
  Else
   BitBlt bmpBack.hDC, 520, 420, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
  End If
 Else
  BitBlt bmpBack.hDC, 520, 420, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
 End If
 DrawTextB bmpBack.hDC, objText.GetText("&Test play"), frmMain.Font, 520, 420, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = -1, vbYellow, vbWhite), , True '"����(&T)"
Else
 If dItemT = -1 Then
  If pressT Then
   BitBlt bmpBack.hDC, 520, 420, 105, 19, bmpButton3.hDC, 0, 19, vbSrcCopy
  Else
   BitBlt bmpBack.hDC, 520, 420, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
  End If
 Else
  BitBlt bmpBack.hDC, 520, 420, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
 End If
 DrawTextB bmpBack.hDC, objText.GetText("&Undo"), frmMain.Font, 520, 420, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = -1, vbYellow, vbWhite), , True
End If
If dItemT = 1 Then
 If pressT Then
  BitBlt bmpBack.hDC, 520, 450, 105, 19, bmpButton3.hDC, 0, 19, vbSrcCopy
 Else
  BitBlt bmpBack.hDC, 520, 450, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
 End If
Else
 BitBlt bmpBack.hDC, 520, 450, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
End If
DrawTextB bmpBack.hDC, objText.GetText("Menu") + " (Esc)", frmMain.Font, 520, 450, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = 1, vbYellow, vbWhite), , True '�˵�
'/////////////////////////////////////////////End
End Sub
Public Function DweepHitTest(ByVal mx As Integer, ByVal my As Integer) As Boolean
Dim bmpNo As Integer
Dim State As Integer
Dim x As Integer, y As Integer
Dim t As Long 't=??????????? unknown TODO:??
State = GData.DweepState
x = GData.DweepX * 4
y = GData.DweepY * 4 - 10
BitBlt bmpCache.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, WHITENESS
If State > 1000 Then
 Select Case State
 Case 1024 To 1036
  bmpNo = State - 933
  DrawSprite bmpCache.hDC, bmpDweep, offDweep, x, y, 0, 0, bmpNo, False
 Case 2048 To 2058
  bmpNo = State - 1945
  DrawSprite bmpCache.hDC, bmpDweep, offDweep, x, y, 0, 0, bmpNo, False
 Case 4096 To 4105
  DrawSprite bmpCache.hDC, bmpIceDweep, offIceDweep, x, y, 0, 0, State - 4095, False
 End Select
Else
 Select Case State Mod 8
 Case 0
  Select Case State \ 32
  Case 0
   bmpNo = 16 + t Mod 10
  Case 32
   bmpNo = 6 + t Mod 10
  Case 64
   bmpNo = 26 + t Mod 10
  Case 96
   bmpNo = 36 + t Mod 10
  End Select
  DrawSprite bmpCache.hDC, bmpDweep, offDweep, x, y, 0, 0, bmpNo, False
 Case 1
  Select Case State \ 32
  Case 0
   bmpNo = 61 + t Mod 10
  Case 32
   bmpNo = 51 + t Mod 10
  Case 64
   bmpNo = 71 + t Mod 10
  Case 96
   bmpNo = 81 + t Mod 10
  End Select
  DrawSprite bmpCache.hDC, bmpDweep, offDweep, x, y, 0, 0, bmpNo, False
 Case 2
  Select Case State \ 32
  Case 96
   bmpNo = 102
  Case Else
   bmpNo = 101
  End Select
  DrawSprite bmpCache.hDC, bmpDweep, offDweep, x, y, 0, 0, bmpNo, False
 Case 3
  DrawSprite bmpCache.hDC, bmpIceDweep, offIceDweep, x, y, 0, 0, 1, False
 End Select
End If
DweepHitTest = GetPixel(bmpCache.hDC, mx, my) <> vbWhite
End Function


Public Function Oh_no(m_cImage As cAlphaDibSection, m_cMask As cAlphaDibSection) As cAlphaDibSection
On Error GoTo a
Dim m_cAlphaImage As New cAlphaDibSection
   ' Load picture:
   'm_cImage.CreateFromPicture LoadPicture(App.Path & "\logo.bmp")
   
   ' Load alpha channel:
   'm_cMask.CreateFromPicture LoadPicture(App.Path & "\logomask.bmp")
   
   ' Create a new image, which is just a copy of the picture
   ' in m_cImage to build the alpha version in.  Note if
   ' we didn't want to display the image without alpha later,
   ' we could just work on m_cImage directly instead.
   m_cAlphaImage.Create m_cImage.Width, m_cImage.Height
   m_cImage.PaintPicture m_cAlphaImage.hDC
   
   ' Point byte arrays at the image bits for ease of
   ' manipulation of the data:
   Dim tMask As SAFEARRAY2D
   Dim bMask() As Byte
   Dim tImage As SAFEARRAY2D
   Dim bImage() As Byte
    
    With tMask
        .cbElements = 1
        .cDims = 2
        .Bounds(0).lLbound = 0
        .Bounds(0).cElements = m_cMask.Height
        .Bounds(1).lLbound = 0
        .Bounds(1).cElements = m_cMask.BytesPerScanLine()
        .pvData = m_cMask.DIBSectionBitsPtr
    End With
    CopyMemory ByVal VarPtrArray(bMask()), VarPtr(tMask), 4
    
    With tImage
        .cbElements = 1
        .cDims = 2
        .Bounds(0).lLbound = 0
        .Bounds(0).cElements = m_cAlphaImage.Height
        .Bounds(1).lLbound = 0
        .Bounds(1).cElements = m_cAlphaImage.BytesPerScanLine()
        .pvData = m_cAlphaImage.DIBSectionBitsPtr
    End With
    CopyMemory ByVal VarPtrArray(bImage()), VarPtr(tImage), 4
   
   Dim x As Long, y As Long
   Dim bAlpha As Long
   For y = 0 To m_cAlphaImage.Height - 1
      For x = 0 To m_cAlphaImage.BytesPerScanLine - 4 Step 4 ' each item has 4 bytes: R,G,B,A
         ' Get the red Value from the mask to use as the alpha
         ' Value:
         bAlpha = bMask(x, y)
                  ' Set the alpha in the alpha image..
         bImage(x + 3, y) = bAlpha
         'the shadow is wrong...
         If bAlpha = 255 Then
          bImage(x, y) = bImage(x, y)
          bImage(x + 1, y) = bImage(x + 1, y)
          bImage(x + 2, y) = bImage(x + 2, y)
         Else
          If bAlpha > 0 Then
           bImage(x + 3, y) = sShadow
          End If
          bImage(x, y) = 0
          bImage(x + 1, y) = 0
          bImage(x + 2, y) = 0
         End If
      Next x
   Next y
   
a: ' Clear up the temporary array descriptors.  You
   ' only need to do this on NT but best to be safe.
   CopyMemory ByVal VarPtrArray(bMask), 0&, 4
   CopyMemory ByVal VarPtrArray(bImage), 0&, 4

   'lblImage.Move 2, 2
   'lblMask.Move 2, 4 + m_cAlphaImage.Height
   'lblFullAlpha.Move 2, 6 + m_cAlphaImage.Height * 2
   'lblFaded.Move 2, 8 + m_cAlphaImage.Height * 3
   'Draw
Set Oh_no = m_cAlphaImage
   
End Function
