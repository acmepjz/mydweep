Attribute VB_Name = "mdlRecord"
Option Explicit

Private Type typeRoot
 Key As String
 Text As String
End Type
Private rRoot() As typeRoot
Private c1 As Long
Private Type typeRec
 Root As Long
 Key As String
 Text As String
 i As Long
End Type
Private rRec() As typeRec
Private c2 As Long

Public Const OptimizeThreshold As Long = 15

Public Sub MenuRecLoop()
On Error Resume Next
Dim i As Integer, vars As String
sItemT = 0
'////////////////////////////////////////Load Levels
'frmMain.lstRecord.Nodes.Clear
Erase rRoot
c1 = 0
c2 = 0
With List1
 Set .Font = frmMain.Font
 .ForeColor = vbWhite
 .ItemHeight = 16
 .SetImageList frmMain.Image1(2).Picture, 16, 16
 .Create bmpBack.hDC, 100, 100, 440, 250
End With
ReDim rRec(1 To fileRec.count)
For i = 1 To fileRec.count
 RecordLoadNode i
Next i
PutNodes
'////////////////////////////////////////End
ReStart:
NowScene = 4
'frmMain.lstRecord.Visible = True
'ShowCursor 1
Do
 RedrawBack
 For i = 1 To 2
  If i = dItemT Then
   If pressT Then
    BitBlt bmpBack.hDC, i * 300 - 250, 400, 240, 33, bmpButton1.hDC, 0, 33, vbSrcCopy
   Else
    BitBlt bmpBack.hDC, i * 300 - 250, 400, 240, 33, bmpButton1.hDC, 0, 0, vbSrcCopy
   End If
   DrawTextB bmpBack.hDC, TheMid(2, 9, i + 3), frmMain.Font, i * 300 - 250, 400, 240, 33, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
  Else
   BitBlt bmpBack.hDC, i * 300 - 250, 400, 240, 33, bmpButton1.hDC, 0, 0, vbSrcCopy
   DrawTextB bmpBack.hDC, TheMid(2, 9, i + 3), frmMain.Font, i * 300 - 250, 400, 240, 33, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
  End If
 Next
 DrawTextB bmpBack.hDC, "选择一个录像", frmMain.Label1.Font, 244, 20, 152, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
 List1.Redraw
 RedrawMouse
 fPaint bmpBack.hDC
 'nowFPS = nowFPS + 1
 'frmMain.lstLvPack.Refresh
 '//////////////////////////////////////////////////End of drawing
 Play2
 DoEvents
 m_objFPS.WaitForNextFrame
 If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
  sItemT = 1
 ElseIf GetAsyncKeyState(vbKeyReturn) = &H8000 Then
  sItemT = 2
 End If
Loop Until sItemT > 0
'frmMain.lstRecord.Visible = False
'ShowCursor 0
Select Case sItemT
Case 2
 'TODO:
 If List1.ListIndex = -1 Then '?
  MsgBox "请选择一个录像。"
  sItemT = 0
 Else
  List1.Enable = False
  OpenRecord List1.Key(List1.ListIndex)
  IsRecord = True
  GameLoop
  IsRecord = False
  List1.Enable = True
 End If
 GoTo ReStart
End Select
List1.Destroy
a:
Close
End Sub

Public Sub GetRecordInfo(ByVal FileName As String, ByRef PackFileName As String, ByRef PackName As String, ByRef LevelName As String, ByRef LevelNo As Integer)
Dim var1 As Byte
Open FileName For Binary As #1
PackFileName = Trim(GetString(1, 1, 15)) '1-10
PackName = Trim(GetString(1, 16, 25))  '11-40
LevelName = GetString(1, 41, 40)  '41-80
Get #1, 280, var1
LevelNo = var1
Close
End Sub

Public Sub SaveRecordLoop()
Dim i As Integer, n As Long, m As Long, m2 As Long
On Error GoTo a
'/////////////////////////////////////////////////////////Analyze Data--optimized!!!
n = 0
For i = 1 To RCount
 If sOpt Then
  If RData(i).Debug_IsActive Then
   m2 = 0
  Else
   m2 = m2 + 1 'ideal time
   If m2 > OptimizeThreshold Then m = m + 1
  End If
  If RData(i).PressState > 0 Then
   n = n + 1
   RD2(n).GameTime = i - m
   RD2(n).MouseX = RData(i).MouseX
   RD2(n).MouseY = RData(i).MouseY
   RD2(n).PressState = RData(i).PressState
   m2 = 0
  End If
 ElseIf RData(i).PressState > 0 Then
  n = n + 1
  RD2(n).GameTime = i
  RD2(n).MouseX = RData(i).MouseX
  RD2(n).MouseY = RData(i).MouseY
  RD2(n).PressState = RData(i).PressState
 End If
Next i
RD2C = n
'///////////////////////////////////////////////////////Save Data
If Not IsCLv Then
 SaveRecord CStr(App.Path) + "\Record\" + Left(LevelPackFile, Len( _
 LevelPackFile) - 4) + Format(LData.LevelNo, "0000") + ".dwr"
Else
 'SaveRecord ShowSave(LoadResString(130), CStr(App.Path) + "\record\")
 SaveRecord CStr(App.Path) + "\Record\" + GetCustomLvRecordName + ".dwr"
End If
fileRec.Refresh
a:
End Sub

Public Sub SaveRecord(ByVal FileName As String)
Dim i As Integer, j As Integer, lp As Long
Dim var1 As Byte, s As String
Open FileName For Output As #1
Close
Open FileName For Binary As #1
If IsCLv Then
 SetString 1, 1, Chr(128), 15
 s = Right(LevelPackFile, InStr(1, StrReverse(LevelPackFile), "\") - 1)
 SetString 1, 16, s, 25
Else
 SetString 1, 1, LevelPackFile, 15 '1-10
 SetString 1, 16, LevelPackName, 25 '11-40
End If
SetString 1, 41, LData.LevelName, 40 '41-80
SetString 1, 81, LData.LevelHint, 200 '81-280
'////////extra:Weight
For i = 0 To 2
 wR(i) = Weight(i)
Next i
Put #1, 271, wR
'////////////////////
var1 = LData.LevelNo
Put #1, 280, var1
For j = 1 To 10
 For i = 1 To 16
  lp = (j - 1) * 16 + i + 280 '281-220
  var1 = LData.LevelData(i, j)
  Put #1, lp, var1
 Next i
Next j
var1 = LData.ItemCount
Put #1, 441, var1 '441
For i = 1 To 10
 lp = i + 441 '442-451
 var1 = LData.ItemStart(i)
 Put #1, lp, var1
Next i
SetLong 1, 452, RD2C '452-455
For i = 1 To RD2C
 SetLong 1, (i - 1) * 9& + 456#, RD2(i).GameTime
 SetInteger 1, (i - 1) * 9& + 460#, RD2(i).MouseX
 SetInteger 1, (i - 1) * 9& + 462#, RD2(i).MouseY
 lp = (i - 1) * 9& + 464#
 var1 = RD2(i).PressState
 Put #1, lp, var1
Next i
Close
End Sub

Private Sub RecordLoadNode(ByVal i As Integer)
'On Error GoTo a
Dim strF As String, var1 As Integer, var2 As Integer
Dim str1 As String, str2 As String, str3 As String
Dim j As Long
strF = fileRec.FileNameWithPath(i)
GetRecordInfo strF, str1, str2, str3, var1
var2 = 2 + (var1 - 1) Mod 5
'b:
'TODO:Single Level
'frmMain.lstRecord.Nodes.Add "?" + str1, tvwChild, strF, "第" + _
'CStr(var1) + "关-" + str3, var2, var2
j = FindKey("?" + str1)
If j = -1 Then
 c1 = c1 + 1
 ReDim Preserve rRoot(1 To c1)
 With rRoot(c1)
  .Key = "?" + str1
  If str1 = Chr(128) Then
   .Text = "自定义关卡"
  Else
   .Text = str1 + "-" + str2
  End If
 End With
 j = c1
End If
c2 = c2 + 1
With rRec(c2)
 .Root = j
 .Key = strF
 If str1 = Chr(128) Then
  .Text = str2 + vbTab + vbTab + str3
 Else
  .Text = "第" + CStr(var1) + "关" + vbTab + vbTab + str3
 End If
 .i = var2
End With
'Exit Sub
'a:
'frmMain.lstRecord.Nodes.Add , tvwNext, "?" + str1, str1 + "-" + str2, 1, 1
'Resume b
End Sub

Private Function FindKey(ByVal s As String) As Long
Dim i As Long
For i = 1 To c1
 If StrComp(rRoot(i).Key, s, vbTextCompare) = 0 Then
  FindKey = i
  Exit Function
 End If
Next i
FindKey = -1
End Function

Private Function PutNodes()
Dim i As Long, j As Long
For i = 1 To c1
 With rRoot(i)
  List1.Add .Key, .Text, 1, &HC0C0C0, False
 End With
 For j = 1 To c2
  With rRec(j)
   If .Root = i Then
    List1.Add .Key, .Text, .i, vbYellow, , 32
   End If
  End With
 Next j
Next i
End Function

Public Sub OpenRecord(ByVal FileName As String)
On Error GoTo a
Dim i As Integer, j As Integer, lp As Long
Dim var1 As Byte
Open FileName For Binary As #1
LevelPackFile = GetString(1, 1, 15) '1-10
LevelPackName = GetString(1, 16, 25) '11-40
LData.LevelName = GetString(1, 41, 40) '41-80
LData.LevelHint = GetString(1, 81, 200) '81-280
'////////extra:Weight
Get #1, 271, wR
'////////////////////
Get #1, 280, var1
LData.LevelNo = var1
For j = 1 To 10
 For i = 1 To 16
  lp = (j - 1) * 16 + i + 280 '281-220
  Get #1, lp, var1
  LData.LevelData(i, j) = var1
  If var1 = 5 Then
   LData.DweepStartX = i
   LData.DweepStartY = j
  End If
 Next i
Next j
Get #1, 441, var1 '441
LData.ItemCount = var1
For i = 1 To 10
 lp = i + 441 '442-451
 Get #1, lp, var1
 LData.ItemStart(i) = var1
Next i
RD2C = GetLong(1, 451) '452-455
For i = 1 To RD2C
 RD2(i).GameTime = GetLong(1, (i - 1) * 9& + 455&)
 RD2(i).MouseX = GetInteger(1, (i - 1) * 9& + 460&)
 RD2(i).MouseY = GetInteger(1, (i - 1) * 9& + 462&)
 lp = (i - 1) * 9& + 464#
 Get #1, lp, var1
 RD2(i).PressState = var1
Next i
a:
Close
End Sub

Public Function RecordExist() As Boolean
If IsCLv Then
 RecordExist = FileExist(CStr(App.Path) + "\Record\" + GetCustomLvRecordName + ".dwr")
Else
 RecordExist = FileExist(CStr(App.Path) + "\Record\" + Left( _
 LevelPackFile, Len(LevelPackFile) - 4) + Format(LData.LevelNo, _
 "0000") + ".dwr")
End If
End Function

Public Function GetCustomLvRecordName() As String
Dim s As String, s1 As String, n As Long
Dim b() As Byte
n = Len(LData.LevelName)
ReDim b(n + n)
CopyMemory b(0), ByVal StrPtr(LData.LevelName), n + n
s = Hex(crc.GetByteArrayCrc32(b))
s1 = String(8 - Len(s), "0") + s
'//
n = Len(LData.LevelHint)
ReDim b(n + n)
CopyMemory b(0), ByVal StrPtr(LData.LevelHint), n + n
s = Hex(crc.GetByteArrayCrc32(b))
s1 = s1 + String(8 - Len(s), "0") + s
'//
n = 320
ReDim b(n - 1)
CopyMemory b(0), LData.LevelData(1, 1), n
s = Hex(crc.GetByteArrayCrc32(b))
s1 = s1 + String(8 - Len(s), "0") + s
'//
n = LData.ItemCount
ReDim b(n + n)
CopyMemory b(0), LData.ItemStart(1), n + n
s = Hex(crc.GetByteArrayCrc32(b))
s1 = s1 + String(8 - Len(s), "0") + s
GetCustomLvRecordName = "{" + Mid(s1, 1, 8) + "-" + Mid(s1, 9, 4) + "-" + Mid(s1, 13, 4) + "-" + Mid(s1, 17, 4) + "-" + Mid(s1, 21, 12) + "}"
End Function
