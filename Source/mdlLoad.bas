Attribute VB_Name = "mdlLoad"
Option Explicit

Public Declare Function SetPixelV Lib "gdi32" (ByVal hDC As Long, ByVal x As Long, ByVal y As Long, ByVal crColor As Long) As Long
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
Private Declare Function FrameRect Lib "user32" (ByVal hDC As Long, lpRect As RECT, ByVal hBrush As Long) As Long
Private Declare Function CreateSolidBrush Lib "gdi32" (ByVal crColor As Long) As Long

Private Const BmpCount As Integer = 40

Public sMusic As Boolean, sMusic2 As Long
Public sSound As Boolean, sSound2 As Long
Public sHint As Boolean, sPath As Boolean
Public sAuto As Boolean
Public sSpeed As Long
Public sOpt As Boolean

Public Weight(2) As Long
Public wR(2) As Byte

'///////////////////options
Public frmOpt As Boolean, sFull As Boolean, sFade As Boolean, sShadow As Long
Public sWrong As Boolean, sFan As Boolean, sNum As Boolean, sTool As Boolean
Public ssQ As Long
'///////////////////

Public Sub LoadBitmapLoop()
On Error GoTo a
Dim bm As New cAlphaDibSection
Dim i As Long
'////////////////////////////////xx
hMusic(1) = ld.LoadToFMusic
FMUSIC_SetLooping hMusic(1), 1
Play 1
'bmpCache.CreateFromFile CStr(App.Path) + "\title.jpg"
ld.LoadJPEG bmpCache
If sFade Then
 For i = 5 To 255 Step 50
  BitBlt bmpBack.hDC, 0, 0, 640, 480, 0, 0, 0, BLACKNESS
  bmpCache.AlphaPaintPicture bmpBack.hDC, , , , , , , i
  fPaint bmpBack.hDC
  DoEvents
  m_objFPS.WaitForNextFrame
 Next i
Else
 bmpCache.PaintPicture bmpBack.hDC
 fPaint bmpBack.hDC
End If
'bmpBmp.CreateFromFile CStr(App.Path) + "\load1.bmp"
ld.LoadJPEG bmpBmp
DrawProgress 1
'mMusic(1).OpenMusic CStr(App.Path) + "\menu.mid" 'eee~~~~~
'hMusic(1) = FMUSIC_LoadSong(CStr(App.Path) + "\menu.mid")
'bmpBackBmp.CreateFromFile CStr(App.Path) + "\back.jpg"
ld.LoadJPEG bmpBackBmp
DrawProgress 2
'bmpButton1.CreateFromFile CStr(App.Path) + "\button1.jpg"
ld.LoadJPEG bmpButton1
DrawProgress 3
'/////////////border
bmpBorder.Create 40, 40
bm.CreateFromPicture frmMain.Image1(3).Picture
bm.PaintPicture bmpBorder.hDC, 0, 0, 40, 40, 40, 0
Set bmpBorder = CreateAlphaImage(bmpBorder, bm)
DrawProgress 4 '///
DrawProgress 5
'bmpButton2.CreateFromFile CStr(App.Path) + "\button2.jpg"
ld.LoadJPEG bmpButton2
DrawProgress 6
'bmpButton3.CreateFromFile CStr(App.Path) + "\button3.jpg"
ld.LoadJPEG bmpButton3
DrawProgress 7
bmpLvBk.Create 640, 2160
'BitBlt bmpLvBk.hdc, 0, 0, 640, 2080, Pict.hdc, 0, 0, vbSrcCopy
DrawProgress 8
'bmpDBmp.CreateFromFile CStr(App.Path) + "\dialog.jpg"
ld.LoadJPEG bmpDBmp
DrawProgress 9
'Pict.Picture = LoadPicture(CStr(App.Path) + "\gameback.jpg") 'be!
BitBlt bmpLvBk.hDC, 0, 0, 640, 2080, ld.LoadJPEGEx.hDC, 0, 0, vbSrcCopy
DrawProgress 10
'Pict.Picture = LoadPicture(CStr(App.Path) + "\editback.jpg") 'be!
BitBlt bmpLvBk.hDC, 0, 2080, 640, 80, ld.LoadJPEGEx.hDC, 0, 0, vbSrcCopy
'////////
'''bmpWall.CreateFromFile CStr(App.Path) + "\texture.bmp"
'''CreateB
'''DrawProgress 11
'''bmpItem.CreateFromFile CStr(App.Path) + "\collect.bmp"
'''DrawProgress 12
'''bmpFire.CreateFromFile CStr(App.Path) + "\bitmap\fire.bmp"
'''DrawProgress 13
'''bmpIce.CreateFromFile CStr(App.Path) + "\bitmap\ice.bmp"
'''DrawProgress 14
'''bmpMirror2.CreateFromFile CStr(App.Path) + "\bitmap\mirror2.bmp"
'''DrawProgress 15
'LoadBitmapWithData CStr(App.Path) + "\bitmap\bomb.bmp", bmpBomb, offBomb
ld.LoadToBitmap2 bmpBomb, offBomb
DrawProgress 16
'LoadBitmapWithData CStr(App.Path) + "\bitmap\cursor.bmp", bmpMouse2, offMouse
ld.LoadToBitmap2 bmpMouse2, offMouse
DrawProgress 17
'LoadBitmapWithData CStr(App.Path) + "\bitmap\dweep.bmp", bmpDweep, offDweep
ld.LoadToBitmap2 bmpDweep, offDweep
DrawProgress 18
'LoadBitmapWithData CStr(App.Path) + "\bitmap\dweep2.bmp", bmpIceDweep, offIceDweep
ld.LoadToBitmap2 bmpIceDweep, offIceDweep
DrawProgress 19
'LoadBitmapWithData CStr(App.Path) + "\bitmap\exit2.bmp", bmpExit, offExit
ld.LoadToBitmap2 bmpExit, offExit
DrawProgress 20
'LoadBitmapWithData CStr(App.Path) + "\bitmap\exp.bmp", bmpExp, offExp
ld.LoadToBitmap2 bmpExp, offExp
DrawProgress 21
'LoadBitmapWithData CStr(App.Path) + "\bitmap\exp2.bmp", bmpExp2, offExp2
ld.LoadToBitmap2 bmpExp2, offExp2
DrawProgress 22
'LoadBitmapWithData CStr(App.Path) + "\bitmap\fan.bmp", bmpFan, offFan
ld.LoadToBitmap2 bmpFan, offFan
DrawProgress 23
'LoadBitmapWithData CStr(App.Path) + "\bitmap\focus.bmp", bmpFocus, offFocus
ld.LoadToBitmap2 bmpFocus, offFocus
DrawProgress 24
'LoadBitmapWithData CStr(App.Path) + "\bitmap\laser.bmp", bmpLaser, offLaser
ld.LoadToBitmap2 bmpLaser, offLaser
DrawProgress 25
'LoadBitmapWithData CStr(App.Path) + "\bitmap\mirror.bmp", bmpMirror, offMirror
ld.LoadToBitmap2 bmpMirror, offMirror
DrawProgress 26
ld.LoadToBitmap bmpItem
ld.LoadToBitmap bmpWall
CreateB
DrawProgress 27
ld.LoadToBitmap bmpMirror2
CreateAlphaChannel bmpMirror2, offMirror, True
DrawProgress 28
ld.LoadToBitmap bmpIce
ld.LoadToBitmap bmpFire
DrawProgress 29
LoadSound
DrawProgress 30
LoadLaser
DrawProgress 31
'Pict.Picture = Nothing
bmpBmp.Create 640, 480
LoadTimeDate
'If TheCRC32(CStr(App.Path) + "\" + CStr(App.EXEName) + ".exe") <> YouArePig Then
' 'Err.Raise 12345, , "版本不符，可能是文件损坏。重新安装程序可能可以解决此问题。"
' ld.CloseFile
' LetUsCrash
'End If
'///////////////////////////////xx2
If sFade Then
 For i = 255 To 5 Step -50
  BitBlt bmpCache.hDC, 0, 0, 640, 480, 0, 0, 0, BLACKNESS
  bmpBack.AlphaPaintPicture bmpCache.hDC, , , , , , , i
  fPaint bmpCache.hDC
  DoEvents
  m_objFPS.WaitForNextFrame
 Next i
End If
Exit Sub
a:
'mMusic(1).CloseMusic
FMUSIC_StopAllSongs
FMUSIC_FreeSong hMusic(1)
FSOUND_Close
ld.CloseFile
MsgBox Err.Description, vbCritical
End
End Sub

Private Sub DrawProgress(ByVal i As Integer)
DrawProgress2 Int(i / BmpCount * 256)
End Sub

Private Sub DrawProgress2(ByVal i As Integer)
DoEvents
BitBlt bmpBack.hDC, 192, 416, i, 24, bmpBmp.hDC, 0, 0, vbSrcCopy
fPaint bmpBack.hDC
End Sub

'Private Sub XOption(ByVal hDC As Long)
'Dim i As Long, j As Boolean
'DrawDialogBox hDC, 100, 140, 440, 200
'DrawTextB hDC, "选项", frmMain.Label1.Font, 100, 160, 440, 32, DT_CENTER, vbBlack, , True
'For i = 1 To 2
'  If i = dItemT And pressT Then
'   BitBlt hDC, i * 160 + 10, 300, 140, 25, bmpButton2.hDC, 0, 25, vbSrcCopy
'  Else
'   BitBlt hDC, i * 160 + 10, 300, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
'  End If
'  If i = dItemT Then
'   DrawTextB hDC, TheMid(2, 9, i + 3), frmMain.Font, i * 160 + 10, 300, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
'  Else
'   DrawTextB hDC, TheMid(2, 9, i + 3), frmMain.Font, i * 160 + 10, 300, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
'  End If
'Next i
'For i = 1 To 5
' Select Case i
' Case 1: j = sMusic
' Case 2: j = sSound
' Case 3: j = sHint
' Case 4: j = sPath
' End Select
' If i <> 5 Then BitBlt hDC, 120, 180 + i * 20, 20, 20, bmpDBmp.hDC, IIf(j, 0, 20), 0, vbSrcCopy
' DrawTextB hDC, LoadResString(119 + i), frmMain.Font, _
' 145, 180 + i * 20, 400, 20, DT_VCENTER Or DT_SINGLELINE, IIf(-dItemT = i, vbBlue, vbBlack), , True
'Next i
'End Sub

Public Sub ShowOptionsLoop()
'Dim aa As Integer, bb As Integer
'Dim cc As Integer, dd As Integer
'If frmOpt Then
 frmAdv.Show 1
 dItemT = 0
' Exit Sub
'End If
'BitBlt bmpCache.hDC, 0, 0, 640, 480, bmpBack.hDC, 0, 0, vbSrcCopy
'If sFade Then
' XOption bmpBmp.hDC
' For aa = 5 To 255 Step 50
'  BitBlt bmpBack.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, vbSrcCopy
'  bmpBmp.AlphaPaintPicture bmpBack.hDC, 100, 140, 440, 200, 100, 140, aa, False
'  RedrawMouse
'  fPaint bmpBack.hDC
'  'nowFPS = nowFPS + 1
'  DoEvents
'  m_objFPS.WaitForNextFrame
' Next aa
'End If
'sItemT = 0
'NowScene = 128
''//////////////////////////////////////////////////////Get Settings
'LoadOption
''///////////////////////////////////////////////////////End
'Do
' If sItemT < 0 Then
'  Select Case sItemT
'  Case -1: sMusic = Not sMusic
'  Case -2: sSound = Not sSound
'  Case -3: sHint = Not sHint
'  Case -4: sPath = Not sPath
'  Case -5
'   frmAdv.Show 1
'  End Select
'  sItemT = 0
' End If
' BitBlt bmpBack.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, vbSrcCopy
' XOption bmpBack.hDC
' RedrawMouse
' fPaint bmpBack.hDC
' 'nowFPS = nowFPS + 1
' Play2
' DoEvents
' m_objFPS.WaitForNextFrame
'Loop Until sItemT > 0
''////////////////////////////////////////////////////Set Settings
'If sItemT = 2 Then
' SaveOption
'Else
' LoadOption
'End If
''////////////////////////////////////////////////////End
'If sFade Then
' XOption bmpBmp.hDC
' For aa = 255 To 5 Step -50
'  BitBlt bmpBack.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, vbSrcCopy
'  bmpBmp.AlphaPaintPicture bmpBack.hDC, 100, 140, 440, 200, 100, 140, aa, False
'  RedrawMouse
'  fPaint bmpBack.hDC
'  'nowFPS = nowFPS + 1
'  DoEvents
'  m_objFPS.WaitForNextFrame
' Next aa
'End If
End Sub

Public Sub DrawDialogBox(ByVal hDC As Long, ByVal Left As Integer, ByVal Top As Integer, ByVal Width As Integer, ByVal Height As Integer, Optional ByVal Thin As Boolean)
Dim x As Integer, y As Integer
Dim i As Integer, j As Integer
Dim var1 As Integer, var2 As Integer
Dim r As RECT, h As Long
If Thin Then
 var1 = Width \ 80 + 1
 var2 = Height \ 80 + 1
 For i = 1 To var1
  If i = var1 Then x = Width Mod 80 Else x = 80
  For j = 1 To var2
   If j = var2 Then y = Height Mod 80 Else y = 80
   BitBlt hDC, Left + (i - 1) * 80, Top + (j - 1) * 80, x, y, bmpDBmp.hDC, 0, 20, vbSrcCopy
  Next j
 Next i
 r.Left = Left
 r.Top = Top
 r.Right = Left + Width
 r.Bottom = Top + Height
 h = CreateSolidBrush(&H808080)
 FrameRect hDC, r, h
 DeleteObject h
Else
 BitBlt hDC, Left, Top, 16, 16, bmpDBmp.hDC, 40, 0, vbSrcCopy
 BitBlt hDC, Left + Width - 16, Top, 16, 16, bmpDBmp.hDC, 40, 0, vbSrcCopy
 BitBlt hDC, Left, Top + Height - 16, 16, 16, bmpDBmp.hDC, 40, 0, vbSrcCopy
 BitBlt hDC, Left + Width - 16, Top + Height - 16, 16, 16, bmpDBmp.hDC, 40, 0, vbSrcCopy
 var1 = Int(Width / 16) - 1
 For i = 1 To var1
  If i = var1 Then
   BitBlt hDC, Left + i * 16, Top, Width Mod 16, 16, bmpDBmp.hDC, 56, 0, vbSrcCopy
   BitBlt hDC, Left + i * 16, Top + Height - 16, Width Mod 16, 16, bmpDBmp.hDC, 56, 0, vbSrcCopy
  Else
   BitBlt hDC, Left + i * 16, Top, 16, 16, bmpDBmp.hDC, 56, 0, vbSrcCopy
   BitBlt hDC, Left + i * 16, Top + Height - 16, 16, 16, bmpDBmp.hDC, 56, 0, vbSrcCopy
  End If
 Next i
 var1 = Int(Height / 16) - 1
 For i = 1 To var1
  If i = var1 Then
   BitBlt hDC, Left, Top + i * 16, 16, Height Mod 16, bmpDBmp.hDC, 72, 0, vbSrcCopy
   BitBlt hDC, Left + Width - 16, Top + i * 16, 16, Height Mod 16, bmpDBmp.hDC, 72, 0, vbSrcCopy
  Else
   BitBlt hDC, Left, Top + i * 16, 16, 16, bmpDBmp.hDC, 72, 0, vbSrcCopy
   BitBlt hDC, Left + Width - 16, Top + i * 16, 16, 16, bmpDBmp.hDC, 72, 0, vbSrcCopy
  End If
 Next i
 var1 = Int((Width - 32) / 80) + 1
 var2 = Int((Height - 32) / 80) + 1
 For i = 1 To var1
  If i = var1 Then x = (Width - 32) Mod 80 Else x = 80
  For j = 1 To var2
   If j = var2 Then y = (Height - 32) Mod 80 Else y = 80
   BitBlt hDC, Left + 16 + (i - 1) * 80, Top + 16 + (j - 1) * 80, x, y, bmpDBmp.hDC, 0, 20, vbSrcCopy
  Next j
 Next i
End If
End Sub

Private Sub LoadLaser()
Dim i As Integer, x As Integer, y As Integer, clr As Long
Dim a As Integer
bmpLaser2.Create 80, 40
For i = 1 To 4
 For x = 1 To 5
  For y = 1 To 40
   a = 200 - 50 * Abs(x - 3) + Int(56 * Rnd)
   clr = RGB(0, a, 0)
   SetPixelV bmpLaser2.hDC, i * 5 + x - 6, y - 1, clr
   a = 200 - 50 * Abs(x - 3) + Int(56 * Rnd)
   clr = RGB(0, a, 0)
   SetPixelV bmpLaser2.hDC, y + 39, i * 5 + x - 6, clr
  Next y
 Next x
Next i
Set bmpLaser2 = ImageOpEx(bmpLaser2, bmpLaser2, osBuildAlphaChannel_UseBlackForTransColor, , 3)
End Sub

Public Sub LoadOption()
Dim var2 As String, i As Long
GetKeyValue "Software\Dexterity Software\Dweep", "Music", var2
sMusic = var2 = "1"
GetKeyValue "Software\Dexterity Software\Dweep", "Sound", var2
sSound = var2 = "1"
GetKeyValue "Software\Dexterity Software\Dweep", "Hints", var2
sHint = var2 = "1"
var2 = reg2_GetSettings("Path", "0")
sPath = var2 = "1"
var2 = reg2_GetSettings("AutoItem", "0")
sAuto = var2 = "1"
sSpeed = Val(reg2_GetSettings("Speed", "10"))
For i = 0 To 2
 var2 = reg2_GetSettings("Weight" + CStr(i), "0")
 Weight(i) = Val(var2)
Next i
frmOpt = CBool(reg2_GetSettings("Dialog", "False"))
sFull = CBool(reg2_GetSettings("Fullscreen", "False"))
sFade = CBool(reg2_GetSettings("Fade", "True"))
sWrong = CBool(reg2_GetSettings("WrongPixel", "False"))
sFan = CBool(reg2_GetSettings("3xFan", "True"))
sShadow = Val(reg2_GetSettings("Shadow", "128"))
sNum = CBool(reg2_GetSettings("Shortcut", "False"))
sTool = CBool(reg2_GetSettings("Tooltip", "False"))
sOpt = CBool(reg2_GetSettings("Optimize", "True"))
ssQ = Val(reg2_GetSettings("Snapshot Quantity", "75"))
sMusic2 = Val(reg2_GetSettings("MusicVol", "128"))
sSound2 = Val(reg2_GetSettings("SoundVol", "128"))
ApplyOption
End Sub

Public Sub SaveOption()
Dim i As Long, var2 As String
'QueryValueEx HKEY_LOCAL_MACHINE, "Software\Dexterity Software\Dweep\音乐", var1
'QueryValueEx HKEY_LOCAL_MACHINE, "Software\Dexterity Software\Dweep\Sound", var1
'QueryValueEx HKEY_LOCAL_MACHINE, "Software\Dexterity Software\Dweep\Hints", var1
SetKeyValue "Software\Dexterity Software\Dweep", "Music", CStr(sMusic And 1&)
SetKeyValue "Software\Dexterity Software\Dweep", "Sound", CStr(sSound And 1&)
SetKeyValue "Software\Dexterity Software\Dweep", "Hints", CStr(sHint And 1&)
SetKeyValue "Software\MyDweep", "Path", IIf(sPath, "1", "0")
SetKeyValue "Software\MyDweep", "AutoItem", IIf(sAuto, "1", "0")
SetKeyValue "Software\MyDweep", "Speed", CStr(sSpeed)
For i = 0 To 2
 SetKeyValue "Software\MyDweep", "Weight" + CStr(i), CStr(Weight(i))
Next i
SetKeyValue "Software\MyDweep", "Dialog", CStr(frmOpt)
SetKeyValue "Software\MyDweep", "Fullscreen", CStr(sFull)
SetKeyValue "Software\MyDweep", "Fade", CStr(sFade)
SetKeyValue "Software\MyDweep", "WrongPixel", CStr(sWrong)
SetKeyValue "Software\MyDweep", "3xFan", CStr(sFan)
SetKeyValue "Software\MyDweep", "Shadow", CStr(sShadow)
SetKeyValue "Software\MyDweep", "Tooltip", CStr(sTool)
SetKeyValue "Software\MyDweep", "Shortcut", CStr(sNum)
SetKeyValue "Software\MyDweep", "Optimize", CStr(sOpt)
SetKeyValue "Software\MyDweep", "Snapshot Quantity", CStr(ssQ)
SetKeyValue "Software\MyDweep", "MusicVol", CStr(sMusic2)
SetKeyValue "Software\MyDweep", "SoundVol", CStr(sSound2)
reg2_SaveFile
ApplyOption
End Sub

'Private Sub LoadBitmapWithData(ByVal FileName As String, bmp As cAlphaDibSection, dat As typeBitmapOffset)
'On Error GoTo a
'Dim varS As String
'Dim m As Integer, i As Integer
'bmp.CreateFromFile FileName ' = False Then
'' Debug.Print "Error: Can't load " + FileName + "!"
''End If
'varS = Left(FileName, Len(FileName) - 4) + ".dat"
'Open varS For Input As #1
'Input #1, m
'dat.Count = m
'For i = 1 To m
' Input #1, dat.OffsetData(i).Top, dat.OffsetData(i).Width, _
' dat.OffsetData(i).CenterX, dat.OffsetData(i).CenterY
'Next i
'Close
'For i = 1 To m - 1
' dat.OffsetData(i).Height = dat.OffsetData(i + 1).Top - dat.OffsetData(i).Top
'Next i
'dat.OffsetData(m).Height = bmp.Height - dat.OffsetData(m).Top
'CreateAlphaChannel bmp, dat
'a:
'If Err.Number <> 0 Then Debug.Print Err.Description + "(" + FileName + ")"
'End Sub

Public Sub CreateAlphaChannel(bmp As cAlphaDibSection, dat As typeBitmapOffset, Optional ByVal n As Boolean)
Dim bm1 As New cAlphaDibSection, bm2 As New cAlphaDibSection
Dim i As Long, w As Long
For i = 1 To dat.count
 With dat.OffsetData(i)
  If w < .Width Then w = .Width
 End With
Next i
bm1.Create w, bmp.Height
bm2.Create w, bmp.Height
For i = 1 To dat.count
 With dat.OffsetData(i)
  BitBlt bm1.hDC, 0, .Top, .Width, .Height, bmp.hDC, 0, .Top, vbSrcCopy
  BitBlt bm2.hDC, 0, .Top, .Width, .Height, bmp.hDC, .Width, .Top, vbSrcCopy
 End With
Next i
If n Then
 BitBlt bm1.hDC, 0, 0, w, bmp.Height, 0, 0, 0, vbDstInvert
 Set bmp = Oh_no(bm2, bm1)
Else
 BitBlt bm2.hDC, 0, 0, w, bmp.Height, 0, 0, 0, vbDstInvert
 Set bmp = Oh_no(bm1, bm2)
End If
End Sub

Private Sub LoadSound()
Dim i As Long
'hSound(1) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\keyclick.wav", FSOUND_NORMAL, 0, 0) '------Button
'hSound(2) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\clickgo.wav", FSOUND_NORMAL, 0, 0) '-------Dweep1
'hSound(3) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\clickgo2.wav", FSOUND_NORMAL, 0, 0) '------Dweep2
'hSound(4) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\clickgo3.wav", FSOUND_NORMAL, 0, 0) '------Dweep3(Hammer)
'hSound(5) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\mirrorexp.wav", FSOUND_NORMAL, 0, 0) '-----Mirror Explode
'hSound(6) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\freeze.wav", FSOUND_NORMAL, 0, 0) '--------Freeze
'hSound(7) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\hammer.wav", FSOUND_NORMAL, 0, 0) '--------Hammer Sound
'hSound(8) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\dead.wav", FSOUND_NORMAL, 0, 0) '----------Dead
'hSound(9) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\getitem.wav", FSOUND_NORMAL, 0, 0) '-------Get Item
'hSound(10) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\bombfuse.wav", FSOUND_NORMAL, 0, 0) '-----Bomb Fusing
'hSound(11) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\water.wav", FSOUND_NORMAL, 0, 0) '--------Water Bucket
'hSound(12) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\exp.wav", FSOUND_NORMAL, 0, 0) '----------Explode
'hSound(13) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\unfreeze.wav", FSOUND_NORMAL, 0, 0) '-----Unfreeze(Fire)
'hSound(14) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\noclick.wav", FSOUND_NORMAL, 0, 0) '------Disable
'hSound(15) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\passlaser.wav", FSOUND_NORMAL, 0, 0) '----Turn to Dry
'hSound(16) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\fly.wav", FSOUND_NORMAL, 0, 0) '----------Flying
'hSound(17) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\sparkexp.wav", FSOUND_NORMAL, 0, 0) '-----Sparkle Explode
'hSound(18) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\wetdweep.wav", FSOUND_NORMAL, 0, 0) '-----Wet Dweep
'hSound(19) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\wrench.wav", FSOUND_NORMAL, 0, 0) '-------Use Wrench
For i = 1 To 20
 hSound(i) = ld.LoadToFSound
Next i
DrawProgress 33
''mMusic(2).OpenMusic CStr(App.Path) + "\victory.mid"
hMusic(2) = ld.LoadToFMusic
FMUSIC_SetLooping hMusic(2), 0
DrawProgress 34
'mMusic(3).OpenMusic CStr(App.Path) + "\defeat.mid"
hMusic(3) = ld.LoadToFMusic
FMUSIC_SetLooping hMusic(3), 0
DrawProgress 35
'mMusic(4).OpenMusic CStr(App.Path) + "\stage1.mid"
hMusic(4) = ld.LoadToFMusic
FMUSIC_SetLooping hMusic(4), 1
DrawProgress 36
'mMusic(5).OpenMusic CStr(App.Path) + "\stage2.mid"
hMusic(5) = ld.LoadToFMusic
FMUSIC_SetLooping hMusic(5), 1
DrawProgress 37
'mMusic(6).OpenMusic CStr(App.Path) + "\stage3.mid"
hMusic(6) = ld.LoadToFMusic
FMUSIC_SetLooping hMusic(6), 1
DrawProgress 38
'mMusic(7).OpenMusic CStr(App.Path) + "\stage4.mid"
hMusic(7) = ld.LoadToFMusic
FMUSIC_SetLooping hMusic(7), 1
DrawProgress 39
'mMusic(8).OpenMusic CStr(App.Path) + "\stage5.mid"
hMusic(8) = ld.LoadToFMusic
FMUSIC_SetLooping hMusic(8), 1
DrawProgress 40
''mMusic(9).OpenMusic CStr(App.Path) + "\sound\error.wav"
'hSound(20) = FSOUND_Sample_Load(-1, CStr(App.Path) + "\sound\error.wav", FSOUND_NORMAL, 0, 0)
''Debug.Print FSOUND_GetErrorString(FSOUND_GetError)
End Sub

Private Sub CreateB()
Dim bm As New cAlphaDibSection
Dim i As Long
bm.Create 200, 80
bmpWall.PaintPicture bm.hDC, 0, 0, 200, 80
For i = 0 To 4
 bmpWall.PaintPicture bmpWall.hDC, i * 40, 0, 40, 80, 200, 0
Next i
Set bmpWall = CreateAlphaImage(bm, bmpWall)
End Sub

Public Sub MakeMPQ()
Dim s As String
s = CStr(App.Path) + "\Dweep.mpq"
CompressFinal s, CompressTo(s)
End Sub

Private Function CompressTo(ByVal FileName As String) As Long
Dim s As String, i As Long
Dim lp As Long
Dim d() As Byte, m As Long
Dim xa As Integer, xb As Integer, xc As Integer, xd As Integer
Dim bm As New cDIBSection
Open FileName For Binary As #1
Open CStr(App.Path) + "\Dweep.lst" For Input As #2
'///////
lp = 1
Do Until EOF(2)
 Line Input #2, s
 If Left(s, 1) = "*" Then
  Exit Do
 Else
  Debug.Print "Attach " + s
  DoEvents
  Open s For Binary As #3
  m = LOF(3)
  ReDim d(1 To m)
  Get #3, 1, d
  Close 3
  Put #1, lp, m
  Put #1, lp + 4, d
  lp = lp + m + 4
 End If
Loop
Do Until EOF(2)
 Line Input #2, s
 If Left(s, 1) = "*" Then
  Exit Do
 Else
  Debug.Print "Attach " + s
  DoEvents
  Open s For Input As #3
  Input #3, m
  Put #1, lp, m
  lp = lp + 4
  For i = 1 To m
   Input #3, xa, xb, xc, xd
   Put #1, lp, xa
   Put #1, lp + 2, xb
   Put #1, lp + 4, xc
   Put #1, lp + 6, xd
   lp = lp + 8
  Next i
  Close 3
  Line Input #2, s
  bm.CreateFromFile s
  xa = bm.Width
  xb = bm.Height
  Put #1, lp, xa
  Put #1, lp + 2, xb
  m = BitmapToArray(bm, d)
  Put #1, lp + 4, d
  lp = lp + m + 4
 End If
Loop
Do Until EOF(2)
 Line Input #2, s
 If Left(s, 1) = "*" Then
  Exit Do
 Else
  Debug.Print "Attach " + s
  DoEvents
  bm.CreateFromFile s
  xa = bm.Width
  xb = bm.Height
  Put #1, lp, xa
  Put #1, lp + 2, xb
  m = BitmapToArray(bm, d)
  Put #1, lp + 4, d
  lp = lp + m + 4
 End If
Loop
Do Until EOF(2)
 Line Input #2, s
 Debug.Print "Attach " + s
 DoEvents
 Open s For Binary As #3
 m = LOF(3)
 ReDim d(1 To m)
 Get #3, 1, d
 Close 3
 Put #1, lp, m
 Put #1, lp + 4, d
 lp = lp + m + 4
Loop
'///////
Close
CompressTo = lp
End Function

Private Function CompressFinal(ByVal FileName As String, ByVal Size As Long)
Dim d() As Byte, s As String * 3
Dim cp As New cCompress
Debug.Print "Compressing..."
DoEvents
Open FileName For Binary As #1
ReDim d(Size - 1)
Get #1, 1, d
Close
Kill FileName
cp.CompressData d
s = "MPQ"
Open FileName For Binary As #1
Put #1, 1, s
Put #1, 4, Size
Put #1, 8, d
Close
Debug.Print "OK!"
End Function

Private Function BitmapToArray(bm As cDIBSection, d() As Byte) As Long
Dim m As Long, i As Long, j As Long
m = bm.BytesPerScanLine * bm.Height
ReDim d(1 To m)
CopyMemory d(1), ByVal bm.DIBSectionBitsPtr, m
BitmapToArray = m
End Function

Public Sub ApplyOption()
Dim i As Long, s As String
If Not sSound Then CloseAllChannels
If Not sMusic Then CloseAllMusic
For i = 1 To 8
 FMUSIC_SetMasterVolume hMusic(i), sMusic2
Next i
For i = 1 To 19
 FSOUND_Sample_SetDefaults hSound(i), -1, sSound2, -1, -1
Next i
FSOUND_Sample_SetDefaults hSound(20), -1, sMusic2, -1, -1
'///
objText.Clear
s = reg2_GetSettings("Language")
If s <> "?" Then
 If s = vbNullString Then
  objText.LoadFileWithLocale App.Path + "\Locale\MyDweep.*.mo", , True
 Else
  objText.LoadFile App.Path + "\Locale\MyDweep." + s + ".mo", True
 End If
End If
'///
End Sub

Public Sub LetUsCrash()
CopyMemory ByVal ObjPtr(frmMain), ByVal 0, 4
End Sub
