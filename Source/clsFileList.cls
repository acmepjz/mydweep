VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsFileList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private pt As String
Private flt As String
Private fn() As String
Private fc As String

Public Property Get FileName(ByVal Index As Long) As String
FileName = fn(Index)
End Property

Public Property Get FileNameWithPath(ByVal Index As Long) As String
FileNameWithPath = pt + fn(Index)
End Property

Public Property Get Count() As Long
Count = fc
End Property

Public Property Get Filter() As String
Filter = flt
End Property

Public Property Get Path() As String
Path = pt
End Property

Public Property Let Filter(ByVal s As String)
flt = s
Refresh
End Property

Public Property Let Path(ByVal s As String)
pt = s
Select Case Right(pt, 1)
Case "/", "\"
Case Else
 pt = pt + "\"
End Select
Refresh
End Property

Public Function Calc(ByVal p As String, Optional ByVal f As String) As Long
pt = p
Select Case Right(pt, 1)
Case "/", "\"
Case Else
 pt = pt + "\"
End Select
If Not f = "" Then flt = f
Refresh
Calc = fc
End Function

Public Sub Refresh()
On Error Resume Next
Dim i As Long, s As String
Erase fn
'count the number
fc = 0
s = Dir(pt + flt)
Do Until s = ""
 fc = fc + 1
 s = Dir
Loop
'make the list
ReDim fn(1 To fc)
s = Dir(pt + flt)
For i = 1 To fc
 fn(i) = s
 s = Dir
Next i
'sort the list
Dim j As Long, k As Long
For i = 1 To fc - 1
 k = i
 For j = i + 1 To fc
  If StrComp(fn(j), fn(k), vbTextCompare) = -1 Then k = j
 Next j
 If k <> i Then
  s = fn(k)
  fn(k) = fn(i)
  fn(i) = s
 End If
Next i
End Sub

Private Sub Class_Initialize()
flt = "*.*"
pt = CStr(App.Path) + "\"
End Sub

Private Sub Class_Terminate()
Erase fn
End Sub
