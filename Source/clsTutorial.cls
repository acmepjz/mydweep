VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTutorial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private Declare Function GetClientRect Lib "user32" (ByVal hWnd As Long, lpRect As RECT) As Long

Private Type typeTextInfo
 Key As String
 Text As String
 t As Long
 x As Long
 y As Long
 clr As Long
End Type

Private ts() As typeTextInfo
Private tsc As Long

Private Type typeVarInfo
 Name As String
 value As Long
End Type

Private vars() As typeVarInfo
Private varc As Long

Private scr As New clsTable
Private st As Long
Private ldat As typeLevelData

'Private bool As New Class3

Public Sub Clear()
Dim var1 As typeLevelData
Erase ts, vars
tsc = 0
varc = 0
ldat = var1
scr.Clear
End Sub

Public Property Get Style() As Long
Style = st
End Property

Public Property Let Style(ByVal n As Long)
st = n
End Property

Friend Property Get LevelData() As typeLevelData
LevelData = ldat
End Property

Friend Property Let LevelData(dat As typeLevelData)
ldat = dat
End Property

Public Sub LoadFile(ByVal FileName As String)
On Error Resume Next
Dim exd As New clsExData
Dim i As Long, s As String
If st = 0 Then
 LoadCustomLevel FileName, ldat
 exd.LoadFile FileName
 i = exd.FindData("SCRP")
 If i > 0 Then
  s = CStr(App.Path) + "\Temp2910342.tmp"
  exd.SaveDataToFile i, s
  scr.LoadFile s
  Kill s
 End If
Else 'TODO:
End If
End Sub

Public Sub LoadScript(ByVal FileName As String)
scr.LoadFile FileName
End Sub

Public Property Get ScriptText(ByVal Index As Long) As String
ScriptText = scr.StringTable(Index)
End Property

'Public Property Let ScriptText(ByVal Index As Long, ByVal s As String)
'scr.StringTable(Index) = s
'End Property

Public Property Get ScriptCount() As Long
ScriptCount = scr.Count
End Property

Public Property Get Script() As clsTable
Set Script = scr
End Property

Friend Sub RunScript()
On Error Resume Next
Dim i As Long, j As Long, k As Long
Dim s As String, s1 As String
Dim cond As Boolean, b As Boolean
Dim r As RECT
'///////////////there's RuneScape
cond = True
For i = 1 To scr.Count
 s = scr.StringTable(i)
 Select Case Left(s, 1)
 Case ">" '////conditions
  j = InStr(1, s, " ")
  If j = 0 Then s1 = Mid(s, 2) Else s1 = Mid(s, 2, j - 2)
  Select Case s1
  Case "end", "always"
   cond = True
  Case "never"
   cond = False
  Case "gametime"
   j = Val(Mid(s, j))
   b = j = GData.GameTime
   cond = cond And b
  Case "dweepx"
   j = Val(Mid(s, j))
   b = j = GData.DweepX
   cond = cond And b
  Case "dweepy"
   j = Val(Mid(s, j))
   b = j = GData.DweepY
   cond = cond And b
  Case "dweepstate"
   j = Val(Mid(s, j))
   b = j = GData.DweepState
   cond = cond And b
  Case "dweeppos"
   k = InStr(j + 1, s, " ")
   j = Val(Mid(s, j, k - j))
   k = Val(Mid(s, k))
   b = j * 10 - 5 = GData.DweepX
   b = b And (k * 10 - 5 = GData.DweepY)
   cond = cond And b
  Case "dweepposex"
   k = InStr(j + 1, s, " ")
   j = Val(Mid(s, j, k - j))
   k = Val(Mid(s, k))
   b = j = GData.DweepX
   b = b And (k = GData.DweepY)
   cond = cond And b
  Case "haslaser"
   k = InStr(j + 1, s, " ")
   j = Val(Mid(s, j, k - j))
   k = Val(Mid(s, k))
   b = GData.LevelLaser(j, k) > 0
   cond = cond And b
  Case "nolaser"
   k = InStr(j + 1, s, " ")
   j = Val(Mid(s, j, k - j))
   k = Val(Mid(s, k))
   b = GData.LevelLaser(j, k) = 0
   cond = cond And b
  Case "dweepdry"
   cond = cond And IsDry
  Case "dweepwet"
   cond = cond And IsWet
  Case "dweepice", "dweepfrozen"
   cond = cond And IsIce
  Case "dweepfly"
   cond = cond And IsFly
  Case "dweepdie"
   cond = cond And (GData.DweepState = 2049)
  Case "samenum"
   k = InStr(j + 1, s, " ")
   s1 = Trim(Mid(s, j, k - j))
   b = GetVar(s1) = Val(Mid(s, k))
   cond = cond And b
  Case "samevar"
   k = InStr(j + 1, s, " ")
   s1 = Trim(Mid(s, j, k - j))
   b = GetVar(s1) = GetVar(Trim(Mid(s, k)))
   cond = cond And b
  Case "hasitem"
   j = Val(Mid(s, j))
   cond = cond And HasItem(j)
  Case "noitem"
   j = Val(Mid(s, j))
   cond = cond And Not HasItem(j)
  Case "nowitem"
   j = Val(Mid(s, j))
   If GData.ItemNow = 0 Or GData.ItemNow > GData.ItemCount Then
    k = 0
   Else
    k = ChangeItemData(GData.ItemData(GData.ItemNow))
   End If
   b = k = j
   cond = cond And b
  Case "mapdata"
   k = InStr(j + 1, s, " ")
   s1 = Trim(Mid(s, j, k - j))
   b = GData.LevelData(Val(Mid(s1, 1, 2)), Val(Mid(s1, 4))) = Val(Mid(s, k))
   cond = cond And b
  Case Else
   If GData.GameTime = 1 Then MsgBox "未知脚本指令在第" + _
   CStr(i) + "行:" + vbCrLf + vbCrLf + s, vbExclamation
  End Select
 Case "#" '////statments
  If cond Then
   Select Case Mid(s, 2)
   Case "message"
    ShowDialog scr.StringTable(i + 1), ConvertString(scr.StringTable(i + 2))
    i = i + 2
   Case "text"
    AddTextInfo scr.StringTable(i + 2), _
    ConvertString(scr.StringTable(i + 1)), _
    Val(Mid(scr.StringTable(i + 3), 1, 3)), _
    Val(Mid(scr.StringTable(i + 3), 5, 3)), _
    Val(scr.StringTable(i + 4)), _
    QBColor(Val(Mid(scr.StringTable(i + 3), 9)))
    i = i + 4
   Case "attachballoon" 'Haha! XP Balloon
'    GetClientRect frmMain.hWnd, r
'    j = Val(Mid(scr.StringTable(i + 3), 1, 3))
'    k = Val(Mid(scr.StringTable(i + 3), 5, 3))
'    bool.Create CStr(Rnd), r.Left + j, r.TOp + k, 150, 100, , _
'    scr.StringTable(i + 1), ConvertString(scr.StringTable(i + 2)), _
'    xpbtInformation, , vbBlack, 8, , 3000
   Case "deletetext"
    RemoveTextInfo scr.StringTable(i + 1)
    i = i + 1
   Case "setnum"
    SetVar scr.StringTable(i + 1), Val(scr.StringTable(i + 2))
    i = i + 2
   Case "setvar"
    SetVar scr.StringTable(i + 1), GetVar(scr.StringTable(i + 2))
    i = i + 2
   Case "cancelitem"
    GData.ItemNow = 0
   Case "setmapdata"
    GData.LevelData(Val(Mid(scr.StringTable(i + 1), 1, 2)), _
    Val(Mid(scr.StringTable(i + 1), 4, 2))) = _
    Val(Mid(scr.StringTable(i + 1), 7))
    i = i + 1
   Case Else
    MsgBox "未知脚本指令在第" + CStr(i) + _
    "行:" + vbCrLf + vbCrLf + s, vbExclamation
   End Select
  End If
 End Select
Next i
'///////////////show text ...
For i = 1 To tsc
 With ts(i)
  If .t >= 0 Then
   DrawTextB bmpBack.hDC, .Text, frmMain.Font, .x, .y, 400, 400, DT_WORDBREAK, .clr, , True
   If .t = 1 Then
    .t = -1
   ElseIf .t > 1 Then
    .t = .t - 1
   End If
  End If
 End With
Next i
End Sub

Private Sub Class_Terminate()
Erase ts, vars
End Sub

Private Sub AddTextInfo(ByVal Key As String, ByVal Text As String, ByVal x As Long, ByVal y As Long, Optional ByVal ShowTime As Long, Optional ByVal clr As Long)
Dim i As Long
For i = 1 To tsc
 With ts(i)
  If .t = -1 Then
   .Key = Key
   .Text = Text
   .t = ShowTime
   .x = x
   .y = y
   .clr = clr
   Exit Sub
  End If
 End With
Next i
tsc = tsc + 1
ReDim Preserve ts(1 To tsc)
With ts(tsc)
 .Key = Key
 .Text = Text
 .t = ShowTime
 .x = x
 .y = y
 .clr = clr
End With
End Sub

Private Sub RemoveTextInfo(ByVal Key As String)
Dim i As Long
For i = 1 To tsc
 If ts(i).Key = Key Then ts(i).t = -1
Next i
End Sub

Public Sub Refresh()
Erase ts, vars
tsc = 0
varc = 0
End Sub

Public Sub StartGame()
If st = 0 Then
 LData = ldat
 PutGameData
Else 'TODO:
End If
End Sub

Private Sub SetVar(ByVal Name As String, ByVal value As Long)
Dim i As Long
For i = 1 To varc
 If vars(i).Name = Name Then
  vars(i).value = value
  Exit Sub
 End If
Next i
varc = varc + 1
ReDim Preserve vars(1 To varc)
With vars(varc)
 .Name = Name
 .value = value
End With
End Sub

Private Function GetVar(ByVal Name As String) As Long
Dim i As Long
For i = 1 To varc
 If vars(i).Name = Name Then
  GetVar = vars(i).value
  Exit Function
 End If
Next i
End Function

Private Sub ShowDialog(ByVal Title As String, ByVal Text As String)
Dim OldScene As Long, i As Long

OldScene = NowScene

 sItemT = 0
  BitBlt bmpCache.hDC, 0, 0, 640, 480, bmpBack.hDC, 0, 0, vbSrcCopy
  NowScene = 3333
  Do
   BitBlt bmpBack.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, vbSrcCopy
   DrawDialogBox bmpBack.hDC, 100, 120, 440, 240
   DrawTextB bmpBack.hDC, Title, frmMain.Label1.Font, 140, 135, 360, 24, DT_CENTER, &H8000&, , True
   DrawTextB bmpBack.hDC, Text, frmMain.Font, 140, 165, 360, 150, DT_WORDBREAK, vbBlack, , True
   For i = 2 To 2
    If dItemT = i And pressT Then
     BitBlt bmpBack.hDC, 60 + i * 110, 320, 105, 19, bmpButton3.hDC, 0, 19, vbSrcCopy
    Else
     BitBlt bmpBack.hDC, 60 + i * 110, 320, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
    End If
   Next i
   'DrawTextB bmpBack.hdc, "确定", frmMain.Font, 170, 320, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = 1, vbYellow, vbWhite), , True
   DrawTextB bmpBack.hDC, "确定(Enter)", frmMain.Font, 280, 320, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = 2, vbYellow, vbWhite), , True
   'DrawTextB bmpBack.hdc, "确定", frmMain.Font, 390, 320, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = 3, vbYellow, vbWhite), , True
   RedrawMouse
   fPaint bmpBack.hDC
   '///
   DoEvents
   m_objFPS.WaitForNextFrame
   'nowFPS = nowFPS + 1
   Play2
   '///
   If GetAsyncKeyState(vbKeyReturn) = &H8000 Then
    sItemT = 2
   End If
   '///
   Select Case sItemT
   Case 2
    Exit Do
   End Select
  Loop
  sItemT = 0
  NowScene = OldScene
End Sub

Private Function HasItem(ByVal ItemNo As Long) As Boolean
Dim i As Long
For i = 1 To GData.ItemCount
 If ChangeItemData(GData.ItemData(i)) = ItemNo Then
  HasItem = True
  Exit Function
 End If
Next i
End Function
