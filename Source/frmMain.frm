VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7200
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9600
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MouseIcon       =   "frmMain.frx":0CCA
   MousePointer    =   99  'Custom
   ScaleHeight     =   480
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   640
   StartUpPosition =   2  '屏幕中心
   Begin VB.PictureBox picDebug 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1095
      Left            =   960
      MousePointer    =   1  'Arrow
      ScaleHeight     =   73
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   105
      TabIndex        =   3
      Top             =   720
      Visible         =   0   'False
      Width           =   1575
      Begin VB.CommandButton cmdThreshold 
         Caption         =   "threshold"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   0
         TabIndex        =   6
         Top             =   720
         Width           =   1575
      End
      Begin VB.CommandButton cmdOptimize 
         Caption         =   "optimize"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   0
         TabIndex        =   5
         Top             =   360
         Width           =   1575
      End
      Begin VB.CommandButton cmdSaveAs 
         Caption         =   "save as"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   0
         TabIndex        =   4
         Top             =   0
         Width           =   1575
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   120
      Top             =   1560
   End
   Begin VB.TextBox txtLvHint 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   645
      Left            =   5040
      MousePointer    =   3  'I-Beam
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Text            =   "frmMain.frx":0E14
      Top             =   1680
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtLvName 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5040
      MousePointer    =   3  'I-Beam
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   1320
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Timer tmrUndo 
      Interval        =   2000
      Left            =   120
      Top             =   600
   End
   Begin VB.Timer tmrFPS 
      Interval        =   1000
      Left            =   120
      Top             =   120
   End
   Begin VB.Image Image1 
      Height          =   1530
      Index           =   4
      Left            =   2880
      Picture         =   "frmMain.frx":0E1A
      Top             =   2880
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Image Image1 
      Height          =   600
      Index           =   3
      Left            =   3720
      Picture         =   "frmMain.frx":4956
      Top             =   1320
      Visible         =   0   'False
      Width           =   1200
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   2400
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Image Image1 
      Height          =   240
      Index           =   2
      Left            =   3720
      Picture         =   "frmMain.frx":4BC2
      Top             =   2520
      Visible         =   0   'False
      Width           =   2640
   End
   Begin VB.Image Image1 
      Height          =   360
      Index           =   1
      Left            =   3720
      Picture         =   "frmMain.frx":7806
      Top             =   2040
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   1530
      Index           =   0
      Left            =   2880
      Picture         =   "frmMain.frx":8A4A
      Top             =   1320
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function GetTickCount Lib "kernel32" () As Long
Private Const WM_MOUSEWHEEL = &H20A

Private Pressed As Boolean
Private theThreshold As Long

Private LetUsUnload As Boolean

Implements ISubclass

Private Sub cmdOptimize_Click()
Dim i As Long, n As Long, m As Long, m2 As Long
Dim LDataBak As typeLevelData, s As String
On Error GoTo a
'/////////////////////////////////////////////////////////Analyze Data
For i = 1 To GData.GameTime
 If RData(i).Debug_IsActive Then
  m2 = 0
 Else
  m2 = m2 + 1 'ideal time
  If m2 > theThreshold Then m = m + 1
 End If
 If RData(i).PressState > 0 Then
  n = n + 1
  RD2(n).GameTime = i - m
  RD2(n).MouseX = RData(i).MouseX
  RD2(n).MouseY = RData(i).MouseY
  RD2(n).PressState = RData(i).PressState
  m2 = 0
 End If
Next i
RD2C = n
'///////////////////////////////////////////////////////Save Data
s = List1.Key(List1.ListIndex)
s = ShowSave(LoadResString(130), CStr(App.Path) + "\record\", s)
Open s For Binary As #1
Get #1, 270, n
Close
SaveRecord s
Open s For Binary As #1
Put #1, 270, n
Close
MsgBox Replace(objText.GetText("%d optimized"), "%d", CStr(m)), vbInformation '已优化
a:
End Sub

Friend Property Let picDebug_Visible(ByVal b As Boolean)
If b Then
 cmdSaveAs.Caption = objText.GetText("Save level record as") '关卡录像另存为
 cmdOptimize.Caption = objText.GetText("Optimize (beta)") '优化测试
 cmdThreshold.Caption = objText.GetText("Set threshold") '设置阈值
End If
picDebug.Visible = b
End Property

Private Sub cmdSaveAs_Click()
Dim i As Integer, n As Integer
Dim LDataBak As typeLevelData, s As String
On Error GoTo a
'/////////////////////////////////////////////////////////Analyze Data
n = 0
For i = 1 To GData.GameTime
 If RData(i).PressState > 0 Then
  n = n + 1
  RD2(n).GameTime = i
  RD2(n).MouseX = RData(i).MouseX
  RD2(n).MouseY = RData(i).MouseY
  RD2(n).PressState = RData(i).PressState
 End If
Next i
RD2C = n
'///////////////////////////////////////////////////////Save Data
LDataBak = LData
LData.LevelNo = 250 + LData.LevelNo Mod 5
LData.LevelName = "复件 " + LData.LevelName
SaveRecord ShowSave(LoadResString(130), CStr(App.Path) + "\record\")
fileRec.Refresh
a:
LData = LDataBak
End Sub

Private Sub cmdThreshold_Click()
Dim s As String
s = InputBox(vbNullString, vbNullString, CStr(theThreshold))
theThreshold = Val(s)
End Sub

Private Sub Form_Load()
On Error Resume Next
MkDir CStr(App.Path) + "\Record"
MkDir CStr(App.Path) + "\Levels"
PostMessage TestWnd, &H2005&, 2, Me.hWnd
theThreshold = OptimizeThreshold
On Error GoTo a
Debug.Assert 1 \ 0
AttachMessage Me, Me.hWnd, &H2005&
AttachMessage Me, Me.hWnd, WM_MOUSEWHEEL
a:
End Sub

Private Sub Form_Paint()
bmpBack.PaintPicture Me.hDC
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim i As Long, s As String
If IsTest And UnloadMode = 0 Then
 s = objText.GetText("Level pack editor is still running, quitting the game will make the level testing unavailable.") '"关卡包编辑器仍在运行中，关闭游戏程序将使游戏试玩无法进行。"
 s = s + vbCrLf + vbCrLf
 s = s + objText.GetText("Do you really want to quit the application? If you choose 'no', only the game window will be hidden.") '"您真的想退出程序吗？如果您选“否”，则只会隐藏游戏窗口。"
 Select Case MsgBox(s, vbExclamation Or vbYesNoCancel Or vbDefaultButton2)
 Case vbNo
  IsCancel = True
  Cancel = 1
  Exit Sub
 Case vbCancel
  Cancel = 1
  Exit Sub
 End Select
End If
SetKeyValue "Software\MyDweep", "App.Path", lvPath
'//////////////////////////////////////////////Draw Dialog
If sFade Then
 For i = 255 To 5 Step -50
  BitBlt bmpCache.hDC, 0, 0, 640, 480, 0, 0, 0, BLACKNESS
  bmpBack.AlphaPaintPicture bmpCache.hDC, , , , , , , i
  fPaint bmpCache.hDC
  DoEvents
  m_objFPS.WaitForNextFrame
 Next i
Else
 BitBlt Workhdc, 1, 1, 640, 480, bmpBack.hDC, 0, 0, vbNotSrcErase
End If
Set Undo = Nothing
Set tb = Nothing
Set tb2 = Nothing
Set tLev = Nothing
Set tut = Nothing
If IsTest Then PostMessage TestWnd, &H2005&, 128, 0
DetachMessage Me, Me.hWnd, &H2005&
DetachMessage Me, Me.hWnd, WM_MOUSEWHEEL
'///////////////////////////////////////////////End
For i = 1 To 20
 'mSound(i).CloseMusic
 'Set mSound(i) = Nothing
 FSOUND_Sample_Free hSound(i)
Next i
'///////////close music
FMUSIC_StopAllSongs
'For i = 1 To 9
' mMusic(i).CloseMusic
' Set mMusic(i) = Nothing
'Next i
For i = 1 To 8
 FMUSIC_FreeSong hMusic(i)
Next i
FSOUND_Close
ld.Destroy
'///////////////////////////////////////////////
End
End Sub

'Public Sub Title_Wait()
'Pressed = False
'Do Until Pressed
' If mMusic(1).status = "STOPPED" Then Pressed = True
' DoEvents
'Loop
'End Sub

'Private Sub picWork_Click()
''Pressed = True
'End Sub

Private Sub form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
If IsRecord And NowScene = 1025 Then
 RPress = True
Else
 If Button = 2 Then
  pressT = False
  pressR = True
 Else
  pressT = True
  pressR = False
  pressD = True
  If dItemT <> 0 Then sPlay 1
 End If
End If
List1.MouseDown Button, x, y
End Sub

Private Sub form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
If x < 0 Or x >= 640 Or y < 0 Or y >= 480 Then Exit Sub
If IsRecord And NowScene = 1025 Then
 RMouseX = x
 RMouseY = y
Else
 MouseX = x
 MouseY = y
End If
Select Case NowScene
Case 0 'mainmenu
 If x > 200 And x < 440 And y < 390 Then
  If y Mod 50 < 33 Then
   dItemT = y \ 50 - 1
  Else
   dItemT = 0
  End If
 Else
  dItemT = 0
 End If
Case 1, 4 'selectlvpack or record
 If y > 400 And y < 433 Then
  If x > 50 And x < 290 Then
   dItemT = 1
  ElseIf x > 350 And x < 590 Then
   dItemT = 2
  Else
   dItemT = 0
  End If
 Else
  dItemT = 0
 End If
Case 2 'selectlevel
 If y > 400 And y < 425 Then
  If x > 60 And x < 200 Then
   dItemT = 1
  ElseIf x > 250 And x < 390 Then
   dItemT = 2
  ElseIf x > 440 And x < 580 Then
   dItemT = 3
  Else
   dItemT = 0
  End If
 Else
  dItemT = 0
 End If
Case 5 'select custom level
 If y > 400 And y < 425 Then
  If x > 60 And x < 200 Then
   dItemT = 1
  ElseIf x > 440 And x < 580 Then
   dItemT = 3
  Else
   dItemT = 0
  End If
 Else
  dItemT = 0
 End If
Case 128 'Options
 If y > 300 And y < 325 Then
  If x > 170 And x < 310 Then
   dItemT = 1
  ElseIf x > 330 And x < 470 Then
   dItemT = 2
  End If
 ElseIf x > 120 And x < 520 And y > 200 And y < 300 Then
  dItemT = -((y - 180) \ 20)
 Else
  dItemT = 0
 End If
Case 1024 'hint
 If y > 280 And y < 305 Then
  If x > 170 And x < 310 Then
   dItemT = 1
  ElseIf x > 330 And x < 450 Then
   dItemT = 2
  Else
   dItemT = 0
  End If
 Else
  dItemT = 0
 End If
Case 1025 'game
 If x > 520 And x < 625 Then
  If y > 420 And y < 440 Then
   dItemT = -1
  ElseIf y > 450 And y < 470 Then
   dItemT = 1
  Else
   dItemT = 0
  End If
 Else
  dItemT = 0
 End If
Case 1026 'win
 If x > 270 And x < 375 And y > 230 And y < 306 Then
  dItemT = (y - 230) \ 19 + 1
  If (IsRecord Or IsEdit) And dItemT > 2 Then dItemT = 0
 Else
  dItemT = 0
 End If
Case 1027 'lose
 If y > 280 And y < 305 Then
  If x > 170 And x < 310 And Not IsRecord Then
   dItemT = 1
  ElseIf x > 330 And x < 450 Then
   dItemT = 2
  Else
   dItemT = 0
  End If
 ElseIf y > 250 And y < 275 And x > 250 And x < 390 And Not IsRecord Then
  dItemT = 3
 Else
  dItemT = 0
 End If
Case 1028 'menu
 If x > 250 And x < 390 And y > 170 And y < 315 And (y - 10) Mod 40 < 25 Then
  dItemT = (y - 130) \ 40
 Else
  dItemT = 0
 End If
Case 1111 'custom:MsgBox
Case 2005 'help
 If x > 80 And x < 560 And y > 430 And y < 450 Then
  If (x - 80) Mod 125 < 105 Then
   dItemT = (x - 80) \ 125 + 1
  Else
   dItemT = 0
  End If
 Else
  dItemT = 0
 End If
Case 3333 'tools
 If y > 320 And y < 339 Then
  If x > 170 And x < 275 Then
   dItemT = 1
  ElseIf x > 280 And x < 385 Then
   dItemT = 2
  ElseIf x > 390 And x < 495 Then
   dItemT = 3
  Else
   dItemT = 0
  End If
 Else
  dItemT = 0
 End If
Case Else
 dItemT = 1
End Select
List1.MouseMove Button, x, y
End Sub

Private Sub form_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
If dItemT <> 0 Then sItemT = dItemT
If IsRecord And NowScene = 1025 Then
 RPress = False
Else
 pressT = False
 pressR = False
 pressD = False
End If
List1.MouseUp Button, x, y
End Sub

Private Property Let ISubclass_MsgResponse(ByVal RHS As EMsgResponse)
'
End Property

Private Property Get ISubclass_MsgResponse() As EMsgResponse
'
End Property

Private Function ISubclass_WindowProc(ByVal hWnd As Long, ByVal iMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Dim zDelta As Long, lDelta As Long
Select Case iMsg
Case &H2005&
 Select Case wParam
 Case 3
  TestMap
 Case 3333
  LetUsUnload = True
 End Select
Case WM_MOUSEWHEEL
      ' High order word is the distance the wheel has been rotated,
      ' in multiples of WHEEL_DELTA:
      If (wParam And &H8000000) Then
         ' Towards the user:
         zDelta = &H8000& - (wParam And &H7FFF0000) \ &H10000
      Else
         ' Away from the user:
         zDelta = -((wParam And &H7FFF0000) \ &H10000)
      End If
      lDelta = zDelta \ 120
      List1.MouseWheel lDelta
End Select
End Function

Private Sub Timer1_Timer()
Dim n As Integer
Static b As Boolean
n = GetAsyncKeyState(vbKeySnapshot)
If n = &H8001 And Not b Then
 b = True
 SnapShot
ElseIf (n And &H8000) = 0 Then
 b = False
End If
If dbgNoRedraw Then
 If (GetTickCount And &H3&) = 0 Then Form_Paint
End If
If LetUsUnload Then Unload Me
End Sub

Private Sub tmrFPS_Timer()
Me.Caption = "My Dweep - FPS: " + Format(m_objFPS.FPS, "0.0")
End Sub

Private Sub tmrUndo_Timer()
UNew = True
End Sub
