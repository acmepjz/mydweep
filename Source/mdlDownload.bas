Attribute VB_Name = "mdlDownload"
Option Explicit

Public Type SECURITY_ATTRIBUTES
       nLength  As Long
       lpSecurityDescriptor  As Long
       bInheritHandle  As Boolean
End Type
'1)createFile 用于打开文件,该函数VB的声明如下:
Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, lpSecurityAttributes As SECURITY_ATTRIBUTES, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
'2)CloseHandle 用于关闭被打开文件的句柄,该函数VB的声明如下:
Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
'3)ReadFile 用于从被打开文件中读取数据,该函数VB的声明如下:
Declare Function ReadFile Lib "kernel32" (ByVal hFile As Long, lpBuffer As Byte, ByVal dwNumberOfBytesToRead As Long, lpNumberOfBytesRead As Long, ByVal lpOverlapped As Long) As Long

'4)WriteFile 用于把读取出的数据写入文件,该函数VB的声明如下:用来写字符串
Declare Function WriteFile Lib "kernel32" (ByVal hFile As Long, ByVal lpBuffer As String, ByVal dwNumberOfBytesToWrite As Long, lpNumberOfBytesWritten As Long, ByVal lpOverlapped As Long) As Long
'4)WriteFile 用于把读取出的数据写入文件,该函数VB的声明如下:用来写byte数据
Declare Function WriteFileB Lib "kernel32" Alias "WriteFile" (ByVal hFile As Long, lpBuffer As Byte, ByVal dwNumberOfBytesToWrite As Long, lpNumberOfBytesWritten As Long, ByVal lpOverlapped As Long) As Long

'5)SetFilePoniter移动文件指针,该函数VB的声明如下:
Declare Function SetFilePointer Lib "kernel32" (ByVal hFile As Long, ByVal lDistanceToMove As Long, ByVal lpDistanceToMoveHigh As Long, ByVal dwMoveMethod As Long) As Long
Public Declare Function SetEndOfFile Lib "kernel32" (ByVal hFile As Long) As Long

'6)下面是以上函数的参数声明
Public Const GENERIC_READ As Long = &H80000000
Public Const GENERIC_WRITE As Long = &H40000000
Public Const FILE_SHARE_READ As Long = 1
Public Const FILE_SHARE_WRITE As Long = 2
Public Const create_NEW As Long = 1
Public Const CREATE_ALWAYS As Long = 2
Public Const OPEN_EXISTING As Long = 3
Public Const OPEN_ALWAYS As Long = 4
Public Const TRUNCATE_EXISTING As Long = 5
Public Const INVALID_HANDLE_value As Long = -1
Public Const FILE_ATTRIBUTE_NORMAL As Long = &H80
Public Const FILE_END = 2


Public Const INTERNET_OPEN_TYPE_PRECONFIG = 0
Public Const INTERNET_OPEN_TYPE_DIRECT = 1
Public Const INTERNET_OPEN_TYPE_PROXY = 3
Public Const scUserAgent = "VB OpenUrl"
Public Const INTERNET_FLAG_RELOAD = &H80000000
Public Const HTTP_QUERY_CONTENT_LENGTH = 5
Public Const HTTP_QUERY_FLAG_NUMBER = &H20000000
Public Const HTTP_QUERY_STATUS_CODE = 19

Public Declare Function GetLastError Lib "kernel32" () As Long

Public Declare Function InternetOpen Lib "wininet.dll" _
    Alias "InternetOpenA" (ByVal sAgent As String, _
    ByVal lAccessType As Long, ByVal sProxyName As String, _
    ByVal sProxyBypass As String, ByVal lFlags As Long) As Long

Public Declare Function InternetOpenUrl Lib "wininet.dll" _
   Alias "InternetOpenUrlA" (ByVal hOpen As Long, _
   ByVal sUrl As String, ByVal sHeaders As String, _
   ByVal lLength As Long, ByVal lFlags As Long, _
   ByVal lContext As Long) As Long
'
'BOOLAPI HttpQueryInfo ( IN HINTERNET hRequest, IN DWORD dwInfoLevel,
'IN OUT LPVOID lpBuffer OPTIONAL, IN OUT LPDWORD lpdwBufferLength,
'IN OUT LPDWORD lpdwIndex OPTIONAL )

'Public Declare Function HttpQueryInfo Lib "wininet.dll" Alias "HttpQueryInfoA" _
'    (ByVal hRequest As Long, ByVal InfoLevel As Long, _
'     lpBuffer As String, lpdwBufferLength As Long, _
'    ByVal lpdwIndex As Long) As Boolean

Public Declare Function HttpQueryInfo Lib "wininet.dll" Alias "HttpQueryInfoA" _
(ByVal hHttpRequest As Long, ByVal lInfoLevel As Long, ByVal sBuffer As String, _
ByRef lBufferLength As Long, ByRef lIndex As Long) As Integer

Public Declare Function InternetReadFileB Lib "wininet.dll" Alias "InternetReadFile" _
   (ByVal hFile As Long, sBuffer As Byte, _
   ByVal lNumBytesToRead As Long, lNumberOfBytesRead As Long) _
   As Integer
   
Public Declare Function InternetReadFile Lib "wininet.dll" _
   (ByVal hFile As Long, ByVal sBuffer As String, _
   ByVal lNumBytesToRead As Long, lNumberOfBytesRead As Long) _
   As Integer
   
Public Declare Function InternetCloseHandle Lib "wininet.dll" _
        (ByVal hInet As Long) As Integer
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Private sz As Long, dsz As Long

'得到html源代码
Public Function DownloadHTML(ByVal sUrl As String) As String
Dim s As String
Dim hOpen As Long
Dim hOpenUrl As Long
Dim bDoLoop As Boolean
Dim bRet As Boolean
Dim sReadBuffer As String * 2048
Dim lNumberOfBytesRead As Long
On Error GoTo errout
    hOpen = InternetOpen(scUserAgent, INTERNET_OPEN_TYPE_PRECONFIG, _
    vbNullString, vbNullString, 0)
    'htmlTask(lp).hOpen = hOpen '记录hopen
    hOpenUrl = InternetOpenUrl(hOpen, sUrl, vbNullString, 0, _
    INTERNET_FLAG_RELOAD, 0)
    'htmlTask(lp).hOpenUrl = hOpenUrl '记录hopenUrl
Dim sFileSize As String * 32
Dim lFileSizeBytes As Long
Dim lIndex As Long
lFileSizeBytes = Len(sFileSize)
Dim r As Long
    sFileSize = vbNullString
    r = HttpQueryInfo(hOpenUrl, HTTP_QUERY_CONTENT_LENGTH, ByVal sFileSize, lFileSizeBytes, 0)
    '////////////add
    sz = Val(sFileSize)
    dsz = 0
    '///////////end
    bDoLoop = True
    Do While bDoLoop
        sReadBuffer = vbNullString
        bRet = InternetReadFile(hOpenUrl, sReadBuffer, Len(sReadBuffer), lNumberOfBytesRead)
        If Not CBool(lNumberOfBytesRead) Then
            bDoLoop = False
        Else
            s = s & Left$(sReadBuffer, lNumberOfBytesRead)
            dsz = dsz + lNumberOfBytesRead
        End If
        DoEvents
        Sleep 10
    Loop
    If hOpenUrl <> 0 Then InternetCloseHandle (hOpenUrl)
    'htmlTask(lp).hOpenUrl = 0
    If hOpen <> 0 Then InternetCloseHandle (hOpen)
    'htmlTask(lp).hOpen = 0
    DownloadHTML = s
    Exit Function
errout:
Debug.Print Err.Number, Err.Description
End Function

'将网络上的文件保存到磁盘
Public Function DownloadFile(ByVal sUrl As String, ByVal sFileName As String) As Boolean
Dim s() As Byte
Dim hOpen As Long
Dim hOpenUrl As Long
Dim bDoLoop As Boolean
Dim bRet As Long
Dim sReadBuffer(4095) As Byte
Dim lNumberOfBytesRead As Long
On Error GoTo errout
    '打开
    hOpen = InternetOpen(scUserAgent, INTERNET_OPEN_TYPE_PRECONFIG, _
    vbNullString, vbNullString, 0)
    'swfTask(lp).hOpen = hOpen
    hOpenUrl = InternetOpenUrl(hOpen, sUrl, vbNullString, 0, _
    INTERNET_FLAG_RELOAD, 0)
    'swfTask(lp).hOpenUrl = hOpenUrl
    'writeDebug "hOpenUrl=" & hOpenUrl & "  ===" & lp
Dim sFileSize As String * 32
Dim lFileSizeBytes As Long
Dim lIndex As Long
lFileSizeBytes = Len(sFileSize)
Dim r As Long
Dim lSize As Long
    sFileSize = vbNullString
    '这里获得文件的大小
    r = HttpQueryInfo(hOpenUrl, HTTP_QUERY_CONTENT_LENGTH, ByVal sFileSize, lFileSizeBytes, 0)
    '///////////add
    sz = Val((sFileSize))
    dsz = 0
    '////////////////end
    'writeDebug "httpqueryinfo" & "  ===" & lp
    'swfTask(lp).fileSize = lSize
    'writeDebug "fileSize=" & lSize & "  ===" & lp
    bDoLoop = True
    '准备createfile
Dim MyLP   As SECURITY_ATTRIBUTES, hFile As Long
Dim lNumWriten As Long
    MyLP.nLength = Len(MyLP)
    MyLP.lpSecurityDescriptor = 0
    MyLP.bInheritHandle = False
    hFile = CreateFile(sFileName, GENERIC_WRITE, FILE_SHARE_READ, MyLP, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, ByVal 0&)
    'swfTask(lp).fileHandle = hFile
    'readfile并且保存到磁盘
Dim i As Long
    Do While bDoLoop
        bRet = InternetReadFileB(hOpenUrl, sReadBuffer(0), 4096, lNumberOfBytesRead)
        Debug.Assert bRet
        '/////////////add
        If lNumberOfBytesRead > 0 Then WriteFileB hFile, sReadBuffer(0), lNumberOfBytesRead, lNumWriten, 0&
        dsz = dsz + lNumberOfBytesRead
        If Not CBool(lNumberOfBytesRead) Then bDoLoop = False '??? speed=0? error TODO:timeout
        If dsz >= sz Then bDoLoop = False
        '/////////////end
        DoEvents
        Sleep 10
    Loop
    'writeDebug "完成了读取和存储  ===" & lp
    CloseHandle hFile
    'swfTask(lp).fileHandle = 0
    If hOpenUrl <> 0 Then InternetCloseHandle (hOpenUrl)
    'swfTask(lp).hOpenUrl = 0
    If hOpen <> 0 Then InternetCloseHandle (hOpen)
    'swfTask(lp).hOpen = 0
    DownloadFile = True
    Exit Function
errout:
    DownloadFile = False
End Function

Public Property Get DownloadFileSize() As Long
DownloadFileSize = sz
End Property

Public Property Get DownloadedSize() As Long
DownloadedSize = dsz
End Property

