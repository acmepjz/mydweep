Attribute VB_Name = "mdlGame"
Option Explicit

Public Type typeGameData
 GameTime As Long
 LevelNo As Integer
 LevelData(1 To 16, 1 To 10) As Integer
 LevelState(1 To 16, 1 To 10) As Integer
 LevelLaser(1 To 16, 1 To 10) As Integer
 LevelExplode(1 To 16, 1 To 10) As Integer
 DweepX As Integer
 DweepY As Integer
 DweepState As Integer
'0=Normal
'1=Wet
'2=Flying
'3=Ice
'+8=Moving
'+32=Happy
'+64=Sad
'+96=Close the eye
'+1024-1036(c=12)=To fly (1036=ce)
'+2048-2058(c=11)=Die (2058=ce)
'+4096-4105(c=10)=Unfreeze
 ItemData(1 To 10) As Integer
 ItemCount As Integer
 ItemNow As Integer
End Type

Public GData As typeGameData
'Public UData(1 To 64) As typeGameData,UStart As Integer, UNow As Integer
Public UNew As Boolean
Public Undo As New clsUndo

Public nowMouse As Integer
'0=none
'1=left
'2=right

Public Type typeLevelRecordItem
 MouseX As Integer
 MouseY As Integer
 PressState As Byte
 Debug_IsActive As Byte
End Type

Public Type typeFileRecordItem
 GameTime As Long
 MouseX As Integer
 MouseY As Integer
 PressState As Integer
End Type

Public RCount As Integer
Public RData(1 To 32767) As typeLevelRecordItem
Public PData As typePathInfo
Public PDataNow As Integer

Public RD2(1 To 1024) As typeFileRecordItem
Public RD2C As Integer
Public RD2N As Integer
Public RD_I As Integer
Public IsRecord As Boolean
Public RMouseX As Integer
Public RMouseY As Integer

Public HitX As Integer, HitY As Integer
Public NextX As Integer, NextY As Integer, IsGo As Boolean
Public IsStop As Boolean
Public IsInt As Boolean
Public IsMoment As Boolean
Public IsDry As Boolean
Public IsWet As Boolean
Public IsIce As Boolean
Public IsFly As Boolean
Public IsActive As Byte

Private tipTrans As Long, tipX As Long

Public ReCalcPath As Boolean

Public itemFlag As Integer
'0-disable
'1-block
'2-dweep
Public itemX As Integer, itemY As Integer, itemLast As Boolean
Public tLev As clsTutorial

Private hSol As Long, solNow As Long, solBtnNo As Long

Public Function RecErrHandler() As Boolean
Dim s As String
s = "程序在播放关卡录像“" + LData.LevelName + "”时发生错误:"
s = s + vbCr + vbCr + "Dweep死亡在游戏时间第" + CStr(GData.GameTime) + "帧" + vbCr
s = s + vbCr + "Dweep位置:(" + CStr(GData.DweepX) + "," + CStr(GData.DweepY) + ")"
s = s + vbTab + "Dweep状态:" + CStr(GData.DweepState) + vbCr + vbCr
s = s + "当前录像节点编号:" + CStr(RD2N) + vbTab + "当前录像鼠标位置:("
s = s + CStr(RMouseX) + "," + CStr(RMouseY) + ")" + vbCr
s = s + "节点游戏时间:第" + CStr(RD2(RD2N).GameTime) + "帧" + vbTab + "节点鼠标位置:("
s = s + CStr(RD2(RD2N).MouseX) + "," + CStr(RD2(RD2N).MouseY) + ")" + vbCr
s = s + "节点状态:" + CStr(RD2(RD2N).PressState) + vbCr + vbCr
s = s + "请确认关卡文件没有错误，并且本程序是最新版本。"
Select Case MsgBox(s, vbCritical + vbAbortRetryIgnore)
Case vbAbort
 Unload frmMain
Case vbRetry
 RecErrHandler = True
End Select
End Function

Public Sub GameLoop()
Dim i As Integer, j As Integer
Dim xx As Integer, yy As Integer
Dim RStop As Boolean
Dim isU As Boolean
Dim s As String
NextLevel:
If Not (IsRecord Or IsCLv Or IsTest Or IsT) Then
 If List1.ListIndex = -1 Then
  MsgBox "请选择一个关卡。", vbCritical
  Exit Sub
 End If
 If LoadLevelData(lvFile, List1.ListIndex + 1, LData) Then
  MsgBox "打开关卡时出错或非法关卡。", vbCritical
  Exit Sub
 End If
End If
RePlay:
'////////////////////////////////////////////////////////////Open the music
Play (LData.LevelNo - 1) Mod 5 + 4
'//////////////////////////////////////////////////////////////Move Data
PutGameData
transD = -1
Erase RData
UNew = False
'//////////////////////////////////////////////////////////////Show Hint
sItemT = 0
NowScene = 1024
DrawLevel bmpCache.hDC, GData
If IsEdit Or IsT Then
 frmMain.txtLvName.Text = LData.LevelName
ElseIf IsCLv Or LevelPackFile = Chr(128) Then
 frmMain.txtLvName.Text = "自定义关卡：" + LData.LevelName
Else
 frmMain.txtLvName.Text = "第" + CStr(LData.LevelNo) + "关-" + LData.LevelName
End If
frmMain.txtLvHint.Text = LData.LevelHint
If IsEdit Then
 frmMain.txtLvName.Move 170, 170, 300 'zxzz
 frmMain.txtLvHint.Move 170, 190, 300, 80 'zxzz
 frmMain.txtLvName.Visible = True 'zxzz
 frmMain.txtLvHint.Visible = True 'zzzz
End If
'ShowCursor 1
Do
 BitBlt bmpBack.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, vbSrcCopy
 DrawDialogBox bmpBack.hDC, 150, 150, 340, 180
 If Not IsEdit Then
  If sHint Then
   DrawTextB bmpBack.hDC, frmMain.txtLvName.Text, frmMain.txtLvName.Font, _
   170, 170, 300, 20, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, &H8000&, , True
   DrawTextB bmpBack.hDC, "提示：" + LData.LevelHint, frmMain.Font, _
   170, 190, 300, 80, DT_WORDBREAK, , , True
  Else
   DrawTextB bmpBack.hDC, frmMain.txtLvName.Text, frmMain.Label1.Font, _
   170, 170, 300, 32, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, &H8000&, , True
   DrawTextB bmpBack.hDC, "(已设置为不显示关卡提示)", frmMain.Font, _
   170, 200, 300, 16, , vbRed, , True
  End If
 End If
 If dItemT = 1 Then
  If pressT Then
   BitBlt bmpBack.hDC, 170, 280, 140, 25, bmpButton2.hDC, 0, 25, vbSrcCopy
  Else
   BitBlt bmpBack.hDC, 170, 280, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
  End If
  DrawTextB bmpBack.hDC, "退出(Esc)", frmMain.Font, 170, 280, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
 Else
  BitBlt bmpBack.hDC, 170, 280, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
  DrawTextB bmpBack.hDC, "退出(Esc)", frmMain.Font, 170, 280, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
 End If
 If dItemT = 2 Then
  If pressT Then
   BitBlt bmpBack.hDC, 330, 280, 140, 25, bmpButton2.hDC, 0, 25, vbSrcCopy
  Else
   BitBlt bmpBack.hDC, 330, 280, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
  End If
  DrawTextB bmpBack.hDC, "确定(Enter)", frmMain.Font, 330, 280, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
 Else
  BitBlt bmpBack.hDC, 330, 280, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
  DrawTextB bmpBack.hDC, "确定(Enter)", frmMain.Font, 330, 280, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
 End If
 RedrawMouse
 fPaint bmpBack.hDC
 'nowFPS = nowFPS + 1
 DoEvents
 m_objFPS.WaitForNextFrame
 If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
  sItemT = 1
 ElseIf GetAsyncKeyState(vbKeyReturn) = &H8000 Then
  sItemT = 1234
 End If
Loop Until sItemT > 0
If IsEdit Then
 LData.LevelName = frmMain.txtLvName.Text
 LData.LevelHint = frmMain.txtLvHint.Text
 frmMain.txtLvName.Visible = False
 frmMain.txtLvHint.Visible = False
End If
If sItemT = 1 Then
 IsHOU123 = False
 GoTo ExitGame
End If
Undo.Destroy
If (IsT Or IsCLv) And Not tLev Is Nothing Then tLev.Refresh
UndoTheGame:
NowScene = 1025
sItemT = 0
'ShowCursor 0
tipTrans = 0
RD2N = 1: RStop = False
solNow = 1
pDrawSolution
Do
 '/////////////////////////////////////////////////////////////Add game time
 GData.GameTime = GData.GameTime + 1
 IsActive = IIf(IsStop Or IsIce Or IsFly, 0, 1)
 '///////////////////////////////////////////////Set Record Data
 If IsRecord And Not RStop Then
  If GData.GameTime = RD2(RD2N).GameTime Then
   MouseX = RD2(RD2N).MouseX
   MouseY = RD2(RD2N).MouseY
   pressT = RD2(RD2N).PressState Mod 10 = 1
   pressR = RD2(RD2N).PressState Mod 10 = 2
   RD_I = RD2(RD2N).PressState \ 10
   If RD2N = RD2C Then RStop = True
   RD2N = RD2N + 1
  Else
   pressT = False
   pressR = False
   RD_I = 0
   If RD2N = 1 Then
    MouseX = 320 + Int((RD2(1).MouseX - 320) * (GData.GameTime / RD2(1).GameTime))
    MouseY = 240 + Int((RD2(1).MouseY - 240) * (GData.GameTime / RD2(1).GameTime))
   Else
    MouseX = RD2(RD2N - 1).MouseX + Int((RD2(RD2N).MouseX - _
    RD2(RD2N - 1).MouseX) * ((GData.GameTime - RD2(RD2N - 1).GameTime) _
    / (RD2(RD2N).GameTime - RD2(RD2N - 1).GameTime)))
    MouseY = RD2(RD2N - 1).MouseY + Int((RD2(RD2N).MouseY - _
    RD2(RD2N - 1).MouseY) * ((GData.GameTime - RD2(RD2N - 1).GameTime) _
    / (RD2(RD2N).GameTime - RD2(RD2N - 1).GameTime)))
   End If
  End If
 End If
 '///////////////////////////////////////////////Calc boolean
 IsInt = (GData.DweepX Mod 10 = 5) And (GData.DweepY Mod 10 = 5)
 IsStop = (GData.DweepState < 1000) And (GData.DweepState Mod 16 = 0 Or _
 GData.DweepState Mod 16 = 1)
 '//////////////////////////////////////////////Call A*
 If IsInt And (IsGo Or (ReCalcPath And Not IsStop)) And (IsWet Or IsDry) Then
  IsGo = False
  For i = 1 To 16
   For j = 1 To 10
    Select Case GData.LevelData(i, j)
    Case 1, 6 To 16
     PData.IsHedge(i, j) = 1
    Case Else
     PData.IsHedge(i, j) = 0
    End Select
   Next j
  Next i
  PData.StartPoint.x = 1 + GData.DweepX \ 10
  PData.StartPoint.y = 1 + GData.DweepY \ 10
  PData.EndPoint.x = HitX
  PData.EndPoint.y = HitY
  If AStar(PData) Then
   If GData.DweepState < 8 Then GData.DweepState = GData.DweepState + 8
   PDataNow = 0
  End If
  ReCalcPath = False
 End If
 '//////////////////////////////////////////////Move Dweep
 IsStop = (GData.DweepState < 1000) And (GData.DweepState Mod 16 = 0 Or _
 GData.DweepState Mod 16 = 1)
 If Not IsStop And (IsWet Or IsDry) Then
  If IsInt Then
   PDataNow = PDataNow + 1
  End If
  If PDataNow > PData.PathLength Then
   If GData.DweepState < 1000 Then
    If GData.DweepState >= 8 Then GData.DweepState = GData.DweepState - 8
   End If
  Else
   xx = PData.PathDat(PDataNow).x * 10 - 5
   yy = PData.PathDat(PDataNow).y * 10 - 5
   GData.DweepX = GData.DweepX + Sgn(xx - GData.DweepX)
   GData.DweepY = GData.DweepY + Sgn(yy - GData.DweepY)
   IsActive = 1 'moving!
   Debug.Assert Not IsFly
  End If
 End If
 '//////////////////////////////////////////////Choose item
 If Not IsRecord Then RD_I = 0
 For i = 1 To 10
  If IsRecord Then
   If i = RD_I Then GData.ItemNow = i
  Else
   j = i Mod 10 + 48
   If GetAsyncKeyState(j) < 0 Then
    RD_I = i
    GData.ItemNow = i
   End If
  End If
 Next i
 If GData.ItemNow > 0 Then
  If GData.ItemNow > GData.ItemCount Or GData.ItemData(GData.ItemNow) = 0 Then
   GData.ItemNow = 0
  End If
 End If
 If MouseX < 500 And MouseX Mod 50 > 10 And MouseY > 420 And MouseY < 460 And pressT Then
  GData.ItemNow = MouseX \ 50 + 1
  If GData.ItemNow > GData.ItemCount Or GData.ItemData(GData.ItemNow) = 0 Then
   GData.ItemNow = 0
  End If
 End If
 If pressR Then GData.ItemNow = 0: transD = -1
 '//////////////////////////////////////////////Calc Data
 IsInt = (GData.DweepX Mod 10 = 5) And (GData.DweepY Mod 10 = 5)
 IsStop = (GData.DweepState < 1000) And (GData.DweepState Mod 16 = 0 Or _
 GData.DweepState Mod 16 = 1)
 IsMoment = Not IsStop And GData.DweepState < 1000 And IsInt
 CalcGameData
 '////////////////////////////////////////////////Save Record(Play only) but available on record debug
 If (Not IsRecord Or DebugReg) And sItemT = 0 Then
  RData(GData.GameTime).MouseX = MouseX
  RData(GData.GameTime).MouseY = MouseY
  RData(GData.GameTime).PressState = RD_I * 10 + IIf(pressT, 1, IIf(pressR, 2, 0))
  RData(GData.GameTime).Debug_IsActive = IsActive 'new!!
 End If
 '//////////////////////////////////////////////Lose?
 If GData.DweepState = 2058 Then Exit Do
 '//////////////////////////////////////////////Win
 If IsStop And (IsWet Or IsDry) Then
  If GData.LevelData(GData.DweepX \ 10 + 1, GData.DweepY \ 10 + 1) = 4 Then
   GoTo WinTheGame
  End If
 End If
 '//////////////////////////////////////////////Draw Level
 ShowLevel
 'nowFPS = nowFPS + 1
 DoEvents
 m_objFPS.WaitForNextFrame
 '/////////////////////////////////////////////Play Music
 Play2
 '/////////////////////////////////////////////Shortcut Key
 If GetAsyncKeyState(vbKeyR) = &H8000 Then GoTo RePlay
 If GetAsyncKeyState(vbKeyQ) = &H8000 Or IsCancel Then
  IsCancel = False
  GoTo ExitGame
 End If
 '/////////////////////////////////////////////Undo
 If isU Then
  If GetAsyncKeyState(vbKeyU) = 0 Then isU = False
 End If
 If sItemT = -1 Or (GetAsyncKeyState(vbKeyU) = &H8000 And Not isU And Not IsRecord) _
 Or (GetAsyncKeyState(vbKeyP) = &H8000 And IsRecord) Then
  If IsRecord Then
   '//////////////////////////////////////////Pause
   sItemT = 0
   isPause = True
   Do
    DrawLevel bmpBack.hDC, GData
    DrawMouseEx bmpBack.hDC, RMouseX, RMouseY, 0, False
    fPaint bmpBack.hDC
    DoEvents
    m_objFPS.WaitForNextFrame
    'nowFPS = nowFPS + 1
   Loop Until sItemT = -1 Or GetAsyncKeyState(vbKeyC) = &H8000
   sItemT = 0
   isPause = False
  Else
   If Undo.IsExist Then
    'UNow = UNow - 1
    'If UNow = 0 Then UNow = 64
    Undo.Pop GData
    sItemT = 0
    PDataNow = 1
    PData.PathLength = 0
    PData.PathDat(1).x = GData.DweepX \ 10 + 1
    PData.PathDat(1).y = GData.DweepY \ 10 + 1
    PData.PathDat(2).x = GData.DweepX \ 10 + 1
    PData.PathDat(2).y = GData.DweepY \ 10 + 1
   End If
  End If
 End If
 '/////////////////////////////////////////////Save Undo
 If UNew And IsInt And Not IsRecord Then
  Undo.Push GData
  UNew = False
 End If
 '/////////////////////////////////////////////Menu
 If sItemT = 1 Or GetAsyncKeyState(vbKeyEscape) = &H8000 Then
  sItemT = 0
  NowScene = 1028
  DrawLevel bmpCache.hDC, GData
  Do
   BitBlt bmpBack.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, vbSrcCopy
   DrawDialogBox bmpBack.hDC, 220, 150, 200, 180
   For i = 1 To 4
    If dItemT = i Then
     If pressT Then
      BitBlt bmpBack.hDC, 250, 40 * i + 130, 140, 25, bmpButton2.hDC, 0, 25, vbSrcCopy
     Else
      BitBlt bmpBack.hDC, 250, 40 * i + 130, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
     End If
     DrawTextB bmpBack.hDC, TheMid(2, 9, i), frmMain.Font, 250, 40 * i + 130, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
    Else
     BitBlt bmpBack.hDC, 250, 40 * i + 130, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
     DrawTextB bmpBack.hDC, TheMid(2, 9, i), frmMain.Font, 250, 40 * i + 130, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
    End If
   Next i
   RedrawMouse
   fPaint bmpBack.hDC
   'nowFPS = nowFPS + 1
   DoEvents
   m_objFPS.WaitForNextFrame
   If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
    sItemT = 4
   ElseIf GetAsyncKeyState(vbKeyQ) = &H8000 Then
    sItemT = 2
   ElseIf GetAsyncKeyState(vbKeyO) = &H8000 Then
    sItemT = 3
   ElseIf GetAsyncKeyState(vbKeyR) = &H8000 Then
    sItemT = 1
   End If
  Loop Until sItemT > 0
  Select Case sItemT
  Case 1
   GoTo RePlay
  Case 2
   GoTo ExitGame
  Case 3
   ShowOptionsLoop
  End Select
  sItemT = 0
  NowScene = 1025
 End If
Loop
'///////////////////////////////////////////End Main Loop
'If sItemT > 0 Then GoTo ExitGame
'/////////////////////////////////////Lose game
CloseAllChannels
If IsRecord Then
 If RecErrHandler Then GoTo RePlay
End If
If IsRecord Then
 FMUSIC_StopAllSongs
 sPlay 20
Else
 Play 3
End If
sItemT = 0
NowScene = 1027
DrawLevel bmpCache.hDC, GData
xx = Int(10 * Rnd)
LoseTheGame:
Do
 BitBlt bmpBack.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, vbSrcCopy
 DrawDialogBox bmpBack.hDC, 150, 150, 340, 180
 If IsRecord Then
  DrawTextB bmpBack.hDC, "出错了！！！！！", frmMain.Label1.Font, 200, 170, 235, 24, DT_CENTER, vbRed, , True
  DrawTextB bmpBack.hDC, "关卡录像文件存在错误", frmMain.Font, 180, 200, 225, 16, , vbRed, , True
 Else
  DrawTextB bmpBack.hDC, "唉，可怜的Dweep！", frmMain.Label1.Font, 200, 170, 235, 24, DT_CENTER, &H8000&, , True
  DrawTextB bmpBack.hDC, LoadResString(101 + xx), frmMain.Font, 180, 200, 225, 16, , vbBlack, , True
 End If
 If Not IsRecord Then
  If dItemT = 1 Then
   If pressT Then
    BitBlt bmpBack.hDC, 170, 280, 140, 25, bmpButton2.hDC, 0, 25, vbSrcCopy
   Else
    BitBlt bmpBack.hDC, 170, 280, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
   End If
   DrawTextB bmpBack.hDC, "重玩(&R)", frmMain.Font, 170, 280, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
   Else
   BitBlt bmpBack.hDC, 170, 280, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
   DrawTextB bmpBack.hDC, "重玩(&R)", frmMain.Font, 170, 280, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
  End If
 End If
 If dItemT = 2 Then
  If pressT Then
   BitBlt bmpBack.hDC, 330, 280, 140, 25, bmpButton2.hDC, 0, 25, vbSrcCopy
  Else
   BitBlt bmpBack.hDC, 330, 280, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
  End If
  DrawTextB bmpBack.hDC, "退出(Esc)", frmMain.Font, 330, 280, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
 Else
  BitBlt bmpBack.hDC, 330, 280, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
  DrawTextB bmpBack.hDC, "退出(Esc)", frmMain.Font, 330, 280, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
 End If
 If Not IsRecord Then
  If dItemT = 3 Then
   If pressT Then
    BitBlt bmpBack.hDC, 250, 250, 140, 25, bmpButton2.hDC, 0, 25, vbSrcCopy
   Else
    BitBlt bmpBack.hDC, 250, 250, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
   End If
   DrawTextB bmpBack.hDC, "撤销(&U)", frmMain.Font, 250, 250, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
  Else
   BitBlt bmpBack.hDC, 250, 250, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
   DrawTextB bmpBack.hDC, "撤销(&U)", frmMain.Font, 250, 250, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
  End If
 End If
 RedrawMouse
 fPaint bmpBack.hDC
 'nowFPS = nowFPS + 1
 DoEvents
 m_objFPS.WaitForNextFrame
 If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
  sItemT = 2
 ElseIf GetAsyncKeyState(vbKeyReturn) = &H8000 Then
  sItemT = 1
 ElseIf GetAsyncKeyState(vbKeyR) = &H8000 Then
  sItemT = 1
 ElseIf GetAsyncKeyState(vbKeyU) = &H8000 Then
  sItemT = 3
 End If
Loop Until sItemT > 0
Select Case sItemT
Case 1
 GoTo RePlay
Case 2
 GoTo ExitGame
Case 3
 If IsRecord Then
  MsgBox "没有这个功能。"
  GoTo LoseTheGame
 Else
  If Undo.IsExist Then
'   UNow = UNow - 1
'   If UNow = 0 Then UNow = 64
'   GData = UData(UNow)
   sItemT = 0
   PData.PathLength = 0
   Play (LData.LevelNo - 1) Mod 5 + 4
   Undo.Pop GData
   GoTo UndoTheGame
  Else
   GoTo LoseTheGame
  End If
 End If
End Select
'/////////////////////////////////////Win game
WinTheGame:
CloseAllChannels
Play 2
NowScene = 1026
DrawLevel bmpCache.hDC, GData
xx = Int(7 * Rnd)
If Not (IsRecord Or IsEdit Or IsT Or IsTest) Then
 If IsCLv Then
  SetLevelSolved LevelPackFile, True
 Else
  lvPass = Left(lvPass, LData.LevelNo - 1) + "1" + Right(lvPass, Len(lvPass) - LData.LevelNo)
  If SetKeyValue("Software\Dexterity Software\Dweep", lpName, lvPass) = False Then
   MsgBox "打开注册表失败。", vbCritical
  End If
 End If
 RCount = GData.GameTime
 If sPath And Not RecordExist Then '///////////////////////TODO:Autosave?
  DrawDialogBox bmpBack.hDC, 240, 210, 160, 60
  DrawTextB bmpBack.hDC, "正在保存游戏录像……", frmMain.Font, 260, 235, 120, 11, DT_CENTER, , , True
  fPaint bmpBack.hDC
  '///////////////////////////////////////////////////////////////Save Record
  SaveRecordLoop
 End If
End If
WinTheGame2:
sItemT = 0
If IsRecord And DebugReg Then frmMain.picDebug_Visible = True
Do
 BitBlt bmpBack.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, vbSrcCopy
 DrawDialogBox bmpBack.hDC, 150, 150, 340, 180
 DrawTextB bmpBack.hDC, "祝贺你啦！！", frmMain.Label1.Font, 255, 170, 130, 24, DT_CENTER, &H8000&, , True
 DrawTextB bmpBack.hDC, LoadResString(111 + xx), frmMain.Font, 180, 200, 225, 16, , , , True
 For i = 1 To 4
  If i = 3 And (IsRecord Or IsEdit Or IsTest) Then Exit For
  If dItemT = i And pressT Then
   BitBlt bmpBack.hDC, 270, 230 + (i - 1) * 19, 105, 19, bmpButton3.hDC, 0, 19, vbSrcCopy
  Else
   BitBlt bmpBack.hDC, 270, 230 + (i - 1) * 19, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
  End If
  If dItemT = i Then
   DrawTextB bmpBack.hDC, TheMid(IIf(IsRecord, 6, 5), 9, i), frmMain.Font, 270, 230 + (i - 1) * 19, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
  Else
   DrawTextB bmpBack.hDC, TheMid(IIf(IsRecord, 6, 5), 9, i), frmMain.Font, 270, 230 + (i - 1) * 19, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
  End If
 Next i
 RedrawMouse
 fPaint bmpBack.hDC
 'nowFPS = nowFPS + 1
 DoEvents
 m_objFPS.WaitForNextFrame
 If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
  sItemT = 1
 ElseIf GetAsyncKeyState(vbKeyReturn) = &H8000 Then
  sItemT = 4
 ElseIf GetAsyncKeyState(vbKeyR) = &H8000 Then
  sItemT = 2
 ElseIf GetAsyncKeyState(vbKeyS) = &H8000 Then
  sItemT = 3
 End If
Loop Until sItemT > 0
If IsRecord And DebugReg Then frmMain.picDebug_Visible = False
Select Case sItemT
Case 1
 IsHOU123 = False
 GoTo ExitGame
Case 2
 GoTo RePlay
Case 3
 If IsRecord Or IsEdit Or IsT Or IsTest Then
  MsgBox "没有这个功能。"
 Else
  DrawDialogBox bmpBack.hDC, 240, 210, 160, 60
  DrawTextB bmpBack.hDC, "正在保存游戏录像……", frmMain.Font, 260, 235, 120, 11, DT_CENTER, , , True
  fPaint bmpBack.hDC
  '///////////////////////////////////////////////////////////////Save Record
  SaveRecordLoop
 End If
 GoTo WinTheGame2
Case 4
 If IsRecord Or IsEdit Or IsCLv Or IsTest Then
  MsgBox "没有这个功能。"
  GoTo WinTheGame2
 ElseIf IsT Then
  IsHOU123 = True
  GoTo ExitGame
 Else
  i = GetNextNotSolvedLevel(List1.ListIndex + 1) '?
  If i = -1 Then
   GoTo ExitGame
  Else
   'frmMain.lstLevel.ListItems(i).Selected = True
   List1.ListIndex = i - 1
   GoTo NextLevel
  End If
 End If
End Select
'//////////////////////////////////////////////Exit game, play menu music
ExitGame:
Undo.Destroy
CloseAllMusic
CloseAllChannels
If IsTest Then
 frmMain.Hide
 PostMessage TestWnd, &H2005&, 1, 0
End If
If Not ((IsT And IsHOU123) Or IsTest) Then Play 1
sItemT = 0
'ShowCursor 1
End Sub

Public Sub A_Laser(dat As typeGameData)
Dim i As Integer, j As Integer
Dim nx As Integer, ny As Integer, nFs As Integer
'Dim xn As Integer, yn As Integer
Dim endFlag As Boolean
Erase dat.LevelLaser
For i = 1 To 16
 For j = 1 To 10
  nFs = dat.LevelData(i, j)
  Select Case nFs
  Case 6, 7, 8, 9
   nx = i: ny = j
   nFs = nFs - 5
   endFlag = False
   Do
    '//////////////////////Set Next Block
    Select Case nFs
    Case 1
     If ny > 1 Then ny = ny - 1 Else endFlag = True
    Case 2
     If nx < 16 Then nx = nx + 1 Else endFlag = True
    Case 3
     If ny < 10 Then ny = ny + 1 Else endFlag = True
    Case 4
     If nx > 1 Then nx = nx - 1 Else endFlag = True
    End Select
    If endFlag Then Exit Do
    '//////////////////////Set Next Direction and Add Data
    Select Case dat.LevelData(nx, ny)
    Case 1, 5, 6, 7, 8, 9, 12, 13, 14, 15 '////////End
     dat.LevelLaser(nx, ny) = 10
     endFlag = True
    Case Else
    If dat.LevelData(nx, ny) = 10 And dat.LevelState(nx, ny) = 0 Then
'    Case 10 '//////////////////////////////////////Mirror NE-WS
     If nFs Mod 2 = 0 Then nFs = nFs - 1 Else nFs = nFs + 1
     Select Case dat.LevelLaser(nx, ny)
     Case 0
      If nFs = 2 Or nFs = 3 Then dat.LevelLaser(nx, ny) = 4 Else dat.LevelLaser(nx, ny) = 5
     Case 4
      If nFs = 1 Or nFs = 4 Then dat.LevelLaser(nx, ny) = 6
     Case 5
      If nFs = 2 Or nFs = 3 Then dat.LevelLaser(nx, ny) = 6
     End Select
    ElseIf dat.LevelData(nx, ny) = 11 And dat.LevelState(nx, ny) = 0 Then
'    Case 11 '//////////////////////////////////////Mirror NW-ES
     nFs = 5 - nFs
     Select Case dat.LevelLaser(nx, ny)
     Case 0
      If nFs > 2 Then dat.LevelLaser(nx, ny) = 7 Else dat.LevelLaser(nx, ny) = 8
     Case 7
      If nFs < 3 Then dat.LevelLaser(nx, ny) = 9
     Case 8
      If nFs > 2 Then dat.LevelLaser(nx, ny) = 9
     End Select
    Else
'    Case 0, 2, 3, 4, 16 To 32 '////////////////////Through
     Select Case dat.LevelLaser(nx, ny)
     Case 0
      dat.LevelLaser(nx, ny) = 2 - nFs Mod 2
     Case 1
      If nFs Mod 2 = 0 Then dat.LevelLaser(nx, ny) = 3
     Case 2
      If nFs Mod 2 = 1 Then dat.LevelLaser(nx, ny) = 3
     End Select
    End If
    '///////////////////////////////////////////////
    End Select
   Loop Until endFlag
  End Select
 Next j
Next i
End Sub

Public Sub ShowLevel()
On Error Resume Next
Dim mx As Integer, my As Integer, s As String
Dim i As Integer, var1 As Integer, j As Integer
DrawLevel bmpBack.hDC, GData
If sTool Then
 If MouseX < 500 And MouseX Mod 50 > 10 And MouseY > 420 And MouseY < 460 And sTool Then
  i = MouseX \ 50 + 1
  If i > GData.ItemCount Then
   j = 0
  Else
   j = 1
   var1 = ChangeItemData(GData.ItemData(i)) Mod 17
   s = LoadResString(140 + var1)
   tipX = i * 50 - 40
   DrawDialogBox bmpCache.hDC, 0, 0, 128, 60, True
   DrawTextB bmpCache.hDC, s, frmMain.Font, 4, 0, 120, 16, DT_VCENTER Or DT_SINGLELINE, vbBlack, , True
   DrawTextB bmpCache.hDC, "快捷键:" + CStr(i Mod 10), frmMain.Font, 4, 0, 120, 16, DT_RIGHT Or DT_VCENTER Or DT_SINGLELINE, vbBlack, , True
   s = LoadResString(160 + var1)
   DrawTextB bmpCache.hDC, s, frmMain.Font, 4, 16, 120, 48, DT_WORDBREAK, &H8000&, , True
  End If
 End If
 If j = 0 Then
  If tipTrans > 0 Then
   bmpCache.AlphaPaintPicture bmpBack.hDC, tipX, 375, 128, 60, 0, 0, tipTrans, False
   tipTrans = tipTrans - 51
  End If
 Else
  bmpCache.AlphaPaintPicture bmpBack.hDC, tipX, 375, 128, 60, 0, 0, tipTrans, False
  If tipTrans < 255 Then tipTrans = tipTrans + 51
 End If
End If
'/////////////////////////
If MouseY >= 400 Then
 transD = -1
 RedrawMouse
Else
 mx = 1 + MouseX \ 40
 my = 1 + MouseY \ 40
 If GData.ItemNow > 0 Or GData.ItemNow <= GData.ItemCount Then var1 = ChangeItemData(GData.ItemData(GData.ItemNow))
 If var1 Mod 17 = 0 Or itemLast Then
  If itemLast Then
   itemLast = False
  Else
   If IsDry Or IsWet Then
    Select Case GData.LevelData(mx, my)
    Case 1, 6 To 16
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 1, True
    Case Else
     If pressT Then
      HitX = mx
      HitY = my
      NextX = mx
      NextY = my
      focusX = mx
      focusY = my
      focusN = 1
      IsGo = True
      pressT = False
     End If
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 1, False
    End Select
   Else
    DrawMouseEx bmpBack.hDC, MouseX, MouseY, 1, True
   End If
  End If
 Else
  transD = -1
  Select Case var1
  Case 1 To 11 '////////////////////////////////////Lasers,Mirrors,Fans
   i = GData.DweepX \ 10 + 1
   j = GData.DweepY \ 10 + 1
   If GData.LevelData(mx, my) = 0 And GData.LevelExplode(mx, my) = 0 _
   And Not (mx = PData.PathDat(PDataNow).x And my = PData.PathDat(PDataNow).y And Not IsStop) _
   And Not (i = mx And j = my And IsStop) Then
    RedrawMouse
    transX = mx
    transY = my
    transD = var1 + 5
    If pressT Then
     sPlay 1
     GData.LevelData(mx, my) = transD
     DeleteItem
     transD = -1
     ReCalcPath = True
    End If
   Else
    'Debug.Assert Not pressT
    DrawMouseEx bmpBack.hDC, MouseX, MouseY, 0, True
    transD = -1
   End If
  Case 12 '///////////////////////////////////////////////////CCW
   Select Case GData.LevelData(mx, my)
   Case 6 To 15
    DrawMouseEx bmpBack.hDC, MouseX, MouseY, 2, False
    If pressT Then
     sPlay 19
     Select Case GData.LevelData(mx, my)
     Case 6: var1 = 9
     Case 7: var1 = 6
     Case 8: var1 = 7
     Case 9: var1 = 8
     Case 10: var1 = 11
     Case 11: var1 = 10
     Case 12: var1 = 15
     Case 13: var1 = 12
     Case 14: var1 = 13
     Case 15: var1 = 14
     End Select
     GData.LevelData(mx, my) = var1
     DeleteItem
    End If
   Case Else
    DrawMouseEx bmpBack.hDC, MouseX, MouseY, 2, True
   End Select
  Case 13 '////////////////////////////////////////////////////CW
   Select Case GData.LevelData(mx, my)
   Case 6 To 15
    DrawMouseEx bmpBack.hDC, MouseX, MouseY, 3, False
    If pressT Then
     sPlay 19
     Select Case GData.LevelData(mx, my)
     Case 6: var1 = 7
     Case 7: var1 = 8
     Case 8: var1 = 9
     Case 9: var1 = 6
     Case 10: var1 = 11
     Case 11: var1 = 10
     Case 12: var1 = 13
     Case 13: var1 = 14
     Case 14: var1 = 15
     Case 15: var1 = 12
     End Select
     GData.LevelData(mx, my) = var1
     DeleteItem
    End If
   Case Else
    DrawMouseEx bmpBack.hDC, MouseX, MouseY, 3, True
   End Select
  Case 14 '/////////////////////////////////////////////////////////Water
   If DweepHitTest(MouseX, MouseY) Then
    If IsDry Then
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 4, False
     If pressT Then
      sPlay 11
      GData.DweepState = GData.DweepState + 1
      DeleteItem
     End If
    ElseIf IsFly Or (GData.DweepState > 1000 And GData.DweepState < 2000) Then
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 4, False
     If pressT Then
      IsInt = (GData.DweepX Mod 10 = 5) And (GData.DweepY Mod 10 = 5)
      i = GData.DweepX \ 10 + 1
      j = GData.DweepY \ 10 + 1
      If PData.PathDat(2).x = 0 Or PData.PathDat(2).y = 0 Then
       var1 = GData.LevelData(i, j)
      Else
       var1 = GData.LevelData(PData.PathDat(2).x, PData.PathDat(2).y)
      End If
      Select Case var1
      Case 0, 2, 3, 4, 17 To 32
       sPlay 11
       If IsInt Then
        If GData.LevelData(i, j) = 3 Then GData.DweepState = 3 Else GData.DweepState = 1
       Else
        GData.DweepState = 9
        PDataNow = 2
       End If
       DeleteItem
      End Select
     End If
    Else
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 4, True
    End If
   Else
    If GData.LevelData(mx, my) = 16 Then
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 4, False
     If pressT Then
      sPlay 11
      GData.LevelState(mx, my) = 0
      DeleteItem
     End If
    Else
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 4, True
    End If
   End If
  Case 15 '//////////////////////////////////////////////////////////Hammer
   If DweepHitTest(MouseX, MouseY) Then
    If IsIce Then
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 5, False
     If pressT Then
      sPlay 7
      GData.DweepState = 1
      DeleteItem
     End If
    ElseIf IsFly Or (GData.DweepState > 1000 And GData.DweepState < 2000) Then
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 5, False
     If pressT Then
      IsInt = (GData.DweepX Mod 10 = 5) And (GData.DweepY Mod 10 = 5)
      i = GData.DweepX \ 10 + 1
      j = GData.DweepY \ 10 + 1
      If PData.PathDat(2).x = 0 Or PData.PathDat(2).y = 0 Then
       var1 = GData.LevelData(i, j)
      Else
       var1 = GData.LevelData(PData.PathDat(2).x, PData.PathDat(2).y)
      End If
      Select Case var1
      Case 0, 2, 3, 4, 17 To 32
       sPlay 7
       sPlay 4
       If IsInt Then
        If GData.LevelData(i, j) = 3 Then GData.DweepState = 3 Else GData.DweepState = 0
       Else
        GData.DweepState = 8
        PDataNow = 2
       End If
       DeleteItem
      End Select
     End If
    Else
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 5, True
    End If
   Else
    Select Case GData.LevelData(mx, my)
    Case 6 To 9, 12 To 15
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 5, False
     If pressT Then
      sPlay 7
      GData.LevelState(mx, my) = 10
      DeleteItem
     End If
    Case 10, 11
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 5, False
     If pressT Then
      sPlay 7
      GData.LevelState(mx, my) = 1
      DeleteItem
     End If
    Case 16
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 5, False
     If pressT Then
      sPlay 7
      GData.LevelState(mx, my) = 27
      DeleteItem
     End If
    Case Else
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 5, True
    End Select
   End If
  Case 16 '/////////////////////////////////////////////////////////Fire
   If DweepHitTest(MouseX, MouseY) Then
    DrawMouseEx bmpBack.hDC, MouseX, MouseY, 6, False
    If IsIce Then
     If pressT Then
      sPlay 13
      GData.DweepState = 4096
      DeleteItem
     End If
    ElseIf IsWet Then
     If pressT Then
      sPlay 15
      GData.DweepState = 0
      DeleteItem
     End If
    Else
     If pressT Then
      GData.DweepState = 2048
      DeleteItem
     End If
    End If
   Else
    If GData.LevelData(mx, my) = 16 And GData.LevelState(mx, my) = 0 Then
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 6, False
     If pressT Then
      GData.LevelState(mx, my) = 1
      DeleteItem
     End If
    Else
     DrawMouseEx bmpBack.hDC, MouseX, MouseY, 6, True
    End If
   End If
  End Select
 End If
End If
'For i = 1 To PData.PathLength
' mx = PData.PathDat(i).x * 40 - 40
' my = PData.PathDat(i).y * 40 - 40
' DrawLaser bmpBack.hdc, mx, my, 1, 0, 0, True
'Next i
'////////////////////////////////////////Record Mouse
If IsRecord Then
 If RMouseY < 400 Then
  DrawMouseEx bmpBack.hDC, RMouseX, RMouseY, 0, True
 Else
  DrawMouseEx bmpBack.hDC, RMouseX, RMouseY, 0, False
 End If
End If
'////////////////////debug info
If DebugReg And IsRecord And RD2N > 1 Then
 bmpCache.Cls
 s = "调试信息" + vbCr + "Last Node=" + CStr(RD2N - 1) _
 + vbTab + "(Time=" + CStr(RD2(RD2N - 1).GameTime) _
 + "(0x" + Hex(RD2(RD2N - 1).GameTime) + ")" _
 + vbTab + "X=" + CStr(RD2(RD2N - 1).MouseX) _
 + vbTab + "Y=" + CStr(RD2(RD2N - 1).MouseY) + ")" + _
 vbCr + "Now=" + CStr(GData.GameTime) _
 + vbTab + "X=" + CStr(MouseX) + vbTab + "Y=" + CStr(MouseY) + _
 vbCr + "DweepX=" + CStr(GData.DweepX) + _
 vbTab + "DweepY=" + CStr(GData.DweepY)
 DrawTextB bmpCache.hDC, s, frmMain.Font, 0, 0, 640, 96, DT_WORDBREAK Or DT_EXPANDTABS, vbYellow, , True
 If IsActive = 0 Then DrawTextB bmpCache.hDC, "[非活动状态]", frmMain.Label1.Font, 0, 0, 640, 96, DT_VCENTER Or DT_RIGHT Or DT_SINGLELINE, vbYellow, , True
 'Debug.Assert IsActive
 bmpCache.AlphaPaintPicture bmpBack.hDC, , , 640, 96, , , 128, False
End If
'////////////////////////////////////Show Tips!!!
If sol.Loaded Then
 If LCase(Right(LevelPackFile, 9)) = "dweep.dwp" And LData.LevelNo < 76 Then
  If GetAsyncKeyState(vbKeyUp) = &H8001 And solNow > 1 Then
   solNow = solNow - 1
   pDrawSolution
  End If
  If GetAsyncKeyState(vbKeyDown) = &H8001 And solNow < sol.StepsCount(LData.LevelNo) Then
   solNow = solNow + 1
   pDrawSolution
  End If
  If GetAsyncKeyState(vbKeySpace) = &H8001 Then
   sol.IsChinese = Not sol.IsChinese
   pDrawSolution
  End If
  bmpBmp.AlphaPaintPicture bmpBack.hDC, , , 640, 16 + hSol, , , 128, False
 End If
End If
'///////////////////////////////////////End
If (IsT Or IsCLv) And Not tLev Is Nothing Then tLev.RunScript
fPaint bmpBack.hDC
End Sub

Private Sub pDrawSolution()
If sol.Loaded Then
 If LCase(Right(LevelPackFile, 9)) = "dweep.dwp" And LData.LevelNo < 76 Then
  bmpBmp.Cls
  DrawTextB bmpBmp.hDC, sol.LevelName(LData.LevelNo), frmMain.txtLvName.Font, 0, 0, 610, 16, , vbYellow, , True
  DrawTextB bmpBmp.hDC, CStr(solNow) + "/" + CStr(sol.StepsCount(LData.LevelNo)), frmMain.txtLvName.Font, 0, 0, 620, 16, DT_RIGHT Or DT_SINGLELINE, vbYellow, , True
  DrawTextB bmpBmp.hDC, IIf(sol.IsChinese, "CHS", "ENG"), frmMain.txtLvName.Font, 0, 0, 640, 16, DT_RIGHT Or DT_SINGLELINE, vbRed, , True
  'calc rect
  DrawTextB bmpBmp.hDC, sol.Steps(LData.LevelNo, solNow), frmMain.Font, 0, 16, 640, hSol, DT_WORDBREAK Or DT_CALCRECT, vbYellow, , True
  'draw
  DrawTextB bmpBmp.hDC, sol.Steps(LData.LevelNo, solNow), frmMain.Font, 0, 16, 640, hSol, DT_WORDBREAK, vbYellow, , True
  'TODO:Buttons etc
 End If
End If
End Sub

Public Sub DeleteItem()
Dim i As Integer
For i = GData.ItemNow To 9
 GData.ItemData(i) = GData.ItemData(i + 1)
Next i
GData.ItemCount = GData.ItemCount - 1
GData.ItemNow = 0
itemLast = True
'///////////////xx Chaos
pressT = False
pressR = False
pressD = False
End Sub

Public Sub CalcGameData()
Dim i As Integer, j As Integer
Dim p As Integer, q As Integer
Dim windX As Integer, windY As Integer
Dim windX2 As Integer, windY2 As Integer
A_Laser GData
For i = 1 To 16
 For j = 1 To 10
  '/////////////////////////////////Bombs and Explodes
  'Laser to the Bomb
  If GData.LevelData(i, j) = 16 And GData.LevelLaser(i, j) > 0 And GData.LevelState(i, j) = 0 Then
   GData.LevelState(i, j) = 1
   IsActive = 1 'level is moving
  End If
  'Exploding
  If GData.LevelExplode(i, j) > 0 Then
   IsActive = 1 'level is moving
   If GData.LevelExplode(i, j) = 1 Then sPlay 12
   GData.LevelExplode(i, j) = GData.LevelExplode(i, j) + 1
   'Dweep?
   If Abs(GData.DweepX - i * 10 + 5) + Abs(GData.DweepY - j * 10 + 5) < 10 Then
    IsFly = (GData.DweepState < 1000 And GData.DweepState Mod 8 = 2)
    If Not IsFly And GData.DweepState < 1000 Then GData.DweepState = 2048
   End If
   If GData.LevelExplode(i, j) = 8 Then
    'Explode over, destroy other bomb
    Select Case GData.LevelData(i, j)
    Case 16
     GData.LevelState(i, j) = 27 'Bomb fuse max
    End Select
    GData.LevelExplode(i, j) = 0
   End If
  End If
  'Bomb fusing...  (Bomb fuse=27)
  If GData.LevelData(i, j) = 16 And GData.LevelState(i, j) > 0 Then
   IsActive = 1 'level is moving
   sPlay 10, True
   GData.LevelState(i, j) = GData.LevelState(i, j) + 1
   If GData.LevelState(i, j) = 28 Then
    'Bomb exploding...
    GData.LevelState(i, j) = 0
    GData.LevelData(i, j) = 0
    For p = i - 1 To i + 1
     For q = j - 1 To j + 1
      If p >= 1 And p <= 16 And q >= 1 And q <= 10 Then
       GData.LevelExplode(p, q) = 1
      End If
     Next q
    Next p
   End If
  End If
  '///////////////////////////////////Exploding Lasers,Fans,Mirrors
  Select Case GData.LevelData(i, j)
  Case 6 To 9, 12 To 15 'Lasers,Fans
   If GData.LevelState(i, j) > 0 Or GData.LevelLaser(i, j) > 0 Or GData.LevelExplode(i, j) > 0 Then
    IsActive = 1 'level is moving
    sPlay 17, True
    GData.LevelState(i, j) = GData.LevelState(i, j) + 1
    'Sparkle over,exploding
    If GData.LevelState(i, j) = 12 Then
     GData.LevelData(i, j) = 0
     GData.LevelExplode(i, j) = 1
     GData.LevelState(i, j) = 0
    End If
   End If
  Case 10, 11 'Mirrors
   If GData.LevelState(i, j) > 0 Or GData.LevelExplode(i, j) > 0 Then
    IsActive = 1 'level is moving
    If GData.LevelState(i, j) = 1 Then sPlay 5
    GData.LevelState(i, j) = GData.LevelState(i, j) + 1
    'break over
    If GData.LevelState(i, j) = 20 Then
     GData.LevelData(i, j) = 0
     GData.LevelState(i, j) = 0
    End If
   End If
  End Select
  '/////////////////////////////////And else...
 Next j
Next i
'////////////////////////////////////////////////////Dweep State
'Calc Dweep Data
IsDry = (GData.DweepState < 1000 And GData.DweepState Mod 8 = 0)
IsWet = (GData.DweepState < 1000 And GData.DweepState Mod 8 = 1)
IsIce = (GData.DweepState < 1000 And GData.DweepState Mod 8 = 3)
IsFly = (GData.DweepState < 1000 And GData.DweepState Mod 8 = 2)
i = GData.DweepX \ 10 + 1
j = GData.DweepY \ 10 + 1
'//////////////////////////////Activity State
If IsMoment And GData.LevelData(i, j) = 3 And (IsDry Or IsWet) Then
 'Ice plate
 sPlay 6
 KillPathData
 GData.DweepState = 3
 IsIce = True: IsDry = False
 IsMoment = False
End If
If IsMoment And GData.LevelData(i, j) = 2 And IsWet Then
 'Heat plate and wet
 If GData.LevelLaser(i, j) > 0 Then
  GData.DweepState = 2048
 Else
  sPlay 15
  GData.DweepState = GData.DweepState - 1
  IsMoment = False
 End If
End If
If IsInt And Not IsFly And GData.LevelLaser(i, j) > 0 Then
 'Laser
 If IsDry And sAuto And Not IsRecord Then UseWater1 'auto use water
 If IsDry Or IsStop Then
  If GData.DweepState < 2048 Then GData.DweepState = 2048
 ElseIf IsIce Then
  sPlay 13
  GData.DweepState = 4096
 ElseIf IsWet Then
  sPlay 15
  GData.DweepState = GData.DweepState - 1
 End If
End If
If IsMoment And GData.LevelData(i, j) = 2 And IsDry Then
 'Heat plate and dry
 If GData.LevelLaser(i, j) > 0 Then
  GData.DweepState = 2048
 Else
  sPlay 16
  KillPathData
  GData.DweepState = 1024
  IsMoment = False
 End If
End If
If IsFly Then '//////////////////////////////////////////////Wind
 IsInt = (GData.DweepX Mod 10 = 5) And (GData.DweepY Mod 10 = 5)
 If IsInt Then
  i = GData.DweepX \ 10 + 1
  j = GData.DweepY \ 10 + 1
  If i > 1 Then
   If GData.LevelData(i - 1, j) = 13 And windX < 1 Then windX = windX + 1
  End If
  If i > 2 Then
   If GData.LevelData(i - 2, j) = 13 And windX < 1 Then windX = windX + 1
  End If
  If i < 16 Then
   If GData.LevelData(i + 1, j) = 15 And windX2 > -1 Then windX2 = windX2 - 1
  End If
  If i < 15 Then
   If GData.LevelData(i + 2, j) = 15 And windX2 > -1 Then windX2 = windX2 - 1
  End If
  If j > 1 Then
   If GData.LevelData(i, j - 1) = 14 And windY < 1 Then windY = windY + 1
  End If
  If j > 2 Then
   If GData.LevelData(i, j - 2) = 14 And windY < 1 Then windY = windY + 1
  End If
  If j < 10 Then
   If GData.LevelData(i, j + 1) = 12 And windY2 > -1 Then windY2 = windY2 - 1
  End If
  If j < 9 Then
   If GData.LevelData(i, j + 2) = 12 And windY2 > -1 Then windY2 = windY2 - 1
  End If
  windX = windX + windX2
  windY = windY + windY2
  PData.PathDat(1).x = i
  PData.PathDat(1).y = j
  i = i + windX
  j = j + windY
  If i < 1 Then i = 1
  If i > 16 Then i = 16
  If j < 1 Then j = 1
  If j > 10 Then j = 10
  PData.PathDat(2).x = i
  PData.PathDat(2).y = j
  PData.PathLength = 2
  
Dim xx As Long, yy As Long

   xx = PData.PathDat(2).x * 10 - 5
   yy = PData.PathDat(2).y * 10 - 5
   GData.DweepX = GData.DweepX + Sgn(xx - GData.DweepX)
   GData.DweepY = GData.DweepY + Sgn(yy - GData.DweepY)
 Else
' IsStop = (GData.DweepState < 1000) And (GData.DweepState Mod 16 = 0 Or _
' GData.DweepState Mod 16 = 1)
' If Not IsStop And (IsWet Or IsDry) Then
'  If IsInt Then
'   PDataNow = PDataNow + 1
'  End If
'  If PDataNow > PData.PathLength Then
'   If GData.DweepState < 1000 Then
'    If GData.DweepState >= 8 Then GData.DweepState = GData.DweepState - 8
'   End If
'  Else
   xx = PData.PathDat(2).x * 10 - 5
   yy = PData.PathDat(2).y * 10 - 5
   GData.DweepX = GData.DweepX + Sgn(xx - GData.DweepX)
   GData.DweepY = GData.DweepY + Sgn(yy - GData.DweepY)
  '//////moving test
  If xx <> GData.DweepX Or yy <> GData.DweepY Then IsActive = 1
  '//////
 End If
End If
'If (IsWet Or IsDry) Then
' 'TODO:Bomb explode
'End If
IsMoment = False
'////////////////////Static state
Select Case GData.DweepState
Case 1000 To 1034
 GData.DweepState = GData.DweepState + 1
 If GData.DweepState = 1035 Then GData.DweepState = 2
Case 2000 To 2057
 If GData.DweepState = 2048 Then sPlay 8
 GData.DweepState = GData.DweepState + 1
Case 4000 To 5000
 GData.DweepState = GData.DweepState + 1
 If GData.DweepState = 4106 Then
  If GData.LevelLaser(i, j) > 0 Then
   GData.DweepState = 2048
  Else
   GData.DweepState = 1
  End If
 End If
End Select
'Reset State
IsDry = (GData.DweepState < 1000 And GData.DweepState Mod 8 = 0)
IsWet = (GData.DweepState < 1000 And GData.DweepState Mod 8 = 1)
IsIce = (GData.DweepState < 1000 And GData.DweepState Mod 8 = 3)
IsFly = (GData.DweepState < 1000 And GData.DweepState Mod 8 = 2)
'/////////////////////////////////////////////////////////////Collect Item
If IsInt And (IsDry Or IsWet) And GData.ItemCount < 10 Then
 Select Case GData.LevelData(i, j)
 Case 17 To 32
  sPlay 9
  GData.ItemCount = GData.ItemCount + 1
  GData.ItemData(GData.ItemCount) = GData.LevelData(i, j) + 84
  GData.LevelData(i, j) = 0
 End Select
End If
'////////////////////////////////////////////////////////////Play Wet Sound
If IsWet And GData.GameTime Mod 10 = 0 Then sPlay 18
End Sub

Public Function GetDweepState()
'''
End Function

Public Sub UseWater1()
Dim i As Long, j As Long
For i = 1 To GData.ItemCount
 If ChangeItemData(GData.ItemData(i)) = 14 Then  'water
  IsDry = False
  IsWet = True
  j = GData.ItemNow
  If j >= i Then j = IIf(j = i, 0, j - 1)
  GData.ItemNow = i
  DeleteItem
  GData.DweepState = GData.DweepState + 1
  'TODO: xx Record
  RData(GData.GameTime - 1).MouseX = GData.DweepX * 4
  RData(GData.GameTime - 1).MouseY = GData.DweepY * 4 - 10
  RData(GData.GameTime - 1).PressState = i * 10 + 1
  Exit Sub
 End If
Next i
End Sub

Private Sub KillPathData()
Erase PData.PathDat
PData.PathLength = 0
End Sub
