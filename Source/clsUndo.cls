VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsUndo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Head As clsUndoNode

Friend Sub Push(dat As typeGameData)
Dim NewNode As New clsUndoNode
NewNode.dat = dat
If Head Is Nothing Then
 Set Head = NewNode
Else
 Set NewNode.NextNode = Head
 Set Head = NewNode
End If
End Sub

Friend Sub Pop(dat As typeGameData)
If Not Head Is Nothing Then
 dat = Head.dat
 Set Head = Head.NextNode
End If
End Sub

Public Function IsExist() As Boolean
IsExist = Not Head Is Nothing
End Function

Public Sub Destroy()
Set Head = Nothing
End Sub

Private Sub Class_Terminate()
Destroy
End Sub
