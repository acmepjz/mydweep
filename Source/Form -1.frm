VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dweep版本更新程序"
   ClientHeight    =   4095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5505
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Form -1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   273
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   367
   StartUpPosition =   2  'CenterScreen
   Begin DwpUpdate.SimpleProgress pb 
      Height          =   375
      Left            =   1680
      TabIndex        =   7
      Top             =   1200
      Visible         =   0   'False
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   661
      Caption         =   ""
   End
   Begin VB.PictureBox px1 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   120
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   105
      TabIndex        =   2
      Top             =   120
      Width           =   1575
      Begin VB.Label lblTitle 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   4575
      End
      Begin VB.Line ln1 
         BorderColor     =   &H80000010&
         X1              =   0
         X2              =   48
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.PictureBox px2 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   0
      ScaleHeight     =   41
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   369
      TabIndex        =   1
      Top             =   3480
      Width           =   5535
      Begin VB.CommandButton Command4 
         Caption         =   "<上一步(&P)"
         Height          =   375
         Left            =   2880
         TabIndex        =   8
         Top             =   120
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.CommandButton Command3 
         Caption         =   "退出(&X)"
         Height          =   375
         Left            =   4200
         TabIndex        =   6
         Top             =   120
         Width           =   1215
      End
      Begin VB.CommandButton Command2 
         Caption         =   "取消(&C)"
         Height          =   375
         Left            =   4200
         TabIndex        =   5
         Top             =   120
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.CommandButton Command1 
         Caption         =   "开始更新(&U)"
         Height          =   375
         Left            =   2880
         TabIndex        =   4
         Top             =   120
         Width           =   1215
      End
      Begin VB.Line ln2 
         BorderColor     =   &H80000010&
         X1              =   0
         X2              =   80
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Timer t1 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   4200
      Top             =   120
   End
   Begin VB.Image i1 
      Height          =   3000
      Left            =   0
      Picture         =   "Form -1.frx":1042
      Top             =   480
      Width           =   1560
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Height          =   2295
      Left            =   1680
      TabIndex        =   0
      Top             =   600
      Width           =   3735
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private upd As New clsDownloader

Private Sub Command1_Click()
Dim ret As Long
Command1.Enabled = False
Command2.Visible = True
Command3.Visible = False
pCaption 1
pb.Max = 0
pb.Caption = ""
pb.Visible = True
t1.Enabled = True
DoEvents
ret = upd.StartUpdate
upd.KillTempFile
t1.Enabled = False
pb.Visible = False
Command1.Enabled = True
Command1.Visible = False
Command2.Visible = False
Command3.Visible = True
pCaption ret + 100
Command4.Visible = ret > 0
End Sub

Private Sub Command2_Click()
If MsgBox("你确定吗?", vbExclamation Or vbYesNo) = vbYes Then
 upd.Abort
End If
End Sub

Private Sub Command3_Click()
Unload Me
End Sub

Private Sub Command4_Click()
Command1.Visible = True
Command4.Visible = False
pCaption 0
End Sub

Private Sub Form_Load()
On Error Resume Next
upd.Page = "http://acme.pjz.googlepages.com/"
upd.TempPath = CStr(App.Path) + "\temp\"
Form_Resize
pCaption 0
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
If upd.Status > 0 Then Cancel = 1
End Sub

Private Sub Form_Resize()
On Error Resume Next
px1.Move 0, 0, Me.ScaleWidth, i1.Top
px2.Move 0, i1.Top + i1.Height, Me.ScaleWidth, Me.ScaleHeight - i1.Top - i1.Height
ln1.Y1 = px1.ScaleHeight - 1
ln1.X2 = px1.ScaleWidth
ln1.Y2 = px1.ScaleHeight - 1
ln2.X2 = px1.ScaleWidth
End Sub

Private Sub t1_Timer()
Dim s As String
Select Case upd.Status
Case 0
 s = "" ':-/
 pb.Max = 0
Case 1
 s = "正在连接服务器..."
 pb.Max = 0
Case 2
 s = "正在检查版本..."
 pb.Max = 0
Case 3
 s = "正在下载文件..."
 pb.Max = upd.DownloadMax
 pb.Value = upd.DownloadValue
Case 4
 s = "正在更新..."
 pb.Max = upd.DownloadMax
 pb.Value = upd.DownloadValue
End Select
pb.Caption = s
End Sub

Private Sub pCaption(ByVal i As Long)
Dim s As String
Select Case i
Case 0
 s = "欢迎使用MyDweep版本更新程序。该程序将连接至Internet检查MyDweep版本的更新，并自动下载。"
 s = s + vbCr + vbCr + "在更新前，请关闭MyDweep、关卡包编辑器、Dweep Gold以及其他使用Dweep文件的程序，并确认网络已经连接。"
 s = s + vbCr + vbCr + "单击“开始更新”开始更新，或单击“退出”退出更新程序。"
 lblTitle.Caption = "欢迎使用MyDweep版本更新程序"
Case 1
 s = "现在已经开始MyDweep版本的更新。下面是更新状态。"
 lblTitle.Caption = "正在更新..."
Case 99
 s = "你的MyDweep已经是最新版本，无须更新。"
 lblTitle.Caption = "更新完成!"
Case 100
 s = "版本更新程序已经完成对MyDweep的更新。"
 lblTitle.Caption = "更新完成!"
Case Else
 lblTitle.Caption = "更新中断"
 s = "版本更新程序已经停止对MyDweep的更新。原因:" + vbCr + vbCr
 Select Case i - 100
 Case 1
  s = s + "无法连接服务器"
 Case 2
  s = s + "用户中断"
 Case 3
  s = s + "与服务器的连接中断"
 Case 4
  s = s + "更新文件时出现错误"
 End Select
End Select
Label1.Caption = s
End Sub
