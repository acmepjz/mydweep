VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsHelp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetTickCount Lib "kernel32" () As Long

Public Property Get count() As Long
count = 14
End Property

Public Sub PredrawTo(ByVal hdc As Long, ByVal index As Long)
RedrawBack hdc
Select Case index
Case 1
 DrawItem hdc, tb.StringTable(1), tb.StringTable(2), 80
 DrawItem hdc, tb.StringTable(3), tb.StringTable(4), 230
Case 2
 DrawItem hdc, tb.StringTable(5), tb.StringTable(6), 80
 DrawItem hdc, , tb.StringTable(7), 230
Case 3
 DrawItem hdc, tb.StringTable(8), tb.StringTable(9), 80
 DrawItem hdc, tb.StringTable(10), tb.StringTable(11), 230
Case 4
 DrawItem hdc, tb.StringTable(12), tb.StringTable(13), 80
 DrawItem hdc, tb.StringTable(14), tb.StringTable(15), 230
Case 5
 DrawItem hdc, tb.StringTable(16), tb.StringTable(17), 80
 DrawItem hdc, tb.StringTable(18), tb.StringTable(19), 230
Case 6
 DrawItem hdc, tb.StringTable(20), tb.StringTable(21), 80
 DrawItem hdc, , tb.StringTable(34), 150, , vbRed
 DrawItem hdc, tb.StringTable(22), tb.StringTable(23), 230
 DrawItem hdc, , tb.StringTable(35), 280, , vbRed
Case 7
 DrawItem hdc, tb.StringTable(24), tb.StringTable(25), 80
 DrawItem hdc, , tb.StringTable(36), 130, , vbRed
 DrawItem hdc, tb.StringTable(26), tb.StringTable(27), 230
Case 8
 DrawItem hdc, tb.StringTable(28), tb.StringTable(29), 80
 DrawItem hdc, tb.StringTable(30), tb.StringTable(31), 180
 DrawItem hdc, tb.StringTable(32), tb.StringTable(33), 280
Case 9
 DrawItem hdc, tb.StringTable(39), tb.StringTable(46), 80
Case 10
 DrawItem hdc, tb.StringTable(37), tb.StringTable(38), 80
Case 11
 DrawItem hdc, tb.StringTable(40), tb.StringTable(41), 80
Case 12
 DrawItem hdc, tb.StringTable(40), tb.StringTable(42), 80
Case 13
 DrawItem hdc, tb.StringTable(43), tb.StringTable(44), 80
Case 14
 DrawItem hdc, tb.StringTable(43), tb.StringTable(45), 80
End Select
End Sub

Public Sub DrawTo(ByVal hdc As Long, ByVal index As Long)
Dim t As Long, i As Long
t = GetTickCount \ 64
Select Case index
Case 1
 'DrawItem hdc, tb.StringTable(1), tb.StringTable(2), 80
 DrawDweep hdc, 70, 150, 0, t
 'DrawItem hdc, tb.StringTable(3), tb.StringTable(4), 230
 DrawEnd hdc, 50, 280, t
Case 2
 'DrawItem hdc, tb.StringTable(5), tb.StringTable(6), 80
 'DrawItem hdc, , tb.StringTable(7), 230
 BitBlt hdc, 100, 180, 480, 80, bmpLvBk.hdc, 0, 2000, vbSrcCopy
 BitBlt hdc, 111, 204, 36, 37, bmpItem.hdc, 17 * 40 - 38, 2, vbSrcCopy
 BitBlt hdc, 161, 204, 36, 37, bmpItem.hdc, 3 * 40 - 38, 2, vbSrcCopy
 BitBlt hdc, 211, 204, 36, 37, bmpItem.hdc, 12 * 40 - 38, 2, vbSrcCopy
 BitBlt hdc, 261, 204, 36, 37, bmpItem.hdc, 14 * 40 - 38, 2, vbSrcCopy
 BitBlt hdc, 311, 204, 36, 37, bmpItem.hdc, (Int(Timer) Mod 15) * 40 + 2, 2, vbSrcCopy
 i = t Mod 100
 If i > 50 Then
  DrawMouseEx hdc, 680 - i * 4, 320 - i, 0, False
 Else
  DrawMouseEx hdc, 280 + i * 4, 220 + i, 4, False
 End If
Case 3
 'DrawItem hdc, tb.StringTable(8), tb.StringTable(9), 80
 DrawCollect hdc, 50, 130, 17
 'DrawItem hdc, tb.StringTable(10), tb.StringTable(11), 230
 DrawCollect hdc, 50, 280, 1 + Int(16 * Rnd)
Case 4
 'DrawItem hdc, tb.StringTable(12), tb.StringTable(13), 80
 'DrawItem hdc, tb.StringTable(14), tb.StringTable(15), 230
 DrawLaser hdc, 50, 280, 4 - Int(Timer) Mod 4, 0, t
 Select Case (Timer \ 5) Mod 6
 Case 0: DrawLaser hdc, 50, 130, 1 + Int(Timer) Mod 4, 0, t
 Case 1: DrawMirror hdc, 50, 130, 1 + Int(Timer) Mod 2, 1, 0
 Case 2: DrawFan hdc, 50, 130, 1 + Int(Timer) Mod 4, 0, t
 Case 3: DrawBomb hdc, 50, 130, t Mod 5
 Case 4: DrawTile hdc, 50, 130, 1, t
 Case 5: DrawTile hdc, 50, 130, 2, t
 End Select
Case 5
 'DrawItem hdc, tb.StringTable(16), tb.StringTable(17), 80
 DrawMirror hdc, 50, 130, 1 + Int(Timer) Mod 2, 1, 0
 'DrawItem hdc, tb.StringTable(18), tb.StringTable(19), 230
 DrawCollect hdc, 50, 280, 12 + Int(Timer) Mod 2
Case 6
 'DrawItem hdc, tb.StringTable(20), tb.StringTable(21), 80
 'DrawItem hdc, , tb.StringTable(34), 150, , vbRed
 DrawBomb hdc, 50, 130, t Mod 5
 'DrawItem hdc, tb.StringTable(22), tb.StringTable(23), 230
 'DrawItem hdc, , tb.StringTable(35), 280, , vbRed
 DrawCollect hdc, 50, 280, 16
Case 7
 'DrawItem hdc, tb.StringTable(24), tb.StringTable(25), 80
 'DrawItem hdc, , tb.StringTable(36), 130, , vbRed
 DrawCollect hdc, 50, 130, 14
 'DrawItem hdc, tb.StringTable(26), tb.StringTable(27), 230
 DrawFan hdc, 50, 280, 1 + Int(Timer) Mod 4, 0, t
Case 8
 'DrawItem hdc, tb.StringTable(28), tb.StringTable(29), 80
 DrawCollect hdc, 50, 130, 15
 'DrawItem hdc, tb.StringTable(30), tb.StringTable(31), 180
 DrawTile hdc, 50, 230, 2, t
 'DrawItem hdc, tb.StringTable(32), tb.StringTable(33), 280
 DrawTile hdc, 50, 330, 1, t
'Case 9
' DrawItem hdc, tb.StringTable(39), tb.StringTable(46), 80
'Case 10
' DrawItem hdc, tb.StringTable(37), tb.StringTable(38), 80
'Case 11
' DrawItem hdc, tb.StringTable(40), tb.StringTable(41), 80
'Case 12
' DrawItem hdc, tb.StringTable(40), tb.StringTable(42), 80
'Case 13
' DrawItem hdc, tb.StringTable(43), tb.StringTable(44), 80
'Case 14
' DrawItem hdc, tb.StringTable(43), tb.StringTable(45), 80
End Select
End Sub

Private Sub DrawItem(ByVal hdc As Long, Optional ByVal s1 As String, Optional ByVal s2 As String, Optional ByVal Y As Long, Optional ByVal clr1 As Long = &HFF8080, Optional ByVal clr2 As Long = vbYellow)
DrawTextB hdc, s1, frmMain.Label1.Font, 50, Y, 440, 25, DT_LEFT, clr1, , True
DrawTextB hdc, s2, frmMain.Font, 100, Y + 40, 490, 400, DT_WORDBREAK Or DT_EXPANDTABS, clr2, , True
End Sub
