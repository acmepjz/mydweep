VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private st() As String
Private stc As Long

Private m_nTableType As Long

Public Sub Clear()
Erase st
stc = 0
End Sub

Public Property Get TableType() As Long
TableType = m_nTableType
End Property

Public Property Let TableType(ByVal n As Long)
m_nTableType = n
End Property

Public Property Get count() As Long
count = stc
End Property

Public Property Get StringTable(ByVal Index As Long) As String
Dim s As String
'///
Select Case m_nTableType
Case 1 'help screen
 Select Case Index
 Case 1: StringTable = objText.GetText("Overview")
 Case 2: StringTable = objText.GetText("This is Dweep. Dweep is a cute, furry defenseless creature trapped in a hostile world. You must help Dweep to solve all levels of unique puzzles. Levels will gradually increase in difficulty and complexity. And many levels have multiple solutions. You can play the levels in any sequence, but it is best to attempt them in order.")
 Case 3: StringTable = objText.GetText("Objective")
 Case 4: StringTable = objText.GetText("Your goal is to save the baby Dweeps. To do so, you must maneuver Dweep past a series of obstacles. If you get stuck, you can always restart the level and try a different approach. Pick up special items along the way, and use them wisely. Every item has many possible uses, so you must decide the best use for each item to lead Dweep to the baby Dweeps.")
 Case 5: StringTable = objText.GetText("How To Play")
 Case 6: StringTable = objText.GetText("You control Dweep with the mouse. The panel below contains a sample inventory. Left-click on an icon to select an action to perform. The cursor will change to indicate your selection. Then click on the map area to use the current item at a specific location. For example, to make Dweep wet, click on the water bucket icon, then click on Dweep.")
 Case 7: StringTable = objText.GetText("When you pick a new item, it will be added to your inventory. When you use an item, it will be removed from your inventory permanently and cannot be reused.  It is possible to have more than one of any given item. Right-click at any time to cancel the currently selected item and return to the MOVE DWEEP command.")
 Case 8: StringTable = objText.GetText("Moving Dweep")
 Case 9: StringTable = objText.GetText("The first command in the list of icons is usually the MOVE DWEEP command. When the movement cursor is selected, just left-click anywhere on the map, and Dweep will move there if possible. Your goal on every level is to guide Dweep safely to the baby Dweeps.")
 Case 10: StringTable = objText.GetText("Items")
 Case 11: StringTable = objText.GetText("On most levels you will start with no items in your inventory, so you will have to collect them along the way. Items will appear as icons on individual tiles. If you move Dweep onto an icon, it will be added to your inventory. Often Dweep will be surrounded by several items you can pick up and use right away. Items include lasers, mirrors, bombs, wrenches, torches, water buckets, fans, and hammers. Most item have multiple functions.")
 Case 12: StringTable = objText.GetText("Obstacles")
 Case 13: StringTable = objText.GetText("On every level you will encounter obstacles blocking Dweep's path to the baby Dweeps. These include lasers, mirrors, bombs, fans, freeze plates, and heat plates. Some objects can be used to your advantage, others must be avoided.")
 Case 14: StringTable = objText.GetText("Laser")
 Case 15: StringTable = objText.GetText("A laser shoots a continuous beam of light in one of four directions. The beam will incinerate Dweep and destroy any other objects it hits. A laser beam can be avoided by deflecting it with a mirror. You can permanently destroy a laser with a bomb, hammer, or another laser, you can pick up laser icons to add to your inventory. There are four types of lasers, one for each direction the laser can fire. You can place a laser on any empty tile. An active laser will then appear at that location.")
 Case 16: StringTable = objText.GetText("Mirror")
 Case 17: StringTable = objText.GetText("A mirror can deflect any laser beam by 90 degrees. Mirrors can face one of two directions, and they can deflect laser beams on both their front and back sides. Mirrors cannot be destroyed by laser beams directly. Mirror icons can be picked up to add to your inventory. You can place a mirror on any empty tile. A mirror object will then appear at that location.")
 Case 18: StringTable = objText.GetText("Wrench")
 Case 19: StringTable = objText.GetText("A wrench can rotate a fan, laser, or mirror by 90 degrees. There are both clockwise and counterclockwise wrenches, each represented by a different icon. Both types of wrenches will have the same effect on a mirror, since a mirror can face only one of two directions.")
 Case 20: StringTable = objText.GetText("Bomb")
 Case 21: StringTable = objText.GetText("A bomb is normally inactive, but it will explode when its fuse is lit by a laser beam or a torch. An exploding bomb will destroy all adjacent objects. There is a short delay between the lighting of a bomb's fuse and its subsequent explosion. A lit bomb can be defused with a water bucket. A bomb can still be lit again after being soaked. Bomb icons can be picked up to add to your inventory. You can place a bomb on any empty tile. A bomb object will then appear at that location. If you place a bomb directly in the path of a laser beam, the fuse will light instantly.")
 Case 22: StringTable = objText.GetText("Torch")
 Case 23: StringTable = objText.GetText("A torch can be used to light a bomb's fuse or to melt a frozen Dweep. It has no effect on inflammable objects, such as lasers, mirrors, and fans. A torch can also make a wet Dweep dry again.")
 Case 24: StringTable = objText.GetText("Water Bucket")
 Case 25: StringTable = objText.GetText("Use a water bucket to douse an object with water. It can make Dweep wet, which will allow Dweep to safely cross a laser beam or heat plate. It has no effect on lasers, mirrors, or fans. A lit bomb can be defused by using a water bucket on it. A water bucket will bring a floating Dweep back down to the ground. Water buckets have no effect on a frozen Dweep.")
 Case 26: StringTable = objText.GetText("Fan")
 Case 27: StringTable = objText.GetText("A fan can blow a floating Dweep a short distance. If floating in front of a fan, Dweep will be blown two tiles away and can pass over obstacles, including walls. Dweep can only be affected by fans while floating. Fans have no effect on any other objects. You can pick up fan icons to add to your inventory. There are four types of fans, one for each direction the fan can face. You can place a fan on any empty tile. An active fan will then appear at that location.")
 Case 28: StringTable = objText.GetText("Hammer")
 Case 29: StringTable = objText.GetText("A hammer can destroy any object instantly, including lasers, mirrors, fans, and bombs. Use a hammer to knock a floating Dweep pack down to the ground or to free a frozen Dweep from the ice.")
 Case 30: StringTable = objText.GetText("Freeze Plate")
 Case 31: StringTable = objText.GetText("If Dweep steps onto a freeze plate, Dweep will become frozen inside a block of ice. Use a torch or a hammer on the ice to free Dweep. Freeze plates cannot be destroyed.")
 Case 32: StringTable = objText.GetText("Heat Plate")
 Case 33: StringTable = objText.GetText("If Dweep steps onto a heat plate, Dweep will float like a balloon. A floating Dweep can only be moved by fans. Use a water bucket or a hammer to bring a floating Dweep back down to the ground. A wet Dweep who steps onto a heat plate will become dry again. Heat plates cannot be destroyed.")
 '------------------------------------------------------------------
 Case 34: StringTable = objText.GetText("Note: a bomb under a laser beam cannot be defused with a water bucket. The laser beam must be removed first, then you can use a water bucket to defuse the bomb.")
 Case 35: StringTable = objText.GetText("You will burn Dweep if you use a torch on a dry Dweep!")
 Case 36: StringTable = objText.GetText("Note: a wet Dweep still cannot pass through a heat plate with a laser beam on it!")
 Case 37: StringTable = objText.GetText("Level Editor")
 Case 38: StringTable = ConvertString("虽然Dweep有超过180个关卡供您探索，但是您仍希望独立设计自己的关卡。与原版相比，My Dweep加入了关卡编辑器，可以满足您的愿望。在编辑自己的关卡之前，请花费一些时间来阅读关卡编辑器使用说明。\n\n关卡编辑器界面与游戏界面类似，您只须从画面下方选取游戏要素，在您想放置的地方点击鼠标即可。鼠标右键单击可以将当前的地面给消掉。在画面下方的游戏要素中，有激光器、镜子、风扇和炸弹，鼠标左键选取为放置物体，鼠标右键选取则为放置可以拾取的道具。另外，其中的前五个图标是设定本关的材质，五个图标分别对应于五个不同的材质。用菜单中的“道具箱”选项，可以设置本关开始时Dweep持有什么道具。点击上面一栏中的图标删除道具，点击下面一栏中的图标添加道具。在关卡设计完成之后，您就可以试玩了。在试玩的关卡介绍对话框，您可以修改本关的名称和提示。\n\n由于我的设计失误，在关卡编辑器的菜单上并没有“取消”按钮。您可以单击鼠标右键或按Esc返回编辑画面。另外，还有一些常用热键是：\n\n方向键\t\t\t滚动关卡\n[X]\t\t\t水平翻转\n[Y]\t\t\t垂直翻转\n\n如果您编出了新的关卡，欢迎发电子邮件给我。我的邮箱是：acme_pjz@hotmail.com")
 Case 39: StringTable = objText.GetText("Level Record")
 Case 40: StringTable = objText.GetText("Level Design Tips")
 Case 41: StringTable = ConvertString("★仔细阅读帮助文件，掌握关卡编辑器的使用方法，这样可以节省您的宝贵时间，先编写一些简单的关卡，太难的关卡可能会使您感到气馁。\n★认真玩玩内置的关卡，这样可使您获得更多的设计思路，那里面有大量的小窍门。\n★在关卡的第一行不要设计太多的内容，Dweep在那里会跳出屏幕，漂浮状态的Dweep仅能看到一点点，大多数内置关卡第一行都是砖块。\n★设计关卡时，首先构建合适完整的过关路径，然后增加一些错误的路径来误导玩家，增加路径时要确保不会增加新的过关路径或破坏原有的过关路径。经常地测试您的关卡，您能提炼出许多设计思路。\n★多看别人玩您的关卡，您会从他们那里学来更多的设计思路，您能在片刻之间发现关键所在。\n★您不必一次完成整个关卡的设计，休息一会儿，以后再来，一些好的思路就是在您休息或散步后萌发出来的。")
 Case 42: StringTable = ConvertString("★在关卡中小心使用有关精确定时的窍门，因为这样您会让那些不能正确确定时间的玩家感到灰心。建议您应尽少的使用定时的窍门。\n★将一个关卡分成几个小块，假想您的关卡是三个或四个部分，需要玩家使用某些道具通过每个舞台，这样能减化您的设计，您仅需设计几个小的关卡并把它们联接起来。\n★尽量减少重复性的操作。在同一关中，不要要求他们使用完全相同的技巧去解决相同的问题。\n★在关卡中小心使用锤子。它在游戏中的功能很强大，要确保它们不会破坏原来设计的过关路径，而找到一个新的过关捷径。\n★如果一个道具可以被拿走而不会影响最终的过关路径，那么就将它拿走，除非它是用来误导玩家的。")
 Case 43: StringTable = objText.GetText("Level Design Tricks")
 Case 44: StringTable = "    " + ConvertString("下面的单个技巧可以组合变化，而构成一个有趣的关卡。\n★使用对称的方式设计来简化关卡，而且可以使您的关卡看上去是第一流的。设计类似的结构在关卡的左边或右边，上边或下边。\n★需要玩家在使用道具前将Dweep移动到几个确定的点上，象旋转激光器。\n★用一系列的反应使玩家必须提前作出考虑，象激光点燃炸弹，然后炸毁哪几个镜子，怎样改变激光束的路径。\n★要求玩家通过使用扳手或镜子使两个激光器相互烧毁。\n★要求玩家在激光束的路径中放置炸弹或镜子。\n★利用障碍物强置玩家必须找出一条精确的路径，后面再给玩家用来通过这些障碍物的道具。如用冷冻盘或加热盘破坏到达目的的路径，然后需要玩家在周围拾起火，水或锤子。\n★提供多种选择，使玩家必须作出考虑。如面对一个炸弹，是用火还是使激光器偏转来点燃炸弹；如通过激光束，是选择浇水呢，还是用冰冻Dweep再用火解冻呢，或是用加热盘使Dweep漂浮起来，再用风扇吹呢。") + vbCrLf + _
 ConvertString("★需要玩家在几个确定的点上安排不同的镜子来改变激光束的方向。\n★需要玩家放置炸弹一次来炸毁若干个物体，一个炸弹能炸毁它周围的八个物体，如果有连锁反应则更多。\n★需要玩家使用一个镜子改变多个激光束的方向。\n★将一个关卡用障碍墙分成几个的区域，然后再每个区域的入口处放置障碍，在其它的区域放置通过障碍的道具。\n★通过将破解障碍的道具与相应障碍分开放置的办法来提供一条间接的过关路径，如将道具放在激光束下面，使玩家必须穿过激光束才能拿到道具。")
 Case 45: StringTable = ConvertString("★使用一些诱骗的道具，只要玩家拾起它们就会失败。\n★让玩家没有储存道具的空间，使玩家在拾起新的道具之间必须使用掉旧的。\n★防止玩家由于使用锤子摧毁一个关卡后面将要使用的特定的物体，如一个激光器必须点燃一个炸弹，所以如果玩家过早地摧毁了那个激光器就不能胜利。用冷冻盘、加热盘或墙来防止玩家在那些地方放置激光器镜子或风扇。\n★用墙来防止激光束由于偏转而被射入某些区域。用风扇作为临时的障碍物来阻止进入某些新的区域。\n★在使用道具后提供一些附加的有利条件，用水浇一个漂浮的Dweep或用火把释放冰冻的Dweep会使Dweep成为湿状态，这些可被用来通过激光束或加热盘。") + vbCrLf + _
 ConvertString("★需要玩家在炸弹点燃时作出行动，如旋转镜子。\n★需要Dweep成为漂浮状态来消除激光束的作用。\n★需要Dweep成为漂浮状态来消除炸弹爆炸的作用。\n★将关卡分成几个区域，用风扇把Dweep从一个区域吹到另一个区域，沿路拾起新的道具。\n★用两个风扇对角地斜吹Dweep。\n★用多个风扇吹Dweep，使其移动一个较长的距离。\n★利用其旁边放置两个或多个加热盘来多次重复使用风扇，然后提供扳手来改变风扇方向。\n★需要用水或锤子在适当的时间将漂浮状态的Dweep改变成正常状态。\n★需要玩家在激光束通过的地方放置风扇。\n★需要玩家用锤子打碎一个镜子，使两个激光器能相互摧毁。\n★需要玩家用火烤干一个湿状态的Dweep。\n★需要玩家用水浇灭一个点燃的炸弹。")
 Case 46: StringTable = ConvertString("当您通过了一个很难的关卡，您也许想记录下这关的过关方法。以前您可能用文字来记录，也有可能用截图来记录，但是现在，您可以不必为此担心了！过关录像是My Dweep添加的功能。当您通过了一关后，在胜利的菜单中有一个选项“保存录像”，点击即可。如果这关是您自定义的，那么将会弹出一个对话框要求输入一个文件名。如果您在选项中选择了“自动保存关卡录像”，那么在您完成了一个以前没有完成的关卡时，过关录像就会自动保存。当您想看过关录像时，进入主菜单中的“录像”即可。关卡录像的播放不需要打开原关卡，所以您可以在网上下载别人的过关录像。要注意的是，您在编辑地图的试玩时不能保存过关录像，在运行教程时也不能。\n\n最新版本的MyDweep的关卡录像功能可以说是100%的没有Bug，大家可以放心使用:-)")
 'Case 21: StringTable = objText.GetText("")
 'Case 22: StringTable = objText.GetText("")
 'Case 23: StringTable = objText.GetText("")
 Case Else: StringTable = "TODO:"
 End Select
Case Else
 StringTable = st(Index)
End Select
End Property

'Public Property Let StringTable(ByVal Index As Long, ByVal s As String)
'If m_nTableType Then Exit Property
'st(Index) = s
'End Property

Public Sub LoadFile(ByVal FileName As String, Optional b As Boolean)
On Error GoTo a
Dim s As String, i As Long
Erase st
stc = 0
Open FileName For Input As #1
Do Until EOF(1)
 stc = stc + 1
 Line Input #1, s
Loop
Close #1
ReDim st(1 To stc)
Open FileName For Input As #1
For i = 1 To stc
 Line Input #1, s
 If b Then s = ConvertString(s)
 st(i) = s
Next i
a:
Close #1
End Sub

Private Sub Class_Terminate()
Clear
End Sub
