VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsScript"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Name As String
Private str1() As String
Private s1c As Long
Private str2() As String
Private s2c As Long

Public Property Get Count1() As Long
Count1 = s1c
End Property

Public Property Get Count2() As Long
Count2 = s1c
End Property

Public Property Get Data1(ByVal Index As Long) As String
Data1 = str1(Index)
End Property

Public Property Let Data1(ByVal Index As Long, ByVal s As String)
str1(Index) = s
End Property

Public Property Get Data2(ByVal Index As Long) As String
Data2 = str2(Index)
End Property

Public Property Let Data2(ByVal Index As Long, ByVal s As String)
str2(Index) = s
End Property

Public Sub Add1(ByVal s As String)
s1c = s1c + 1
ReDim Preserve str1(1 To s1c)
str1(s1c) = s
End Sub

Public Sub Remove1(ByVal Index As Long)
Dim i As Long
If s1c > 1 Then
 For i = Index To s1c - 1
  str1(i) = str1(i + 1)
 Next i
 s1c = s1c - 1
 ReDim Preserve str1(1 To s1c)
Else
 Erase str1
 s1c = 0
End If
End Sub

Public Sub Clear1()
Erase str1
s1c = 0
End Sub

Public Sub Add2(ByVal s As String)
s2c = s2c + 1
ReDim Preserve str2(1 To s2c)
str2(s2c) = s
End Sub

Public Sub Remove2(ByVal Index As Long)
Dim i As Long
If s2c > 1 Then
 For i = Index To s2c - 1
  str2(i) = str2(i + 1)
 Next i
 s2c = s2c - 1
 ReDim Preserve str2(1 To s2c)
Else
 Erase str2
 s2c = 0
End If
End Sub

Public Sub Clear2()
Erase str2
s2c = 0
End Sub

Public Sub Destroy()
Clear1
Clear2
End Sub

Private Sub Class_Terminate()
Destroy
End Sub
