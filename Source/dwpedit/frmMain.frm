VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "Dweep Level Pack Editor"
   ClientHeight    =   5580
   ClientLeft      =   165
   ClientTop       =   510
   ClientWidth     =   8295
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   372
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   553
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   4080
      Top             =   3120
   End
   Begin Dwpedit.vbalCommandBar tb 
      Align           =   2  'Align Bottom
      Height          =   135
      Index           =   3
      Left            =   0
      Top             =   5445
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   238
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Transparency    =   225
   End
   Begin Dwpedit.vbalCommandBar tb 
      Align           =   1  'Align Top
      Height          =   135
      Index           =   2
      Left            =   0
      Top             =   270
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   238
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Transparency    =   225
   End
   Begin Dwpedit.vbalImageList il2 
      Left            =   5760
      Top             =   360
      _ExtentX        =   953
      _ExtentY        =   953
      IconSizeX       =   24
      IconSizeY       =   24
      ColourDepth     =   24
      Size            =   86100
      Images          =   "frmMain.frx":0CCA
      Version         =   65561
      KeyCount        =   35
      Keys            =   "����������������������������������"
   End
   Begin Dwpedit.vbalCommandBar tb 
      Align           =   1  'Align Top
      Height          =   135
      Index           =   1
      Left            =   0
      Top             =   135
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   238
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Transparency    =   225
   End
   Begin Dwpedit.vbalCommandBar tb 
      Align           =   1  'Align Top
      Height          =   135
      Index           =   0
      Left            =   0
      Top             =   0
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   238
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Transparency    =   225
   End
   Begin Dwpedit.vbalImageList il1 
      Left            =   5160
      Top             =   360
      _ExtentX        =   953
      _ExtentY        =   953
      ColourDepth     =   24
      Size            =   34440
      Images          =   "frmMain.frx":15D3E
      Version         =   65561
      KeyCount        =   30
      Keys            =   "�����������������������������"
   End
   Begin VB.PictureBox picResize 
      BorderStyle     =   0  'None
      Height          =   1575
      Left            =   2520
      MousePointer    =   9  'Size W E
      ScaleHeight     =   105
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   9
      TabIndex        =   4
      Top             =   3240
      Width           =   135
   End
   Begin VB.PictureBox picName 
      Height          =   495
      Left            =   4080
      ScaleHeight     =   29
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   296
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   1200
      Width           =   4500
      Begin Dwpedit.Button2003 cmdF 
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   120
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   450
         Caption         =   "7"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Webdings"
            Size            =   8.25
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ButtonSpace     =   1
         ShowFocusRect   =   0   'False
      End
      Begin VB.TextBox txtLvHint 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   765
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   11
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox txtLvName 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   10
         Top             =   480
         Width           =   615
      End
      Begin VB.TextBox txtlpName 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3000
         TabIndex        =   7
         Top             =   120
         Width           =   255
      End
      Begin VB.TextBox txtLv 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   600
         TabIndex        =   5
         Text            =   "1"
         Top             =   120
         Width           =   600
      End
      Begin Dwpedit.Button2003 cmdP 
         Height          =   255
         Left            =   360
         TabIndex        =   13
         Top             =   120
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   450
         Caption         =   "3"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Webdings"
            Size            =   8.25
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ButtonSpace     =   1
         ShowFocusRect   =   0   'False
      End
      Begin Dwpedit.Button2003 Command1 
         Height          =   255
         Left            =   1200
         TabIndex        =   14
         Top             =   120
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   450
         Caption         =   "4"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Webdings"
            Size            =   8.25
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ButtonSpace     =   1
         ShowFocusRect   =   0   'False
      End
      Begin Dwpedit.Button2003 cmdL 
         Height          =   255
         Left            =   1440
         TabIndex        =   15
         Top             =   120
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   450
         Caption         =   "8"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Webdings"
            Size            =   8.25
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ButtonSpace     =   1
         ShowFocusRect   =   0   'False
      End
      Begin VB.Label Label3 
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Level Pack Name"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1725
         TabIndex        =   6
         Top             =   120
         Width           =   1335
      End
   End
   Begin VB.HScrollBar Hs1 
      Height          =   255
      LargeChange     =   10
      Left            =   1440
      SmallChange     =   10
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3240
      Width           =   2535
   End
   Begin VB.VScrollBar Vs1 
      Height          =   1335
      LargeChange     =   10
      Left            =   3720
      SmallChange     =   10
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1320
      Width           =   255
   End
   Begin VB.PictureBox picLv 
      Height          =   1935
      Left            =   120
      ScaleHeight     =   125
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   389
      TabIndex        =   0
      Top             =   1080
      Width           =   5895
   End
   Begin VB.Image img 
      BorderStyle     =   1  'Fixed Single
      Height          =   780
      Index           =   1
      Left            =   4920
      Picture         =   "frmMain.frx":1E3E6
      Top             =   4320
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image img 
      BorderStyle     =   1  'Fixed Single
      Height          =   660
      Index           =   8
      Left            =   1080
      Picture         =   "frmMain.frx":1EB96
      Top             =   4560
      Visible         =   0   'False
      Width           =   1860
   End
   Begin VB.Image img 
      BorderStyle     =   1  'Fixed Single
      Height          =   1860
      Index           =   7
      Left            =   960
      Picture         =   "frmMain.frx":1F4A2
      Top             =   4440
      Visible         =   0   'False
      Width           =   3060
   End
   Begin VB.Image img 
      BorderStyle     =   1  'Fixed Single
      Height          =   2460
      Index           =   6
      Left            =   840
      Picture         =   "frmMain.frx":240A6
      Top             =   4320
      Visible         =   0   'False
      Width           =   3660
   End
   Begin VB.Image img 
      BorderStyle     =   1  'Fixed Single
      Height          =   1260
      Index           =   5
      Left            =   720
      Picture         =   "frmMain.frx":29AC6
      Top             =   4200
      Visible         =   0   'False
      Width           =   1860
   End
   Begin VB.Image img 
      BorderStyle     =   1  'Fixed Single
      Height          =   2460
      Index           =   4
      Left            =   600
      Picture         =   "frmMain.frx":2A606
      Top             =   4080
      Visible         =   0   'False
      Width           =   4260
   End
   Begin VB.Image img 
      BorderStyle     =   1  'Fixed Single
      Height          =   1860
      Index           =   3
      Left            =   480
      Picture         =   "frmMain.frx":2E18A
      Top             =   3960
      Visible         =   0   'False
      Width           =   3660
   End
   Begin VB.Image img 
      BorderStyle     =   1  'Fixed Single
      Height          =   2460
      Index           =   2
      Left            =   360
      Picture         =   "frmMain.frx":31496
      Top             =   3840
      Visible         =   0   'False
      Width           =   3660
   End
   Begin VB.Image img 
      BorderStyle     =   1  'Fixed Single
      Height          =   660
      Index           =   0
      Left            =   120
      Picture         =   "frmMain.frx":33E82
      Top             =   3600
      Visible         =   0   'False
      Width           =   9660
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function WindowFromPoint Lib "user32" (ByVal xPoint As Long, ByVal yPoint As Long) As Long
Private Declare Function PostMessage Lib "user32" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private Declare Function GetProp Lib "user32" Alias "GetPropA" (ByVal hwnd As Long, ByVal lpString As String) As Long
Private Declare Function SetProp Lib "user32" Alias "SetPropA" (ByVal hwnd As Long, ByVal lpString As String, ByVal hData As Long) As Long
Private Declare Function RemoveProp Lib "user32" Alias "RemovePropA" (ByVal hwnd As Long, ByVal lpString As String) As Long
Private Declare Function BringWindowToTop Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function FlashWindow Lib "user32" (ByVal hwnd As Long, ByVal bInvert As Long) As Long
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
Private Declare Function InvalidateRect Lib "user32.dll" (ByVal hwnd As Long, ByRef lpRect As Any, ByVal bErase As Long) As Long

Private bmpBack As New cAlphaDibSection

Private bmpItem1 As New cAlphaDibSection
Private bmpItem2 As New cAlphaDibSection
Private bmpItem3 As New cAlphaDibSection
Private bmpItem4 As New cAlphaDibSection

Private bmpL1 As New cAlphaDibSection
Private bmpL2 As New cAlphaDibSection

Private bmpTexture As New cAlphaDibSection
Private bmpEx As New cAlphaDibSection

Private bmpResize As New cAlphaDibSection
Private bmpTemp As New cAlphaDibSection

Private rsX As Integer, rsOn As Boolean
Private DrawType As Integer
Private lastX As Integer, lastY As Integer
Private startX As Integer, startY As Integer
Private ZoomOff As Boolean
Private GridWidth As Integer
Private ShowWay As Boolean
Private ShowFan As Boolean

Private lpName As String
Private lvMax As Long
Private lvDat(1 To 120) As tLevel
Private lvCB As tLevel
Private lvNow As Long
Private lvItem As Integer

Private Type tTool
 tlName As Byte
 tlType As Byte
End Type

Private Type tLevel
 lvName As String
 lvHint As String
 lvData(1 To 16, 1 To 10) As Byte
 lvTools(1 To 11) As tTool
End Type

Private lvLaser(1 To 16, 1 To 10) As Byte
'0-none
'123-laser
'456789-mirror
'10-end

Private Filename As String

Private cd As New cCommonDialog
Private cdFilter As String
Private GameWnd As Long
Private IsBaidu As Boolean
Private luke As Boolean

Implements ISubclass

Private Sub cmdAL_Click()
Dim i As Integer
If lvMax = 120 Then
 If xxN > 0 Then
  MsgBox LoadResString(501), vbCritical
 Else
  MsgBox "Level pack is full(max=120). Can't add level.", vbCritical
 End If
Else
 lvMax = lvMax + 1
 Erase lvDat(lvMax).lvData, lvDat(lvMax).lvTools
 lvDat(lvMax).lvName = IIf(xxN = 0, "Unnamed", LoadResString(142))
 lvDat(lvMax).lvHint = ""
End If
End Sub

Private Sub cmdF_Click()
txtLv.Text = "1"
txtLv_Change
End Sub

Private Sub cmdL_Click()
txtLv.Text = IIf(lvMax > 0, CStr(lvMax), "120")
'TODO:
txtLv_Change
End Sub

Private Sub cmdP_Click()
txtLv.Text = CStr(IIf(lvNow = 1, 1, lvNow - 1))
txtLv_Change
End Sub

Private Sub Command1_Click()
txtLv.Text = CStr(IIf(lvNow >= lvMax, lvMax, lvNow + 1))
txtLv_Change
End Sub

Private Sub Form_Load()
Debug.Assert (Baidu = True)
bmpBack.Create 640, 400
bmpItem1.CreateFromPicture img(2).Picture
bmpItem2.CreateFromPicture img(3).Picture
bmpItem3.CreateFromPicture img(4).Picture
bmpItem4.CreateFromPicture img(0).Picture
bmpL1.CreateFromPicture img(5).Picture
bmpL2.CreateFromPicture img(6).Picture
bmpTexture.CreateFromPicture img(7).Picture
bmpEx.CreateFromPicture img(8).Picture
bmpTemp.CreateFromPicture img(1).Picture
lvNow = 1
lvMax = 1
Filename = "?"
Form_Resize
DrawMap
RedrawTools
'tbToolBar.Buttons("1a").Value = tbrPressed
DrawType = 1
GridWidth = 40
ChangeTitle
'//////////////////////////////////////////
'On Error Resume Next
'With tb(0)
' .Buttons.Clear
' .CommandBars.Clear
'End With
'On Error GoTo 0
'/////////////////////////////////////////
Dim btn As cButton, btns As cButtons, i As Long
Set btns = tb(0).Buttons
tb(0).MenuImageList = il1.hIml
tb(1).ToolbarImageList = il1.hIml
tb(1).MenuImageList = il1.hIml
tb(2).ToolbarImageList = il2.hIml
tb(2).MenuImageList = il2.hIml
tb(3).ToolbarImageList = il2.hIml
tb(3).MenuImageList = il2.hIml
'//////
With tb(0).CommandBars.Add("File").Buttons
 .Add btns.Add("new__1", 0, , , , vbKeyN, vbCtrlMask)
 .Add btns.Add("opn__1", 1, , , , vbKeyO, vbCtrlMask)
 .Add btns.Add("__1", , , eSeparator)
 .Add btns.Add("sav__1", 2, , , , vbKeyS, vbCtrlMask)
 .Add btns.Add("sas__1", , , , , vbKeyS, vbCtrlMask Or vbAltMask)
 .Add btns.Add("__2", , , eSeparator)
 .Add btns.Add("__i", 26, , , , vbKeyI, vbCtrlMask)
 .Add btns.Add("__e", 27, , , , vbKeyE, vbCtrlMask)
 .Add btns.Add("__3", , , eSeparator)
 .Add btns.Add("tst__1", 29, , , , vbKeyF5)
 .Add btns.Add("__4", , , eSeparator)
 .Add btns.Add("ext__1", 19, , , , vbKeyQ, vbAltMask)
End With
With tb(0).CommandBars.Add("Edit").Buttons
 .Add btns.Add("cut__1", 3, , , , vbKeyX, vbCtrlMask)
 .Add btns.Add("cpy__1", 4, , , , vbKeyC, vbCtrlMask)
 .Add btns.Add("pas__1", 5, , , , vbKeyV, vbCtrlMask)
 .Add btns("__1")
 .Add btns.Add("add", 15, , , , vbKeyA, vbCtrlMask)
 .Add btns.Add("ins", 17, , , , vbKeyF8, 0)
 .Add btns("__2")
 .Add btns.Add("clr", 18, , , , vbKeyDelete, vbShiftMask)
 .Add btns.Add("del", 16, , , , vbKeyDelete, vbCtrlMask)
 .Add btns("__3")
 .Add btns.Add("for__1", 14, , , , vbKeyRight)
 .Add btns.Add("bak__1", 13, , , , vbKeyLeft)
 .Add btns("__4")
 .Add btns.Add("up_", 22, , , , vbKeyUp, vbAltMask Or vbCtrlMask)
 .Add btns.Add("dwn", 24, , , , vbKeyDown, vbAltMask Or vbCtrlMask)
 .Add btns.Add("lft", 25, , , , vbKeyLeft, vbAltMask Or vbCtrlMask)
 .Add btns.Add("rit", 23, , , , vbKeyRight, vbAltMask Or vbCtrlMask)
 .Add btns.Add("__5", , , eSeparator)
 .Add btns.Add("xfl", , , , , vbKeyX, vbAltMask Or vbCtrlMask)
 .Add btns.Add("yfl", , , , , vbKeyY, vbAltMask Or vbCtrlMask)
End With
With tb(0).CommandBars.Add("lng").Buttons
 .Add btns.Add("eng", , "&English", eRadio)
 .Add btns.Add("chs", , "&Chinese", eRadio)
End With
With tb(0).CommandBars.Add("View").Buttons
 .Add btns.Add("got__1", 20, , , , vbKeyG, vbCtrlMask)
 .Add btns("__1")
 Set btn = btns.Add("lng", , "&Language")
 Set btn.Bar = tb(0).CommandBars("lng")
 .Add btn
 If IsBaidu Then
  .Add btns("__2")
  .Add btns.Add("scr", 28)
 End If
End With
With tb(0).CommandBars.Add("Help").Buttons
 .Add btns.Add("abt", 21)
End With
With tb(0).CommandBars.Add("Menu").Buttons
 Set btn = btns.Add("file")
 Set btn.Bar = tb(0).CommandBars("File")
 btn.ShowCaptionInToolbar = True
 .Add btn
 Set btn = btns.Add("edit")
 Set btn.Bar = tb(0).CommandBars("Edit")
 btn.ShowCaptionInToolbar = True
 .Add btn
 Set btn = btns.Add("view")
 Set btn.Bar = tb(0).CommandBars("View")
 btn.ShowCaptionInToolbar = True
 .Add btn
 Set btn = btns.Add("help")
 Set btn.Bar = tb(0).CommandBars("Help")
 btn.ShowCaptionInToolbar = True
 .Add btn
End With
With tb(0).CommandBars.Add("tb").Buttons
 .Add btns.Add("new__2", 0)
 .Add btns.Add("opn__2", 1)
 .Add btns.Add("sav__2", 2)
 .Add btns("__1")
 .Add btns.Add("cut__2", 3)
 .Add btns.Add("cpy__2", 4)
 .Add btns.Add("pas__2", 5)
 .Add btns("__2")
 .Add btns.Add("zom", 6, , eCheck)
 .Add btns.Add("way", 7, , eCheck)
 .Add btns.Add("fan", 8, , eCheck)
 .Add btns("__3")
 Set btn = btns.Add("opt1", 9, , eRadio)
 btn.Checked = True
 .Add btn
 .Add btns.Add("opt2", 10, , eRadio)
 .Add btns.Add("opt3", 11, , eRadio)
 '.Add btns.Add("opt4", 12, , eRadio)
 .Add btns("__4")
 .Add btns.Add("add__2", 15)
 .Add btns.Add("ins__2", 17)
 .Add btns("__5")
 .Add btns.Add("clr__2", 18)
 .Add btns.Add("del__2", 16)
 .Add btns.Add("__6", , , eSeparator)
 .Add btns.Add("bak__2", 13)
 .Add btns.Add("for__2", 14)
 .Add btns.Add("__7", , , eSeparator)
 .Add btns.Add("got__2", 20)
 .Add btns.Add("tst__2", 29)
 .Add btns.Add("__8", , , eSeparator)
 Set btn = btns.Add("tip", , , ePanel)
 btn.ShowCaptionInToolbar = True
 .Add btn
End With
tb(0).MainMenu = True
Set tb(0).Toolbar = tb(0).CommandBars("Menu")
Set tb(1).Toolbar = tb(0).CommandBars("tb")
'///////////
With tb(0).CommandBars.Add("Lasers").Buttons
 .Add btns.Add("las6", 6, TheDir(1), eRadio)
 .Add btns.Add("las7", 7, TheDir(2), eRadio)
 .Add btns.Add("las8", 8, TheDir(3), eRadio)
 .Add btns.Add("las9", 9, TheDir(4), eRadio)
End With
With tb(0).CommandBars.Add("Fans").Buttons
 .Add btns.Add("fas12", 12, TheDir(1), eRadio)
 .Add btns.Add("fas13", 13, TheDir(2), eRadio)
 .Add btns.Add("fas14", 14, TheDir(3), eRadio)
 .Add btns.Add("fas15", 15, TheDir(4), eRadio)
End With
With tb(0).CommandBars.Add("las2").Buttons
 .Add btns.Add("ils17", 17, TheDir(1), eRadio)
 .Add btns.Add("ils18", 18, TheDir(2), eRadio)
 .Add btns.Add("ils19", 19, TheDir(3), eRadio)
 .Add btns.Add("ils20", 20, TheDir(4), eRadio)
End With
With tb(0).CommandBars.Add("fan2").Buttons
 .Add btns.Add("ifs23", 23, TheDir(1), eRadio)
 .Add btns.Add("ifs24", 24, TheDir(2), eRadio)
 .Add btns.Add("ifs25", 25, TheDir(3), eRadio)
 .Add btns.Add("ifs26", 26, TheDir(4), eRadio)
End With
With tb(0).CommandBars.Add("Brush").Buttons
 Set btn = btns.Add("itm00", 0, , eRadio)
 btn.Checked = True
 .Add btn
 .Add btns.Add("itm01", 1, , eRadio)
 .Add btns.Add("itm02", 2, , eRadio)
 .Add btns.Add("itm03", 3, , eRadio)
 .Add btns.Add("itm04", 4, , eRadio)
 .Add btns.Add("itm05", 5, , eRadio)
 Set btn = btns.Add("itx06", 6, , eRadio)
 Set btn.Bar = tb(0).CommandBars("Lasers")
 btn.ShowDropDownInToolbar = True
 .Add btn
 .Add btns.Add("itm10", 10, , eRadio)
 .Add btns.Add("itm11", 11, , eRadio)
 Set btn = btns.Add("itx12", 12, , eRadio)
 Set btn.Bar = tb(0).CommandBars("Fans")
 btn.ShowDropDownInToolbar = True
 .Add btn
 .Add btns.Add("itm16", 16, , eRadio)
 Set btn = btns.Add("itx17", 17, , eRadio)
 Set btn.Bar = tb(0).CommandBars("las2")
 btn.ShowDropDownInToolbar = True
 .Add btn
 .Add btns.Add("itm21", 21, , eRadio)
 .Add btns.Add("itm22", 22, , eRadio)
 Set btn = btns.Add("itx23", 23, , eRadio)
 Set btn.Bar = tb(0).CommandBars("fan2")
 btn.ShowDropDownInToolbar = True
 .Add btn
 .Add btns.Add("itm27", 27, , eRadio)
 .Add btns.Add("itm28", 28, , eRadio)
 .Add btns.Add("itm29", 29, , eRadio)
 .Add btns.Add("itm30", 30, , eRadio)
 .Add btns.Add("itm31", 31, , eRadio)
 .Add btns.Add("itm32", 32, , eRadio)
End With
Set tb(2).Toolbar = tb(0).CommandBars("Brush")
'///////////
With tb(0).CommandBars.Add("Add").Buttons
 For i = 1 To 17
  .Add btns.Add("its" + Format(i, "00"), 16 + i)
 Next i
 .Add btns("__2")
End With
With tb(0).CommandBars.Add("Tools").Buttons
 Set btn = btns.Add("aaa", , , ePanel)
 btn.ShowCaptionInToolbar = True
 .Add btn
 For i = 1 To 10
  Set btn = btns.Add("box" + Format(i, "00"))
  btn.Visible = False
  .Add btn
 Next i
 .Add btns("__1")
 .Add btns.Add("cit", 34)
 .Add btns("__2")
 Set btn = btns.Add("ad.")
 Set btn.Bar = tb(0).CommandBars("Add")
 btn.ShowCaptionInToolbar = True
 .Add btn
End With
Set tb(3).Toolbar = tb(0).CommandBars("Tools")
'/////////////////////////////////////////
AttachMessage Me, Me.hwnd, &H2005&
TheButton
'/////////////////////////////////////////
If command <> "" Then
 LoadFile command
 ChangeTitle
 txtLv_Change
End If
luke = True
RedrawTools
End Sub

Private Sub TheButton()
On Error Resume Next
If xxN > 0 Then
 With tb(0)
 .Buttons("file").Caption = LoadResString(101)
 .Buttons("new__1").Caption = LoadResString(102)
 .Buttons("opn__1").Caption = LoadResString(103)
 .Buttons("sav__1").Caption = LoadResString(104)
 .Buttons("sas__1").Caption = LoadResString(105)
 .Buttons("ext__1").Caption = LoadResString(123)
 .Buttons("edit").Caption = LoadResString(106)
 .Buttons("add").Caption = LoadResString(110)
 .Buttons("ins").Caption = LoadResString(111)
 .Buttons("clr").Caption = LoadResString(112)
 .Buttons("del").Caption = LoadResString(113)
 .Buttons("for__1").Caption = LoadResString(114)
 .Buttons("bak__1").Caption = LoadResString(115)
 .Buttons("cut__1").Caption = LoadResString(116)
 .Buttons("cpy__1").Caption = LoadResString(117)
 .Buttons("pas__1").Caption = LoadResString(118)
 .Buttons("view").Caption = LoadResString(119)
 .Buttons("got__1").Caption = LoadResString(151)
 .Buttons("help").Caption = LoadResString(121)
 .Buttons("abt").Caption = LoadResString(122)
 .Buttons("aaa").Caption = LoadResString(165)
 .Buttons("ad.").Caption = LoadResString(166)
 .Buttons("up_").Caption = "���Ϲ���(&U)"
 .Buttons("dwn").Caption = "���¹���(&D)"
 .Buttons("lft").Caption = "�������(&L)"
 .Buttons("rit").Caption = "���ҹ���(&R)"
 .Buttons("xfl").Caption = "ˮƽ��ת(&H)"
 .Buttons("yfl").Caption = "��ֱ��ת(&V)"
 .Buttons("scr").Caption = "�ű��༭��(&S)"
 .Buttons("tst__1").Caption = "����(&T)"
 .Buttons("__i").Caption = "����(&I)"
 .Buttons("__e").Caption = "����(&E)"
 .Buttons("new__2").ToolTip = LoadResString(130)
 .Buttons("opn__2").ToolTip = LoadResString(131)
 .Buttons("sav__2").ToolTip = LoadResString(132)
 .Buttons("cut__2").ToolTip = LoadResString(133)
 .Buttons("cpy__2").ToolTip = LoadResString(134)
 .Buttons("pas__2").ToolTip = LoadResString(135)
 .Buttons("zom").ToolTip = LoadResString(136)
 .Buttons("way").ToolTip = LoadResString(137)
 .Buttons("fan").ToolTip = LoadResString(138)
 .Buttons("add__2").ToolTip = LoadResString(154)
 .Buttons("ins__2").ToolTip = LoadResString(155)
 .Buttons("clr__2").ToolTip = LoadResString(156)
 .Buttons("del__2").ToolTip = LoadResString(157)
 .Buttons("bak__2").ToolTip = LoadResString(158)
 .Buttons("for__2").ToolTip = LoadResString(159)
 .Buttons("got__2").ToolTip = LoadResString(160)
 .Buttons("tst__2").ToolTip = "����"
 .Buttons("add__3").ToolTip = "����"
 .Buttons("del__3").ToolTip = "ɾ��"
 .Buttons("xp!1").ToolTip = "����"
 .Buttons("xp!2").ToolTip = "����"
 .Buttons("xp!3").ToolTip = "�¼�"
 .Buttons("up___2").ToolTip = "����"
 .Buttons("dwn__2").ToolTip = "����"
 .Buttons("chs").Checked = True
 End With
 Label1.Caption = LoadResString(127)
 Label2.Caption = LoadResString(128)
 Label3.Caption = LoadResString(129)
Else
 With tb(0)
 .Buttons("file").Caption = "&File"
 .Buttons("new__1").Caption = "&New"
 .Buttons("opn__1").Caption = "&Open"
 .Buttons("sav__1").Caption = "&Save"
 .Buttons("sas__1").Caption = "Save &As"
 .Buttons("ext__1").Caption = "E&xit"
 .Buttons("edit").Caption = "&Edit"
 .Buttons("add").Caption = "A&dd Level"
 .Buttons("ins").Caption = "&Insert Level"
 .Buttons("clr").Caption = "C&lear Level"
 .Buttons("del").Caption = "&Delete Level"
 .Buttons("for__1").Caption = "Move &Forward"
 .Buttons("bak__1").Caption = "Move &Backward"
 .Buttons("cut__1").Caption = "C&ut Level"
 .Buttons("cpy__1").Caption = "C&opy Level"
 .Buttons("pas__1").Caption = "P&aste Level"
 .Buttons("__i").Caption = "&Import"
 .Buttons("__e").Caption = "&Export"
 .Buttons("view").Caption = "&View"
 .Buttons("got__1").Caption = "&Go to..."
 .Buttons("help").Caption = "&Help"
 .Buttons("abt").Caption = "&About"
 .Buttons("aaa").Caption = "Item Box"
 .Buttons("ad.").Caption = "Add Item"
 .Buttons("up_").Caption = "Slide &Up"
 .Buttons("dwn").Caption = "Slide &Down"
 .Buttons("lft").Caption = "Slide &Left"
 .Buttons("rit").Caption = "Slide &Right"
 .Buttons("xfl").Caption = "Flip &Horizontal"
 .Buttons("yfl").Caption = "Flip &Vertical"
 .Buttons("scr").Caption = "&Script Editor"
 .Buttons("tst__1").Caption = "&Test Play"
 .Buttons("new__2").ToolTip = "New"
 .Buttons("opn__2").ToolTip = "Open"
 .Buttons("sav__2").ToolTip = "Save"
 .Buttons("cut__2").ToolTip = "Cut Level"
 .Buttons("cpy__2").ToolTip = "Copy Level"
 .Buttons("pas__2").ToolTip = "Paste Level"
 .Buttons("zom").ToolTip = "Zoom Out"
 .Buttons("way").ToolTip = "Show Way"
 .Buttons("fan").ToolTip = "Show Fan Wind"
 .Buttons("add__2").ToolTip = "Add Level"
 .Buttons("ins__2").ToolTip = "Insert Level"
 .Buttons("clr__2").ToolTip = "Clear Level"
 .Buttons("del__2").ToolTip = "Delete Level"
 .Buttons("bak__2").ToolTip = "Move Backward"
 .Buttons("for__2").ToolTip = "Move Forward"
 .Buttons("got__2").ToolTip = "Go to"
 .Buttons("tst__2").ToolTip = "Test Play"
 .Buttons("add__3").ToolTip = "Add"
 .Buttons("del__3").ToolTip = "Delete"
 .Buttons("xp!1").ToolTip = "Properties"
 .Buttons("xp!2").ToolTip = "Conditions"
 .Buttons("xp!3").ToolTip = "Events"
 .Buttons("up___2").ToolTip = "Move Up"
 .Buttons("dwn__2").ToolTip = "Move Down"
 .Buttons("eng").Checked = True
 End With
 Label1.Caption = "Level Pack Name"
 Label2.Caption = "Level Name"
 Label3.Caption = "Level Hint"
End If
End Sub

Private Sub Form_Resize()
On Error Resume Next
Dim the17 As Long, the5 As Long
Dim nt As Integer, nh As Integer
Dim picsw As Integer, picsh As Integer
the5 = IIf(Me.ScaleMode = 3, 7, 7 * 15)
the17 = IIf(Me.ScaleMode = 3, 17, 17 * 15)
nt = tb(0).Height + tb(1).Height + tb(2).Height
nh = Me.ScaleHeight - tb(3).Height
'If tab1.Pinned Then
If picResize.Tag = "" Then
 picName.Visible = True
 picName.Move Me.ScaleWidth - picName.Width, nt, picName.Width, nh - nt
 picLv.Move 0, nt, picName.Left - the17 - the5, nh - nt - the17
Else
 picName.Visible = False
 picLv.Move 0, nt, Me.ScaleWidth - the17 - the5, nh - nt - the17
End If
Hs1.Move 0, nh - the17, picLv.Width, the17
Vs1.Move picLv.Left + picLv.Width, nt, the17, nh - nt - the17
If picResize.Tag = "" Then
 picResize.Move picName.Left - the5, nt, the5, nh - nt
Else
 picResize.Move Me.ScaleWidth - the5, nt, the5, nh - nt
End If
'End If
picsw = picLv.ScaleWidth
picsh = picLv.ScaleHeight
If ZoomOff Then picsw = picsw * 2: picsh = picsh * 2
Hs1.Enabled = 640 > picsw
Hs1.max = 640 - picsw
Hs1.LargeChange = picsw
Vs1.Enabled = 400 > picsh
Vs1.max = 400 - picsh
Vs1.LargeChange = picsh
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim i As Integer
On Error Resume Next
Kill CStr(App.Path) + "\test.dwl"
DetachMessage Me, Me.hwnd, &H2005&
If GameWnd <> 0 Then ExitDweep

    'close all sub forms
    For i = Forms.count - 1 To 1 Step -1
        Unload Forms(i)
    Next
''''
End Sub

Private Sub mnuChs_Click()
reg2_SetSettings "Language", "1"
reg2_SaveFile
xxN = 1
DoEvents
TheButton
End Sub

Private Sub mnuCL_Click()
Dim s As String
If xxN = 0 Then
 s = "Really to clear this level?"
Else
 s = LoadResString(125)
End If
If MsgBox(s, vbQuestion + vbYesNo) = vbYes Then
 Erase lvDat(lvNow).lvData
End If
DrawMap
txtLv_Change
End Sub

Private Sub mnuCoL_Click()
lvCB = lvDat(lvNow)
End Sub

Private Sub mnuCuL_Click()
Dim i As Integer
Dim s As String
If xxN = 0 Then
 s = "Click 'Yes' to cut this level, or click 'No' to cut the level data."
Else
 s = LoadResString(139)
End If
Select Case MsgBox(s, vbQuestion + vbYesNoCancel)
Case vbYes
 lvCB = lvDat(lvNow)
 For i = lvNow To lvMax
  lvDat(i) = lvDat(i + 1)
 Next i
 Erase lvDat(lvMax).lvData, lvDat(lvMax).lvTools
 lvDat(lvMax).lvName = ""
 lvDat(lvMax).lvHint = ""
 lvMax = lvMax - 1
Case vbNo
 lvCB = lvDat(lvNow)
 Erase lvDat(lvNow).lvData, lvDat(lvNow).lvTools
 lvDat(lvNow).lvName = "Unnamed"
 lvDat(lvNow).lvHint = ""
End Select
DrawMap
txtLv_Change
End Sub

Private Sub mnuDL_Click()
Dim i As Integer
Dim s As String
If xxN = 0 Then
 s = "Really to delete this level?"
Else
 s = LoadResString(140)
End If
If lvMax <= 1 Then
 If xxN > 0 Then
  MsgBox LoadResString(503), vbCritical
 Else
  MsgBox "Couldn't delete this level.", vbCritical
 End If
Else
 If MsgBox(s, vbQuestion + vbYesNo) = vbYes Then
  For i = lvNow To lvMax - 1
   lvDat(i) = lvDat(i + 1)
  Next i
  Erase lvDat(lvMax).lvData, lvDat(lvMax).lvTools
  lvDat(lvMax).lvName = ""
  lvDat(lvMax).lvHint = ""
  lvMax = lvMax - 1
 End If
End If
DrawMap
txtLv_Change
End Sub

Private Sub mnuEng_Click()
reg2_SetSettings "Language", "0"
reg2_SaveFile
xxN = 0
DoEvents
TheButton
End Sub

Private Sub mnuGT_Click()
On Error GoTo a
Dim i As Long
For i = 1 To lvMax
 frmCheck.l1.AddItem CStr(i) + vbTab + lvDat(i).lvName
Next i
frmCheck.FormLoad lvNow
If frmCheck.xxxxxxx Then
 txtLv.Text = CStr(frmCheck.dgvh)
 txtLv_Change
 DrawMap
End If
a:
End Sub

Private Sub mnuIL_Click()
Dim i As Integer
Dim s As String
If xxN = 0 Then
 s = "Really to insert a level to there?"
Else
 s = LoadResString(141)
End If
If lvMax = 120 Then
 If xxN > 0 Then
  MsgBox LoadResString(501), vbCritical
 Else
  MsgBox "Level pack is full(max=120). Can't insert level.", vbCritical
 End If
Else
 If MsgBox(s, vbQuestion + vbYesNo) = vbYes Then
  For i = lvMax To lvNow Step -1
   lvDat(i + 1) = lvDat(i)
  Next i
  Erase lvDat(lvNow).lvData, lvDat(lvNow).lvTools
  lvDat(lvNow).lvName = IIf(xxN = 0, "Unnamed", LoadResString(142))
  lvDat(lvNow).lvHint = ""
  lvMax = lvMax + 1
 End If
End If
DrawMap
txtLv_Change
End Sub

Private Sub mnuMB_Click()
Dim var1 As tLevel
Dim s As String
If xxN = 0 Then
 s = "Really to move this level back?"
Else
 s = LoadResString(143)
End If
If lvNow = 1 Then
 If xxN > 0 Then
  MsgBox LoadResString(502), vbCritical
 Else
  MsgBox "First level can't move back.", vbCritical
 End If
Else
 If MsgBox(s, vbQuestion + vbYesNo) = vbYes Then
  var1 = lvDat(lvNow - 1)
  lvDat(lvNow - 1) = lvDat(lvNow)
  lvDat(lvNow) = var1
  lvNow = lvNow - 1
 End If
End If
DrawMap
txtLv_Change
End Sub

Private Sub mnuMF_Click()
Dim var1 As tLevel
Dim s As String
If xxN = 0 Then
 s = "Really to move this level forward?"
Else
 s = LoadResString(143)
End If
If lvNow >= lvMax Then
 If xxN > 0 Then
  MsgBox LoadResString(502), vbCritical
 Else
  MsgBox "Last level can't move forward.", vbCritical
 End If
Else
 If MsgBox(s, vbQuestion + vbYesNo) = vbYes Then
  var1 = lvDat(lvNow + 1)
  lvDat(lvNow + 1) = lvDat(lvNow)
  lvDat(lvNow) = var1
  lvNow = lvNow + 1
 End If
End If
DrawMap
txtLv_Change
End Sub

Private Sub mnuPL_Click()
Dim i As Integer
Dim s As String
If xxN = 0 Then
 s = "Click 'Yes' to insert to there, or click 'No' to overlay the level data."
Else
 s = LoadResString(144)
End If
Select Case MsgBox(s, vbQuestion + vbYesNoCancel)
Case vbYes 'error checking!!!
 If lvMax = 120 Then
  If xxN > 0 Then
   MsgBox LoadResString(501), vbCritical
  Else
   MsgBox "Level pack is full(max=120). Can't insert level.", vbCritical
  End If
 Else
  For i = lvMax To lvNow Step -1
   lvDat(i + 1) = lvDat(i)
  Next i
  lvDat(lvNow) = lvCB
  lvMax = lvMax + 1
 End If
Case vbNo
 lvDat(lvNow) = lvCB
End Select
DrawMap
txtLv_Change
End Sub

Private Property Let ISubclass_MsgResponse(ByVal RHS As EMsgResponse)
'
End Property

Private Property Get ISubclass_MsgResponse() As EMsgResponse
'
End Property

Private Function ISubclass_WindowProc(ByVal hwnd As Long, ByVal iMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
On Error Resume Next
Select Case wParam
Case 1
 Me.Enabled = True
 BringWindowToTop Me.hwnd
 FlashWindow Me.hwnd, 1
Case 2
 GameWnd = lParam
Case 128
 Me.Enabled = True
 BringWindowToTop Me.hwnd
 FlashWindow Me.hwnd, 1
 GameWnd = 0
End Select
End Function

Private Sub picLv_MouseDown(button As Integer, Shift As Integer, x As Single, y As Single)
On Error Resume Next
Dim nX As Integer, nY As Integer
Dim i As Integer, j As Integer
Dim nItem As Integer
nX = Int((x + Hs1.value) / GridWidth) + 1
nY = Int((y + Vs1.value) / GridWidth) + 1

Select Case DrawType
Case 1, 2

Select Case button
Case 1
 nItem = lvItem
Case 2
 nItem = 0
End Select
If nItem = 5 Then
 For i = 1 To 16
  For j = 1 To 10
   If lvDat(lvNow).lvData(i, j) = 5 Then lvDat(lvNow).lvData(i, j) = 0
  Next j
 Next i
End If
Select Case lvDat(lvNow).lvData(nX, nY)
Case 6, 7, 8, 9
 lvDat(lvNow).lvData(nX, nY) = nItem
 A_Laser
Case Else
 lvDat(lvNow).lvData(nX, nY) = nItem
 Select Case nItem
 Case 6, 7, 8, 9
  A_Laser
 Case Else
  If lvLaser(nX, nY) > 0 Then A_Laser
 End Select
End Select
DrawMap

Case 3

startX = nX
startY = nY
lastX = nX
lastY = nY
DrawBox nX, nY, nX, nY

End Select
End Sub

Private Sub picLv_MouseMove(button As Integer, Shift As Integer, x As Single, y As Single)
On Error Resume Next
Dim nX As Integer, nY As Integer
nX = Int((x + Hs1.value) / GridWidth) + 1
nY = Int((y + Vs1.value) / GridWidth) + 1
tb(0).Buttons("tip").Caption = IIf(nX > 0 And nX <= 16 And nY > 0 And nY <= 10, "X:" + CStr(nX) + ",Y:" + CStr(nY), "")
Select Case DrawType
Case 2

If button = 1 Then
 If nX <> lastX Or nY <> lastY Then
  lastX = nX
  lastY = nY
  picLv_MouseDown button, Shift, x, y
 End If
End If

Case 3

If button = 1 Then
 DrawBox startX, startY, lastX, lastY
 DrawBox startX, startY, nX, nY
 lastX = nX
 lastY = nY
 RedrawMap
End If

End Select
End Sub

Private Sub picLv_MouseUp(button As Integer, Shift As Integer, x As Single, y As Single)
On Error Resume Next
Dim nX As Integer, nY As Integer
Dim i As Integer, j As Integer
lastX = -1

Select Case DrawType
Case 3

If button = 1 Then
 nX = Int((x + Hs1.value) / GridWidth) + 1
 nY = Int((y + Vs1.value) / GridWidth) + 1
 For i = startX To nX Step IIf(startX < nX, 1, -1)
  For j = startY To nY Step IIf(startY < nY, 1, -1)
   lvDat(lvNow).lvData(i, j) = lvItem
  Next j
 Next i
 A_Laser
 DrawMap
End If

End Select
End Sub


Private Sub picTool_MouseDown(ByVal nX As Long, ByVal nY As Long)
On Error Resume Next
Dim i As Integer, isNothing As Boolean
Dim s As String
If xxN = 0 Then
 s = "Really to deleting tools?"
Else
 s = LoadResString(145)
End If
If nY = 1 Then
 isNothing = True
 For i = 1 To 11
  If lvDat(lvNow).lvTools(i).tlName = 9 Then isNothing = False
 Next i
 For i = 1 To 10
  If lvDat(lvNow).lvTools(i).tlName = 9 Then Exit For
 Next i
 If isNothing Then
  For i = 10 To 1 Step -1
   If lvDat(lvNow).lvTools(i).tlName > 0 Then i = i + 1: Exit For
  Next i
  If i = 0 Then i = 1
 End If
 If i = 11 Or nX = 18 Then
  Beep
 Else
  lvDat(lvNow).lvTools(i) = ChangeTools(nX)
  lvDat(lvNow).lvTools(i + 1) = ChangeTools(18)
 End If
Else
 Select Case nX
 Case 1 To 10
  For i = nX To 10
   lvDat(lvNow).lvTools(i) = lvDat(lvNow).lvTools(i + 1)
  Next i
  lvDat(lvNow).lvTools(i + 1) = ChangeTools(17)
 Case 11
  If MsgBox(s, vbQuestion + vbYesNo) = vbYes Then
   For i = 1 To 11
    If i = 2 Then
     lvDat(lvNow).lvTools(i) = ChangeTools(18)
    Else
     lvDat(lvNow).lvTools(i) = ChangeTools(17)
    End If
   Next i
   RedrawTools
  End If
 Case Else
  Beep
 End Select
End If
RedrawTools
End Sub

Private Function ChangeTools(ByVal tlNo As Integer) As tTool
Dim a As Byte, b As Byte
Select Case tlNo
Case 1 To 4
 a = 1
 b = tlNo - 1
Case 5, 6
 a = 2
 b = tlNo - 5
Case 7 To 10
 a = 3
 b = tlNo - 7
Case 11
 a = 6
 b = 0
Case 12, 13
 a = 4
 b = tlNo - 12
Case 14
 a = 5
 b = 0
Case 15, 16
 a = tlNo - 8
 b = 0
Case 17
 a = 0
 b = 0
Case 18
 a = 9
 b = 0
End Select
ChangeTools.tlName = a
ChangeTools.tlType = b
End Function

Private Sub picName_Resize()
On Error Resume Next
Dim r As Long
r = IIf(picName.ScaleMode = 3, 1, 15)
txtlpName.Move 200 * r, 8 * r, picName.ScaleWidth - 208 * r
txtLvName.Move 72 * r, 32 * r, picName.ScaleWidth - 80 * r
txtLvHint.Move 8 * r, 64 * r, picName.ScaleWidth - 16 * r, picName.ScaleHeight - 72 * r
End Sub

Private Sub picResize_Click()
If rsOn Then
 picResize.Tag = IIf(picResize.Tag = "", vbCrLf, "")
 Form_Resize
 pRedraw
End If
End Sub

Private Sub picResize_MouseMove(button As Integer, Shift As Integer, x As Single, y As Single)
Dim p As POINTAPI, h As Long
Dim yy As Long
Static r As RECT
If button = 1 And Not rsOn Then
 If picResize.Tag = "" Then
  h = GetWindowDC(0)
  'DrawFocusRect h, r
  BitBlt h, r.Left, r.TOp, 7, r.Bottom - r.TOp, h, 0, 0, vbDstInvert
  GetWindowRect picResize.hwnd, r
  GetCursorPos p
  r.Left = p.x - 3
  r.Right = p.x + 3
  'DrawFocusRect h, r
  BitBlt h, r.Left, r.TOp, 7, r.Bottom - r.TOp, h, 0, 0, vbDstInvert
  ReleaseDC 0, h
 End If
Else
 yy = picResize.ScaleHeight \ 2 - 24
 If y > yy And y < yy + 48 Then
  If Not rsOn Then
   rsOn = True
   Timer1.Enabled = True
   picResize.MousePointer = 1
   pRedraw
  End If
 ElseIf rsOn Then
  rsOn = False
  Timer1.Enabled = False
  picResize.MousePointer = 9
  pRedraw
 End If
End If
End Sub

Private Sub picResize_Paint()
bmpResize.PaintPicture picResize.hDC
End Sub

Private Sub picResize_Resize()
bmpResize.Create 7, picResize.ScaleHeight
pRedraw
End Sub

Private Sub tb_ButtonClick(index As Integer, btn As cButton)
Dim i As Long
Select Case Left(btn.Key, 3)
Case "new": mnuFileNew_Click
Case "opn": mnuFileOpen_Click
Case "sav": mnuFileSave_Click
Case "sas": mnuFileSaveAs_Click
Case "__i": mnuFileOpen_Click True
Case "__e": mnuFileSaveAs_Click True
Case "ext": mnuFileExit_Click
Case "cut": mnuCuL_Click
Case "cpy": mnuCoL_Click
Case "pas": mnuPL_Click
Case "add": cmdAL_Click
Case "ins": mnuIL_Click
Case "clr": mnuCL_Click
Case "del": mnuDL_Click
Case "for": mnuMF_Click
Case "bak": mnuMB_Click
Case "got": mnuGT_Click
Case "eng": mnuEng_Click
Case "chs": mnuChs_Click
Case "abt": mnuHelpAbout_Click
Case "zom"
 ZoomOff = btn.Checked
 With tb(0).Buttons("opt3")
  .Enabled = Not ZoomOff
  If DrawType = 3 And ZoomOff Then
   DrawType = 1
   .Checked = False
   tb(0).Buttons("opt1").Checked = True
  End If
 End With
 GridWidth = IIf(ZoomOff, 20, 40)
 Form_Resize
 picLv.Cls
 RedrawMap
Case "way"
 ShowWay = btn.Checked
 DrawMap
Case "fan"
 ShowFan = btn.Checked
 DrawMap
Case "opt"
 DrawType = Val(Right(btn.Key, 1))
 If btn.Key = "opt1" Then
  tb(0).Buttons("itm05").Enabled = True
 Else
  tb(0).Buttons("itm05").Enabled = False
  If lvItem = 5 Then
   tb(0).Buttons("itm00").Checked = True
   lvItem = 0
  End If
 End If
Case "itm"
 lvItem = Val(Right(btn.Key, 2))
Case "las"
 With tb(0).Buttons("itx06")
  lvItem = Val(Right(btn.Key, 1))
  .IconIndex = lvItem
  .itemData = lvItem
  .Checked = True
 End With
Case "fas"
 With tb(0).Buttons("itx12")
  lvItem = Val(Right(btn.Key, 2))
  .IconIndex = lvItem
  .itemData = lvItem
  .Checked = True
 End With
Case "ils"
 With tb(0).Buttons("itx17")
  lvItem = Val(Right(btn.Key, 2))
  .IconIndex = lvItem
  .itemData = lvItem
  .Checked = True
 End With
Case "ifs"
 With tb(0).Buttons("itx23")
  lvItem = Val(Right(btn.Key, 2))
  .IconIndex = lvItem
  .itemData = lvItem
  .Checked = True
 End With
Case "box"
 picTool_MouseDown Val(Right(btn.Key, 2)), 0
Case "cit"
 picTool_MouseDown 11, 0
Case "its"
 picTool_MouseDown Val(Right(btn.Key, 2)), 1
Case "up_"
 RotLevel lvDat(lvNow), 0, -1
 DrawMap
Case "dwn"
 RotLevel lvDat(lvNow), 0, 1
 DrawMap
Case "lft"
 RotLevel lvDat(lvNow), -1, 0
 DrawMap
Case "rit"
 RotLevel lvDat(lvNow), 1, 0
 DrawMap
Case "xfl"
 XLevel lvDat(lvNow)
 DrawMap
Case "yfl"
 YLevel lvDat(lvNow)
 DrawMap
Case "scr"
 frmNothing.Show , Me
Case "tst"
 TestMap
End Select
End Sub

Private Sub tb_ButtonDropDown(index As Integer, btn As cButton, Cancel As Boolean)
Select Case Left(btn.Key, 3)
Case "itx"
 If btn.itemData = 0 Then
  lvItem = Val(Right(btn.Key, 2))
 Else
  lvItem = btn.itemData
 End If
End Select
End Sub


Private Sub tb_RequestNewInstance(index As Integer, ctl As Object)
Dim obj As VBControlExtender
Load tb(tb.count)
Set obj = tb(tb.UBound)
obj.Align = 0
Set ctl = obj
End Sub

Private Sub Timer1_Timer()
Dim p As POINTAPI
GetCursorPos p
If WindowFromPoint(p.x, p.y) <> picResize.hwnd Then
 rsOn = False
 Timer1.Enabled = False
 picResize.MousePointer = 9
 pRedraw
End If
End Sub

Private Sub txtlpName_Change()
lpName = txtlpName.Text
End Sub

Private Sub txtLvHint_Change()
lvDat(lvNow).lvHint = txtLvHint.Text
End Sub

Private Sub txtLvName_Change()
lvDat(lvNow).lvName = txtLvName.Text
End Sub

Private Sub Vs1_Change()
RedrawMap
End Sub
Private Sub Vs1_scroll()
RedrawMap
End Sub
Private Sub hs1_Change()
RedrawMap
End Sub
Private Sub hs1_scroll()
RedrawMap
End Sub

Private Sub picLv_Paint()
RedrawMap
End Sub

Private Sub picResize_MouseDown(button As Integer, Shift As Integer, x As Single, y As Single)
Dim h As Long, r As RECT
If button = 1 And Not rsOn And picResize.Tag = "" Then
 rsX = x
 h = GetWindowDC(0)
 GetWindowRect picResize.hwnd, r
 BitBlt h, r.Left, r.TOp, 7, r.Bottom - r.TOp, h, 0, 0, vbDstInvert
 ReleaseDC 0, h
End If
End Sub

Private Sub picResize_MouseUp(button As Integer, Shift As Integer, x As Single, y As Single)
On Error Resume Next
'Dim w1 As New clsHwnd
If button = 1 And Not rsOn And picResize.Tag = "" Then
 InvalidateRect Me.hwnd, ByVal 0, 1
 rsX = x - rsX
 picName.Width = picName.Width - rsX
 rsX = 0
 Form_Resize
End If
End Sub

Private Sub mnuHelpAbout_Click()
On Error Resume Next
    frmAbout.Show vbModal, Me
End Sub

Private Sub mnuFileExit_Click()
    'unload the form
    Unload Me

End Sub

Private Sub mnuFileSaveAs_Click(Optional ByVal b As Boolean)
Dim s As String
'On Error GoTo a
If b Then
 cdFilter = LoadResString(6 + xxN)
 If cd.VBGetSaveFileName(s, , , cdFilter, , CStr(App.Path), IIf(xxN, "����", "Export"), "dwp", Me.hwnd) Then
  SaveFile s, True
 End If
Else
 cdFilter = LoadResString(4 + xxN)
 If cd.VBGetSaveFileName(s, , , cdFilter, , CStr(App.Path), , "dwp", Me.hwnd) Then
  SaveFile s
  ChangeTitle
 End If
End If
a:
End Sub

Private Sub mnuFileSave_Click()
If Filename = "?" Then mnuFileSaveAs_Click: Exit Sub
SaveFile Filename
End Sub

Private Sub mnuFileOpen_Click(Optional ByVal b As Boolean)
Dim s As String
On Error GoTo a
If b Then
 cdFilter = LoadResString(6 + xxN)
 If cd.VBGetOpenFileName(s, , , , , True, cdFilter, , CStr(App.Path), IIf(xxN, "����", "Import"), , Me.hwnd) Then
  If MsgBox(LoadResString(50 + xxN), vbInformation Or vbYesNo) = vbYes Then
   LoadFile s, True
   txtLv_Change
  End If
 End If
Else
 cdFilter = LoadResString(4 + xxN)
 If cd.VBGetOpenFileName(s, , , , , True, cdFilter, , CStr(App.Path), , , Me.hwnd) Then
  LoadFile s
  ChangeTitle
  txtLv_Change
 End If
End If
a:
End Sub

Private Sub mnuFileNew_Click()
Dim s As String
If xxN = 0 Then
 s = "Really clear all levels?"
Else
 s = LoadResString(146)
End If
If MsgBox(s, vbQuestion + vbYesNo) = vbYes Then
 Erase lvDat
 lvNow = 1
 lvMax = 1
 lpName = IIf(xxN = 0, "Unnamed Level Pack", LoadResString(147))
 txtlpName.Text = lpName
 Filename = "?"
 DrawMap
 txtLv_Change
 ChangeTitle
End If
End Sub

Private Sub ChangeTitle()
If Filename = "?" Then Me.Caption = AppTitle Else Me.Caption = Filename + " - " + AppTitle
End Sub

Private Function IsFakeDwp() As Boolean
Dim i As Long
If LOF(1) > 600 Then
 IsFakeDwp = False
Else
 Get #1, 47, i
 IsFakeDwp = i <> 1
End If
End Function

Private Sub LoadFile(ByVal s As String, Optional ByVal b As Boolean)
Dim var1 As Byte, lp As Long
Dim n As Long
Dim i As Long, j As Long
On Error GoTo a
Open s For Binary As #1
If LCase(Right(s, 4)) = ".dwl" Then
 If b Or Not luke Then
  lvDat(lvNow).lvName = GetString(1, 3, 48)
  lvDat(lvNow).lvHint = GetString(1, 51, 200)
  For j = 1 To 10
   For i = 1 To 16
    lp = 250& + (j - 1) * 16& + i
    Get #1, lp, var1
    lvDat(lvNow).lvData(i, j) = var1
   Next i
  Next j
  Get #1, 411, var1
  For i = 1 To var1
   lp = 411 + i
   Get #1, lp, var1
   lvDat(lvNow).lvTools(i) = ChangeTools(var1 - 100)
   lvDat(lvNow).lvTools(i + 1) = ChangeTools(18)
  Next i
 Else
  MsgBox LoadResString(52 + xxN), vbInformation
 End If
 Close
ElseIf IsFakeDwp Then 'fake dwp, but the file format=?
 If luke And Not b Then
  If MsgBox(LoadResString(54 + xxN), vbExclamation Or vbYesNo) = vbNo Then
   Close
   Exit Sub
  End If
 End If
 With lvDat(lvNow)
  .lvName = GetString(1, 47, 40)
  .lvHint = GetString(1, 87, 200)
  Get #1, 287, .lvData
  For i = 1 To 10 'i=11 is wrong!! there is extra data
   lp = 447 + (i - 1) * 8&
   Get #1, lp, var1
   .lvTools(i).tlName = var1
   lp = lp + 4
   Get #1, lp, var1
   .lvTools(i).tlType = var1
  Next i
  With .lvTools(11) 'fix it!
   .tlName = 0
   .tlType = 0
  End With
 End With
 Close
Else
 If b Then
  MsgBox LoadResString(56 + xxN), vbCritical
  Close
  Exit Sub
 End If
 Erase lvDat
 Filename = s
 lpName = GetString(1, 7, 40)
 lp = 47
 Get #1, lp, var1
 lvMax = var1
 For n = 1 To lvMax
  lvDat(n).lvName = GetString(1, 51& + 488& * (n - 1), 40)
  lvDat(n).lvHint = GetString(1, 91& + 488& * (n - 1), 200)
  For i = 1 To 16
   For j = 1 To 10
    lp = 274& + 488& * (n - 1) + j * 16& + i
    Get #1, lp, var1
    lvDat(n).lvData(i, j) = var1
   Next j
  Next i
  For i = 1 To 10 'i=11 is wrong!! there is extra data
   lp = 443& + 488& * (n - 1) + i * 8&
   Get #1, lp, var1
   lvDat(n).lvTools(i).tlName = var1
   lp = lp + 4
   Get #1, lp, var1
   lvDat(n).lvTools(i).tlType = var1
  Next i
  With lvDat(n).lvTools(11) 'fix it!
   .tlName = 0
   .tlType = 0
  End With
 Next n
 'For i = 1 To LvC
 ' varS = GetString(1, 51&+ 488&* (i - 1))
 ' Text1.Text = Text1.Text + "Level " + CStr(i) + " Name: " + varS
 ' Text1.Text = Text1.Text + enter
 ' varS = GetString(1, 91&+ 488&* (i - 1))
 ' Text1.Text = Text1.Text + "Level " + CStr(i) + " Hint: " + varS
 ' Text1.Text = Text1.Text + enter
 'Next i
 Close
 txtlpName.Text = lpName
 lvNow = 1
End If
Exit Sub
a:
If xxN > 0 Then
 MsgBox LoadResString(124), vbCritical
Else
 MsgBox "Internal Error.", vbCritical
End If
Close
End Sub

Private Function GetString(ByVal FileNo As Integer, ByVal lpStart As Long, Optional ByVal MaxLen As Long) As String
Dim lp As Long, str1 As String
Dim var1 As Byte, varc As Long
lp = lpStart
Do
 Get #FileNo, lp, var1
 If var1 > 128 Then
  varc = var1
  Get #FileNo, lp + 1, var1
  varc = varc * 256 + var1
  str1 = str1 + Chr(varc)
  lp = lp + 2
 Else
  str1 = str1 + Chr(var1)
  lp = lp + 1
 End If
Loop Until var1 = 0 Or (MaxLen > 0 And lp - lpStart >= MaxLen)
GetString = str1
End Function

Private Sub DrawItem(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal ItemNo As Integer)
Dim sx As Long
Select Case ItemNo
Case 1
 BitBlt hDC, x, y - 40, 40, 80, bmpItem1.hDC, 40, 80, vbSrcAnd
 BitBlt hDC, x, y - 40, 40, 80, bmpTexture.hDC, 40 * ((lvNow - 1) Mod 5), 40, vbSrcPaint
Case 0 To 5
 sx = ItemNo * 40
 BitBlt hDC, x, y - 40, 40, 80, bmpItem1.hDC, sx, 80, vbSrcAnd
 BitBlt hDC, x, y - 40, 40, 80, bmpItem1.hDC, sx, 0, vbSrcPaint
Case 6 To 9
 sx = (ItemNo - 6) * 60
 BitBlt hDC, x - 9, y - 20, 60, 60, bmpItem2.hDC, sx, 60, vbSrcAnd
 BitBlt hDC, x - 9, y - 20, 60, 60, bmpItem2.hDC, sx, 0, vbSrcPaint
Case 10 To 16
 sx = (ItemNo - 10) * 40
 BitBlt hDC, x, y - 40, 40, 80, bmpItem3.hDC, sx, 80, vbSrcAnd
 BitBlt hDC, x, y - 40, 40, 80, bmpItem3.hDC, sx, 0, vbSrcPaint
Case 17 To 32
 sx = (ItemNo - 17) * 40
 BitBlt hDC, x, y, 40, 40, bmpItem4.hDC, sx, 0, vbSrcCopy
End Select
End Sub

Private Sub DrawWind(ByVal hDC As Long, ByVal x As Integer, ByVal y As Integer, ByVal FS As Integer)
On Error Resume Next
Dim hobj As Long, var1 As POINTAPI
hobj = CreatePen(0, 3, vbRed)
hobj = SelectObject(hDC, hobj)
Select Case FS
Case 1
 MoveToEx hDC, x + 20, y + 30, var1
 LineTo hDC, x + 20, y + 10
 LineTo hDC, x + 15, y + 20
 MoveToEx hDC, x + 20, y + 10, var1
 LineTo hDC, x + 25, y + 20
Case 2
 MoveToEx hDC, x + 10, y + 30, var1
 LineTo hDC, x + 30, y + 10
 LineTo hDC, x + 20, y + 15
 MoveToEx hDC, x + 30, y + 10, var1
 LineTo hDC, x + 25, y + 20
Case 3
 MoveToEx hDC, x + 10, y + 20, var1
 LineTo hDC, x + 30, y + 20
 LineTo hDC, x + 20, y + 15
 MoveToEx hDC, x + 30, y + 20, var1
 LineTo hDC, x + 20, y + 25
Case 4
 MoveToEx hDC, x + 10, y + 10, var1
 LineTo hDC, x + 30, y + 30
 LineTo hDC, x + 25, y + 20
 MoveToEx hDC, x + 30, y + 30, var1
 LineTo hDC, x + 20, y + 25
Case 5
 MoveToEx hDC, x + 20, y + 10, var1
 LineTo hDC, x + 20, y + 30
 LineTo hDC, x + 15, y + 20
 MoveToEx hDC, x + 20, y + 30, var1
 LineTo hDC, x + 25, y + 20
Case 6
 MoveToEx hDC, x + 30, y + 10, var1
 LineTo hDC, x + 10, y + 30
 LineTo hDC, x + 20, y + 25
 MoveToEx hDC, x + 10, y + 30, var1
 LineTo hDC, x + 15, y + 20
Case 7
 MoveToEx hDC, x + 30, y + 20, var1
 LineTo hDC, x + 10, y + 20
 LineTo hDC, x + 20, y + 15
 MoveToEx hDC, x + 10, y + 20, var1
 LineTo hDC, x + 20, y + 25
Case 8
 MoveToEx hDC, x + 30, y + 30, var1
 LineTo hDC, x + 10, y + 10
 LineTo hDC, x + 15, y + 20
 MoveToEx hDC, x + 10, y + 10, var1
 LineTo hDC, x + 20, y + 15
End Select
DeleteObject hobj
End Sub

Private Sub RedrawTools()
On Error Resume Next
Dim i As Integer, j As Long
Dim dX As Integer
Dim endFlag As Boolean
tb(3).Visible = False
For i = 1 To 10
 If endFlag Then
  tb(0).Buttons("box" + Format(i, "00")).Visible = False
 Else
  j = lvDat(lvNow).lvTools(i).tlName * 10 + _
  lvDat(lvNow).lvTools(i).tlType
  j = ChangeItemData(j)
  With tb(0).Buttons("box" + Format(i, "00"))
   .Visible = True
   .IconIndex = j + 16
  End With
  If j = 0 Then
   endFlag = True
   i = i - 1
  End If
 End If
Next i
tb(0).Buttons("ad.").Enabled = endFlag
tb(3).Visible = True
End Sub

Private Sub DrawMap()
On Error GoTo a
Dim i As Integer, j As Integer
Dim i2 As Integer, j2 As Integer
Dim fanFS As Integer
Dim fanDat(1 To 4) As Integer
'  1    8 1 2
'  |     \|/
'4- -2  7- -3
'  |     /|\
'  3    6 5 4
A_Laser
For j = 1 To 10
 For i = 1 To 16
  BitBlt bmpBack.hDC, (i - 1) * 40, (j - 1) * 40, 40, 40, bmpTexture.hDC, 40 * ((lvNow - 1) Mod 5), 0, vbSrcCopy
  DrawItem bmpBack.hDC, (i - 1) * 40, (j - 1) * 40, lvDat(lvNow).lvData(i, j)
  Select Case lvLaser(i, j)
  Case 1 To 3
   BitBlt bmpBack.hDC, (i - 1) * 40, (j - 1) * 40, 40, 40, bmpL1.hDC, lvLaser(i, j) * 40 - 40, 40, vbSrcAnd
   BitBlt bmpBack.hDC, (i - 1) * 40, (j - 1) * 40, 40, 40, bmpL1.hDC, lvLaser(i, j) * 40 - 40, 0, vbSrcPaint
  Case 4 To 9
   BitBlt bmpBack.hDC, (i - 1) * 40, (j - 2) * 40, 40, 80, bmpL2.hDC, lvLaser(i, j) * 40 - 160, 80, vbSrcAnd
   BitBlt bmpBack.hDC, (i - 1) * 40, (j - 2) * 40, 40, 80, bmpL2.hDC, lvLaser(i, j) * 40 - 160, 0, vbSrcPaint
  End Select
 Next i
Next j
If ShowWay Then
 For i = 1 To 16
  For j = 1 To 10
   Select Case lvDat(lvNow).lvData(i, j)
   Case 1, 2, 3, 6 To 16
    BitBlt bmpBack.hDC, (i - 1) * 40, (j - 1) * 40, 40, 80, bmpEx.hDC, 80, 0, vbSrcAnd
    BitBlt bmpBack.hDC, (i - 1) * 40, (j - 1) * 40, 40, 80, bmpEx.hDC, 40, 0, vbSrcPaint
   Case 17 To 32
    BitBlt bmpBack.hDC, (i - 1) * 40, (j - 1) * 40, 40, 80, bmpEx.hDC, 80, 0, vbSrcAnd
    BitBlt bmpBack.hDC, (i - 1) * 40, (j - 1) * 40, 40, 80, bmpEx.hDC, 0, 0, vbSrcPaint
   End Select
  Next j
 Next i
End If
If ShowFan Then
 For i = 1 To 16
  For j = 1 To 10
   Erase fanDat
   '////////////////////////////////////////////////////////Wind for Direction
   If j <= 8 Then
    If lvDat(lvNow).lvData(i, j + 2) = 12 Then fanDat(1) = 1
   End If
   If j <= 9 Then
    If lvDat(lvNow).lvData(i, j + 1) = 12 Then fanDat(1) = 1
   End If
   If i >= 3 Then
    If lvDat(lvNow).lvData(i - 2, j) = 13 Then fanDat(2) = 1
   End If
   If i >= 2 Then
    If lvDat(lvNow).lvData(i - 1, j) = 13 Then fanDat(2) = 1
   End If
   If j >= 3 Then
    If lvDat(lvNow).lvData(i, j - 2) = 14 Then fanDat(3) = 1
   End If
   If j >= 2 Then
    If lvDat(lvNow).lvData(i, j - 1) = 14 Then fanDat(3) = 1
   End If
   If i <= 14 Then
    If lvDat(lvNow).lvData(i + 2, j) = 15 Then fanDat(4) = 1
   End If
   If i <= 15 Then
    If lvDat(lvNow).lvData(i + 1, j) = 15 Then fanDat(4) = 1
   End If
   '////////////////////////////////////////////////////////Trans Data
   fanFS = 0
   If fanDat(1) = 1 And fanDat(3) = 1 Then fanDat(1) = 0: fanDat(3) = 0
   If fanDat(2) = 1 And fanDat(4) = 1 Then fanDat(2) = 0: fanDat(4) = 0
   If fanDat(1) = 1 Then fanFS = 1
   If fanDat(3) = 1 Then fanFS = 5
   If fanDat(2) = 1 Then
    Select Case fanFS
    Case 0
     fanFS = 3
    Case 1
     fanFS = 2
    Case 5
     fanFS = 4
    End Select
   End If
   If fanDat(4) = 1 Then
    Select Case fanFS
    Case 0
     fanFS = 7
    Case 1
     fanFS = 8
    Case 5
     fanFS = 6
    End Select
   End If
   DrawWind bmpBack.hDC, (i - 1) * 40, (j - 1) * 40, fanFS
  Next j
 Next i
End If
RedrawMap
Exit Sub
a:
If xxN > 0 Then
 MsgBox LoadResString(124), vbCritical
Else
 MsgBox "Error!", vbCritical
End If
End Sub

Private Sub RedrawMap()
If ZoomOff Then
 StrBlt picLv.hDC, 0, 0, picLv.ScaleWidth, picLv.ScaleHeight, bmpBack.hDC, Hs1.value, Vs1.value, picLv.ScaleWidth * 2, picLv.ScaleHeight * 2, vbSrcCopy
Else
 BitBlt picLv.hDC, 0, 0, picLv.ScaleWidth, picLv.ScaleHeight, bmpBack.hDC, Hs1.value, Vs1.value, vbSrcCopy
End If
End Sub

Private Sub txtLv_Change()
On Error GoTo a
lvNow = Val(txtLv.Text)
If lvNow > 120 Then lvNow = 120: Beep
If lvNow < 1 Then lvNow = 1: Beep
txtLvName.Text = lvDat(lvNow).lvName
txtLvHint.Text = lvDat(lvNow).lvHint
'TODO:
A_Laser
DrawMap
RedrawTools
Exit Sub
a:
If xxN > 0 Then
 MsgBox LoadResString(124), vbCritical
Else
 Beep
End If
End Sub

Private Sub A_Laser()
Dim i As Integer, j As Integer
Dim nX As Integer, nY As Integer, nFs As Integer
'Dim xn As Integer, yn As Integer
Dim endFlag As Boolean
Erase lvLaser
For i = 1 To 16
 For j = 1 To 10
  nFs = lvDat(lvNow).lvData(i, j)
  Select Case nFs
  Case 6, 7, 8, 9
   nX = i: nY = j
   nFs = nFs - 5
   endFlag = False
   Do
    '//////////////////////Set Next Block
    Select Case nFs
    Case 1
     If nY > 1 Then nY = nY - 1 Else endFlag = True
    Case 2
     If nX < 16 Then nX = nX + 1 Else endFlag = True
    Case 3
     If nY < 10 Then nY = nY + 1 Else endFlag = True
    Case 4
     If nX > 1 Then nX = nX - 1 Else endFlag = True
    End Select
    If endFlag Then Exit Do
    '//////////////////////Set Next Direction and Add Data
    Select Case lvDat(lvNow).lvData(nX, nY)
    Case 0, 2, 3, 4, 16 To 32 '////////////////////Through
     Select Case lvLaser(nX, nY)
     Case 0
      lvLaser(nX, nY) = 2 - nFs Mod 2
     Case 1
      If nFs Mod 2 = 0 Then lvLaser(nX, nY) = 3
     Case 2
      If nFs Mod 2 = 1 Then lvLaser(nX, nY) = 3
     End Select
    Case 1, 5, 6, 7, 8, 9, 12, 13, 14, 15 '////////End
     lvLaser(nX, nY) = 10
     endFlag = True
    Case 10 '//////////////////////////////////////Mirror NE-WS
     If nFs Mod 2 = 0 Then nFs = nFs - 1 Else nFs = nFs + 1
     Select Case lvLaser(nX, nY)
     Case 0
      If nFs = 2 Or nFs = 3 Then lvLaser(nX, nY) = 4 Else lvLaser(nX, nY) = 5
     Case 4
      If nFs = 1 Or nFs = 4 Then lvLaser(nX, nY) = 6
     Case 5
      If nFs = 2 Or nFs = 3 Then lvLaser(nX, nY) = 6
     End Select
    Case 11 '//////////////////////////////////////Mirror NW-ES
     nFs = 5 - nFs
     Select Case lvLaser(nX, nY)
     Case 0
      If nFs > 2 Then lvLaser(nX, nY) = 7 Else lvLaser(nX, nY) = 8
     Case 7
      If nFs < 3 Then lvLaser(nX, nY) = 9
     Case 8
      If nFs > 2 Then lvLaser(nX, nY) = 9
     End Select
    End Select
   Loop Until endFlag
  End Select
 Next j
Next i
End Sub

Private Function ChangeItemData(ByVal id As Integer) As Integer
Select Case id
Case 0, 100, 117
 ChangeItemData = 17
Case 10, 101
 ChangeItemData = 1
Case 11, 102
 ChangeItemData = 2
Case 12, 103
 ChangeItemData = 3
Case 13, 104
 ChangeItemData = 4
Case 20, 105
 ChangeItemData = 5
Case 21, 106
 ChangeItemData = 6
Case 30, 107
 ChangeItemData = 7
Case 31, 108
 ChangeItemData = 8
Case 32, 109
 ChangeItemData = 9
Case 33, 110
 ChangeItemData = 10
Case 40, 112
 ChangeItemData = 12
Case 41, 113
 ChangeItemData = 13
Case 50, 114
 ChangeItemData = 14
Case 60, 111
 ChangeItemData = 11
Case 70, 115
 ChangeItemData = 15
Case 80, 116
 ChangeItemData = 16
End Select
End Function

Private Sub SetStringToProp(ByVal hwnd As Long, ByVal PropName As String, ByVal s As String)
Dim i As Long, m As Long
m = Len(s)
SetProp hwnd, PropName + "_Count", m
For i = 1 To m
 SetProp hwnd, PropName + "__" + CStr(i), Asc(Mid(s, i, 1))
Next i
End Sub

Private Function GetStringFromProp(ByVal hwnd As Long, ByVal PropName As String) As String
Dim s As String
Dim i As Long, m As Long
m = GetPropAndDeleteIt(hwnd, PropName + "_Count")
For i = 1 To m
 s = s + Chr(GetPropAndDeleteIt(hwnd, PropName + "__" + CStr(i)))
Next i
GetStringFromProp = s
End Function

Private Function GetPropAndDeleteIt(ByVal hwnd As Long, ByVal PropName As String) As Long
GetPropAndDeleteIt = GetProp(hwnd, PropName)
RemoveProp hwnd, PropName
End Function

Private Sub SaveFile(ByVal s As String, Optional b As Boolean)
Dim var1 As Byte, lp As Long
Dim n As Long
Dim i As Long, j As Long
Dim s1 As String * 46
On Error GoTo a
If s = vbNullChar Then 'xxxx!!!!
 SetProp Me.hwnd, "__lvNo", lvNow
 SetStringToProp Me.hwnd, "__lvName", Trim(lvDat(lvNow).lvName)
 SetStringToProp Me.hwnd, "__lvHint", Trim(lvDat(lvNow).lvHint)
 For j = 1 To 10
  For i = 1 To 16 Step 4
   CopyMemory n, lvDat(lvNow).lvData(i, j), 4&
   SetProp Me.hwnd, "__lvDat__" + CStr(i) + "_" + CStr(j), n
  Next i
 Next j
 For i = 1 To 10
  With lvDat(lvNow).lvTools(i)
   n = 100 + ChangeItemData(.tlName * 10 + .tlType)
   If n Mod 17 = 15 And i > 1 Then
    SetProp Me.hwnd, "__lvTC", i - 1
    Exit For
   End If
   SetProp Me.hwnd, "__lvT__" + CStr(i), n
  End With
 Next i
ElseIf LCase(Right(s, 4)) = ".dwl" Then
 Open s For Binary As #1
 var1 = 1
 Put #1, 1, var1
 SetString 1, 3, Trim(lvDat(lvNow).lvName), 48
 SetString 1, 51, Trim(lvDat(lvNow).lvHint), 200
 For j = 1 To 10
  For i = 1 To 16
   lp = 250& + (j - 1) * 16& + i
   var1 = lvDat(lvNow).lvData(i, j)
   Put #1, lp, var1
  Next i
 Next j
 For i = 1 To 10
  lp = 411 + i
  With lvDat(lvNow).lvTools(i)
   var1 = 100 + ChangeItemData(.tlName * 10 + .tlType)
   If var1 Mod 17 = 15 And i > 1 Then Exit For
   Put #1, lp, var1
  End With
 Next i
 var1 = i - 1
 Put #1, 411, var1
ElseIf b Then
 s1 = "Dweep" + vbNullChar + "Copyright   2000 by Dexterity Software." + vbNullChar
 Open s For Binary As #1
 Put #1, 1, s1
 var1 = &HA9 'lui!
 Put #1, 17, var1
 SetString 1, 47, Trim(lvDat(lvNow).lvName), 40
 SetString 1, 87, Trim(lvDat(lvNow).lvHint), 200
 Put #1, 287, lvDat(lvNow).lvData
 For i = 1 To 10 '11=error
  j = lvDat(lvNow).lvTools(i).tlName
  Put #1, 447 + (i - 1) * 8, j
  j = lvDat(lvNow).lvTools(i).tlType
  Put #1, 451 + (i - 1) * 8, j
 Next i
 i = (lvNow - 1) Mod 5
 Put #1, 535, i
Else
 Open s For Binary As #1
 Filename = s
 Put #1, 1, 68
 Put #1, 2, 119
 Put #1, 3, 101
 Put #1, 4, 101
 Put #1, 5, 112
 Put #1, 6, 0
 SetString 1, 7, Trim(lpName), 40
 Put #1, 47, lvMax
 Put #1, 48, 0
 Put #1, 49, 0
 Put #1, 50, 0
 For n = 1 To lvMax
  'lvDat(n).lvName = GetString(1, 51&+ 488&* (n - 1), 40)
  'lvDat(n).lvHint = GetString(1, 91&+ 488&* (n - 1), 200)
  SetString 1, 51& + 488& * (n - 1), Trim(lvDat(n).lvName), 40
  SetString 1, 91& + 488& * (n - 1), Trim(lvDat(n).lvHint), 200
  For i = 1 To 16
   For j = 1 To 10
    lp = 274& + 488& * (n - 1) + j * 16& + i
    var1 = lvDat(n).lvData(i, j)
    Put #1, lp, var1
   Next j
  Next i
  For i = 1 To 10 '11=error
   lp = 443& + 488& * (n - 1) + i * 8#
   var1 = lvDat(n).lvTools(i).tlName
   Put #1, lp, var1
   Put #1, lp + 1, 0
   Put #1, lp + 2, 0
   Put #1, lp + 3, 0
   lp = lp + 4
   var1 = lvDat(n).lvTools(i).tlType
   Put #1, lp, var1
   Put #1, lp + 1, 0
   Put #1, lp + 2, 0
   Put #1, lp + 3, 0
  Next i
  'TODO:extra info?
 Next n
End If
Close
Exit Sub
a:
If xxN > 0 Then
 MsgBox LoadResString(124), vbCritical
Else
 MsgBox "Internal Error.", vbCritical
End If
Close
End Sub

Private Sub SetString(ByVal FileNo As Integer, ByVal lpStart As Long, ByVal stString As String, ByVal MaxLen As Long)
Dim lp As Long, str1 As String
Dim var1 As Byte, varc As Long
Dim lpS As Long
lp = lpStart
lpS = 1
Do
 If lpS > Len(stString) Then
  var1 = 0
  Put #FileNo, lp, var1
  lp = lp + 1
 Else
  varc = Asc(Mid(stString, lpS, 1))
  lpS = lpS + 1
  If varc < 0 Or varc > 255 Then
   If lp < MaxLen + lpStart - 1 Then
    varc = (varc + 65536) Mod 65536
    var1 = varc \ 256
    Put #FileNo, lp, var1
    var1 = varc And &HFF&
    Put #FileNo, lp + 1, var1
   End If
   lp = lp + 2
  Else
   var1 = varc
   Put #FileNo, lp, var1
   lp = lp + 1
  End If
 End If
Loop Until lp - lpStart >= MaxLen
End Sub

Private Sub DrawBox(ByVal X1 As Integer, ByVal y1 As Integer, ByVal x2 As Integer, ByVal y2 As Integer)
DrawRect bmpBack.hDC, IIf(X1 < x2, X1 * 40 - 40, x2 * 40 - 40), _
IIf(y1 < y2, y1 * 40 - 40, y2 * 40 - 40) _
, Abs(x2 - X1) * 40 + 40, Abs(y2 - y1) * 40 + 40
End Sub

Private Function TheDir(ByVal n As Long) As String
If xxN > 0 Then
 TheDir = Left(LoadResString(160 + n), 6)
Else
 TheDir = Mid(LoadResString(160 + n), 7)
End If
End Function

Private Sub RotLevel(gd As tLevel, ByVal x As Long, ByVal y As Long)
Dim tmp As tLevel
Dim i As Long, j As Long, k As Long
tmp = gd
For i = 1 To 16
 k = 1 + (i + x + 15) Mod 16
 For j = 1 To 10
  tmp.lvData(k, 1 + (j + y + 9) Mod 10) = gd.lvData(i, j)
 Next j
Next i
gd = tmp
End Sub

Private Sub XLevel(gd As tLevel)
Dim tmp As tLevel
Dim i As Long, j As Long, k As Long
tmp = gd
For i = 1 To 16
 For j = 1 To 10
  k = gd.lvData(17 - i, j)
  Select Case k
  Case 7, 9: k = 16 - k
  Case 10, 11: k = 21 - k
  Case 13, 15: k = 28 - k
  Case 18, 20: k = 38 - k
  Case 21, 22: k = 43 - k
  Case 24, 26: k = 50 - k
  Case 28, 29: k = 57 - k
  End Select
  tmp.lvData(i, j) = k
 Next j
Next i
gd = tmp
End Sub

Private Sub YLevel(gd As tLevel)
Dim tmp As tLevel
Dim i As Long, j As Long, k As Long
tmp = gd
For i = 1 To 16
 For j = 1 To 10
  k = gd.lvData(i, 11 - j)
  Select Case k
  Case 6, 8: k = 14 - k
  Case 10, 11: k = 21 - k
  Case 12, 14: k = 26 - k
  Case 17, 19: k = 36 - k
  Case 21, 22: k = 43 - k
  Case 23, 25: k = 48 - k
  Case 28, 29: k = 57 - k
  End Select
  tmp.lvData(i, j) = k
 Next j
Next i
gd = tmp
End Sub

Private Sub TestMap()
'On Error GoTo a
Dim s1 As String
Dim ret As Long
SaveFile vbNullChar
If GameWnd = 0 Then
 s1 = reg2_GetSettings("DweepFile", CStr(App.Path) + "\dweep.exe")
 ret = ShellExecute(Me.hwnd, "open", s1, "test " + CStr(Me.hwnd), CStr(App.Path), 5)
 If ret <= 32 Then
  MsgBox IIf(xxN, "�Ҳ���My Dweep����ָ��һ���ļ���", "Couldn't find My Dweep to start test play."), vbCritical
  If cd.VBGetOpenFileName(s1, , , , , True, "Dweep.exe|Dweep.exe", , CStr(App.Path), , , Me.hwnd) Then
   s1 = Replace(s1, vbNullChar, "")
   reg2_SetSettings "DweepFile", s1
   reg2_SaveFile
   ret = ShellExecute(Me.hwnd, "open", s1, "test " + CStr(Me.hwnd), CStr(App.Path), 5)
   If ret <= 32 Then
    MsgBox IIf(xxN, "������Ϸ����ʱ���ִ���", "Test play error."), vbCritical
    Exit Sub
   End If
  End If
 Else
  MsgBox IIf(xxN, "�޷�������Ϸ���档", "Couldn't start test play."), vbCritical
  Exit Sub
 End If
Else
 TestMap2
End If
Me.Enabled = False
Exit Sub
a:
End Sub

Private Sub TestMap2()
PostMessage GameWnd, &H2005&, 3, 0
End Sub

Private Sub ExitDweep()
PostMessage GameWnd, &H2005&, 3333, 0
End Sub

Private Function Baidu() As Boolean
Baidu = True
IsBaidu = True
End Function

Private Sub pRedraw()
Dim y As Long
GradientFillRectangle bmpResize.hDC, 0, 0, 7, bmpResize.Height, &HFBE3D1, &HE3AB84, GRADIENT_FILL_RECT_H
y = bmpResize.Height \ 2 - 24
If picResize.Tag = "" Then
 If rsOn Then
  bmpTemp.PaintPicture bmpResize.hDC, 0, y, 7, 48, 7, 0
 Else
  bmpTemp.PaintPicture bmpResize.hDC, 0, y, 7, 48, 0, 0
 End If
Else
 If rsOn Then
  bmpTemp.PaintPicture bmpResize.hDC, 0, y, 7, 48, 21, 0
 Else
  bmpTemp.PaintPicture bmpResize.hDC, 0, y, 7, 48, 14, 0
 End If
End If
picResize_Paint
End Sub
