VERSION 5.00
Begin VB.Form frmAbout 
   BackColor       =   &H00808080&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "About Dwpedit"
   ClientHeight    =   3000
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5865
   ClipControls    =   0   'False
   Icon            =   "frmAbout.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   200
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   391
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  '所有者中心
   Tag             =   "About Dwpedit"
   Begin Dwpedit.Button2003 cmdOK 
      Cancel          =   -1  'True
      Default         =   -1  'True
      Height          =   375
      Left            =   4200
      TabIndex        =   3
      Top             =   2520
      Width           =   1470
      _ExtentX        =   2593
      _ExtentY        =   661
      Caption         =   ""
      BackColor       =   -2147483636
      Picture         =   "frmAbout.frx":058A
   End
   Begin VB.Label Label1 
      BackColor       =   &H0000FF00&
      BackStyle       =   0  'Transparent
      Caption         =   "Note: ""Solve Level"" can calcuate ""Simple""(Static) solution only."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   495
      Left            =   720
      TabIndex        =   2
      Top             =   2520
      Width           =   3375
   End
   Begin VB.Image Image2 
      Height          =   375
      Left            =   120
      Top             =   2280
      Width           =   495
   End
   Begin VB.Label lblDesc 
      BackStyle       =   0  'Transparent
      Caption         =   $"frmAbout.frx":08DE
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   690
      Left            =   3600
      TabIndex        =   1
      Tag             =   "App Description"
      Top             =   1080
      Width           =   1935
   End
   Begin VB.Label lblTitle 
      BackStyle       =   0  'Transparent
      Caption         =   "Application Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   240
      Left            =   3360
      TabIndex        =   0
      Tag             =   "Application Title"
      Top             =   2040
      Width           =   2415
   End
   Begin VB.Line Line1 
      BorderColor     =   &H0000C000&
      BorderStyle     =   6  'Inside Solid
      Index           =   1
      X1              =   15
      X2              =   377.133
      Y1              =   162
      Y2              =   162
   End
   Begin VB.Line Line1 
      BorderColor     =   &H0000FF00&
      BorderWidth     =   2
      Index           =   0
      X1              =   16
      X2              =   377.133
      Y1              =   163
      Y2              =   163
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Private Sub cmdOK_Click()
        Unload Me
End Sub

Private Sub flash1_FSCommand(ByVal command As String, ByVal args As String)
If command = "VBShell" Then
 'Shell "explorer " + args, vbNormalFocus
 ShellExecute Me.hWnd, "open", args, "", "", 0
End If
End Sub

Private Sub Form_Load()
Dim b As Boolean
Image2.Picture = frmMain.Icon
lblTitle.Caption = AppTitle
'LoadFlash
'flash1.SetVariable "ver", CStr(App.Major) + "." + CStr(App.Minor) + "." + Format(App.Revision, "0000")
If xxN > 0 Then
 Me.Caption = LoadResString(xxN * 100 + 48)
 cmdOK.Caption = LoadResString(xxN * 100 + 49)
 lblDesc.Caption = LoadResString(xxN * 100 + 50)
 'flash1.SetVariable "chs", "chs"
End If
End Sub

Private Sub LoadFlash()
'On Error GoTo a
'Dim l As Long, m As Long, d() As Byte, i As Long
'If Dir(CStr(App.Path) + "\dwpedit.dll") <> "" Then
' Open CStr(App.Path) + "\dwpedit.dll" For Binary As #1
' Get #1, 553, m
' ReDim d(1 To m)
' Get #1, 557, d
' Close 1
'Else
' d = LoadResData(101, "CUSTOM")
' Open s For Binary As #1
' Put #1, 1, d
' Get #1, 1, m
' ReDim d(1 To m)
' Get #1, 5, d
' Close 1
' Kill s
'End If
'Open s For Binary As #2
'Put #2, 1, d
''run xor decrypt
'For i = 1 To 1001 Step 4
' Get #2, i, l
' m = m Xor l
' Put #2, i, m
' m = l
'Next i
'Close
'flash1.Movie = s
''flash1.Movie = CStr(App.Path) + "\dwpedit.swf"
'Exit Sub
'a:
'Close
End Sub
