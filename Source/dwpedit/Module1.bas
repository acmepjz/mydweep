Attribute VB_Name = "Module1"
Option Explicit

Public AppTitle As String
Public Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
Public Declare Function DrawFocusRect Lib "user32" (ByVal hDC As Long, lpRect As RECT) As Long
Public Declare Function StrBlt Lib "gdi32" Alias "StretchBlt" (ByVal hDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal dwRop As Long) As Long
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Public Declare Function SelectObject Lib "gdi32" (ByVal hDC As Long, ByVal hObject As Long) As Long
Public Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long
Public Declare Function CreatePen Lib "gdi32" (ByVal nPenStyle As Long, ByVal nWidth As Long, ByVal crColor As Long) As Long
'Public Declare Function GetWindowRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
'Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function GetWindowDC Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function ReleaseDC Lib "user32" (ByVal hWnd As Long, ByVal hDC As Long) As Long

Public chkText As String
'Public Declare Function MoveToEx Lib "gdi32" (ByVal hdc As Long, ByVal x As Long, ByVal y As Long, lpPoint As POINTAPI) As Long
Public Declare Function LineTo Lib "gdi32" (ByVal hDC As Long, ByVal x As Long, ByVal y As Long) As Long
'Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Private Declare Sub InitCommonControls Lib "COMCTL32" ()
'Public Type POINTAPI
'        x As Long
'        y As Long
'End Type
'Public Type RECT
'    Left As Long
'    TOp As Long
'    Right As Long
'    Bottom As Long
'End Type

Public xxN As Long

Public Type tLevelNode '346 bytes
 lvData(1 To 16, 1 To 10) As Byte
 lvArea(1 To 16, 1 To 10) As Byte
 lvTools(1 To 10) As Byte
 Depth As Long
 Cost As Single
 dX As Byte
 dy As Byte
 ex As Byte
 ey As Byte
 Prev As Long
End Type
Public xxL As tLevelNode

Private Type TRIVERTEX
   x As Long
   y As Long
   Red As Integer
   Green As Integer
   Blue As Integer
   Alpha As Integer
End Type
Private Type GRADIENT_RECT
    UpperLeft As Long
    LowerRight As Long
End Type
Private Declare Function GradientFill Lib "msimg32" ( _
   ByVal hDC As Long, _
   pVertex As TRIVERTEX, _
   ByVal dwNumVertex As Long, _
   pMesh As GRADIENT_RECT, _
   ByVal dwNumMesh As Long, _
   ByVal dwMode As Long) As Long

Private Declare Function OleTranslateColor Lib "OLEPRO32.DLL" (ByVal OLE_COLOR As Long, ByVal HPALETTE As Long, pccolorref As Long) As Long

Public Enum GradientFillStyle
 GRADIENT_FILL_RECT_H = 0
 GRADIENT_FILL_RECT_V = 1
 GRADIENT_FILL_TRIANGLE = &H2&
End Enum

Private m_reg As clsFakeRegistry

Public Sub GradientFillRect( _
      ByVal lHDC As Long, _
      tR As RECT, _
      ByVal oStartColor As OLE_COLOR, _
      ByVal oEndColor As OLE_COLOR, _
      ByVal eDir As GradientFillStyle _
   )
Dim hBrush As Long
Dim lStartColor As Long
Dim lEndColor As Long
Dim lR As Long
   
   ' Use GradientFill:
   'If (HasGradientAndTransparency) Then
      lStartColor = TranslateColor(oStartColor)
      lEndColor = TranslateColor(oEndColor)
   
      Dim tTV(0 To 1) As TRIVERTEX
      Dim tGR As GRADIENT_RECT
      
      setTriVertexColor tTV(0), lStartColor
      tTV(0).x = tR.Left
      tTV(0).y = tR.Top
      setTriVertexColor tTV(1), lEndColor
      tTV(1).x = tR.Right
      tTV(1).y = tR.Bottom
      
      tGR.UpperLeft = 0
      tGR.LowerRight = 1
      
      GradientFill lHDC, tTV(0), 2, tGR, 1, eDir
      
   'Else
   '   ' Fill with solid brush:
   '   hBrush = CreateSolidBrush(TranslateColor(oEndColor))
   '   FillRect lHDC, tR, hBrush
   '   DeleteObject hBrush
   'End If
   
End Sub

Private Sub setTriVertexColor(tTV As TRIVERTEX, lColor As Long)
Dim lRed As Long
Dim lGreen As Long
Dim lBlue As Long
   lRed = (lColor And &HFF&) * &H100&
   lGreen = (lColor And &HFF00&)
   lBlue = (lColor And &HFF0000) \ &H100&
   setTriVertexColorComponent tTV.Red, lRed
   setTriVertexColorComponent tTV.Green, lGreen
   setTriVertexColorComponent tTV.Blue, lBlue
End Sub

'Public Function TranslateColor(ByVal oClr As OLE_COLOR, _
'                        Optional hPal As Long = 0) As Long
'    ' Convert Automation color to Windows color
'    If OleTranslateColor(oClr, hPal, TranslateColor) Then
'        TranslateColor = CLR_INVALID
'    End If
'End Function

Private Sub setTriVertexColorComponent(ByRef iColor As Integer, ByVal lComponent As Long)
   If (lComponent And &H8000&) = &H8000& Then
      iColor = (lComponent And &H7F00&)
      iColor = iColor Or &H8000
   Else
      iColor = lComponent
   End If
End Sub

'
'Friend Sub RunScript()
'On Error Resume Next
'Dim i As Long, j As Long, k As Long
'Dim s As String, s1 As String
'Dim cond As Boolean, b As Boolean
'Dim r As RECT
''///////////////there's RuneScape
'cond = True
'For i = 1 To scr.Count
' s = scr.StringTable(i)
' Select Case Left(s, 1)
' Case ">" '////conditions
'  j = InStr(1, s, " ")
'  If j = 0 Then s1 = Mid(s, 2) Else s1 = Mid(s, 2, j - 2)
'  Select Case s1
'  Case "end", "always"
'   cond = True
'  Case "never"
'   cond = False
'  Case "gametime"
'   j = Val(Mid(s, j))
'   b = j = GData.GameTime
'   cond = cond And b
'  Case "dweepx"
'   j = Val(Mid(s, j))
'   b = j = GData.DweepX
'   cond = cond And b
'  Case "dweepy"
'   j = Val(Mid(s, j))
'   b = j = GData.DweepY
'   cond = cond And b
'  Case "dweepstate"
'   j = Val(Mid(s, j))
'   b = j = GData.DweepState
'   cond = cond And b
'  Case "dweeppos"
'   k = InStr(j + 1, s, " ")
'   j = Val(Mid(s, j, k - j))
'   k = Val(Mid(s, k))
'   b = j * 10 - 5 = GData.DweepX
'   b = b And (k * 10 - 5 = GData.DweepY)
'   cond = cond And b
'  Case "dweepposex"
'   k = InStr(j + 1, s, " ")
'   j = Val(Mid(s, j, k - j))
'   k = Val(Mid(s, k))
'   b = j = GData.DweepX
'   b = b And (k = GData.DweepY)
'   cond = cond And b
'  Case "haslaser"
'   k = InStr(j + 1, s, " ")
'   j = Val(Mid(s, j, k - j))
'   k = Val(Mid(s, k))
'   b = GData.LevelLaser(j, k) > 0
'   cond = cond And b
'  Case "nolaser"
'   k = InStr(j + 1, s, " ")
'   j = Val(Mid(s, j, k - j))
'   k = Val(Mid(s, k))
'   b = GData.LevelLaser(j, k) = 0
'   cond = cond And b
'  Case "dweepdry"
'   cond = cond And IsDry
'  Case "dweepwet"
'   cond = cond And IsWet
'  Case "dweepice", "dweepfrozen"
'   cond = cond And IsIce
'  Case "dweepfly"
'   cond = cond And IsFly
'  Case "dweepdie"
'   cond = cond And (GData.DweepState = 2049)
'  Case "samenum"
'   k = InStr(j + 1, s, " ")
'   s1 = Trim(Mid(s, j, k - j))
'   b = GetVar(s1) = Val(Mid(s, k))
'   cond = cond And b
'  Case "samevar"
'   k = InStr(j + 1, s, " ")
'   s1 = Trim(Mid(s, j, k - j))
'   b = GetVar(s1) = GetVar(Trim(Mid(s, k)))
'   cond = cond And b
'  Case "hasitem"
'   j = Val(Mid(s, j))
'   cond = cond And HasItem(j)
'  Case "noitem"
'   j = Val(Mid(s, j))
'   cond = cond And Not HasItem(j)
'  Case "nowitem"
'   j = Val(Mid(s, j))
'   If GData.ItemNow = 0 Or GData.ItemNow > GData.ItemCount Then
'    k = 0
'   Else
'    k = ChangeItemData(GData.ItemData(GData.ItemNow))
'   End If
'   b = k = j
'   cond = cond And b
'  Case "mapdata"
'   k = InStr(j + 1, s, " ")
'   s1 = Trim(Mid(s, j, k - j))
'   b = GData.LevelData(Val(Mid(s1, 1, 2)), Val(Mid(s1, 4))) = Val(Mid(s, k))
'   cond = cond And b
'  Case Else
'   If GData.GameTime = 1 Then MsgBox "未知脚本指令在第" + _
'   CStr(i) + "行:" + vbCrLf + vbCrLf + s, vbExclamation
'  End Select
' Case "#" '////statments
'  If cond Then
'   Select Case Mid(s, 2)
'   Case "message"
'    ShowDialog scr.StringTable(i + 1), ConvertString(scr.StringTable(i + 2))
'    i = i + 2
'   Case "text"
'    AddTextInfo scr.StringTable(i + 2), _
'    ConvertString(scr.StringTable(i + 1)), _
'    Val(Mid(scr.StringTable(i + 3), 1, 3)), _
'    Val(Mid(scr.StringTable(i + 3), 5, 3)), _
'    Val(scr.StringTable(i + 4)), _
'    QBColor(Val(Mid(scr.StringTable(i + 3), 9)))
'    i = i + 4
'   Case "attachballoon" 'Haha! XP Balloon
'    GetClientRect frmMain.hwnd, r
'    j = Val(Mid(scr.StringTable(i + 3), 1, 3))
'    k = Val(Mid(scr.StringTable(i + 3), 5, 3))
'    bool.Create CStr(Rnd), r.Left + j, r.TOp + k, 150, 100, , _
'    scr.StringTable(i + 1), ConvertString(scr.StringTable(i + 2)), _
'    xpbtInformation, , vbBlack, 8, , 3000
'   Case "deletetext"
'    RemoveTextInfo scr.StringTable(i + 1)
'    i = i + 1
'   Case "setnum"
'    SetVar scr.StringTable(i + 1), Val(scr.StringTable(i + 2))
'    i = i + 2
'   Case "setvar"
'    SetVar scr.StringTable(i + 1), GetVar(scr.StringTable(i + 2))
'    i = i + 2
'   Case "cancelitem"
'    GData.ItemNow = 0
'   Case "setmapdata"
'    GData.LevelData(Val(Mid(scr.StringTable(i + 1), 1, 2)), _
'    Val(Mid(scr.StringTable(i + 1), 4, 2))) = _
'    Val(Mid(scr.StringTable(i + 1), 7))
'    i = i + 1
'   Case Else
'    MsgBox "未知脚本指令在第" + CStr(i) + _
'    "行:" + vbCrLf + vbCrLf + s, vbExclamation
'   End Select
'  End If
' End Select
'Next i
''///////////////show text ...
'For i = 1 To tsc
' With ts(i)
'  If .t >= 0 Then
'   DrawTextB bmpBack.hdc, .Text, frmMain.Font, .x, .y, 400, 400, DT_WORDBREAK, .clr, , True
'   If .t = 1 Then
'    .t = -1
'   ElseIf .t > 1 Then
'    .t = .t - 1
'   End If
'  End If
' End With
'Next i
'End Sub
'
'

Public Function reg2_GetSettings(ByVal sName As String, Optional ByVal sDefault As String) As String
Dim s As String
If Not GetKeyValue("Config", sName, s) Then s = sDefault
reg2_GetSettings = s
End Function

Public Function reg2_SetSettings(ByVal sName As String, ByVal KeyVal As String) As Boolean
reg2_SetSettings = SetKeyValue("Config", sName, KeyVal)
End Function

Public Sub reg2_SaveFile()
If m_reg Is Nothing Then
 Set m_reg = New clsFakeRegistry
 m_reg.LoadFile App.Path + "\dwpedit.cfg"
End If
m_reg.SaveFile
End Sub

Public Function GetKeyValue(KeyName As String, SubKeyRef As String, ByRef KeyVal As String) As Boolean
If m_reg Is Nothing Then
 Set m_reg = New clsFakeRegistry
 m_reg.LoadFile App.Path + "\dwpedit.cfg"
End If
GetKeyValue = m_reg.GetKeyValue(KeyName, SubKeyRef, KeyVal)
End Function

Public Function SetKeyValue(KeyName As String, SubKeyRef As String, ByRef KeyVal As String) As Boolean
If m_reg Is Nothing Then
 Set m_reg = New clsFakeRegistry
 m_reg.LoadFile App.Path + "\dwpedit.cfg"
End If
SetKeyValue = m_reg.SetKeyValue(KeyName, SubKeyRef, KeyVal)
End Function

Sub Main()
InitCommonControls
AddFileFormat
'Exit Sub
xxN = Val(reg2_GetSettings("Language", "1"))
If xxN > 0 Then
 AppTitle = LoadResString(xxN * 100 + 20)
Else
 AppTitle = "Dweep Level Pack Editor"
End If
Load frmMain
frmMain.Show
End Sub

Private Sub AddFileFormat()
lukeFileFormat "dwp", "DWPFile", "Dweep Level", _
CStr(App.Path) + "\dweep.icl,1", """" + CStr(App.Path) + "\" + CStr(App.EXEName) + ".exe"" %1"
lukeFileFormat "dwl", "DWLFile", "My Dweep Level", _
CStr(App.Path) + "\dweep.icl,0", """" + CStr(App.Path) + "\" + CStr(App.EXEName) + ".exe"" %1"
End Sub

Private Sub lukeFileFormat(ByVal ext As String, ByVal Name As String, ByVal desc As String, ByVal ico As String, ByVal opn As String)
'On Error Resume Next
'Dim s As String
'reg.ClassKey = HKEY_CLASSES_ROOT
'reg.SectionKey = "." + ext + "\"
'If reg.KeyExists Then
' s = reg.value
'Else
' reg.CreateKey
' s = Name
' reg.value = Name
'End If
'If s = "" Then s = Name
'reg.SectionKey = s
'If Not reg.KeyExists Then reg.CreateKey
'reg.value = desc
'reg.SectionKey = s + "\DefaultIcon"
'If Not reg.KeyExists Then reg.CreateKey
'reg.value = ico
'reg.SectionKey = s + "\Shell\Open\Command"
'If Not reg.KeyExists Then reg.CreateKey
'reg.value = opn
End Sub

Public Sub DrawRect(ByVal hDC As Long, Left As Long, Top As Long, Width As Long, Height As Long)
Dim var1 As RECT
var1.Left = Left
var1.Top = Top
var1.Right = Left + Width
var1.Bottom = Top + Height
DrawFocusRect hDC, var1
End Sub

'Public Sub ALE(lv As tLevelNode, lvLas() As Byte)
'Dim i As Integer, j As Integer
'Dim nx As Integer, ny As Integer, nFs As Integer
''Dim xn As Integer, yn As Integer
'Dim endFlag As Boolean
'Erase lvLas
'For i = 1 To 16
' For j = 1 To 10
'  nFs = lv.lvData(i, j)
'  Select Case nFs
'  Case 6, 7, 8, 9
'   nx = i: ny = j
'   nFs = nFs - 5
'   endFlag = False
'   Do
'    '//////////////////////Set Next Block
'    Select Case nFs
'    Case 1
'     If ny > 1 Then ny = ny - 1 Else endFlag = True
'    Case 2
'     If nx < 16 Then nx = nx + 1 Else endFlag = True
'    Case 3
'     If ny < 10 Then ny = ny + 1 Else endFlag = True
'    Case 4
'     If nx > 1 Then nx = nx - 1 Else endFlag = True
'    End Select
'    If endFlag Then Exit Do
'    '//////////////////////Set Next Direction and Add Data
'    Select Case lv.lvData(nx, ny)
'    Case 0, 2, 3, 4, 16 To 32 '////////////////////Through
'     Select Case lvLas(nx, ny)
'     Case 0
'      lvLas(nx, ny) = 2 - nFs Mod 2
'     Case 1
'      If nFs Mod 2 = 0 Then lvLas(nx, ny) = 3
'     Case 2
'      If nFs Mod 2 = 1 Then lvLas(nx, ny) = 3
'     End Select
'    Case 1, 5, 6, 7, 8, 9, 12, 13, 14, 15 '////////End
'     lvLas(nx, ny) = 10
'     endFlag = True
'    Case 10 '//////////////////////////////////////Mirror NE-WS
'     If nFs Mod 2 = 0 Then nFs = nFs - 1 Else nFs = nFs + 1
'     Select Case lvLas(nx, ny)
'     Case 0
'      If nFs = 2 Or nFs = 3 Then lvLas(nx, ny) = 4 Else lvLas(nx, ny) = 5
'     Case 4
'      If nFs = 1 Or nFs = 4 Then lvLas(nx, ny) = 6
'     Case 5
'      If nFs = 2 Or nFs = 3 Then lvLas(nx, ny) = 6
'     End Select
'    Case 11 '//////////////////////////////////////Mirror NW-ES
'     nFs = 5 - nFs
'     Select Case lvLas(nx, ny)
'     Case 0
'      If nFs > 2 Then lvLas(nx, ny) = 7 Else lvLas(nx, ny) = 8
'     Case 7
'      If nFs < 3 Then lvLas(nx, ny) = 9
'     Case 8
'      If nFs > 2 Then lvLas(nx, ny) = 9
'     End Select
'    End Select
'   Loop Until endFlag
'  End Select
' Next j
'Next i
'End Sub
'
'Public Function CalcArea(lv As tLevelNode) As Long
'Dim las(1 To 16, 1 To 10) As Byte
'Dim i As Long, j As Long
'Dim n As Long
'Erase lv.lvArea
'ALE lv, las
'For i = 1 To 16
' For j = 1 To 10
'  If las(i, j) = 0 And lv.lvArea(i, j) = 0 Then
'   Select Case lv.lvData(i, j)
'   Case 0, 4, 5, 17 To 32
'    n = n + 1
'    FillArea lv, las, i, j, n
'   End Select
'  End If
' Next j
'Next i
'CalcArea = n
'End Function
'
'Private Sub FillArea(lv As tLevelNode, las() As Byte, ByVal x As Long, ByVal y As Long, ByVal n As Long)
'Dim c As Long
'Dim i As Long, j As Long
'Dim p As Long, q As Long
'Dim b As Boolean
'lv.lvArea(x, y) = n
'Do
' c = 0
' For i = 1 To 16
'  For j = 1 To 10
'   If lv.lvArea(i, j) = 0 And las(i, j) = 0 Then
'    Select Case lv.lvData(i, j)
'    Case 0, 4, 5, 17 To 32
'     b = False
'     For p = i - 1 To i + 1
'      For q = j - 1 To j + 1
'       If p >= 1 And p <= 16 And q >= 1 And q <= 10 Then
'        If lv.lvArea(p, q) = n Then
'         b = True
'         Exit For
'        End If
'       End If
'      Next q
'      If b Then Exit For
'     Next p
'     If b Then
'      lv.lvArea(i, j) = n
'      c = c + 1
'     End If
'    End Select
'   End If
'  Next j
' Next i
'Loop Until c = 0
'End Sub

Public Function GetHotKey(ByVal s As String) As String
Dim s1 As String, i As Long
s1 = UCase(s)
s1 = Replace(s1, "&&", "")
i = InStr(1, s1, "&")
If i > 0 Then
 GetHotKey = Mid(s1, i + 1, 1)
End If
End Function


