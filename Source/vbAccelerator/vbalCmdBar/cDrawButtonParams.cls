VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDrawButtonParams"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public hwnd As Long
Public hdc As Long
Public FontFace As String
Public FontSize As Long
Public hFont As Long
Public Left As Long
Public TOp As Long
Public Height As Long
Public RightToLeft As Boolean
Public MouseOverButton As Boolean
Public MouseDownButton As Boolean
Public MouseOverSplit As Boolean
Public MouseDownSplit As Boolean
Public ShowingMenu As Boolean
Public SizeStyle As Long ' see constants COMMANDBARSIZESTYLE
Public Orientation As ECommandBarOrientation
Public Enabled As Boolean ' Is the toolbar enabled
Private sz As Long
Public ToolbarSize As Long
Public ImageList As cCommandBarImageList
Public Hidden As Boolean
Public ButtonPosition As ECommandBarButtonTextPosition
'////////////Add

Friend Property Get Size() As Long
Size = sz
End Property

Friend Property Let Size(ByVal n As Long)
sz = n
End Property

Public Property Get Width() As Long
Width = sz
End Property
