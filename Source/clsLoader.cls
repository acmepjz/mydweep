VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsLoader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim fn As String
Dim lp As Long

Public Sub Destroy()
''
End Sub

Public Sub LoadFile(ByVal Filename As String)
Open Filename For Binary As #11
fn = Filename
lp = 1
End Sub

Public Sub CloseFile()
Close #11
Kill fn
End Sub

Public Function LoadToArray(d() As Byte) As Long
Dim m As Long
Get #11, lp, m
lp = lp + 4
ReDim d(1 To m)
Get #11, lp, d
lp = lp + m
LoadToArray = m
End Function

Public Function LoadToFMusic() As Long
Dim m As Long, d() As Byte, s As String
s = CStr(App.Path) + "\temp.tmp"
Get #11, lp, m
lp = lp + 4
ReDim d(1 To m)
Get #11, lp, d
'//////////////////////zzzz
Open s For Binary As #19
Put #19, 1, d
Close 19
'//////////////////xxxxxxxx
'LoadToFMusic = FMUSIC_LoadSongEx(fn, lp - 1, m, 0, ByVal 0, 0)
LoadToFMusic = FMUSIC_LoadSongEx(s, 0, 0, 0, ByVal 0, 0)
'//////////////////xxxxxxxx
lp = lp + m
''////////////
'LoadToFMusic = FMUSIC_LoadSongEx2(ByVal pCopy(d, m), 0, m, FSOUND_LOADMEMORY, ByVal 0, 0)
'Sleep 100
'Debug.Assert FMOD_Debug(d)
Debug.Assert FMOD_Debug2
End Function

Public Function LoadToFSound() As Long
Dim m As Long, d() As Byte
Get #11, lp, m
lp = lp + 4
'ReDim d(1 To m)
'Get #11, lp, d
'//////////////////xxxxxxxx
LoadToFSound = FSOUND_Sample_Load(-1, fn, FSOUND_NORMAL, lp - 1, m)
'//////////////////xxxxxxxx
lp = lp + m
''////////////
'LoadToFSound = FSOUND_Sample_Load(-1, ByVal pCopy(d, m), FSOUND_LOADMEMORY Or FSOUND_NORMAL, 0, m)
'Sleep 100
'Debug.Assert FMOD_Debug(d)
Debug.Assert FMOD_Debug2
End Function

Private Function FMOD_Debug(b() As Byte) As Boolean
Dim i As Long, s As String
For i = 1 To 16
 s = s + Chr(b(i))
Next i
Debug.Print "The header of file is:" + s
i = FSOUND_GetError
If i <> 0 Then
 Debug.Print "ERROR! " + FSOUND_GetErrorString(i)
End If
FMOD_Debug = True
End Function

Private Function FMOD_Debug2() As Boolean
Dim i As Long
i = FSOUND_GetError
If i <> 0 Then
 Debug.Print "ERROR! " + FSOUND_GetErrorString(i)
End If
FMOD_Debug2 = True
End Function

Public Sub LoadJPEG(bm As cAlphaDibSection)
Dim bm24 As New cDIBSection
Dim m As Long, d() As Byte
Dim jpg As New clsJpeg
Get #11, lp, m
lp = lp + 4
ReDim d(1 To m)
Get #11, lp, d
lp = lp + m
jpg.LoadJPGFromPtr bm24, VarPtr(d(1)), m
bm.Create bm24.Width, bm24.Height
bm24.PaintPicture bm.hdc
End Sub

Public Function LoadJPEGEx() As cDIBSection
Dim bm24 As New cDIBSection
Dim m As Long, d() As Byte
Dim jpg As New clsJpeg
Get #11, lp, m
lp = lp + 4
ReDim d(1 To m)
Get #11, lp, d
lp = lp + m
jpg.LoadJPGFromPtr bm24, VarPtr(d(1)), m
Set LoadJPEGEx = bm24
End Function

Public Sub LoadToBitmap(bm As cAlphaDibSection)
Dim bm24 As New cDIBSection
Dim w As Integer, h As Integer
Dim i As Long, clr As Long
Dim d() As Byte
Get #11, lp, w
Get #11, lp + 2, h
bm24.Create w, h
i = bm24.BytesPerScanLine * bm24.Height
ReDim d(1 To i)
Get #11, lp + 4, d
CopyMemory ByVal bm24.DIBSectionBitsPtr, d(1), i
bm.Create w, h
BitBlt bm.hdc, 0, 0, w, h, bm24.hdc, 0, 0, vbSrcCopy
lp = lp + i + 4
End Sub

Friend Sub LoadToBitmap2(bm As cAlphaDibSection, dat As typeBitmapOffset)
Dim bm24 As New cDIBSection
Dim w As Integer, h As Integer
Dim xa As Integer, xb As Integer, xc As Integer, xd As Integer
Dim i As Long, m As Long
Dim d() As Byte
'/////////////
Get #11, lp, m
lp = lp + 4
dat.count = m
For i = 1 To m
 Get #11, lp, xa
 Get #11, lp + 2, xb
 Get #11, lp + 4, xc
 Get #11, lp + 6, xd
 dat.OffsetData(i).Top = xa
 dat.OffsetData(i).Width = xb
 dat.OffsetData(i).CenterX = xc
 dat.OffsetData(i).CenterY = xd
 lp = lp + 8
Next i
Get #11, lp, w
Get #11, lp + 2, h
For i = 1 To m - 1
 dat.OffsetData(i).Height = dat.OffsetData(i + 1).Top - dat.OffsetData(i).Top
Next i
dat.OffsetData(m).Height = h - dat.OffsetData(m).Top
'/////////////
bm24.Create w, h
i = bm24.BytesPerScanLine * bm24.Height
ReDim d(1 To i)
Get #11, lp + 4, d
CopyMemory ByVal bm24.DIBSectionBitsPtr, d(1), i
bm.Create w, h
BitBlt bm.hdc, 0, 0, w, h, bm24.hdc, 0, 0, vbSrcCopy
lp = lp + i + 4
'/////////////
CreateAlphaChannel bm, dat
End Sub

Private Sub Class_Terminate()
Destroy
End Sub
