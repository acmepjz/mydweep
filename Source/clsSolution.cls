VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSolution"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Type typeSolution
 Name As String
 Name_CHS As String
 c As Long
 s(1 To 67) As String
 s_CHS(1 To 67) As String
End Type

Private sols(1 To 75) As typeSolution

Private ld As Boolean, chs As Boolean

Public Property Get Loaded() As Boolean
Loaded = ld
End Property

Public Property Get IsChinese() As Boolean
IsChinese = chs
End Property

Public Property Let IsChinese(ByVal b As Boolean)
chs = b
End Property

Public Property Get LevelName(ByVal Index As Long) As String
LevelName = IIf(chs, sols(Index).Name_CHS, sols(Index).Name)
End Property

Public Property Get StepsCount(ByVal Index As Long) As Long
StepsCount = sols(Index).c
End Property

Public Property Get Steps(ByVal Index As Long, ByVal n As Long) As String
Steps = IIf(chs, sols(Index).s_CHS(n), sols(Index).s(n))
End Property

Public Sub LoadFile(ByVal filename As String)
On Error GoTo a
Dim i As Long, j As Long, s As String
Open filename For Input As #1
Line Input #1, s
For i = 1 To 75
 sols(i).Name = s
 j = 0
 Line Input #1, s
 Do Until Val(s) <> 0
  j = j + 1
  sols(i).s(j) = s
  Line Input #1, s
 Loop
 sols(i).c = j
Next i
ld = True
a:
Close 1
End Sub

Public Sub LoadFile2(ByVal filename As String)
On Error GoTo a
Dim i As Long, j As Long, s As String
Open filename For Input As #1
For i = 1 To 75
 Line Input #1, s
 sols(i).Name_CHS = s
 For j = 1 To sols(i).c
  Line Input #1, s
  sols(i).s_CHS(j) = s
 Next j
Next i
a:
Close 1
End Sub

