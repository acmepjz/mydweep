VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsExData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Type Type1
 s As String
 st As Long
 l As Long
End Type

Private d() As Byte
Private sc() As Type1
Private cnt As Long

Public Property Get Count() As Long
Count = cnt
End Property

Public Function FindData(ByVal Name As String) As Long
Dim i As Long
For i = 1 To cnt
 If sc(i).s = Name Then
  FindData = i
  Exit Function
 End If
Next i
End Function

Public Property Get DataName(ByVal Index As Long) As String
DataName = sc(Index).s
End Property

Public Property Get DataSize(ByVal Index As Long) As Long
DataSize = sc(Index).l
End Property

Public Property Get DataPtr(ByVal Index As Long) As Long
DataPtr = VarPtr(d(sc(Index).st))
End Property

Public Sub SaveDataToFile(ByVal Index As Long, ByVal FileName As String)
On Error GoTo a
Dim d1() As Byte
With sc(Index)
 ReDim d1(1 To .l)
 CopyMemory d1(1), d(.st), .l
End With
Open FileName For Binary As #17
Put #17, 1, d1
a:
Close 17
End Sub

Public Sub LoadFile(ByVal FileName As String)
On Error GoTo a
Dim i As Long, m As Long
Dim s1 As String * 4
Erase d, sc
cnt = 0
Open FileName For Binary As #17
m = LOF(17) - 421
If m > 8 Then
 ReDim d(1 To m)
 Get #17, 422, d
 i = 422
 Do
  Get #17, i, m
  If m = 0 Then Exit Do
  Get #17, i, s1
  Get #17, i + 4, m
  i = i + 8
  cnt = cnt + 1
  ReDim Preserve sc(1 To cnt)
  With sc(cnt)
   .s = Trim(Replace(s1, vbNullChar, ""))
   .st = i - 421
   .l = m
  End With
  i = i + m
 Loop
End If
a:
Close 17
End Sub

Private Sub Class_Terminate()
Erase d
End Sub
