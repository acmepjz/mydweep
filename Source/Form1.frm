VERSION 5.00
Object = "{6FAC5BE4-F62D-4581-9089-FE28296C38A9}#5.0#0"; "vbalCmdBar6.ocx"
Object = "{396F7AC0-A0DD-11D3-93EC-00C0DFE7442A}#1.0#0"; "vbalIml6.ocx"
Object = "{DE8CE233-DD83-481D-844C-C07B96589D3A}#1.1#0"; "VBALSG~1.OCX"
Begin VB.Form Form1 
   BackColor       =   &H80000013&
   BorderStyle     =   0  'None
   Caption         =   "Data Editor"
   ClientHeight    =   4560
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5760
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   304
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   384
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox pic 
      BackColor       =   &H80000013&
      BorderStyle     =   0  'None
      Height          =   1335
      Index           =   3
      Left            =   1680
      ScaleHeight     =   89
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   209
      TabIndex        =   25
      Top             =   2040
      Visible         =   0   'False
      Width           =   3135
      Begin VB.CommandButton Command13 
         Caption         =   "Add Script"
         Height          =   255
         Left            =   1080
         TabIndex        =   27
         Top             =   120
         Width           =   975
      End
      Begin VB.Label Label3 
         BackColor       =   &H80000013&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "QBColor..."
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   120
         Width           =   855
      End
   End
   Begin VB.OptionButton opt 
      BackColor       =   &H80000013&
      Caption         =   "Danger!"
      Height          =   255
      Index           =   2
      Left            =   3240
      TabIndex        =   21
      Top             =   480
      Width           =   975
   End
   Begin VB.OptionButton opt 
      BackColor       =   &H80000013&
      Caption         =   "Tools"
      Height          =   255
      Index           =   3
      Left            =   2520
      TabIndex        =   24
      Top             =   480
      Width           =   735
   End
   Begin VB.PictureBox pic 
      BackColor       =   &H80000013&
      BorderStyle     =   0  'None
      Height          =   735
      Index           =   2
      Left            =   1200
      ScaleHeight     =   49
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   113
      TabIndex        =   15
      Top             =   960
      Visible         =   0   'False
      Width           =   1695
      Begin VB.CommandButton Command12 
         Caption         =   "New Instance"
         Height          =   495
         Left            =   120
         TabIndex        =   22
         Top             =   120
         Width           =   1335
      End
   End
   Begin VB.OptionButton opt 
      BackColor       =   &H80000013&
      Caption         =   "Tutorial Script"
      Height          =   255
      Index           =   1
      Left            =   1200
      TabIndex        =   14
      Top             =   480
      Width           =   1335
   End
   Begin VB.PictureBox pic 
      BackColor       =   &H80000013&
      BorderStyle     =   0  'None
      Height          =   615
      Index           =   1
      Left            =   3120
      ScaleHeight     =   41
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   73
      TabIndex        =   13
      Top             =   960
      Width           =   1095
      Begin VB.CommandButton Command11 
         Caption         =   "Save"
         Height          =   255
         Left            =   3720
         TabIndex        =   19
         Top             =   0
         Width           =   855
      End
      Begin VB.CommandButton Command10 
         Caption         =   "Save As"
         Height          =   255
         Left            =   4680
         TabIndex        =   18
         Top             =   0
         Width           =   855
      End
      Begin VB.CommandButton Command9 
         Caption         =   "Open"
         Height          =   255
         Left            =   0
         TabIndex        =   17
         Top             =   0
         Width           =   855
      End
      Begin VB.TextBox Text2 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3375
         Left            =   0
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   16
         Top             =   360
         Width           =   5535
      End
      Begin VB.Label Label1 
         BackColor       =   &H80000001&
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   960
         TabIndex        =   20
         Top             =   0
         Width           =   2655
      End
   End
   Begin VB.OptionButton opt 
      BackColor       =   &H80000013&
      Caption         =   "Compress"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   12
      Top             =   480
      Width           =   1095
   End
   Begin VB.PictureBox pic 
      Appearance      =   0  'Flat
      BackColor       =   &H80000013&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3255
      Index           =   0
      Left            =   120
      ScaleHeight     =   217
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   377
      TabIndex        =   0
      Top             =   960
      Width           =   5655
      Begin VB.CommandButton Command14 
         Caption         =   "Direct Compress"
         Height          =   255
         Left            =   4200
         TabIndex        =   28
         Top             =   3000
         Width           =   1335
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Open..."
         Height          =   255
         Left            =   0
         TabIndex        =   10
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton Command2 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   8.25
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   960
         TabIndex        =   9
         Top             =   2640
         Width           =   255
      End
      Begin VB.CommandButton Command2 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   8.25
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   960
         TabIndex        =   8
         Top             =   3000
         Width           =   255
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Delete"
         Height          =   255
         Left            =   1320
         TabIndex        =   7
         Top             =   2640
         Width           =   615
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Clear"
         Height          =   255
         Left            =   2040
         TabIndex        =   6
         Top             =   2640
         Width           =   615
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Make"
         Enabled         =   0   'False
         Height          =   255
         Left            =   4920
         TabIndex        =   5
         Top             =   2640
         Width           =   615
      End
      Begin VB.CommandButton Command6 
         Caption         =   "Open List"
         Height          =   255
         Left            =   0
         TabIndex        =   4
         Top             =   3000
         Width           =   855
      End
      Begin VB.CommandButton Command7 
         Caption         =   "Quick! >"
         Height          =   255
         Left            =   0
         TabIndex        =   3
         Top             =   3360
         Width           =   855
      End
      Begin VB.CommandButton Command8 
         Caption         =   "Last Compile"
         Height          =   255
         Left            =   1320
         TabIndex        =   2
         Top             =   3000
         Width           =   1335
      End
      Begin VB.ComboBox Text1 
         Height          =   315
         ItemData        =   "Form1.frx":0000
         Left            =   960
         List            =   "Form1.frx":000D
         TabIndex        =   1
         Text            =   "Bitmap"
         Top             =   3360
         Width           =   975
      End
      Begin vbAcceleratorSGrid6.vbalGrid grd1 
         Height          =   2535
         Left            =   0
         TabIndex        =   11
         Top             =   0
         Width           =   5535
         _ExtentX        =   9763
         _ExtentY        =   4471
         RowMode         =   -1  'True
         BackgroundPictureHeight=   0
         BackgroundPictureWidth=   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         HeaderDragReorderColumns=   0   'False
         DisableIcons    =   -1  'True
         DrawFocusRectangle=   0   'False
         DefaultRowHeight=   16
         SelectionAlphaBlend=   -1  'True
         SelectionOutline=   -1  'True
      End
      Begin VB.Label Label2 
         BackColor       =   &H80000013&
         Height          =   255
         Left            =   2760
         TabIndex        =   23
         Top             =   2640
         Width           =   2055
      End
   End
   Begin VB.Timer t1 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   1440
      Top             =   120
   End
   Begin vbalIml6.vbalImageList il1 
      Left            =   840
      Top             =   120
      _ExtentX        =   953
      _ExtentY        =   953
      ColourDepth     =   24
      Size            =   8036
      Images          =   "Form1.frx":0029
      Version         =   131072
      KeyCount        =   7
      Keys            =   "������"
   End
   Begin vbalCmdBar6.vbalCommandBar tb 
      Height          =   255
      Index           =   0
      Left            =   120
      Top             =   120
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   450
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin vbalCmdBar6.vbalCommandBar tb 
      Height          =   255
      Index           =   1
      Left            =   480
      Top             =   120
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   450
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function FrameRect Lib "user32" (ByVal hdc As Long, lpRect As RECT, ByVal hBrush As Long) As Long
Private Declare Function CreateSolidBrush Lib "gdi32" (ByVal crColor As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long
Private Declare Function GetWindowRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
Private Type RECT
        Left As Long
        Top As Long
        Right As Long
        Bottom As Long
End Type
Private Type POINTAPI
        x As Long
        y As Long
End Type
Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Private Const WM_LBUTTONDOWN = &H201
Private Const WM_LBUTTONUP = &H202
Private Declare Function GetTickCount Lib "kernel32" () As Long
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)

Private WithEvents tray As clsSysTray
Attribute tray.VB_VarHelpID = -1
Private cd As New cCommonDialog

Private aaa As Boolean, oHeight As Long

Private ww As Long, hh As Long

Implements ISubclass

Private Sub Command1_Click()
Dim s As String
If cd.VBGetOpenFileName(s, , , , , True, , , App.Path, , , Me.hwnd) = True Then
 grd1.AddRow
 grd1.cell(grd1.Rows, 1).Text = s
 With grd1.cell(grd1.Rows, 2)
  Select Case LCase(Right(s, 4))
  Case ".jpg"
   .Text = "JPEG"
  Case ".bmp"
   .Text = "Bitmap"
  Case ".dat"
   .Text = "Data"
  Case Else
   .Text = "Unknown"
  End Select
 End With
End If
End Sub

Private Sub Command10_Click()
Dim s As String, s1 As String, i As Long, j As Long
If cd.VBGetSaveFileName(s, , , "Tutorial Script|*.dws", , CStr(App.Path), , Me.hwnd) Then
 Label1.Caption = s
 Me.Caption = s
 Open s For Output As #1
 s = Text2.Text
 j = InStr(i + 1, s, vbCrLf)
 Do Until j = 0
  i = i + 1
  s1 = Mid(s, i, j - i)
  Print #1, s1
  i = j + 1
  j = InStr(i + 1, s, vbCrLf)
 Loop
 s1 = Mid(s, i + 1)
 If s1 <> "" Then Print #1, s1
 Close
End If
End Sub

Private Sub Command11_Click()
Dim s As String, s1 As String, i As Long, j As Long
If Label1.Caption = "" Then
 Command10_Click
Else
 s = Label1.Caption
 Open s For Output As #1
 s = Text2.Text
 j = InStr(i + 1, s, vbCrLf)
 Do Until j = 0
  i = i + 1
  s1 = Mid(s, i, j - i)
  Print #1, s1
  i = j + 1
  j = InStr(i + 1, s, vbCrLf)
 Loop
 s1 = Mid(s, i + 1)
 If s1 <> "" Then Print #1, s1
 Close
End If
End Sub

Private Sub Command12_Click()
Dim frm As Form1
Set frm = New Form1
Load frm
frm.Show
End Sub

Private Sub Command13_Click()
Dim d() As Byte, d1() As Byte, m As Long
Dim s1 As String * 4
Dim s As String
If cd.VBGetOpenFileName(s, , , , , True, "Script|*.dws", , CStr(App.Path), , , Me.hwnd) Then
 Open s For Binary As #1
 m = LOF(1)
 ReDim d(1 To m)
 Get #1, 1, d
 Close
 If cd.VBGetOpenFileName(s, , , , , True, "Level|*.dwl", , CStr(App.Path), , , Me.hwnd) Then
  ReDim d1(1 To 421)
  Open s For Binary As #1
  Get #1, 1, d1
  Close
  Kill s
  Open s For Binary As #1
  Put #1, 1, d1
  s1 = "SCRP"
  Put #1, 422, s1
  Put #1, 426, m
  Put #1, 430, d
  Close
  MsgBox "OK!"
 End If
End If
End Sub

Private Sub Command14_Click()
Dim s As String
Open CStr(App.Path) + "\Last Compile.txt" For Output As #1
s = CStr(FileDateTime(CStr(App.Path) + "\dweep.exe"))
Print #1, s
s = TheCRC32(CStr(App.Path) + "\dweep.exe")
Print #1, s
Close
s = CStr(App.Path) + "\Dweep.mpq"
CompressFinal s, CompressTo(s)
End Sub

Private Sub Command2_Click(Index As Integer)
On Error GoTo a
Dim s As String, i As Long
If Index = 0 And grd1.SelectedRow > 1 Then
 s = grd1.cell(grd1.SelectedRow, 1).Text
 grd1.cell(grd1.SelectedRow, 1).Text = grd1.cell(grd1.SelectedRow - 1, 1).Text
 grd1.cell(grd1.SelectedRow - 1, 1).Text = s
 s = grd1.cell(grd1.SelectedRow, 2).Text
 grd1.cell(grd1.SelectedRow, 2).Text = grd1.cell(grd1.SelectedRow - 1, 2).Text
 grd1.cell(grd1.SelectedRow - 1, 2).Text = s
 grd1.SelectedRow = grd1.SelectedRow - 1
ElseIf Index = 1 And grd1.SelectedRow < grd1.Rows Then
 s = grd1.cell(grd1.SelectedRow, 1).Text
 grd1.cell(grd1.SelectedRow, 1).Text = grd1.cell(grd1.SelectedRow + 1, 1).Text
 grd1.cell(grd1.SelectedRow + 1, 1).Text = s
 s = grd1.cell(grd1.SelectedRow, 2).Text
 grd1.cell(grd1.SelectedRow, 2).Text = grd1.cell(grd1.SelectedRow + 1, 2).Text
 grd1.cell(grd1.SelectedRow + 1, 2).Text = s
 grd1.SelectedRow = grd1.SelectedRow + 1
End If
a:
End Sub

Private Sub Command3_Click()
On Error Resume Next
grd1.RemoveRow grd1.SelectedRow
End Sub

Private Sub Command4_Click()
grd1.Clear
End Sub

Private Sub Command5_Click()
Dim i As Long, j As Long, s As String
Open CStr(App.Path) + "\Dweep.lst" For Output As #1
With grd1
 .Visible = False
 For i = 1 To .Rows
  If .cell(i, 2).Text = "JPEG" Then
   Print #1, .cell(i, 1).Text
  End If
  .cell(i, 2).ItemData = 0
 Next i
 Print #1, "**********"
 For i = 1 To .Rows
  If .cell(i, 2).Text = "Data" Then
   Print #1, .cell(i, 1).Text
   For j = 1 To .Rows
    If Left(LCase(.cell(i, 1).Text), Len(.cell(i, 1).Text) - 4) + ".bmp" = LCase(.cell(j, 1).Text) Then
     Print #1, .cell(j, 1).Text
     .cell(j, 2).ItemData = 1
     Exit For
    End If
   Next j
  End If
 Next i
 Print #1, "**********"
 For i = 1 To .Rows
  If .cell(i, 2).Text = "Bitmap" And .cell(i, 2).ItemData = 0 Then
   Print #1, .cell(i, 1).Text
  End If
 Next i
 Print #1, "**********"
 For i = 1 To .Rows
  If .cell(i, 2).Text = "Unknown" Then
   Print #1, .cell(i, 1).Text
  End If
 Next i
 .Visible = True
End With
Close
s = CStr(App.Path) + "\Dweep.mpq"
CompressFinal s, CompressTo(s)
End Sub

Private Sub Command6_Click()
Dim s As String
Open CStr(App.Path) + "\dweep.lst" For Input As #1
Do Until EOF(1)
 Line Input #1, s
 Select Case Left(s, 1)
 Case "?", "*"
 Case Else
  grd1.AddRow
  grd1.cell(grd1.Rows, 1).Text = s
  With grd1.cell(grd1.Rows, 2)
   Select Case LCase(Right(s, 4))
   Case ".jpg"
    .Text = "JPEG"
   Case ".bmp"
    .Text = "Bitmap"
   Case ".dat"
    .Text = "Data"
   Case Else
    .Text = "Unknown"
   End Select
  End With
 End Select
Loop
Close
End Sub

Private Sub Command7_Click()
Dim s As String
s = Dir(CStr(App.Path) + "\" + Text1.Text + "\", vbArchive)
Do Until s = ""
 grd1.AddRow
 grd1.cell(grd1.Rows, 1).Text = CStr(App.Path) + "\" + Text1.Text + "\" + s
 With grd1.cell(grd1.Rows, 2)
  Select Case LCase(Right(s, 4))
  Case ".jpg"
   .Text = "JPEG"
  Case ".bmp"
   .Text = "Bitmap"
  Case ".dat"
   .Text = "Data"
  Case Else
   .Text = "Unknown"
  End Select
 End With
 s = Dir
Loop
End Sub

Private Sub Command8_Click()
Dim s As String
Open CStr(App.Path) + "\Last Compile.txt" For Output As #1
s = CStr(FileDateTime(CStr(App.Path) + "\dweep.exe"))
Print #1, s
s = TheCRC32(CStr(App.Path) + "\dweep.exe")
Print #1, s
Close
MsgBox "It's " + s
End Sub

Private Function TheCRC32(ByVal FileName As String) As String
Dim s As String
Dim d(2005) As Byte
Dim crc As New cCRC32
s = Hex(crc.GetFileCrc32(FileName))
Open FileName For Binary As #67
Get #67, 13, d
s = s + Hex(crc.GetByteArrayCrc32(d))
Get #67, 133, d
s = s + Hex(crc.GetByteArrayCrc32(d))
Get #67, 2005, d
s = s + Hex(crc.GetByteArrayCrc32(d))
Get #67, 142857, d
s = s + Hex(crc.GetByteArrayCrc32(d))
Get #67, 6174, d
s = s + Hex(crc.GetByteArrayCrc32(d))
Close 67
TheCRC32 = s
End Function

Private Sub Command9_Click()
Dim s As String, s1 As String
If cd.VBGetOpenFileName(s, , , , , True, "Tutorial Script|*.dws", , CStr(App.Path), , , Me.hwnd) Then
 Label1.Caption = s
 Me.Caption = s
 Open s For Input As #1
 s = ""
 Do Until EOF(1)
  Line Input #1, s1
  s = s + s1 + vbCrLf
 Loop
 Close
 Text2.Text = s
End If
End Sub

Private Sub Form_Load()
On Error Resume Next
Dim btn As cButton, i As Long, s As String
If Command = "makempq" Then
 Command14_Click
 MsgBox "OK!"
 End
End If
'///////////
il1.AddFromHandle Me.Icon.Handle, IMAGE_ICON
tb(0).MenuImageList = il1.hIml
tb(0).ToolbarImageList = il1.hIml
tb(1).MenuImageList = il1.hIml
tb(1).ToolbarImageList = il1.hIml
'///////////
With tb(0).CommandBars.Add("aaa")
 .Buttons.Add tb(0).Buttons.Add("xp1", 4, "C&ollapse", , "Collapse")
 .Buttons.Add tb(0).Buttons.Add("xp2", 5, "&Expand", , "Expand")
 .Buttons.Add tb(0).Buttons.Add("try", 6, "Minimize to &Tray", , "Minimize to Tray")
 .Buttons.Add tb(0).Buttons.Add("min", 0, "Mi&nimize", , "Minimize")
 .Buttons.Add tb(0).Buttons.Add("max", 1, "Ma&ximize", , "Maximize")
 .Buttons.Add tb(0).Buttons.Add("nor", 2, "&Restore", , "Restore")
 .Buttons.Add tb(0).Buttons.Add("cls", 3, "&Close", , "Close", vbKeyF4, vbAltMask)
End With
tb(0).Buttons("min").Enabled = Me.MinButton
tb(0).Buttons("max").Enabled = Me.MaxButton
tb(0).Buttons("nor").Enabled = False 'xxxx!!!
With tb(0).CommandBars.Add("Title")
 Set btn = tb(0).Buttons.Add("mnu", 7)
 Set btn.Bar = tb(0).CommandBars("aaa")
 .Buttons.Add btn
 Set btn = tb(0).Buttons.Add("cap", , Me.Caption, ePanel)
 btn.ShowCaptionInToolbar = True
 .Buttons.Add btn
End With
With tb(0).CommandBars.Add("qbc")
 For i = 0 To 15
  Set btn = tb(0).Buttons.Add("qbc" + CStr(i), , , , "QBColor:" + CStr(i))
  btn.ColorBox = QBColor(i)
  .Buttons.Add btn
 Next i
 .Buttons.Add tb(0).Buttons.Add("__1", , , eSeparator)
End With
'///////////
Set tb(0).Toolbar = tb(0).CommandBars("Title")
Set tb(1).Toolbar = tb(0).CommandBars("aaa")
Set tray = New clsSysTray
AttachMessage Me, tb(0).hwnd, WM_LBUTTONDOWN
AttachMessage Me, tb(0).hwnd, WM_LBUTTONUP
'///////////
With grd1
 .AddColumn "fn", "File Name", ecgHdrTextALignLeft, , 256
 .AddColumn "tp", "Type", ecgHdrTextALignLeft, , 64
End With
'///////////
For i = 0 To pic.UBound
 pic(i).Move 8, 50, Me.ScaleWidth - 14, Me.ScaleHeight - 54
Next i
opt(0).Value = True
Me.Visible = True
Form_Resize
End Sub

Private Sub Form_Paint()
Dim r As RECT, h As Long
Me.Cls
r.Right = Me.ScaleWidth
r.Bottom = Me.ScaleHeight
h = CreateSolidBrush(&HFF8080)
FrameRect Me.hdc, r, h
DeleteObject h
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Set tray = Nothing
DetachMessage Me, tb(0).hwnd, WM_LBUTTONDOWN
DetachMessage Me, tb(0).hwnd, WM_LBUTTONUP
End Sub

Private Sub Form_Resize()
On Error Resume Next
tb(0).Move 1, 1, Me.ScaleWidth - 122
tb(1).Move Me.ScaleWidth - 122, 1, 120
'/////////
With tb(0)
 If aaa Then
  .Buttons("xp1").Visible = False
  .Buttons("xp2").Visible = True
 Else
  .Buttons("xp2").Visible = False
  .Buttons("xp1").Visible = True
 End If
 If Not Me.Visible Then
  .Buttons("xp1").Visible = False
  .Buttons("xp2").Visible = False
  .Buttons("nor").Visible = True
  .Buttons("max").Visible = True
 ElseIf Me.WindowState = 0 Then
  .Buttons("nor").Visible = False
  .Buttons("max").Visible = True
 Else
  .Buttons("max").Visible = False
  .Buttons("nor").Visible = True
 End If
End With
Form_Paint
End Sub

Private Property Let ISubclass_MsgResponse(ByVal RHS As SSubTimer6.EMsgResponse)
'
End Property

Private Property Get ISubclass_MsgResponse() As SSubTimer6.EMsgResponse
'
End Property

Private Function ISubclass_WindowProc(ByVal hwnd As Long, ByVal iMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Dim p As POINTAPI
Select Case iMsg
Case WM_LBUTTONDOWN
 GetCursorPos p
 ww = Me.Left - p.x * 15
 hh = Me.Top - p.y * 15
 t1.Enabled = True
Case WM_LBUTTONUP
 t1.Enabled = False
End Select
ISubclass_WindowProc = CallOldWindowProc(hwnd, iMsg, wParam, lParam)
End Function

Private Sub Label3_Click()
Dim p As POINTAPI
GetCursorPos p
tb(0).ShowPopupMenu p.x, p.y, tb(0).CommandBars("qbc")
End Sub

Private Sub opt_Click(Index As Integer)
Dim i As Long
For i = 0 To opt.UBound
 pic(i).Visible = opt(i).Value
Next i
End Sub

Private Sub t1_Timer()
On Error Resume Next
Dim p As POINTAPI
GetCursorPos p
Me.Move ww + p.x * 15, hh + p.y * 15
End Sub

Private Sub tb_ButtonClick(Index As Integer, btn As vbalCmdBar6.cButton)
Select Case Left(btn.Key, 3)
Case "nor"
 If Not Me.Visible Then
  Me.Visible = True
  tray.Destroy
 End If
 Me.WindowState = 0
 SetAA False
Case "min"
 If Not Me.Visible Then
  Me.Visible = True
  tray.Destroy
 End If
 Me.WindowState = 1
Case "max"
 If Not Me.Visible Then
  Me.Visible = True
  tray.Destroy
 End If
 Me.WindowState = 2
 SetAA False
Case "try"
 Me.Visible = False
 If Not tray.Loaded Then
  tray.Create Me.Icon, Me.Caption
 End If
 Form_Resize
Case "cls"
 End
Case "xp1", "xp2"
 If Me.WindowState = 0 Then SetAA Not aaa
End Select
End Sub

Private Sub tb_RequestNewInstance(Index As Integer, ctl As Object)
Load tb(tb.Count)
tb(tb.UBound).Align = 0
Set ctl = tb(tb.UBound)
End Sub

Private Sub SetAA(ByVal b As Boolean)
On Error Resume Next
aaa = b
If b Then
 oHeight = Me.Height
 Me.Height = 400
Else
 Me.Height = oHeight
End If
Form_Resize
End Sub

Private Sub tray_SysTrayDoubleClick(ByVal eButton As MouseButtonConstants)
Me.Visible = True
tray.Destroy
End Sub

Private Sub tray_SysTrayMouseDown(ByVal eButton As MouseButtonConstants)
Dim p As POINTAPI
GetCursorPos p
If eButton = vbRightButton Then
 tb(0).ShowPopupMenu p.x - 100, p.y - 100, tb(0).CommandBars("aaa")
End If
End Sub

Private Function CompressTo(ByVal FileName As String) As Long
Dim s As String, i As Long
Dim lp As Long
Dim d() As Byte, m As Long
Dim xa As Integer, xb As Integer, xc As Integer, xd As Integer
Dim bm As New cDIBSection
Open FileName For Binary As #1
Open CStr(App.Path) + "\Dweep.lst" For Input As #2
'///////
lp = 1
Do Until EOF(2)
 Line Input #2, s
 If Left(s, 1) = "*" Then
  Exit Do
 Else
  Label2.Caption = Right(s, 32)
  DoEvents
  Open s For Binary As #3
  m = LOF(3)
  ReDim d(1 To m)
  Get #3, 1, d
  Close 3
  Put #1, lp, m
  Put #1, lp + 4, d
  lp = lp + m + 4
 End If
Loop
Do Until EOF(2)
 Line Input #2, s
 If Left(s, 1) = "*" Then
  Exit Do
 Else
  Label2.Caption = Right(s, 32)
  DoEvents
  Open s For Input As #3
  Input #3, m
  Put #1, lp, m
  lp = lp + 4
  For i = 1 To m
   Input #3, xa, xb, xc, xd
   Put #1, lp, xa
   Put #1, lp + 2, xb
   Put #1, lp + 4, xc
   Put #1, lp + 6, xd
   lp = lp + 8
  Next i
  Close 3
  Line Input #2, s
  bm.CreateFromFile s
  xa = bm.Width
  xb = bm.Height
  Put #1, lp, xa
  Put #1, lp + 2, xb
  m = BitmapToArray(bm, d)
  Put #1, lp + 4, d
  lp = lp + m + 4
 End If
Loop
Do Until EOF(2)
 Line Input #2, s
 If Left(s, 1) = "*" Then
  Exit Do
 Else
  Label2.Caption = Right(s, 32)
  DoEvents
  bm.CreateFromFile s
  xa = bm.Width
  xb = bm.Height
  Put #1, lp, xa
  Put #1, lp + 2, xb
  m = BitmapToArray(bm, d)
  Put #1, lp + 4, d
  lp = lp + m + 4
 End If
Loop
Do Until EOF(2)
 Line Input #2, s
 Label2.Caption = Right(s, 32)
 DoEvents
 Open s For Binary As #3
 m = LOF(3)
 ReDim d(1 To m)
 Get #3, 1, d
 Close 3
 Put #1, lp, m
 Put #1, lp + 4, d
 lp = lp + m + 4
Loop
'///////
Close
CompressTo = lp
End Function

Private Function CompressFinal(ByVal FileName As String, ByVal Size As Long)
Dim d() As Byte, s As String * 3
Dim cp As New cCompress
Label2.Caption = "Compressing..."
DoEvents
Open FileName For Binary As #1
ReDim d(Size - 1)
Get #1, 1, d
Close
Kill FileName
cp.CompressData d
s = "MPQ"
Open FileName For Binary As #1
Put #1, 1, s
Put #1, 4, Size
Put #1, 8, d
Close
Label2.Caption = "OK!"
End Function

Private Function BitmapToArray(bm As cDIBSection, d() As Byte) As Long
Dim m As Long, i As Long, j As Long
m = bm.BytesPerScanLine * bm.Height
ReDim d(1 To m)
CopyMemory d(1), ByVal bm.DIBSectionBitsPtr, m
BitmapToArray = m
End Function
