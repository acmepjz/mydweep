VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDownloader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const GENERIC_READ As Long = &H80000000
Private Const GENERIC_WRITE As Long = &H40000000
Private Const FILE_SHARE_READ As Long = 1
Private Const FILE_SHARE_WRITE As Long = 2
Private Const create_NEW As Long = 1
Private Const CREATE_ALWAYS As Long = 2
Private Const OPEN_EXISTING As Long = 3
Private Const OPEN_ALWAYS As Long = 4
Private Const TRUNCATE_EXISTING As Long = 5
Private Const INVALID_HANDLE_value As Long = -1
Private Const FILE_ATTRIBUTE_NORMAL As Long = &H80
Private Const FILE_END = 2


Private Const INTERNET_OPEN_TYPE_PRECONFIG = 0
Private Const INTERNET_OPEN_TYPE_DIRECT = 1
Private Const INTERNET_OPEN_TYPE_PROXY = 3
Private Const scUserAgent = "VB OpenUrl"
Private Const INTERNET_FLAG_RELOAD = &H80000000
Private Const HTTP_QUERY_CONTENT_LENGTH = 5
Private Const HTTP_QUERY_FLAG_NUMBER = &H20000000
Private Const HTTP_QUERY_STATUS_CODE = 19

Private Declare Function GetLastError Lib "kernel32" () As Long

Private Declare Function InternetOpen Lib "wininet.dll" _
    Alias "InternetOpenA" (ByVal sAgent As String, _
    ByVal lAccessType As Long, ByVal sProxyName As String, _
    ByVal sProxyBypass As String, ByVal lFlags As Long) As Long

Private Declare Function InternetOpenUrl Lib "wininet.dll" _
   Alias "InternetOpenUrlA" (ByVal hOpen As Long, _
   ByVal sUrl As String, ByVal sHeaders As String, _
   ByVal lLength As Long, ByVal lFlags As Long, _
   ByVal lContext As Long) As Long

Private Declare Function HttpQueryInfo Lib "wininet.dll" Alias "HttpQueryInfoA" _
(ByVal hHttpRequest As Long, ByVal lInfoLevel As Long, ByVal sBuffer As String, _
ByRef lBufferLength As Long, ByRef lIndex As Long) As Integer

Private Declare Function InternetReadFileB Lib "wininet.dll" Alias "InternetReadFile" _
   (ByVal hFile As Long, sBuffer As Byte, _
   ByVal lNumBytesToRead As Long, lNumberOfBytesRead As Long) _
   As Integer
   
Private Declare Function InternetReadFile Lib "wininet.dll" _
   (ByVal hFile As Long, ByVal sBuffer As String, _
   ByVal lNumBytesToRead As Long, lNumberOfBytesRead As Long) _
   As Integer
   
Private Declare Function InternetCloseHandle Lib "wininet.dll" _
        (ByVal hInet As Long) As Integer
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
Private Declare Function PostMessage Lib "user32" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Declare Function TerminateProcess Lib "kernel32" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
Private Const PROCESS_ALL_ACCESS = &H1F0FFF

Private pg As String, tmp As String

Private sz As Long, dsz As Long
Private szx As Long, dszx As Long
Private stx As Long
'stx:
'1-connect to server
'2-check version
'3-downloading
'4-updating

Private abr As Boolean

Private Type typeFileX
 Name As String
 Size As Long
End Type

Private Type typeUpdateFile
 Name As String
 Size As Long
 OSize As Long
 PackCount As Long
 crc As Long
 fs() As typeFileX
 fc As Long
 NeedIt As Boolean
End Type

Private ups() As typeUpdateFile
Private upc As Long

Private crc As New cCRC32

'将网络上的文件保存到磁盘
Private Function DownloadFile(ByVal sUrl As String, ByVal sFileName As String) As Boolean
Dim s() As Byte
Dim hOpen As Long
Dim hOpenUrl As Long
Dim bDoLoop As Boolean
Dim bRet As Long
Dim sReadBuffer() As Byte
Dim lNumberOfBytesRead As Long
On Error GoTo errout
    '打开
    hOpen = InternetOpen(scUserAgent, INTERNET_OPEN_TYPE_PRECONFIG, _
    vbNullString, vbNullString, 0)
    If hOpen = 0 Then Err.Raise 5
    hOpenUrl = InternetOpenUrl(hOpen, sUrl, vbNullString, 0, _
    INTERNET_FLAG_RELOAD, 0)
    If hOpenUrl = 0 Then Err.Raise 5
Dim sFileSize As String * 32
Dim lFileSizeBytes As Long
Dim lIndex As Long
lFileSizeBytes = Len(sFileSize)
Dim r As Long
Dim lSize As Long
    sFileSize = vbNullString
    r = HttpQueryInfo(hOpenUrl, HTTP_QUERY_CONTENT_LENGTH, ByVal sFileSize, lFileSizeBytes, 0)
    '///////////add
    sz = Val(sFileSize)
    dsz = 0
    If sz = 0 Then Err.Raise 5
    '////////////////end
    bDoLoop = True
    ReDim sReadBuffer(1 To 4096)
    Open sFileName For Output As #1
    Close
    Open sFileName For Binary As #1
    Do While bDoLoop And Not abr
        bRet = InternetReadFileB(hOpenUrl, sReadBuffer(1), 4096, lNumberOfBytesRead)
        '/////////////add
        If lNumberOfBytesRead = 4096 Then
         Put #1, dsz + 1, sReadBuffer
        ElseIf lNumberOfBytesRead > 0 Then
         ReDim Preserve sReadBuffer(1 To lNumberOfBytesRead)
         Put #1, dsz + 1, sReadBuffer
        End If
        dsz = dsz + lNumberOfBytesRead
        If dsz >= sz Or lNumberOfBytesRead = 0 Then bDoLoop = False
        '/////////////end
        DoEvents
        'Sleep 10
    Loop
    Close
    If hOpenUrl <> 0 Then InternetCloseHandle (hOpenUrl)
    If hOpen <> 0 Then InternetCloseHandle (hOpen)
    DownloadFile = dsz = sz
    Exit Function
errout:
    If hOpenUrl <> 0 Then InternetCloseHandle (hOpenUrl)
    If hOpen <> 0 Then InternetCloseHandle (hOpen)
    DownloadFile = False
    Close
End Function

Public Property Get Status() As Long
Status = stx
End Property

Public Property Get DownloadValue() As Long
DownloadValue = dsz + dszx
End Property

Public Property Get DownloadMax() As Long
DownloadMax = szx
End Property

Public Sub Abort()
abr = True
End Sub

Public Function StartUpdate() As Long
'-1 no update
'0-ok!
'1-can't connect to server
'2-aborted
'3-disconnect
'4-....
'....
Dim i As Long, j As Long, m As Long
Dim lp As Long, s As String
Dim d() As Byte, d2() As Byte
On Error Resume Next
MkDir tmp
On Error GoTo 0
stx = 1
abr = False
DoEvents
'///////connect to server, download update info
If Not DownloadFile(pg + "update.dat", tmp + "update.dat") Then
 StartUpdate = 1
 stx = 0
 Exit Function
End If
'/////////////////////read data
stx = 2
DoEvents
Open tmp + "update.dat" For Binary As #3
Get #3, 1, m
upc = m
ReDim ups(1 To upc)
lp = 5
For i = 1 To m
 With ups(i)
  lp = lp + LoadString(3, lp, .Name)
  Get #3, lp, .fc
  ReDim .fs(1 To .fc)
  lp = lp + 4
  For j = 1 To .fc
   lp = lp + LoadString(3, lp, .fs(j).Name)
   Get #3, lp, .fs(j).Size
   lp = lp + 4
  Next j
  Get #3, lp, .OSize
  Get #3, lp + 4, .PackCount
  Get #3, lp + 8, .Size
  Get #3, lp + 12, .crc
  lp = lp + 16
 End With
Next i
Close 3
'////////////////check
szx = 0
For i = 1 To m
 With ups(i)
  'file exist and size
  For j = 1 To .fc
   s = CStr(App.Path) + "\" + .fs(j).Name
   If Dir(s) = "" Then
    .NeedIt = True
    Exit For
   ElseIf FileLen(s) <> .fs(j).Size Then
    .NeedIt = True
    Exit For
   End If
  Next j
  'crc
  If Not .NeedIt Then
   ReDim d(.OSize - 1)
   lp = 0
   For j = 1 To .fc
    s = CStr(App.Path) + "\" + .fs(j).Name
    m = FileLen(s)
    ReDim d2(1 To m)
    Open s For Binary As #1
    Get #1, 1, d2
    Close 1
    CopyMemory d(lp), d2(1), m
    lp = lp + m
   Next j
   .NeedIt = crc.GetByteArrayCrc32(d) <> .crc
  End If
  If .NeedIt Then
   szx = szx + .Size
  End If
 End With
Next i
'latest version?
If szx = 0 Then
 stx = 0
 StartUpdate = -1
 Exit Function
End If
'terminate MyDweep
ExitDweep
'//////////////////////download
stx = 3
dszx = 0
dsz = 0
DoEvents
For i = 1 To upc
 With ups(i)
  If .NeedIt Then
   For j = 1 To .PackCount
    If abr Then Exit For
    s = tmp + .Name + CStr(j) + ".mpq"
    lp = 0
    Do Until DownloadFile(pg + .Name + CStr(j) + ".mpq", s) Or abr 'TODO
     lp = lp + 1
     If lp > 10 Then
      stx = 0
      StartUpdate = 3
      Exit Function
     End If
    Loop
    'download a lui?
    lp = 0
    If FileLen(s) < 10000 Then
     Open s For Input As #1
     Do Until EOF(1) Or lp = 1
      Line Input #1, s
      If InStr(1, s, "html", vbTextCompare) Then lp = 1
     Loop
     Close 1
     If lp = 1 Then
      stx = 0
      StartUpdate = 3
      Exit Function
     End If
    End If
    'ok
    dszx = dszx + sz
    sz = 0
    dsz = 0
   Next j
  End If
 End With
Next i
If abr Then
 stx = 0
 StartUpdate = 2
 Exit Function
End If
'/////////////////////UP-date
stx = 4
szx = 0
dszx = 0
dsz = 0
For i = 1 To upc
 If ups(i).NeedIt Then szx = szx + ups(i).fc
Next i
DoEvents
'get folder
s = CStr(App.Path) + "\"
On Error Resume Next
Err.Clear
If Dir(s + "dweep" + Chr(46) + Chr(118) + Chr(98) + Chr(112)) <> "" Then
 If Err.Number = 0 Then s = tmp 'test only! don't rewrite files
End If
On Error GoTo 0
'start
For i = 1 To upc
 With ups(i)
  If .NeedIt Then
   'unpack
   lp = 0
   ReDim d(.Size - 1)
   For j = 1 To .PackCount
    m = FileLen(tmp + .Name + CStr(j) + ".mpq") - 7
    If m > 0 Then
     ReDim d2(1 To m)
     Open tmp + .Name + CStr(j) + ".mpq" For Binary As #1
     Get #1, 8, d2
     Close 1
     CopyMemory d(lp), d2(1), m
     lp = lp + m
    End If
   Next j
   DecompressByteArray d, .OSize
   'update file
   lp = 0
   For j = 1 To .fc
    m = .fs(j).Size
    'check file?
    On Error Resume Next
    Do
     Err.Clear
     Open s + .fs(j).Name For Output As #1
     Close 1
     If Err.Number > 0 Then
      Select Case MsgBox("在更新文件" + .fs(j).Name + "时出现错误，请选择操作:", vbCritical Or vbAbortRetryIgnore Or vbDefaultButton2)
      Case vbAbort
       stx = 0
       StartUpdate = 4
       Close
       Exit Function
      Case vbIgnore
       lp = lp + m
       m = 0
       Exit Do
      End Select
     Else
      Exit Do
     End If
    Loop
    On Error GoTo 0
    'end check
    If m > 0 Then
     ReDim d2(1 To m)
     CopyMemory d2(1), d(lp), m
     lp = lp + m
     Open s + .fs(j).Name For Binary As #1
     Put #1, 1, d2
     Close 1
    End If
    dszx = dszx + 1
    DoEvents
   Next j
  End If
 End With
Next i
'/////////////////////end
Close
stx = 0
End Function

Public Sub KillTempFile()
Dim i As Long, j As Long
On Error Resume Next
Kill tmp + "/update.dat"
For i = 1 To upc
 With ups(i)
  For j = 1 To .PackCount
   Kill tmp + .Name + CStr(j) + ".mpq"
  Next j
 End With
Next i
RmDir tmp
End Sub

Public Property Get Page() As String
Page = pg
End Property

Public Property Let Page(ByVal s As String)
pg = s
End Property

Public Property Get TempPath() As String
TempPath = tmp
End Property

Public Property Let TempPath(ByVal s As String)
tmp = s
End Property

Private Function SaveString(ByVal FileNo As Long, ByVal lp As Long, ByVal s As String) As Long
Dim i As Long, m As Integer, x As Integer
m = Len(s)
SaveString = m * 2 + 2
Put #FileNo, lp, m
For i = 1 To m
 x = Asc(Mid(s, i, 1))
 m = m Xor x
 Put #FileNo, lp + 2 * i, m
Next i
End Function

Private Function LoadString(ByVal FileNo As Long, ByVal lp As Long, s As String) As Long
Dim i As Long, m As Integer, x As Integer
s = ""
Get #FileNo, lp, m
LoadString = m * 2 + 2
For i = 1 To m
 Get #FileNo, lp + 2 * i, x
 m = m Xor x
 s = s + Chr(m)
 m = x
Next i
End Function

Private Sub Class_Initialize()
crc.Value = &HDEADBEEF
End Sub

Private Sub ExitDweep()
On Error Resume Next
Dim pid As Long, h As Long
pid = Val(Command)
If pid <> 0 Then
 h = OpenProcess(PROCESS_ALL_ACCESS, 0, pid)
 If h <> 0 Then
  TerminateProcess h, 0
  CloseHandle h
 End If
End If
End Sub
