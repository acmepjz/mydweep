VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTutorials"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private ts() As clsTutorial
Private tsc As Long

Public Property Get Count() As Long
Count = tsc
End Property

Public Sub LoadTutorial(Table As clsTable)
Dim i As Long, s As String
Dim d() As Byte
s = CStr(App.Path) + "\temp.tmp"
tsc = Table.Count
ReDim ts(1 To tsc)
For i = 1 To tsc
 Set ts(i) = New clsTutorial
 With ts(i)
  .Style = IIf(Left(Table.StringTable(i), 3) = "rec", 1, 0)
  '////////
  ld.LoadToArray d
  Open s For Binary As #1
  Put #1, 1, d
  Close 1
  .LoadFile s
  Kill s
  '////////
'  ld.LoadToArray d
'  Open s For Binary As #1
'  Put #1, 1, d
'  Close 1
'  .LoadScript s
'  Kill s
 End With
Next i
End Sub

Private Sub Class_Terminate()
Erase ts
End Sub

Public Property Get TheData(ByVal Index As Long) As clsTutorial
Set TheData = ts(Index)
End Property
