Attribute VB_Name = "mdlEdit"
Option Explicit

Public IsEdit As Boolean
Public IsCLv As Boolean

Public EData As typeGameData

Public SItem As Byte

Public Sub MenuCLvLoop()
'On Error GoTo a
Dim i As Integer, vars As String, varS2 As String
Dim varName As String
Dim strPath As String
Dim lp As Long, var1 As Byte, var2 As Long
Dim strF As String, var3 As Integer, var4 As Boolean
Dim str1 As String, str2 As String, str3 As String
ReStart:
NowScene = 5
'ShowCursor 1
sItemT = 0
'/////////////////////////////////////////////////////////////////////Load Levels
'frmMain.lstRecord.Nodes.Clear
With List1
 Set .Font = frmMain.Font
 .ForeColor = vbWhite
 .ItemHeight = 16
 .SetImageList frmMain.Image1(2).Picture, 16, 16
 .Create bmpBack.hDC, 100, 100, 440, 250
 .Add "?1", "自定义关卡文件", 1, &HC0C0C0, False
End With
'frmMain.lstRecord.Nodes.Add , tvwNext, "?1", "自定义关卡文件", 1, 1
'frmMain.lstRecord.Nodes.Add , tvwNext, "?2", "关卡录像文件", 1, 1
For i = 1 To fileCLv.count
 strF = fileCLv.FileNameWithPath(i)
 GetLevelInfo strF, str1, var3, var4
 var2 = 2 + (var3 - 1) Mod 5 + IIf(var4, 5, 0)
 'frmMain.lstRecord.Nodes.Add "?1", tvwChild, strF, str1, var2, var2
 List1.Add strF, fileCLv.FileName(i) + vbTab + vbTab + str1, var2, IIf(var4, vbYellow, &HC0C0&), , 32
Next i
'/////////////////////////////////////////////////////////////////////Record
For i = 1 To fileRec.count
 strF = fileRec.FileNameWithPath(i)
 GetRecordInfo strF, str1, str2, str3, var3
 var2 = 2 + (var3 - 1) Mod 5
 If Left(str1, 1) = "?" Then
'  frmMain.lstRecord.Nodes.Add "?2", tvwChild, "*" + strF, "自定义关卡-" + str3, var2, var2
 Else
'  frmMain.lstRecord.Nodes.Add "?2", tvwChild, "*"+strF, Trim(str2) + "-第" + _
'  CStr(var3) + "关-" + str3, var2, var2
 End If
Next i
'////////////////////////////////////////////////////////////////////End
'frmMain.lstRecord.Visible = True
Do
 RedrawBack
 For i = 1 To 3 Step 2
  If i = dItemT And pressT Then
   BitBlt bmpBack.hDC, i * 190 - 130, 400, 140, 25, bmpButton2.hDC, 0, 25, vbSrcCopy
  Else
   BitBlt bmpBack.hDC, i * 190 - 130, 400, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
  End If
 Next i
 If dItemT = 1 Then
  DrawTextB bmpBack.hDC, "取消(Esc)", frmMain.Font, 60, 400, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
 Else
  DrawTextB bmpBack.hDC, "取消(Esc)", frmMain.Font, 60, 400, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
 End If
 If dItemT = 3 Then
  DrawTextB bmpBack.hDC, "确定(Enter)", frmMain.Font, 440, 400, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
 Else
  DrawTextB bmpBack.hDC, "确定(Enter)", frmMain.Font, 440, 400, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
 End If
 DrawTextB bmpBack.hDC, "请选择一个关卡", frmMain.Label1.Font, 244, 20, 152, 24, DT_CENTER, vbYellow, , True
 List1.Redraw
 RedrawMouse
 fPaint bmpBack.hDC
 'nowFPS = nowFPS + 1
 '//////////////////////////////////////////////////End of drawing
 Play2
 DoEvents
 m_objFPS.WaitForNextFrame
 If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
  sItemT = 1
 ElseIf GetAsyncKeyState(vbKeyReturn) = &H8000 Then
  sItemT = 3
 End If
Loop Until sItemT > 0
'frmMain.lstRecord.Visible = False
'ShowCursor 0
List1.Destroy
Select Case sItemT
Case 1
Case 3
 LevelPackFile = IIf(List1.ListIndex = -1, "?0", List1.Key(List1.ListIndex))
 Select Case Left(LevelPackFile, 1)
 Case "?"
  MsgBox "请选择一个关卡！"
 Case "*" '/////////////////////////////Record
  LevelPackFile = Right(LevelPackFile, Len(LevelPackFile) - 1)
  MsgBox "TODO:"
 Case Else '////////////////////////////Level
  LoadCustomLevel LevelPackFile, LData
  'script support
  Set tLev = New clsTutorial
  tLev.LoadFile LevelPackFile
  ''
  IsCLv = True
  GameLoop
  IsCLv = False
 End Select
 GoTo ReStart
End Select
a:
Close
End Sub

Public Sub EditLoop()
Dim i As Integer, j As Integer
Dim xx As Integer, yy As Integer
Dim RStop As Boolean
IsEdit = True
'////////////////////////////////////////////////////////////Init Data
With LData
 Erase .LevelData, .ItemStart
 .ItemCount = 0
 .LevelNo = 1
 .LevelName = "未命名关卡"
 .LevelHint = "这是一个玩家自定义的关卡。"
 .DweepStartX = 1
 .DweepStartY = 1
 .LevelData(1, 1) = 5
 .ItemCount = 1
End With
'////////////////////////////////////////////////////////////Open the music
Play (LData.LevelNo - 1) Mod 5 + 4
'/////////////////////////////////////////////////////////////Move Data
PutGameData
'//////////////////////////////////////////////////////////////Edit mode
sItemT = 0
NowScene = 1025
Do
 '/////////////////////////////////////////////////////////////Add game time
 GData.GameTime = (GData.GameTime + 1) Mod 2520
 '//////////////////////////////////////////////Draw Level
 ShowLevelEd
 'nowFPS = nowFPS + 1
 DoEvents
 m_objFPS.WaitForNextFrame
 '/////////////////////////////////////////////Play Music
 Play2
 '/////////////////////////////////////////////Save Record
 '/////////////////////////////////////////////Test
 If sItemT = -1 Or GetAsyncKeyState(vbKeyT) = &H8000 Then
  EData = GData
  GetGameData
  IsCLv = True
  GameLoop
  IsCLv = False
  GData = EData
  NowScene = 1025
  sItemT = 0
 End If
 '/////////////////////////////////////////////Menu
 If sItemT = 1 Or GetAsyncKeyState(vbKeyEscape) = &H8000 Then
  sItemT = 0
  NowScene = 1028
  DrawLevel bmpCache.hDC, GData
  Do
   BitBlt bmpBack.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, vbSrcCopy
   DrawDialogBox bmpBack.hDC, 220, 150, 200, 180
   For i = 1 To 4
    If dItemT = i And pressT Then
     BitBlt bmpBack.hDC, 250, 40 * i + 130, 140, 25, bmpButton2.hDC, 0, 25, vbSrcCopy
    Else
     BitBlt bmpBack.hDC, 250, 40 * i + 130, 140, 25, bmpButton2.hDC, 0, 0, vbSrcCopy
    End If
    If dItemT = i Then
     DrawTextB bmpBack.hDC, TheMid(3, 7, i), frmMain.Font, 250, 40 * i + 130, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbYellow, , True
    Else
     DrawTextB bmpBack.hDC, TheMid(3, 7, i), frmMain.Font, 250, 40 * i + 130, 140, 25, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, vbWhite, , True
    End If
   Next i
   RedrawMouse
   fPaint bmpBack.hDC
   'nowFPS = nowFPS + 1
   DoEvents
   m_objFPS.WaitForNextFrame
   If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
    sItemT = 5
   ElseIf GetAsyncKeyState(vbKeyQ) = &H8000 Then
    sItemT = 4
   ElseIf GetAsyncKeyState(vbKeyO) = &H8000 Then
    sItemT = 1
   ElseIf GetAsyncKeyState(vbKeyS) = &H8000 Then
    sItemT = 2
   ElseIf GetAsyncKeyState(vbKeyT) = &H8000 Then
    sItemT = 3
   End If
   If pressR Then sItemT = 5
  Loop Until sItemT > 0
  Select Case sItemT
  Case 1
   OpenCustomLv
  Case 2
   SaveCustomLv
  Case 3
   sItemT = 0
   NowScene = 3333
   EditToolsLoop
  Case 4
   GoTo ExitGame
  End Select
  sItemT = 0
  NowScene = 1025
 End If
 If GetAsyncKeyState(vbKeyUp) = &H8000 Then
  RotLevel GData, 0, -1
 ElseIf GetAsyncKeyState(vbKeyDown) = &H8000 Then
  RotLevel GData, 0, 1
 ElseIf GetAsyncKeyState(vbKeyLeft) = &H8000 Then
  RotLevel GData, -1, 0
 ElseIf GetAsyncKeyState(vbKeyRight) = &H8000 Then
  RotLevel GData, 1, 0
 ElseIf GetAsyncKeyState(vbKeyX) = &H8000 Then
  XLevel GData
 ElseIf GetAsyncKeyState(vbKeyY) = &H8000 Then
  YLevel GData
 End If
Loop
'///////////////////////////////////////////End Main Loop
'If sItemT > 0 Then GoTo ExitGame
'/////////////////////////////////////Lose game
'//////////////////////////////////////////////Exit game, play menu music
ExitGame:
Play 1
sItemT = 0
'ShowCursor 1
IsEdit = False
End Sub

Public Sub ShowLevelEd()
Dim mx As Integer, my As Integer
Dim i As Integer, var1 As Integer, j As Integer
A_Laser GData
DrawLevel bmpBack.hDC, GData
BitBlt bmpBack.hDC, 0, 400, 520, 80, bmpLvBk.hDC, 0, 2080, vbSrcCopy
If MouseY >= 400 Then
 transD = -1
 If MouseX > 50 And MouseX < 477 And MouseY > 414 And MouseY < 478 And MouseX Mod 33 <> 16 And MouseY <> 446 Then
  var1 = (MouseX - 50) \ 33 + ((MouseY - 414) \ 33) * 13 + 1
  mx = ((var1 - 1) Mod 13) * 33 + 50
  my = ((var1 - 1) \ 13) * 33 + 414
  BitBlt bmpBack.hDC, mx, my, 32, 32, bmpItem.hDC, 800, 0, vbSrcAnd
  BitBlt bmpBack.hDC, mx, my, 32, 32, bmpItem.hDC, 720, 0, vbSrcPaint
  If pressT Then
   Select Case var1
   Case 6 To 21
    SItem = var1 - 5
   Case 1 To 5
    GData.LevelNo = var1
    SItem = 0
   Case 22 To 26
    SItem = var1 + 6
   End Select
  End If
  If pressR Then
   Select Case var1
   Case 11 To 21
    SItem = var1 + 6
   End Select
  End If
 End If
 RedrawMouse
Else
 mx = 1 + MouseX \ 40
 my = 1 + MouseY \ 40
 transX = mx
 transY = my
 transD = SItem
 If pressT Then '////////////////////////////////////////////////Left-click
  If GData.DweepX \ 10 = mx - 1 And GData.DweepY \ 10 = my - 1 Then
   sPlay 14
  ElseIf SItem = 5 Then
   GData.LevelData(mx, my) = 0
   GData.DweepX = mx * 10 - 5
   GData.DweepY = my * 10 - 5
  Else
   GData.LevelData(mx, my) = SItem
  End If
 End If
 If pressR Then '///////////////////////////////////////////////Right-click
  If GData.DweepX \ 10 = mx - 1 And GData.DweepY \ 10 = my - 1 Then
   sPlay 14
  Else
   GData.LevelData(mx, my) = 0
  End If
 End If
 RedrawMouse
End If
'///////////////////////////////////////End
fPaint bmpBack.hDC
End Sub

Private Sub EditToolsLoop()
Dim i As Integer, n As Integer, gd As typeGameData
gd = GData
ReStart:
Do
 BitBlt bmpBack.hDC, 0, 0, 640, 480, bmpCache.hDC, 0, 0, vbSrcCopy
 DrawDialogBox bmpBack.hDC, 100, 120, 440, 240
 DrawTextB bmpBack.hDC, "道具箱", frmMain.Label1.Font, 290, 135, 64, 24, DT_CENTER, &H8000&, , True
 For i = 1 To 3
  If dItemT = i And pressT Then
   BitBlt bmpBack.hDC, 60 + i * 110, 320, 105, 19, bmpButton3.hDC, 0, 19, vbSrcCopy
  Else
   BitBlt bmpBack.hDC, 60 + i * 110, 320, 105, 19, bmpButton3.hDC, 0, 0, vbSrcCopy
  End If
 Next i
 DrawTextB bmpBack.hDC, "取消(Esc)", frmMain.Font, 170, 320, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = 1, vbYellow, vbWhite), , True
 DrawTextB bmpBack.hDC, "清除(Del)", frmMain.Font, 280, 320, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = 2, vbYellow, vbWhite), , True
 DrawTextB bmpBack.hDC, "确定(Enter)", frmMain.Font, 390, 320, 105, 19, DT_CENTER Or DT_VCENTER Or DT_SINGLELINE, IIf(dItemT = 3, vbYellow, vbWhite), , True
 For i = 1 To 10 '//////////////////////////////////////////////////////Draw current item
  If i > GData.ItemCount Then
   n = 18
  Else
   n = ChangeItemData(GData.ItemData(i))
  End If
  BitBlt bmpBack.hDC, 80 + i * 40, 170, 40, 40, bmpItem.hDC, (n - 1) * 40, 0, vbSrcCopy
 Next i
 '////////////////////////////////////////////////////////////////////Draw list item
 BitBlt bmpBack.hDC, 120, 220, 400, 40, bmpItem.hDC, 0, 0, vbSrcCopy
 BitBlt bmpBack.hDC, 120, 260, 280, 40, bmpItem.hDC, 400, 0, vbSrcCopy
 '////////////////////////////////////////////////////////////////////Add item?
 If MouseX > 120 And MouseX < 520 And MouseY > 220 And MouseY < 300 And pressT Then
  n = (MouseX - 120) \ 40 + ((MouseY - 220) \ 40) * 10 + 1
  If n < 18 And GData.ItemCount < 10 Then
   GData.ItemCount = GData.ItemCount + 1
   GData.ItemData(GData.ItemCount) = 100 + n
   sPlay 9
  End If
  pressT = False
 End If
 '////////////////////////////////////////////////////////////////////Remove item?
 If MouseX > 120 And MouseX < 520 And MouseY > 170 And MouseY < 210 And pressT Then
  n = (MouseX - 120) \ 40 + 1
  If n <= GData.ItemCount Then
   For i = n To GData.ItemCount - 1
    GData.ItemData(i) = GData.ItemData(i + 1)
   Next i
   GData.ItemCount = GData.ItemCount - 1
   sPlay 12
  End If
  pressT = False
 End If
 '////////////////////////////////////////////////////////////////////End
 RedrawMouse
 fPaint bmpBack.hDC
 'nowFPS = nowFPS + 1
 DoEvents
 m_objFPS.WaitForNextFrame
 If GetAsyncKeyState(vbKeyEscape) = &H8000 Then
  sItemT = 1
 ElseIf GetAsyncKeyState(vbKeyDelete) = &H8000 Then
  sItemT = 2
 ElseIf GetAsyncKeyState(vbKeyReturn) = &H8000 Then
  sItemT = 3
 End If
Loop Until sItemT > 0
Select Case sItemT
Case 1
 GData = gd
Case 2
 GData.ItemCount = 1
 GData.ItemData(1) = 117
 sItemT = 0
 GoTo ReStart
End Select
End Sub

Public Sub OpenCustomLv()
On Error GoTo a
ShowCursor 1
LoadCustomLevel ShowOpen(LoadResString(131), CStr(App.Path) + "\levels\"), LData
PutGameData
a:
ShowCursor 0
End Sub

Public Sub SaveCustomLv()
Dim s As String
On Error GoTo a
ShowCursor 1
s = ShowSave(LoadResString(131), CStr(App.Path) + "\levels\")
GetGameData
SaveCustomLevel s, LData
SetLevelSolved s, False
fileCLv.Refresh
a:
ShowCursor 0
End Sub

Private Sub RotLevel(gd As typeGameData, ByVal x As Long, ByVal y As Long)
Dim tmp As typeGameData
Dim i As Long, j As Long, k As Long
tmp = gd
tmp.DweepX = (tmp.DweepX + x * 10 + 160) Mod 160
tmp.DweepY = (tmp.DweepY + y * 10 + 100) Mod 100
For i = 1 To 16
 k = 1 + (i + x + 15) Mod 16
 For j = 1 To 10
  tmp.LevelData(k, 1 + (j + y + 9) Mod 10) = gd.LevelData(i, j)
 Next j
Next i
gd = tmp
End Sub

Private Sub XLevel(gd As typeGameData)
Dim tmp As typeGameData
Dim i As Long, j As Long, k As Long
tmp = gd
tmp.DweepX = 160 - tmp.DweepX
For i = 1 To 16
 For j = 1 To 10
  k = gd.LevelData(17 - i, j)
  Select Case k
  Case 7, 9: k = 16 - k
  Case 10, 11: k = 21 - k
  Case 13, 15: k = 28 - k
  Case 18, 20: k = 38 - k
  Case 21, 22: k = 43 - k
  Case 24, 26: k = 50 - k
  Case 28, 29: k = 57 - k
  End Select
  tmp.LevelData(i, j) = k
 Next j
Next i
gd = tmp
End Sub

Private Sub YLevel(gd As typeGameData)
Dim tmp As typeGameData
Dim i As Long, j As Long, k As Long
tmp = gd
tmp.DweepY = 100 - tmp.DweepY
For i = 1 To 16
 For j = 1 To 10
  k = gd.LevelData(i, 11 - j)
  Select Case k
  Case 6, 8: k = 14 - k
  Case 10, 11: k = 21 - k
  Case 12, 14: k = 26 - k
  Case 17, 19: k = 36 - k
  Case 21, 22: k = 43 - k
  Case 23, 25: k = 48 - k
  Case 28, 29: k = 57 - k
  End Select
  tmp.LevelData(i, j) = k
 Next j
Next i
gd = tmp
End Sub

