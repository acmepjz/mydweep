    My Dweep是一个非常有趣的益智游戏，你要控制一个毛茸茸的紫色的小精灵穿过重重障碍到达终点。

    想象一只可爱的紫色的毛茸茸的精灵，它没有手和脚，但是有两只很棒的大眼睛。这就是Dweep。现在将Dweep放入一个被激光、加热盘、炸弹和其它的危险包围的敌意的环境中。你知道——所有这些东西都会立即地烧焦这可爱的紫色精灵的毛皮。而且我们不给它任何的武器——没有刀，没有枪，没有盔甲，没有盾……什么都没有！但是，在它的周围放置一些能被Dweep拾起的非暴力的物品。例如，镜子、扳手和水桶。

    我们将目标改为：将Dweep的毫无防备的小宝贝也放在这充满危险的环境中，使得Dweep必须解救它们。如果你认为镜子和水桶不是激光发射器和炸弹的对手，那么你明显是玩太多类似“在迷宫里到处跑，并打每样会动的东西”的动作游戏了。My Dweep是你玩一些新的东西的机会，而且使你有你以前从未拥有过的游戏经历。玩玩My Dweep吧！它能使你在娱乐时，非暴力地释放压力。

    新版本修正了以下Bug：

★帧速率过高的错误（有一次竟然到了300！严重浪费CPU资源……）

★乱弹框错误。游戏刚发布不久，我有一次玩把鼠标拖到边就弹了个框“运行时错误：下标越界”，游戏自动退出了，连音乐也不关。只好重新启动…… :-\     如果你还发现有这样的错误的话就告诉我。

★鼠标消失掉的错误。在物品栏中的第一个图标是移动Dweep。但是如果点击那个图标鼠标就会消失掉！什么也做不了……

★不能撤销的错误。有些电脑打开音乐很慢，所以弹出输的对话框要卡很久，再按“撤销”就撤销不了了。另外还支持无限撤销。


    这次总算用不着讨厌的控件了，我自己编了一个类来模拟ListBox的功能。而且这一次用上了透明通道，使用道具时不像以前的花花绿绿的。关于透明通道，游戏安装后会有一个“AlphaDIB.dll”的图形类库，VB粉丝们可以用它来编自己的游戏。关于它的代码可以到www.vbAccelerator.com查询。

    另外还有人问我怎么做关卡包，这次我附带一个关卡包编辑器，用它可以编辑扩展名为dwp的文件。因为这个编辑器我刚开始编时是英文，所以上次我没放上去，呵呵:-)现在是中文的了。如果有谁做出了好的关卡，欢迎与我交流。我的邮箱是：acme_pjz@hotmail.com。

    新版本添加了如下功能：

★快捷键的支持。就像Windows一样：文件(F) 编辑(E)……

★关卡包编辑器换了一个新的Office 2003的界面，感谢vbAccelerator提供的控件。

★关卡包编辑器还支持自定义关卡文件(*.dwl)的读写。

★还新增了选项、帮助和教程。不过教程只有5关……

★播放音乐也不会卡机了！现在我不用WinAPI了，改用FMOD Sound System。