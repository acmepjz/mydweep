 教程：使用物品（3）                             这个教程可以教你如何使用物品，以及使用物品的技巧。                                                                                                                                                                               qqqq     SCRP4  >gametime 1
#message
冷冻的Dweep
如果Dweep走到冷冻盘上，Dweep将会结冰。你可以用火把或者锤子来解放Dweep。用火把和锤子的效果是一样的，不管怎样，Dweep都会变湿。/n/n不要忘记捡火把！火把就在Dweep的左边。
>end
>dweeppos 16 9
>dweepwet
>samenum a 0
#message
Dweep和加热盘
将冰块融化之后，Dweep是湿漉漉的。湿的Dweep可以安全通过加热盘，但是它会被烤干。
#setnum
a
1
>end
>dweeppos 16 6
>samenum a 1
#message
膨胀！
如果干燥的Dweep走到加热盘上，它将会膨胀而飞起来，这时你不能让它做任何事。你可以用水桶或锤子让它重新降落，但是使用这两种道具的效果不一样。使用水桶Dweep将会被浇湿，而使用锤子Dweep仍然是干燥的。所以一个水桶能让Dweep顺利地通过两个加热盘。
#setnum
a
2
>end
>dweepdry
>nowitem 14
>samenum a 2
#message
注意！
如果现在你用水桶将Dweep浇湿，你将不能通过第二个加热盘，Dweep将会飞起来。所以你应当等到Dweep飞起来后，再使用水桶。
#cancelitem
>end
>dweeppos 16 5
>dweepwet
>samenum a 2
#message
小技巧
在有些关卡里，你需要在Dweep尚未完全起飞时，用水桶将Dweep浇湿。这样可以防止Dweep被风扇给吹跑。
#setnum
a
3
>end
>dweeppos 15 2
>samenum a 3
#message
Dweep和风扇
风扇可以吹动飞起来的Dweep。但是它的作用范围只有两格。/n/n现在捡起水桶，走到加热盘上，这样风扇就能把Dweep向下吹了。
#setnum
a
4
>end
>dweeppos 14 6
>dweepwet
>samenum a 4
#message
扳手
为了增加游戏的趣味性，我将一个风扇摆成了不正确的方向。现在请你用扳手将风扇给扭回来。还记得扳手怎么用吗？
#setnum
a
5
>end
>dweeppos 13 8
>samenum a 5
#message
多个风扇
虽然一个风扇只能把Dweep吹出两格远，但是多个风扇共同作用就能将Dweep吹出更远的距离。
#setnum
a
6
>end
>dweeppos 8 8
>samenum a 6
#message
风好大！
有时候风扇会把Dweep给吹过头了，吹到了墙上。这样你必须在Dweep飞过界之前，用水桶将Dweep给泼下来。当Dweep即将到达你想让它降落的地方时，用水桶泼它就行了。/n/n要注意的是，如果目的地是加热盘，那么在Dweeo停下来时泼水则Dweep是湿的。要是你在Dweep尚在飞行时泼水，这样子当Dweep真正到达目的地时，就会被烤干。
#setnum
a
7
>end
>dweeppos 8 5
>dweepice
>samenum a 7
#message
又被冻住了！
与加热盘不同，如果Dweep降落的目的地是冷冻盘，那么无论你是让Dweep在停下时降落，还是“强制”给它泼水，Dweep都会被冻住。
#setnum
a
8
>end
>dweeppos 6 5
>dweepfly
>samenum a 8
#message
坐飞机
漂浮的Dweep可以同时被多个风扇作用。特别地，当有两个方向成90度角的风扇同时作用于Dweep，它将会斜着飞。
#setnum
a
9
>end
>dweeppos 9 2
>samenum a 9
#message
风力抵消
两个方向相反的风扇，其发出的风力将会被抵消，如果Dweep同时被这两个风扇所作用，那么他将会停止，或者来回摇摆，举棋不定。/n/n现在我们要往左边走，但是有一个风扇向右吹风。看来我们要用掉一个扳手来解决这个问题。但是要用顺时针的呢，还是逆时针的？自己想想看吧。记得分清顺时针的和逆时针的！
#setnum
a
10
>end
>dweeppos 1 2
>samenum a 10
#message
小技巧
风扇除了能够吹动飞行的Dweep之外，还有一个作用就是暂时挡住激光。现在我们要去终点，要通过有激光的加热盘，但是Dweep不能安全通过有激光的加热盘。现在我们要做的是让Dweep在没有激光的情况下走到加热盘，这样Dweep就能飞起来。炸弹和激光束不能伤害到飞行状态的Dweep。
#setnum
a
11
>end
>dweeppos 3 8
>dweepfly
>samenum a 11
#message
测试
这仅仅是测试而已。
#setmapdata
03,10,6
#setnum
a
12
>end